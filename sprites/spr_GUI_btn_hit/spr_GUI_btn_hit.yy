{
    "id": "525fe12d-1007-4059-91a2-a15adb1964f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_btn_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 0,
    "bbox_right": 262,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ad809b1-4c41-49e3-abc3-17c5a8242d96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "525fe12d-1007-4059-91a2-a15adb1964f8",
            "compositeImage": {
                "id": "4e9f88ad-95f5-48af-bbd0-a7984351dab9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ad809b1-4c41-49e3-abc3-17c5a8242d96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e83d2b0-0480-4d87-9272-b6654f1d5bbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ad809b1-4c41-49e3-abc3-17c5a8242d96",
                    "LayerId": "5cb2a17c-ec2c-4a93-a399-fbc4f8b87dff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 66,
    "layers": [
        {
            "id": "5cb2a17c-ec2c-4a93-a399-fbc4f8b87dff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "525fe12d-1007-4059-91a2-a15adb1964f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 263,
    "xorig": 131,
    "yorig": 33
}