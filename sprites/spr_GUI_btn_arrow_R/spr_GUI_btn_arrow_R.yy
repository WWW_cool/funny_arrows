{
    "id": "7cd1380c-2130-497b-aea3-8b9d85fae38b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_btn_arrow_R",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0d9d226-f757-4433-802a-f51f37b3ce79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cd1380c-2130-497b-aea3-8b9d85fae38b",
            "compositeImage": {
                "id": "e4f8e95d-0d6d-4779-bb72-d5c43c5b4bcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0d9d226-f757-4433-802a-f51f37b3ce79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9636b4dd-769a-4517-89d2-cc8c3b9913b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0d9d226-f757-4433-802a-f51f37b3ce79",
                    "LayerId": "2aadf91f-4ef6-4cce-9f31-81b8904403d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "2aadf91f-4ef6-4cce-9f31-81b8904403d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7cd1380c-2130-497b-aea3-8b9d85fae38b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 19,
    "yorig": 18
}