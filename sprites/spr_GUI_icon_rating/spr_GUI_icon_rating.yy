{
    "id": "b15b61cb-e7d5-4b24-9294-99c7949e7c5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_rating",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff37ba92-c104-46e0-af23-c35be69eeab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b15b61cb-e7d5-4b24-9294-99c7949e7c5e",
            "compositeImage": {
                "id": "e3543074-b99b-4a6d-9834-45948b0d2583",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff37ba92-c104-46e0-af23-c35be69eeab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2159a6f3-b239-4077-968b-c4b1cec2e61f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff37ba92-c104-46e0-af23-c35be69eeab5",
                    "LayerId": "5949b4e9-574c-4fda-b667-30e26cca0f97"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "5949b4e9-574c-4fda-b667-30e26cca0f97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b15b61cb-e7d5-4b24-9294-99c7949e7c5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 9
}