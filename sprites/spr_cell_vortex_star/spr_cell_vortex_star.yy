{
    "id": "1c1814e8-0f67-4af8-90a6-6f199ed493fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_vortex_star",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfd9a05e-68d8-4f94-b54a-300b241ad9e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c1814e8-0f67-4af8-90a6-6f199ed493fe",
            "compositeImage": {
                "id": "6e5d24a3-ddd4-4b2c-8c34-965a64bc7bf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfd9a05e-68d8-4f94-b54a-300b241ad9e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9464ef81-1e24-4501-b395-918930f975f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfd9a05e-68d8-4f94-b54a-300b241ad9e4",
                    "LayerId": "5a5fa020-b600-41c4-81d3-fa74db8b7514"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "5a5fa020-b600-41c4-81d3-fa74db8b7514",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c1814e8-0f67-4af8-90a6-6f199ed493fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}