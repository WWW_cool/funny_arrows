{
    "id": "1a8adef3-f024-48b9-bfa2-af0755be080d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_rating_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49e7235a-2431-440a-a6af-cfdc6b37539e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8adef3-f024-48b9-bfa2-af0755be080d",
            "compositeImage": {
                "id": "06002f36-416e-4eb1-bbce-b2d6969735d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49e7235a-2431-440a-a6af-cfdc6b37539e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d520a2f9-3644-4862-91ff-c771f08dcd93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49e7235a-2431-440a-a6af-cfdc6b37539e",
                    "LayerId": "45958eb7-60e9-4fa5-9dc5-23998d3be411"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "45958eb7-60e9-4fa5-9dc5-23998d3be411",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a8adef3-f024-48b9-bfa2-af0755be080d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 9
}