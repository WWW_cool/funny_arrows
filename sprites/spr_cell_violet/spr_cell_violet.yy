{
    "id": "edd286ae-c6d5-4d36-9e1d-d3b28a3a8b6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_violet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78cc24d8-e666-4632-bfbf-a1b8c31348e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "edd286ae-c6d5-4d36-9e1d-d3b28a3a8b6d",
            "compositeImage": {
                "id": "bbb79db1-8167-4e16-a433-862fd678a546",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78cc24d8-e666-4632-bfbf-a1b8c31348e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ef81e4d-52ae-4f86-b0f0-4b33696c60e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78cc24d8-e666-4632-bfbf-a1b8c31348e3",
                    "LayerId": "2387fed7-8db9-4e0f-ab32-3880930957ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "2387fed7-8db9-4e0f-ab32-3880930957ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "edd286ae-c6d5-4d36-9e1d-d3b28a3a8b6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 35,
    "yorig": 33
}