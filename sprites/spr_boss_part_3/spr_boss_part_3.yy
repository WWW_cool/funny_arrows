{
    "id": "0dd2b392-b03d-4ce8-bb64-8e999cf8f0fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_part_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 0,
    "bbox_right": 114,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d80a711a-dc00-454d-b181-52e4c554a884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dd2b392-b03d-4ce8-bb64-8e999cf8f0fa",
            "compositeImage": {
                "id": "a6eaf136-cf3c-44ef-8947-8bde3b3fea61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d80a711a-dc00-454d-b181-52e4c554a884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ca00882-748e-44a3-9087-f957fc458752",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d80a711a-dc00-454d-b181-52e4c554a884",
                    "LayerId": "e067da4f-c6d2-4bd8-bd26-c424c2a5bc06"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "e067da4f-c6d2-4bd8-bd26-c424c2a5bc06",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0dd2b392-b03d-4ce8-bb64-8e999cf8f0fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}