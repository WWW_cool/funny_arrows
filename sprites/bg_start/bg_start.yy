{
    "id": "b39420b2-0da7-48a9-8f54-e2d936944e84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_start",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 404,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7c614df-e0ee-4e6a-a6d9-6391003903fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b39420b2-0da7-48a9-8f54-e2d936944e84",
            "compositeImage": {
                "id": "a561117e-23b5-4118-82e5-6f4fbbe4a6a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7c614df-e0ee-4e6a-a6d9-6391003903fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cbf55ce-37f9-449e-8f0e-4fa7936bf911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7c614df-e0ee-4e6a-a6d9-6391003903fa",
                    "LayerId": "516c8433-363b-41b5-8989-0427c47749b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "516c8433-363b-41b5-8989-0427c47749b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b39420b2-0da7-48a9-8f54-e2d936944e84",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 405,
    "xorig": 0,
    "yorig": 0
}