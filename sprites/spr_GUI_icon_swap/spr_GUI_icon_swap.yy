{
    "id": "b16c2e86-d32a-40fa-867e-15f09f03b309",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_swap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c363946f-01cd-4f94-ad7a-2d7480cf5f1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b16c2e86-d32a-40fa-867e-15f09f03b309",
            "compositeImage": {
                "id": "75d7e872-86c7-40e1-8253-156f6a5aab42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c363946f-01cd-4f94-ad7a-2d7480cf5f1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abedde0b-dc92-4e06-959e-3fa5e4c306d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c363946f-01cd-4f94-ad7a-2d7480cf5f1b",
                    "LayerId": "1e70fc18-01cf-4b7a-beda-1f99064e51ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "1e70fc18-01cf-4b7a-beda-1f99064e51ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b16c2e86-d32a-40fa-867e-15f09f03b309",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 15
}