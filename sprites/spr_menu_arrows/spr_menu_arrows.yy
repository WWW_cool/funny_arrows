{
    "id": "34644682-ba41-4ad1-b8cd-12dc7ef25647",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_arrows",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50837b44-03a1-4092-98e0-60b0c1a9cbc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34644682-ba41-4ad1-b8cd-12dc7ef25647",
            "compositeImage": {
                "id": "e3322aee-e66e-41bd-ae2f-58a6d5d64543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50837b44-03a1-4092-98e0-60b0c1a9cbc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a5e195-a85c-4b88-a813-3d3524757a5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50837b44-03a1-4092-98e0-60b0c1a9cbc7",
                    "LayerId": "a05842cb-966c-4c86-8287-0880bd8a3ef6"
                }
            ]
        },
        {
            "id": "8d6f2c0a-f5a7-4fc7-b675-ac1fe13f6815",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34644682-ba41-4ad1-b8cd-12dc7ef25647",
            "compositeImage": {
                "id": "1caf02e1-3c93-4289-b87b-40390c8e6904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d6f2c0a-f5a7-4fc7-b675-ac1fe13f6815",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e795b9d1-5ce8-4a04-bb42-8f6ab2a79b26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d6f2c0a-f5a7-4fc7-b675-ac1fe13f6815",
                    "LayerId": "a05842cb-966c-4c86-8287-0880bd8a3ef6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "a05842cb-966c-4c86-8287-0880bd8a3ef6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34644682-ba41-4ad1-b8cd-12dc7ef25647",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 13
}