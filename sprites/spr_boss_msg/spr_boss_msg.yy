{
    "id": "ef9e6b84-161f-4bbf-9969-05533e856ffe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_msg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 112,
    "bbox_left": 0,
    "bbox_right": 202,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9bbc9a33-3af5-49ea-910a-b0fca9a0c79f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef9e6b84-161f-4bbf-9969-05533e856ffe",
            "compositeImage": {
                "id": "d1e99cda-487e-44f0-8217-20eb0f6a4b7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bbc9a33-3af5-49ea-910a-b0fca9a0c79f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "642f0c11-aff6-4f63-97c3-6e4b6dff569c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bbc9a33-3af5-49ea-910a-b0fca9a0c79f",
                    "LayerId": "9e014968-01ed-4a5a-afd3-439255cbfec3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 113,
    "layers": [
        {
            "id": "9e014968-01ed-4a5a-afd3-439255cbfec3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef9e6b84-161f-4bbf-9969-05533e856ffe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 203,
    "xorig": 100,
    "yorig": 43
}