{
    "id": "e0f20e57-4ed0-4e93-83c1-796b26377f50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_diff_warn_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 0,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11da33c8-0bcd-44bf-a7ae-57bbf68d6d6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0f20e57-4ed0-4e93-83c1-796b26377f50",
            "compositeImage": {
                "id": "7915781b-b4bd-4393-895e-de82b80616e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11da33c8-0bcd-44bf-a7ae-57bbf68d6d6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ac58a1d-3796-4d75-b74d-47bdea852a96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11da33c8-0bcd-44bf-a7ae-57bbf68d6d6b",
                    "LayerId": "e3245b77-c03c-4773-a317-a41cb165f323"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 43,
    "layers": [
        {
            "id": "e3245b77-c03c-4773-a317-a41cb165f323",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0f20e57-4ed0-4e93-83c1-796b26377f50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 49,
    "xorig": 24,
    "yorig": 20
}