{
    "id": "7488afe0-7050-43d5-9a9c-fe77e97256b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_btn_circle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 57,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "359a6fc6-ced7-4d91-8798-301e43105fb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7488afe0-7050-43d5-9a9c-fe77e97256b4",
            "compositeImage": {
                "id": "ecb140f3-8e73-4082-b635-02dda039c830",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "359a6fc6-ced7-4d91-8798-301e43105fb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00a52195-fc5e-4fc0-b92f-e99030feda53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "359a6fc6-ced7-4d91-8798-301e43105fb5",
                    "LayerId": "5b2ca94f-9262-4186-888c-7175e01de9db"
                }
            ]
        },
        {
            "id": "956ebbf4-31d2-455d-9bf5-6d0b98f9c156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7488afe0-7050-43d5-9a9c-fe77e97256b4",
            "compositeImage": {
                "id": "cd5d8e98-e1fe-48a4-8865-cb2fb6098b57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "956ebbf4-31d2-455d-9bf5-6d0b98f9c156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01e3be61-98ca-4bfe-896e-2479b24f5a6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "956ebbf4-31d2-455d-9bf5-6d0b98f9c156",
                    "LayerId": "5b2ca94f-9262-4186-888c-7175e01de9db"
                }
            ]
        },
        {
            "id": "71b09ff7-a281-498e-a986-1e4602110b3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7488afe0-7050-43d5-9a9c-fe77e97256b4",
            "compositeImage": {
                "id": "136b5534-1037-4f3b-8303-f02d15d85692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b09ff7-a281-498e-a986-1e4602110b3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cca3ccf-ba75-42e1-8fe0-6b9a8aa9f794",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b09ff7-a281-498e-a986-1e4602110b3a",
                    "LayerId": "5b2ca94f-9262-4186-888c-7175e01de9db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "5b2ca94f-9262-4186-888c-7175e01de9db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7488afe0-7050-43d5-9a9c-fe77e97256b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 58,
    "xorig": 28,
    "yorig": 31
}