{
    "id": "aa4360c9-4ced-423f-b277-e26fa2ebdb2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_heart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 0,
    "bbox_right": 58,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76297200-2899-495b-9fd1-2055be3dc19e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa4360c9-4ced-423f-b277-e26fa2ebdb2e",
            "compositeImage": {
                "id": "1deb6700-9c11-4e05-8ff0-5fbb4886db1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76297200-2899-495b-9fd1-2055be3dc19e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75f94014-a411-4972-b8b6-85b827242562",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76297200-2899-495b-9fd1-2055be3dc19e",
                    "LayerId": "e441616a-578e-4140-a3c5-648da831d9f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "e441616a-578e-4140-a3c5-648da831d9f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa4360c9-4ced-423f-b277-e26fa2ebdb2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 59,
    "xorig": 29,
    "yorig": 23
}