{
    "id": "90c4ee88-5148-4eef-aba5-95c7e07f3695",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_icons_friend",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4711630-7150-4a62-966b-31e5c174e951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90c4ee88-5148-4eef-aba5-95c7e07f3695",
            "compositeImage": {
                "id": "d6138337-4918-4036-a9d5-7aa2eb708963",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4711630-7150-4a62-966b-31e5c174e951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57334e89-9bf2-4a8c-a58a-490dec822727",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4711630-7150-4a62-966b-31e5c174e951",
                    "LayerId": "4fc9874a-8f84-423d-85a3-62e191d91ad7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "4fc9874a-8f84-423d-85a3-62e191d91ad7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90c4ee88-5148-4eef-aba5-95c7e07f3695",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 27
}