{
    "id": "59ca3b07-4e07-4f84-bcc6-9d6d9cde8f87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_back_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8619f2fd-fadc-4806-91fa-2c1d0a389616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59ca3b07-4e07-4f84-bcc6-9d6d9cde8f87",
            "compositeImage": {
                "id": "86e1c5a6-3b01-4d94-aaae-0c0a0020dfc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8619f2fd-fadc-4806-91fa-2c1d0a389616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c83c44d-8cfc-41ed-be9e-99097eb7a921",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8619f2fd-fadc-4806-91fa-2c1d0a389616",
                    "LayerId": "db2352a3-872b-42c2-85d4-3416c5931c62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "db2352a3-872b-42c2-85d4-3416c5931c62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59ca3b07-4e07-4f84-bcc6-9d6d9cde8f87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 11
}