{
    "id": "6b5a2553-5435-4f5b-9555-6a30884a02f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_icon_time",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3c022fa-27c7-4f6a-ad29-ad0c44428a46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5a2553-5435-4f5b-9555-6a30884a02f5",
            "compositeImage": {
                "id": "9ce9083d-8311-4ca2-b1f7-8c515bb66850",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3c022fa-27c7-4f6a-ad29-ad0c44428a46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd7b1906-ab10-4829-8f99-04416f2621fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3c022fa-27c7-4f6a-ad29-ad0c44428a46",
                    "LayerId": "695738d3-5bdb-49bd-ad62-ab443492aa51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "695738d3-5bdb-49bd-ad62-ab443492aa51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b5a2553-5435-4f5b-9555-6a30884a02f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 15
}