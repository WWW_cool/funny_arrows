{
    "id": "41dd4d1b-ae29-49e3-aee2-00f33cd754c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_lvls",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 404,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5bc863d-a17e-4659-98a4-cbd1271fdbaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41dd4d1b-ae29-49e3-aee2-00f33cd754c5",
            "compositeImage": {
                "id": "5580c9cc-0301-4d41-aa99-1b0704bcc4d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5bc863d-a17e-4659-98a4-cbd1271fdbaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49d897db-a1e0-4535-a678-948862a8f514",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5bc863d-a17e-4659-98a4-cbd1271fdbaf",
                    "LayerId": "54f41669-361d-4000-abdb-effd4ee4ef29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "54f41669-361d-4000-abdb-effd4ee4ef29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41dd4d1b-ae29-49e3-aee2-00f33cd754c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 405,
    "xorig": 0,
    "yorig": 0
}