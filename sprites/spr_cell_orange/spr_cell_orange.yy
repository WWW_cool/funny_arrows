{
    "id": "52f9eff0-4313-4be2-a65e-d2c413d42ab8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_orange",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 70,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "895dce92-9540-48f4-809d-c956f3d5c8e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52f9eff0-4313-4be2-a65e-d2c413d42ab8",
            "compositeImage": {
                "id": "336313b2-0b3b-4e90-a00f-b4a768ea34db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "895dce92-9540-48f4-809d-c956f3d5c8e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e373991-91f5-456d-a4b9-7417ed3c4ff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895dce92-9540-48f4-809d-c956f3d5c8e4",
                    "LayerId": "c345be54-63c7-4b35-9195-7cfd26d5864a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "c345be54-63c7-4b35-9195-7cfd26d5864a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52f9eff0-4313-4be2-a65e-d2c413d42ab8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 71,
    "xorig": 35,
    "yorig": 33
}