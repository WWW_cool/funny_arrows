{
    "id": "d1dd7c11-5f32-4666-b242-93bf67e04cf0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_btn_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c68deff-1bf3-453e-9d8f-f80e698a8153",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1dd7c11-5f32-4666-b242-93bf67e04cf0",
            "compositeImage": {
                "id": "e6074a8b-7858-4b9a-b938-05d6f16e641c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c68deff-1bf3-453e-9d8f-f80e698a8153",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "706d4318-b1a9-41e3-98d4-e4a78912904a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c68deff-1bf3-453e-9d8f-f80e698a8153",
                    "LayerId": "d0f93732-346f-4250-9333-00df56108943"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "d0f93732-346f-4250-9333-00df56108943",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1dd7c11-5f32-4666-b242-93bf67e04cf0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 61,
    "yorig": 34
}