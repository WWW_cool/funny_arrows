{
    "id": "b259ecaa-4871-457c-baf8-082b640cfb87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_fstep_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "658657cc-c497-4caf-b4d1-dd29ec886033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b259ecaa-4871-457c-baf8-082b640cfb87",
            "compositeImage": {
                "id": "1d401152-5534-4c80-b27c-23e816c983a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "658657cc-c497-4caf-b4d1-dd29ec886033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c232c5d-11d1-4ac0-b46d-b0de944f6456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "658657cc-c497-4caf-b4d1-dd29ec886033",
                    "LayerId": "ec8597d5-7d5e-4867-ad2e-3aad76813f13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "ec8597d5-7d5e-4867-ad2e-3aad76813f13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b259ecaa-4871-457c-baf8-082b640cfb87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 17
}