{
    "id": "60daaa53-63e6-4f2e-92f8-dfc215809767",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_warn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76ba28d1-43ae-4790-9a74-55633b0032b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60daaa53-63e6-4f2e-92f8-dfc215809767",
            "compositeImage": {
                "id": "69bdae12-8337-413b-baee-6235a40da7c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ba28d1-43ae-4790-9a74-55633b0032b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c30ebc3-4a10-4b9f-a6d5-45aabdc4a5e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ba28d1-43ae-4790-9a74-55633b0032b9",
                    "LayerId": "5302c7f4-ca71-4be6-a98a-a2aed53f9dc1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "5302c7f4-ca71-4be6-a98a-a2aed53f9dc1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60daaa53-63e6-4f2e-92f8-dfc215809767",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 17
}