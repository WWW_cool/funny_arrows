{
    "id": "864c0e8c-67de-415a-9a97-6a7eafcd459c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_lock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8680a86a-0305-4fcc-80c9-990a5b94a98b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "864c0e8c-67de-415a-9a97-6a7eafcd459c",
            "compositeImage": {
                "id": "a3bb7b0a-2fa7-497f-8b58-5a8a1a9e2283",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8680a86a-0305-4fcc-80c9-990a5b94a98b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1827f8f8-2bb3-4d19-acad-ebfbbbe91982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8680a86a-0305-4fcc-80c9-990a5b94a98b",
                    "LayerId": "c2b6ab12-3416-4dcd-aeff-736cdfc30d7b"
                }
            ]
        },
        {
            "id": "9406a3b0-6808-4db9-bb50-8b8df04e7721",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "864c0e8c-67de-415a-9a97-6a7eafcd459c",
            "compositeImage": {
                "id": "6f493c7c-1276-40a5-81f1-0b2b4f24d34b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9406a3b0-6808-4db9-bb50-8b8df04e7721",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30fe5e75-8797-4d85-8e4e-7c91d61da066",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9406a3b0-6808-4db9-bb50-8b8df04e7721",
                    "LayerId": "c2b6ab12-3416-4dcd-aeff-736cdfc30d7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "c2b6ab12-3416-4dcd-aeff-736cdfc30d7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "864c0e8c-67de-415a-9a97-6a7eafcd459c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 9
}