{
    "id": "40a4b90e-c9d0-4be1-861f-76594238cdb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_monster_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 160,
    "bbox_left": 0,
    "bbox_right": 146,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9588c7e-c322-4907-80ae-22b689ea72d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40a4b90e-c9d0-4be1-861f-76594238cdb9",
            "compositeImage": {
                "id": "095c37a0-c2f5-4067-932f-871df59cfb51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9588c7e-c322-4907-80ae-22b689ea72d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a7cabde-d90a-437c-a3fa-e4ff88a72dfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9588c7e-c322-4907-80ae-22b689ea72d7",
                    "LayerId": "d4bfb734-84b5-4a76-8963-2c5afd8d4729"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "d4bfb734-84b5-4a76-8963-2c5afd8d4729",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40a4b90e-c9d0-4be1-861f-76594238cdb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 147,
    "xorig": 68,
    "yorig": 161
}