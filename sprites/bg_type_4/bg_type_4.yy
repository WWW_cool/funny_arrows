{
    "id": "aa1b9044-d923-4781-9fe3-f5c3ea79c8b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_type_4",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0332841-0843-4db7-9527-2c8e4f8b9bb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa1b9044-d923-4781-9fe3-f5c3ea79c8b0",
            "compositeImage": {
                "id": "7dfcda5a-828c-42fd-a717-764ded8754ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0332841-0843-4db7-9527-2c8e4f8b9bb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbf5ef0b-d728-4820-a54e-c232007fd715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0332841-0843-4db7-9527-2c8e4f8b9bb0",
                    "LayerId": "198b2f47-52b3-4a41-b371-5d73377bb01b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "198b2f47-52b3-4a41-b371-5d73377bb01b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa1b9044-d923-4781-9fe3-f5c3ea79c8b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 0,
    "yorig": 0
}