{
    "id": "28a26497-dd53-4abc-89a6-355535bbeb1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_icons_share",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4813dc22-d543-4186-bacb-442d324565bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28a26497-dd53-4abc-89a6-355535bbeb1e",
            "compositeImage": {
                "id": "c15fac61-438c-4dda-b37e-2f39603a35df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4813dc22-d543-4186-bacb-442d324565bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a76a89c-dd5b-49ca-b987-468e5e632352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4813dc22-d543-4186-bacb-442d324565bf",
                    "LayerId": "ee722af8-268a-4a75-b70d-7dbc57494295"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "ee722af8-268a-4a75-b70d-7dbc57494295",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28a26497-dd53-4abc-89a6-355535bbeb1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 27
}