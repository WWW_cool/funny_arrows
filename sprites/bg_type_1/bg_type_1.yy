{
    "id": "aae1c303-e3d5-4a16-bc3a-04829ad22793",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_type_1",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d0a92da-c10a-4563-96b1-7ed45f02ee18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aae1c303-e3d5-4a16-bc3a-04829ad22793",
            "compositeImage": {
                "id": "f7be5278-becf-4059-97ac-3cee6a54217f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d0a92da-c10a-4563-96b1-7ed45f02ee18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2d5cee8-40b2-401d-a00a-e9bcf29a693f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d0a92da-c10a-4563-96b1-7ed45f02ee18",
                    "LayerId": "9ffa3f23-dc89-447b-8410-28c97684f184"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "9ffa3f23-dc89-447b-8410-28c97684f184",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aae1c303-e3d5-4a16-bc3a-04829ad22793",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 0,
    "yorig": 0
}