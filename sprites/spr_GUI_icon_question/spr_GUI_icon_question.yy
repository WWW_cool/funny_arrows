{
    "id": "49d3f800-71a0-4a98-84ab-adbb2902271f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_question",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f09ba2fa-4ce3-43e6-b194-85c37273a082",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49d3f800-71a0-4a98-84ab-adbb2902271f",
            "compositeImage": {
                "id": "2e9f5c55-9111-436c-ab5c-8004c72d7b28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f09ba2fa-4ce3-43e6-b194-85c37273a082",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a1eb558-c0fe-4ab8-a7e6-4b99027befbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f09ba2fa-4ce3-43e6-b194-85c37273a082",
                    "LayerId": "025464e2-16b3-4de0-9c82-8b618bb7586b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "025464e2-16b3-4de0-9c82-8b618bb7586b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49d3f800-71a0-4a98-84ab-adbb2902271f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 18
}