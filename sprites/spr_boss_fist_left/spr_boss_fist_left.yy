{
    "id": "26725358-14b6-465b-9356-4c32b9cda394",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_fist_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c925b99-3df3-4ccd-bfbd-ce5cbbf71444",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26725358-14b6-465b-9356-4c32b9cda394",
            "compositeImage": {
                "id": "223ad960-1832-44e1-aaa2-33f19c55973e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c925b99-3df3-4ccd-bfbd-ce5cbbf71444",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b97f32c-9caa-4223-86ca-4412520a48bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c925b99-3df3-4ccd-bfbd-ce5cbbf71444",
                    "LayerId": "bbe64f8d-ff08-4227-8e83-0f9d2a4346e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "bbe64f8d-ff08-4227-8e83-0f9d2a4346e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26725358-14b6-465b-9356-4c32b9cda394",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 11
}