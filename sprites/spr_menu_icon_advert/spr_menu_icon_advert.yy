{
    "id": "d8da638f-68b8-4d6f-94aa-2031a766fbf3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_icon_advert",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae307cfe-c248-4b7d-8ab4-a680cd1352a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8da638f-68b8-4d6f-94aa-2031a766fbf3",
            "compositeImage": {
                "id": "bdef3c31-1e3d-464c-8550-2230eae5465b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae307cfe-c248-4b7d-8ab4-a680cd1352a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc0a6766-9ee2-4001-a931-3264eef4fef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae307cfe-c248-4b7d-8ab4-a680cd1352a9",
                    "LayerId": "07b313d9-fda4-456e-94f3-9170b4d444b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "07b313d9-fda4-456e-94f3-9170b4d444b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8da638f-68b8-4d6f-94aa-2031a766fbf3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 17,
    "yorig": 17
}