{
    "id": "b2e1df1d-d649-4c01-9db6-29989d265e95",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_return",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c58c631-9a2d-4548-be45-f5b9d7b4832b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2e1df1d-d649-4c01-9db6-29989d265e95",
            "compositeImage": {
                "id": "f4ac21f5-9516-4f14-bce2-978188eb13f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c58c631-9a2d-4548-be45-f5b9d7b4832b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ec9a276-007c-465f-a100-51aeccbfd64a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c58c631-9a2d-4548-be45-f5b9d7b4832b",
                    "LayerId": "207ede82-6f2f-4a23-b67c-410e6eb93ca3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "207ede82-6f2f-4a23-b67c-410e6eb93ca3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2e1df1d-d649-4c01-9db6-29989d265e95",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 17,
    "yorig": 19
}