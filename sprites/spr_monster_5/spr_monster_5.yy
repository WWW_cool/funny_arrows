{
    "id": "1e1c4b31-c670-419c-85cb-4d6b80d07f2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_monster_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 144,
    "bbox_left": 0,
    "bbox_right": 177,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c927f1a-26de-47c8-af07-db3eef52be81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e1c4b31-c670-419c-85cb-4d6b80d07f2b",
            "compositeImage": {
                "id": "a78acd75-58b5-40f7-b19d-e7f2e6ae2fc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c927f1a-26de-47c8-af07-db3eef52be81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6490a0e3-8640-43ec-8a53-44cb9787dca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c927f1a-26de-47c8-af07-db3eef52be81",
                    "LayerId": "e1c90a95-c9f1-4a6c-be56-d9bf030ea27f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 145,
    "layers": [
        {
            "id": "e1c90a95-c9f1-4a6c-be56-d9bf030ea27f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e1c4b31-c670-419c-85cb-4d6b80d07f2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 178,
    "xorig": 85,
    "yorig": 145
}