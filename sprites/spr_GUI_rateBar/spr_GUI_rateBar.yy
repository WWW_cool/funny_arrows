{
    "id": "3f30e7df-a490-460a-8c88-46accddfea77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_rateBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 70,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "981e481d-0e14-472f-86f8-72ab86ff0ae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f30e7df-a490-460a-8c88-46accddfea77",
            "compositeImage": {
                "id": "81b77ac1-76bc-4a31-9be0-28236132a64e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "981e481d-0e14-472f-86f8-72ab86ff0ae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3caa9236-561c-4a59-ac74-1f4692dc4e92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "981e481d-0e14-472f-86f8-72ab86ff0ae0",
                    "LayerId": "715701f8-6698-465b-887c-64cb363bd309"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "715701f8-6698-465b-887c-64cb363bd309",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f30e7df-a490-460a-8c88-46accddfea77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 71,
    "xorig": 0,
    "yorig": 0
}