{
    "id": "e4713124-e2fe-49e9-b2d5-da80482c6735",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hint_arrow_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 0,
    "bbox_right": 61,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78a3e745-7817-4265-b9e5-96afbeb56683",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4713124-e2fe-49e9-b2d5-da80482c6735",
            "compositeImage": {
                "id": "5aaddaf1-7be8-44aa-8468-fb9cff8c4fd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78a3e745-7817-4265-b9e5-96afbeb56683",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a54c7186-c1fd-4e9f-b020-2bda571339ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78a3e745-7817-4265-b9e5-96afbeb56683",
                    "LayerId": "7e2e707c-ee39-4d3c-90f1-900e9c2d6409"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 52,
    "layers": [
        {
            "id": "7e2e707c-ee39-4d3c-90f1-900e9c2d6409",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4713124-e2fe-49e9-b2d5-da80482c6735",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 62,
    "xorig": 31,
    "yorig": 26
}