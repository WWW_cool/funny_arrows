{
    "id": "5aae3e75-8a20-4395-ba8a-51d877c6e794",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_price",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 68,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1ff20a4-d9b7-47ca-9760-dbab5d49f90f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aae3e75-8a20-4395-ba8a-51d877c6e794",
            "compositeImage": {
                "id": "0c28594b-1c65-4a7c-82b1-bf26abf23d99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1ff20a4-d9b7-47ca-9760-dbab5d49f90f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1446c507-7456-4033-9c4d-0bdeced6e61f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1ff20a4-d9b7-47ca-9760-dbab5d49f90f",
                    "LayerId": "c3e955c9-55e8-4583-b642-4d7380bd0c65"
                }
            ]
        },
        {
            "id": "7e50ab70-357d-47d4-99c6-8ae84284432a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aae3e75-8a20-4395-ba8a-51d877c6e794",
            "compositeImage": {
                "id": "eb38a018-1fe8-4a3b-abd4-12b919f30a4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e50ab70-357d-47d4-99c6-8ae84284432a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7e6a1bd-951d-424e-a0ab-05125914e277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e50ab70-357d-47d4-99c6-8ae84284432a",
                    "LayerId": "c3e955c9-55e8-4583-b642-4d7380bd0c65"
                }
            ]
        },
        {
            "id": "0b253620-d760-4759-84fc-471983149c21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aae3e75-8a20-4395-ba8a-51d877c6e794",
            "compositeImage": {
                "id": "b62dbc0e-da3c-4f5f-9e92-ddf79775bd14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b253620-d760-4759-84fc-471983149c21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8dd375d-c3dd-47a7-8224-0c96d8069710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b253620-d760-4759-84fc-471983149c21",
                    "LayerId": "c3e955c9-55e8-4583-b642-4d7380bd0c65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c3e955c9-55e8-4583-b642-4d7380bd0c65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5aae3e75-8a20-4395-ba8a-51d877c6e794",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 69,
    "xorig": 34,
    "yorig": 32
}