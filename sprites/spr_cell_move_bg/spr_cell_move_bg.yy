{
    "id": "be3d5f53-cfba-46a7-afc3-d9574449c7c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_move_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 135,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72bde3a6-b8a3-4ec3-bc98-3344218a09de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be3d5f53-cfba-46a7-afc3-d9574449c7c6",
            "compositeImage": {
                "id": "aeddbeed-c77f-4afe-aa7c-c4c1edcba577",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72bde3a6-b8a3-4ec3-bc98-3344218a09de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "985238cd-27b2-4d52-b7e2-8ea375330c1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72bde3a6-b8a3-4ec3-bc98-3344218a09de",
                    "LayerId": "7e74e114-c1dc-4cc2-a904-2117a9a8cc15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "7e74e114-c1dc-4cc2-a904-2117a9a8cc15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be3d5f53-cfba-46a7-afc3-d9574449c7c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 136,
    "xorig": 32,
    "yorig": 30
}