{
    "id": "03c11e00-5e3d-4f49-8bb0-367bb9adfb79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_multi",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 0,
    "bbox_right": 64,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c12ba895-f281-4333-93a4-90b003934a19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c11e00-5e3d-4f49-8bb0-367bb9adfb79",
            "compositeImage": {
                "id": "22fd3cff-5980-42ba-9d8e-92d157cc237f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c12ba895-f281-4333-93a4-90b003934a19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9dc14e5-5aa4-4821-855f-44e9371b7e0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c12ba895-f281-4333-93a4-90b003934a19",
                    "LayerId": "9674eed0-0a04-4a6e-a05e-264578184d08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "9674eed0-0a04-4a6e-a05e-264578184d08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03c11e00-5e3d-4f49-8bb0-367bb9adfb79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 32,
    "yorig": 32
}