{
    "id": "ff255802-1dec-4c83-bcff-a7eb8e1d9389",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_d_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc4bd8b5-6a37-4a86-b73f-974a73a35517",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff255802-1dec-4c83-bcff-a7eb8e1d9389",
            "compositeImage": {
                "id": "e8c6e4db-244e-486d-a335-723967bd94cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc4bd8b5-6a37-4a86-b73f-974a73a35517",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccd3149a-ee79-429b-888b-ad87b4e37a94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc4bd8b5-6a37-4a86-b73f-974a73a35517",
                    "LayerId": "aea199a5-0271-4ed6-960a-61d84d96998b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "aea199a5-0271-4ed6-960a-61d84d96998b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff255802-1dec-4c83-bcff-a7eb8e1d9389",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}