{
    "id": "4259ee66-6025-4930-9032-f35be965c57f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_monster_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 160,
    "bbox_left": 0,
    "bbox_right": 174,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9014502f-8a7e-4c94-b2ce-3b952eeb004c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4259ee66-6025-4930-9032-f35be965c57f",
            "compositeImage": {
                "id": "f0759351-7cd7-4f64-abd2-4b40c2f03504",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9014502f-8a7e-4c94-b2ce-3b952eeb004c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82b0d50d-134e-4b84-9585-32639ffce9d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9014502f-8a7e-4c94-b2ce-3b952eeb004c",
                    "LayerId": "e002b021-a829-4bdb-a6ab-0fdcc0434a59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "e002b021-a829-4bdb-a6ab-0fdcc0434a59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4259ee66-6025-4930-9032-f35be965c57f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 175,
    "xorig": 80,
    "yorig": 161
}