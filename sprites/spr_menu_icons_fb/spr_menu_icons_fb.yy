{
    "id": "d822ba22-0e89-4897-b5c2-c58c85068880",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_icons_fb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e062f038-5f0c-4d72-8735-2f1335cdad72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d822ba22-0e89-4897-b5c2-c58c85068880",
            "compositeImage": {
                "id": "86f53d32-7525-4dc1-9786-35f7672eb17f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e062f038-5f0c-4d72-8735-2f1335cdad72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0be1a90f-3b41-4567-858a-da21d1274a84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e062f038-5f0c-4d72-8735-2f1335cdad72",
                    "LayerId": "9bf50574-14de-400f-8ee7-5616325cd321"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "9bf50574-14de-400f-8ee7-5616325cd321",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d822ba22-0e89-4897-b5c2-c58c85068880",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 27
}