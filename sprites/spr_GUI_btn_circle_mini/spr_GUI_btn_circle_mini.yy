{
    "id": "020eb884-fe80-4700-9750-d8a9578620fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_btn_circle_mini",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d30b4f45-3747-4632-a94b-477227785e18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "020eb884-fe80-4700-9750-d8a9578620fd",
            "compositeImage": {
                "id": "0eae49e6-bde9-4b28-8101-9b15fe467398",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d30b4f45-3747-4632-a94b-477227785e18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63d1b686-c79d-4313-96a9-53c752935efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d30b4f45-3747-4632-a94b-477227785e18",
                    "LayerId": "4cdb9cda-9a27-43f0-b773-a80bd4f6c86c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4cdb9cda-9a27-43f0-b773-a80bd4f6c86c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "020eb884-fe80-4700-9750-d8a9578620fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 16
}