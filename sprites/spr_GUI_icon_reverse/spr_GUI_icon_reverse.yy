{
    "id": "dc48abe9-74ba-432a-8b88-cd4c4277a93d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_reverse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "365d296c-df86-4836-8db0-58ff0435a0e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc48abe9-74ba-432a-8b88-cd4c4277a93d",
            "compositeImage": {
                "id": "2017c6a0-7ea2-4479-ab07-b1e8e8132f28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "365d296c-df86-4836-8db0-58ff0435a0e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea03ad9a-e227-46bf-8687-01ccc2eb0501",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "365d296c-df86-4836-8db0-58ff0435a0e4",
                    "LayerId": "6a3ab94e-f959-41e3-9d0f-4a076c456f36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "6a3ab94e-f959-41e3-9d0f-4a076c456f36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc48abe9-74ba-432a-8b88-cd4c4277a93d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 14
}