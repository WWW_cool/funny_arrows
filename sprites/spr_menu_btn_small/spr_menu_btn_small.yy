{
    "id": "3e92c901-9e24-406c-b215-2ef93543111b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_btn_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 72,
    "bbox_left": 0,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03805931-5fbe-452f-a96a-2dda73a6362f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e92c901-9e24-406c-b215-2ef93543111b",
            "compositeImage": {
                "id": "1634472b-2a0e-453a-8018-3ae356f80a57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03805931-5fbe-452f-a96a-2dda73a6362f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d389ac4f-c8cb-4c18-80fa-65f83160adf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03805931-5fbe-452f-a96a-2dda73a6362f",
                    "LayerId": "dcda6915-3afc-4da6-958e-2f1555053e34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 73,
    "layers": [
        {
            "id": "dcda6915-3afc-4da6-958e-2f1555053e34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e92c901-9e24-406c-b215-2ef93543111b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 72,
    "yorig": 36
}