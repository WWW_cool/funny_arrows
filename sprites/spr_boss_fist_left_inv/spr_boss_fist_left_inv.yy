{
    "id": "149e7723-059b-4dd4-a490-22d9cdf12578",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_fist_left_inv",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9314f89e-cbdf-4f97-89be-ea359436ab2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "149e7723-059b-4dd4-a490-22d9cdf12578",
            "compositeImage": {
                "id": "8469955d-5efa-4583-918b-6900fc4c7bfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9314f89e-cbdf-4f97-89be-ea359436ab2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8f5e085-01d3-4bf2-a5c4-21fd841a1cc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9314f89e-cbdf-4f97-89be-ea359436ab2e",
                    "LayerId": "949a80e6-6f6d-4bcc-8ea3-e6caf792af24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "949a80e6-6f6d-4bcc-8ea3-e6caf792af24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "149e7723-059b-4dd4-a490-22d9cdf12578",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 11
}