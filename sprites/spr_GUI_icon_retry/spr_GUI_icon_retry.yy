{
    "id": "e01b2b8e-e53d-43f7-a1cc-6c0e8a4cd756",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_retry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08dbf4d7-26c6-422f-97f0-590b97532ce7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e01b2b8e-e53d-43f7-a1cc-6c0e8a4cd756",
            "compositeImage": {
                "id": "514bbc19-2349-43f1-93af-9d4f7d7f792a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08dbf4d7-26c6-422f-97f0-590b97532ce7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3864d960-25c2-4c68-845d-2880d10fc81a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08dbf4d7-26c6-422f-97f0-590b97532ce7",
                    "LayerId": "39be295f-39d1-4075-971d-8bb8625a8a5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "39be295f-39d1-4075-971d-8bb8625a8a5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e01b2b8e-e53d-43f7-a1cc-6c0e8a4cd756",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 17,
    "yorig": 16
}