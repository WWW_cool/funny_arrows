{
    "id": "c843158c-1784-4e81-84a9-7107654263ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_time",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 294,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b43c5a62-79a4-45b1-ad11-62b99f6d3e70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c843158c-1784-4e81-84a9-7107654263ea",
            "compositeImage": {
                "id": "db75c471-0c9f-4713-9f5c-8651e549af19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b43c5a62-79a4-45b1-ad11-62b99f6d3e70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1654c90-f179-473b-ad2c-d06bf42de2ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b43c5a62-79a4-45b1-ad11-62b99f6d3e70",
                    "LayerId": "c6a2eac6-9331-4a95-9a2a-575f7c134e7c"
                }
            ]
        },
        {
            "id": "933cf6f7-4522-4cd1-becb-e16aa77a898c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c843158c-1784-4e81-84a9-7107654263ea",
            "compositeImage": {
                "id": "518868d0-4978-470c-9982-6dbaabc4ef88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "933cf6f7-4522-4cd1-becb-e16aa77a898c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbdc7e6c-0d0e-4071-b265-aba20e19564f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "933cf6f7-4522-4cd1-becb-e16aa77a898c",
                    "LayerId": "c6a2eac6-9331-4a95-9a2a-575f7c134e7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "c6a2eac6-9331-4a95-9a2a-575f7c134e7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c843158c-1784-4e81-84a9-7107654263ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 295,
    "xorig": 0,
    "yorig": 0
}