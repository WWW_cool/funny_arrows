{
    "id": "0a3706b7-1faa-400f-96d0-bbd5de8bd093",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_type_3",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78b90c9c-3fa2-4780-8a83-d01ac0d3f956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a3706b7-1faa-400f-96d0-bbd5de8bd093",
            "compositeImage": {
                "id": "5048441a-bfae-43bc-a65b-605cbd3c9c4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78b90c9c-3fa2-4780-8a83-d01ac0d3f956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea9b07e2-e7e4-4007-9939-9929d41eb2e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78b90c9c-3fa2-4780-8a83-d01ac0d3f956",
                    "LayerId": "cab08b89-bd89-4e49-8ad7-6126c51b59cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "cab08b89-bd89-4e49-8ad7-6126c51b59cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a3706b7-1faa-400f-96d0-bbd5de8bd093",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 0,
    "yorig": 0
}