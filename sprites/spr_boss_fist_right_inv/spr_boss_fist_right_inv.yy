{
    "id": "249c7552-1660-40e2-9430-2c4eaa11b1b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_fist_right_inv",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56ad1a9c-f9cd-4429-bb06-cb700f6ce6d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "249c7552-1660-40e2-9430-2c4eaa11b1b2",
            "compositeImage": {
                "id": "76b3c815-8a36-4ba8-aad1-793f91218b07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56ad1a9c-f9cd-4429-bb06-cb700f6ce6d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b23368c-8af9-4085-a435-0825e7e97dff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56ad1a9c-f9cd-4429-bb06-cb700f6ce6d7",
                    "LayerId": "457e4845-a5f7-46f8-ac08-f035a6fb8c55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "457e4845-a5f7-46f8-ac08-f035a6fb8c55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "249c7552-1660-40e2-9430-2c4eaa11b1b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 11
}