{
    "id": "d9c3ede8-4601-421c-a217-ec32975df751",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hint_finger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "336acaf8-d522-400e-bb87-82f722364ddb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c3ede8-4601-421c-a217-ec32975df751",
            "compositeImage": {
                "id": "bc8005e6-c232-477f-9788-6ced40098a25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "336acaf8-d522-400e-bb87-82f722364ddb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b46e5fd3-f6ed-4b95-b2e5-fc2023ea3633",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "336acaf8-d522-400e-bb87-82f722364ddb",
                    "LayerId": "6b3ed1e1-ecf1-4103-b535-b8435df29b9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 63,
    "layers": [
        {
            "id": "6b3ed1e1-ecf1-4103-b535-b8435df29b9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9c3ede8-4601-421c-a217-ec32975df751",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 6,
    "yorig": 62
}