{
    "id": "67439486-eb36-452f-b0d9-cc30b065f116",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hint_arrow_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 51,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "254b297b-fa78-40a3-9374-224a5dea0de1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67439486-eb36-452f-b0d9-cc30b065f116",
            "compositeImage": {
                "id": "c881352c-965b-4f66-8b34-37573f8d087a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "254b297b-fa78-40a3-9374-224a5dea0de1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef5a30d9-b160-48af-9b2e-9a62eecd7aed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "254b297b-fa78-40a3-9374-224a5dea0de1",
                    "LayerId": "b97e68fe-f3ee-469a-aa5b-ccac03be42b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "b97e68fe-f3ee-469a-aa5b-ccac03be42b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67439486-eb36-452f-b0d9-cc30b065f116",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 52,
    "xorig": 26,
    "yorig": 31
}