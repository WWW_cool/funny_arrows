{
    "id": "c7ecd1b5-88c7-4f12-877c-3860308c72f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d66bf28-7666-4f7c-b714-c7601c088c03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7ecd1b5-88c7-4f12-877c-3860308c72f3",
            "compositeImage": {
                "id": "62023662-3c12-4569-b8d2-de2206b71eb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d66bf28-7666-4f7c-b714-c7601c088c03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8bf7dd9-d390-4473-8646-48b90d0a6005",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d66bf28-7666-4f7c-b714-c7601c088c03",
                    "LayerId": "94118249-c353-4a99-a033-892f884549aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "94118249-c353-4a99-a033-892f884549aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7ecd1b5-88c7-4f12-877c-3860308c72f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 18
}