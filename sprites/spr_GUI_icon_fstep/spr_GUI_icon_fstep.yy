{
    "id": "c7089e0d-00bf-40a3-9519-33fb9c90eb82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_fstep",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3eca8916-5430-490c-96cb-3f1efe9c8936",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7089e0d-00bf-40a3-9519-33fb9c90eb82",
            "compositeImage": {
                "id": "5de5a28d-da91-465e-a014-274de7fbf2ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eca8916-5430-490c-96cb-3f1efe9c8936",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcb0a51e-1145-4b4f-bfa0-dd34546bcff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eca8916-5430-490c-96cb-3f1efe9c8936",
                    "LayerId": "e94b106c-1818-4bde-bd54-e1fd7858ee51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "e94b106c-1818-4bde-bd54-e1fd7858ee51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7089e0d-00bf-40a3-9519-33fb9c90eb82",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 17
}