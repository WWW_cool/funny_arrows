{
    "id": "114a778e-3bb6-44c6-a3d0-05d5828f0ab7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 262,
    "bbox_left": 0,
    "bbox_right": 335,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f18d5549-3c56-4a96-b606-00f5cbc147ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "114a778e-3bb6-44c6-a3d0-05d5828f0ab7",
            "compositeImage": {
                "id": "dd80b061-5656-48bc-9718-944ce1b94a7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f18d5549-3c56-4a96-b606-00f5cbc147ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16d62456-6681-436e-a228-6e3af5a1fbe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f18d5549-3c56-4a96-b606-00f5cbc147ce",
                    "LayerId": "c0026bfd-abc9-4ca7-b54b-191182abcec2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 263,
    "layers": [
        {
            "id": "c0026bfd-abc9-4ca7-b54b-191182abcec2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "114a778e-3bb6-44c6-a3d0-05d5828f0ab7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 336,
    "xorig": 168,
    "yorig": 131
}