{
    "id": "0616200d-a76d-4c0e-b454-8569818388cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 0,
    "bbox_right": 404,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "046f0c75-aca8-4115-9992-102efce14934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0616200d-a76d-4c0e-b454-8569818388cb",
            "compositeImage": {
                "id": "b0ac5468-06f0-4a81-b1ec-45969401eccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "046f0c75-aca8-4115-9992-102efce14934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f83a58db-3993-4b2c-96db-4c5474f1d118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "046f0c75-aca8-4115-9992-102efce14934",
                    "LayerId": "dfba7c99-e940-4210-af7e-79aed2a25357"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 52,
    "layers": [
        {
            "id": "dfba7c99-e940-4210-af7e-79aed2a25357",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0616200d-a76d-4c0e-b454-8569818388cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 405,
    "xorig": 0,
    "yorig": 0
}