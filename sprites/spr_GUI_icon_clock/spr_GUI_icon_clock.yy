{
    "id": "441bc7ea-94f8-4c9a-9e2a-85133afa30fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_clock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 1,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "778d2203-af9f-468f-9e25-3372875489ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "441bc7ea-94f8-4c9a-9e2a-85133afa30fa",
            "compositeImage": {
                "id": "6533a70b-9e70-4071-ad8b-ef1b66e1e482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "778d2203-af9f-468f-9e25-3372875489ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2639cf43-d004-479c-80b0-0083fc944e4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "778d2203-af9f-468f-9e25-3372875489ea",
                    "LayerId": "7e12fe70-17cb-47c7-acc2-33a35118434f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "7e12fe70-17cb-47c7-acc2-33a35118434f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "441bc7ea-94f8-4c9a-9e2a-85133afa30fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 10,
    "yorig": 10
}