{
    "id": "24c986c6-9e17-4b1e-ab7a-95d3fee72545",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_monster_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 172,
    "bbox_left": 1,
    "bbox_right": 172,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "064eeb6a-1553-4110-8c84-bfedfb5b242a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24c986c6-9e17-4b1e-ab7a-95d3fee72545",
            "compositeImage": {
                "id": "3d31cf88-3fe1-4e69-92c5-ebca0241ab98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "064eeb6a-1553-4110-8c84-bfedfb5b242a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a1f11a9-f93f-40d6-95d0-232a1291a722",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "064eeb6a-1553-4110-8c84-bfedfb5b242a",
                    "LayerId": "f147fa1f-7884-457c-ae0d-93cd4d67819d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 173,
    "layers": [
        {
            "id": "f147fa1f-7884-457c-ae0d-93cd4d67819d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24c986c6-9e17-4b1e-ab7a-95d3fee72545",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 173,
    "xorig": 82,
    "yorig": 173
}