{
    "id": "9de59995-7aff-4fba-a521-e80f0ee8ffb7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_fist_down_inv",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8300b60a-563f-476f-a50e-98c19d957376",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9de59995-7aff-4fba-a521-e80f0ee8ffb7",
            "compositeImage": {
                "id": "6a66f8ff-e91e-4054-80a8-e1796ac1dac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8300b60a-563f-476f-a50e-98c19d957376",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00e5956b-2bdf-4a69-a380-f53e1c9f1751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8300b60a-563f-476f-a50e-98c19d957376",
                    "LayerId": "34fb108d-f327-4b88-89b9-6556a136040d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "34fb108d-f327-4b88-89b9-6556a136040d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9de59995-7aff-4fba-a521-e80f0ee8ffb7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 11,
    "yorig": 13
}