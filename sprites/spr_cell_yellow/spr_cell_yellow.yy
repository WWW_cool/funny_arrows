{
    "id": "9dae5ca2-0f9c-4e75-8441-eb7f864a3307",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 70,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1d385ba-5f15-4db8-bf9a-d736d51d08a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9dae5ca2-0f9c-4e75-8441-eb7f864a3307",
            "compositeImage": {
                "id": "6094f3dc-34b7-4c22-a206-a131805ef391",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1d385ba-5f15-4db8-bf9a-d736d51d08a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66f0ec64-abb3-4085-82f2-a5f5fe858f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1d385ba-5f15-4db8-bf9a-d736d51d08a7",
                    "LayerId": "62dcd684-1640-4a3c-b7c2-6de3b68a29be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "62dcd684-1640-4a3c-b7c2-6de3b68a29be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9dae5ca2-0f9c-4e75-8441-eb7f864a3307",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 71,
    "xorig": 35,
    "yorig": 33
}