{
    "id": "818dcbb6-ae03-4ff8-813c-3c6bd5ae2020",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_swap_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08bbeac3-5e3f-44b6-af73-156758a488fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "818dcbb6-ae03-4ff8-813c-3c6bd5ae2020",
            "compositeImage": {
                "id": "a4ed5e92-a6ef-4919-bd08-f7057a194256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08bbeac3-5e3f-44b6-af73-156758a488fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f4f9b22-8e4c-4ad9-bf39-4c69991b1ba0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08bbeac3-5e3f-44b6-af73-156758a488fc",
                    "LayerId": "f9e3098e-ce1b-4ae1-b3df-cc73a997ec65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "f9e3098e-ce1b-4ae1-b3df-cc73a997ec65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "818dcbb6-ae03-4ff8-813c-3c6bd5ae2020",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 15
}