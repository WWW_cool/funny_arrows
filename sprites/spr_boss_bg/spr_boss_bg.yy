{
    "id": "8843c9a2-b9c8-4fb0-9908-62e0b5462bd7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 327,
    "bbox_left": 0,
    "bbox_right": 326,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "751705af-da67-4fb8-a73c-3890103f0806",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8843c9a2-b9c8-4fb0-9908-62e0b5462bd7",
            "compositeImage": {
                "id": "2895f6ea-315b-465a-bbdc-501be9159f5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "751705af-da67-4fb8-a73c-3890103f0806",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a21a92f4-93cf-4cea-a32c-10fb0e65b6aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "751705af-da67-4fb8-a73c-3890103f0806",
                    "LayerId": "ef9e9552-a311-4af6-903a-7664d7094ee0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 328,
    "layers": [
        {
            "id": "ef9e9552-a311-4af6-903a-7664d7094ee0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8843c9a2-b9c8-4fb0-9908-62e0b5462bd7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 327,
    "xorig": 163,
    "yorig": 164
}