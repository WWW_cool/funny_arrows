{
    "id": "45c54b3b-4ac7-4733-83a3-1270e2fca43c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_part_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98307711-a018-4472-afb5-a8f0fc5098cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45c54b3b-4ac7-4733-83a3-1270e2fca43c",
            "compositeImage": {
                "id": "3fd736e9-f442-4a21-8d0c-694940bf7bbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98307711-a018-4472-afb5-a8f0fc5098cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea26fc13-d4d5-4281-a1e0-e3eeb78b5658",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98307711-a018-4472-afb5-a8f0fc5098cf",
                    "LayerId": "b7425e2b-bd0a-4758-85bb-298264c97d48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "b7425e2b-bd0a-4758-85bb-298264c97d48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45c54b3b-4ac7-4733-83a3-1270e2fca43c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}