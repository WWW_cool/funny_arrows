{
    "id": "2c685e88-7b1f-4042-9c60-15ed2036dac7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_logo_nao",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 163,
    "bbox_left": 22,
    "bbox_right": 263,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35443899-422a-45f3-9413-6edb440822d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c685e88-7b1f-4042-9c60-15ed2036dac7",
            "compositeImage": {
                "id": "01a3eb83-0b4f-4d72-b208-b51000d41f6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35443899-422a-45f3-9413-6edb440822d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17408555-ae5a-4d3a-adab-c5aecf79a0a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35443899-422a-45f3-9413-6edb440822d5",
                    "LayerId": "d3e9dc2d-c7f1-4ac6-89dd-d679223c90b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 187,
    "layers": [
        {
            "id": "d3e9dc2d-c7f1-4ac6-89dd-d679223c90b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c685e88-7b1f-4042-9c60-15ed2036dac7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 287,
    "xorig": 143,
    "yorig": 93
}