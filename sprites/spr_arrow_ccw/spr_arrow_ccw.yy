{
    "id": "009e1730-ad78-4829-913f-67883c3fbfdc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow_ccw",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 36,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef6aa38e-01f6-4356-9a29-b0c9f31c8737",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "009e1730-ad78-4829-913f-67883c3fbfdc",
            "compositeImage": {
                "id": "9d903646-e26a-47b2-aa38-5736a6914ac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef6aa38e-01f6-4356-9a29-b0c9f31c8737",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2da35d3c-bd4f-4b5a-8542-1f0245cc4582",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef6aa38e-01f6-4356-9a29-b0c9f31c8737",
                    "LayerId": "ad9543fb-d1bd-47ac-a5b9-8b837f6e498f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "ad9543fb-d1bd-47ac-a5b9-8b837f6e498f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "009e1730-ad78-4829-913f-67883c3fbfdc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 8,
    "yorig": 28
}