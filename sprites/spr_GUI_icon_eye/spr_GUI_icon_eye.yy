{
    "id": "8c489301-4c81-4e19-be9d-01544f92b57f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_eye",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 36,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd39f4d3-d96a-42c6-9f92-3101db30bc04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c489301-4c81-4e19-be9d-01544f92b57f",
            "compositeImage": {
                "id": "c6781801-bd92-498b-b9a7-974567fdede2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd39f4d3-d96a-42c6-9f92-3101db30bc04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a060dd34-837e-46d0-9ca0-69fa8cba88c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd39f4d3-d96a-42c6-9f92-3101db30bc04",
                    "LayerId": "7b846c3e-adc2-4f78-9c90-ccca19a206b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "7b846c3e-adc2-4f78-9c90-ccca19a206b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c489301-4c81-4e19-be9d-01544f92b57f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 18,
    "yorig": 12
}