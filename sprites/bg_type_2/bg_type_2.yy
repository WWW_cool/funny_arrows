{
    "id": "47857c94-5f35-487b-b9a5-b82881b09319",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_type_2",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b06e758f-705b-42dd-b377-955d5a10634b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47857c94-5f35-487b-b9a5-b82881b09319",
            "compositeImage": {
                "id": "0a064bab-be6c-416b-85bc-ab5f58180967",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b06e758f-705b-42dd-b377-955d5a10634b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30c83fe6-822d-4bb2-8d9a-99f033f36c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b06e758f-705b-42dd-b377-955d5a10634b",
                    "LayerId": "0c0e2a6f-c7cf-4e5a-a983-f96504fd03d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "0c0e2a6f-c7cf-4e5a-a983-f96504fd03d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47857c94-5f35-487b-b9a5-b82881b09319",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 0,
    "yorig": 0
}