{
    "id": "2c390555-84ff-4ad8-9ee3-b05942997892",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "524d8973-9798-425f-9209-f33fdc9cff77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c390555-84ff-4ad8-9ee3-b05942997892",
            "compositeImage": {
                "id": "81090cff-66a9-47ad-8c19-364e64f8760d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "524d8973-9798-425f-9209-f33fdc9cff77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dee7bcb-8a3b-48d2-ab32-c99b27409263",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "524d8973-9798-425f-9209-f33fdc9cff77",
                    "LayerId": "f0c25cd3-b868-406f-bda1-a1be5a69e8ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "f0c25cd3-b868-406f-bda1-a1be5a69e8ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c390555-84ff-4ad8-9ee3-b05942997892",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 35,
    "yorig": 33
}