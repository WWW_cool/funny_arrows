{
    "id": "822a2c81-b3cd-4f69-bd00-fed9495f3f2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_shield",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 0,
    "bbox_right": 68,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9efb671-9221-4c1d-a071-15cfe370c178",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "822a2c81-b3cd-4f69-bd00-fed9495f3f2f",
            "compositeImage": {
                "id": "7045e016-749f-4802-a027-d117d591d515",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9efb671-9221-4c1d-a071-15cfe370c178",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c15d639-27e3-4477-8d7f-91240d39cc2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9efb671-9221-4c1d-a071-15cfe370c178",
                    "LayerId": "92311773-8c00-4763-8924-739819ffe072"
                }
            ]
        },
        {
            "id": "b9d54fcc-7d21-4dba-aaae-86b5d6845e8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "822a2c81-b3cd-4f69-bd00-fed9495f3f2f",
            "compositeImage": {
                "id": "5b86843e-75f6-496e-8007-bdd335d3681f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9d54fcc-7d21-4dba-aaae-86b5d6845e8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0a58078-3b46-470b-a0b5-df5cbf663764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d54fcc-7d21-4dba-aaae-86b5d6845e8e",
                    "LayerId": "92311773-8c00-4763-8924-739819ffe072"
                }
            ]
        },
        {
            "id": "89f90925-31da-4263-9a9b-b35b2a1a2131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "822a2c81-b3cd-4f69-bd00-fed9495f3f2f",
            "compositeImage": {
                "id": "120ed64d-a6f6-4d7a-9346-9824c4253f7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89f90925-31da-4263-9a9b-b35b2a1a2131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9d4b56f-78d2-42a5-8d02-4514cba83452",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89f90925-31da-4263-9a9b-b35b2a1a2131",
                    "LayerId": "92311773-8c00-4763-8924-739819ffe072"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "92311773-8c00-4763-8924-739819ffe072",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "822a2c81-b3cd-4f69-bd00-fed9495f3f2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 69,
    "xorig": 34,
    "yorig": 35
}