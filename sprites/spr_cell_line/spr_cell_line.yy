{
    "id": "ed79aa39-aae0-452b-9f70-ed542f3a271b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_line",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 0,
    "bbox_right": 34,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4589816-27c1-4886-b796-1492e9c06eef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed79aa39-aae0-452b-9f70-ed542f3a271b",
            "compositeImage": {
                "id": "d38d0bcd-66d6-43ed-a6ad-83fa688dc87c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4589816-27c1-4886-b796-1492e9c06eef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b953a77-b0a1-474e-8a44-03b373623380",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4589816-27c1-4886-b796-1492e9c06eef",
                    "LayerId": "79528b47-2ccd-4252-bcfd-c16e4f07e51a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "79528b47-2ccd-4252-bcfd-c16e4f07e51a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed79aa39-aae0-452b-9f70-ed542f3a271b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 35,
    "xorig": -17,
    "yorig": 23
}