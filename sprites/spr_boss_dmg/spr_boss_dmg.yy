{
    "id": "2a03afad-4dc0-4bc6-84d6-4f3b6f085508",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_dmg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 294,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94ee8ca0-5647-4a46-b736-59bee9ef41c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a03afad-4dc0-4bc6-84d6-4f3b6f085508",
            "compositeImage": {
                "id": "21791856-456f-4e2c-bc6e-95cee55f754e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94ee8ca0-5647-4a46-b736-59bee9ef41c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7006c30-f1e5-4f1c-8d6d-a2505d059ad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94ee8ca0-5647-4a46-b736-59bee9ef41c9",
                    "LayerId": "98dc8dc2-402f-4a83-a455-b8c76f3d5ad4"
                }
            ]
        },
        {
            "id": "7ca95ad8-3006-48b7-9eec-a9ed7bdc3960",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a03afad-4dc0-4bc6-84d6-4f3b6f085508",
            "compositeImage": {
                "id": "ab2a7b68-12ba-412a-9d49-c33852604449",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ca95ad8-3006-48b7-9eec-a9ed7bdc3960",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d737c04-da94-45ce-840e-0334f9d4ac20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ca95ad8-3006-48b7-9eec-a9ed7bdc3960",
                    "LayerId": "98dc8dc2-402f-4a83-a455-b8c76f3d5ad4"
                }
            ]
        },
        {
            "id": "ad79a1e2-7a4a-4ccd-95f0-a5c17c12b94d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a03afad-4dc0-4bc6-84d6-4f3b6f085508",
            "compositeImage": {
                "id": "36aa08bb-6b2d-4b27-af23-b5ff0fba09ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad79a1e2-7a4a-4ccd-95f0-a5c17c12b94d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f68842e0-b557-46a3-8eb5-cd745408bbeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad79a1e2-7a4a-4ccd-95f0-a5c17c12b94d",
                    "LayerId": "98dc8dc2-402f-4a83-a455-b8c76f3d5ad4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "98dc8dc2-402f-4a83-a455-b8c76f3d5ad4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a03afad-4dc0-4bc6-84d6-4f3b6f085508",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 295,
    "xorig": 0,
    "yorig": 0
}