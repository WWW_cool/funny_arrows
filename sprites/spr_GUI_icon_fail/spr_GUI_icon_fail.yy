{
    "id": "0434a9f2-7569-4eaf-add9-6b81d87ebd51",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_fail",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 68,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "955fff78-bce3-4b73-b487-2bf6e70da4df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0434a9f2-7569-4eaf-add9-6b81d87ebd51",
            "compositeImage": {
                "id": "30d85d65-ad6a-41ba-9cf3-9d62b2b2cdba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955fff78-bce3-4b73-b487-2bf6e70da4df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8115366f-fe42-4f43-b075-b0a65bebf99f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955fff78-bce3-4b73-b487-2bf6e70da4df",
                    "LayerId": "1e57e9fc-aded-4cd1-86b6-a539a60e4aee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 61,
    "layers": [
        {
            "id": "1e57e9fc-aded-4cd1-86b6-a539a60e4aee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0434a9f2-7569-4eaf-add9-6b81d87ebd51",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 69,
    "xorig": 34,
    "yorig": 30
}