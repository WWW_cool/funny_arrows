{
    "id": "ad306b67-c0ab-4572-aeea-78f2f568d00b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c50ee23b-3b81-49ee-ade3-23491d5c9001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad306b67-c0ab-4572-aeea-78f2f568d00b",
            "compositeImage": {
                "id": "fe77cec1-082b-4c9b-bbed-1e10ace2ef72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c50ee23b-3b81-49ee-ade3-23491d5c9001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "753f442a-4e73-4178-999a-30b5c41472de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c50ee23b-3b81-49ee-ade3-23491d5c9001",
                    "LayerId": "85dad890-afec-43de-8a6f-9a10e2c6e290"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "85dad890-afec-43de-8a6f-9a10e2c6e290",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad306b67-c0ab-4572-aeea-78f2f568d00b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 35,
    "yorig": 33
}