{
    "id": "3a1e4702-f0ee-4041-ae75-55a8fe5eeeff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_price",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "299f761c-c93b-47a9-93cb-395cd75c9ec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a1e4702-f0ee-4041-ae75-55a8fe5eeeff",
            "compositeImage": {
                "id": "797f594d-870c-4b0d-87ae-7fa9e059348d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "299f761c-c93b-47a9-93cb-395cd75c9ec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd4d51d3-43df-4642-a988-8bf6a0fbbc04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "299f761c-c93b-47a9-93cb-395cd75c9ec1",
                    "LayerId": "e147c29d-3626-4a1f-be77-30f10ac487ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "e147c29d-3626-4a1f-be77-30f10ac487ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a1e4702-f0ee-4041-ae75-55a8fe5eeeff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 19,
    "yorig": 21
}