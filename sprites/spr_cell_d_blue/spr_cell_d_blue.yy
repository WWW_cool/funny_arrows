{
    "id": "90bc2e3d-ba4c-44a6-b8a3-b82c3cb1f950",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_d_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31a773f9-840c-46a4-a5fd-179cc19bcce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90bc2e3d-ba4c-44a6-b8a3-b82c3cb1f950",
            "compositeImage": {
                "id": "50d12028-c281-4b13-bbd1-a3fd0ff4705a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31a773f9-840c-46a4-a5fd-179cc19bcce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcc143bc-b444-476b-9323-7f4574408862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31a773f9-840c-46a4-a5fd-179cc19bcce4",
                    "LayerId": "077de8ed-3769-49f2-ba7d-0eb3ce48109d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "077de8ed-3769-49f2-ba7d-0eb3ce48109d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90bc2e3d-ba4c-44a6-b8a3-b82c3cb1f950",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}