{
    "id": "95ff3ff2-1915-4b54-919d-c13b75f88b5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_quest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "debe8f64-9aec-4876-8620-54e36da025e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ff3ff2-1915-4b54-919d-c13b75f88b5e",
            "compositeImage": {
                "id": "f2373572-5e73-4f80-9f94-8cc765b6fdc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "debe8f64-9aec-4876-8620-54e36da025e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2c737d9-9142-4ae3-aeb2-b35c23015ff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "debe8f64-9aec-4876-8620-54e36da025e7",
                    "LayerId": "2c18de2b-e5a3-4766-b86c-98d5f3fdf0ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 63,
    "layers": [
        {
            "id": "2c18de2b-e5a3-4766-b86c-98d5f3fdf0ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95ff3ff2-1915-4b54-919d-c13b75f88b5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 31
}