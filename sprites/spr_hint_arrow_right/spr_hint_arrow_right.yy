{
    "id": "11e9dd8d-2e8b-4f81-a756-5ba357d44c60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hint_arrow_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 0,
    "bbox_right": 61,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da9729f2-a721-47fb-8ce5-6e72064abdf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11e9dd8d-2e8b-4f81-a756-5ba357d44c60",
            "compositeImage": {
                "id": "faa35bef-d30e-4dc9-91af-28e9a10fe20e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da9729f2-a721-47fb-8ce5-6e72064abdf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cfc967d-309c-4551-94f6-9b2ddb54892e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da9729f2-a721-47fb-8ce5-6e72064abdf8",
                    "LayerId": "f7df4399-6fa6-4e01-b8a4-a335d6a203b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 53,
    "layers": [
        {
            "id": "f7df4399-6fa6-4e01-b8a4-a335d6a203b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11e9dd8d-2e8b-4f81-a756-5ba357d44c60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 62,
    "xorig": 62,
    "yorig": 26
}