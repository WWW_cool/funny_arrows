{
    "id": "d03d9468-8e50-4ac7-9e7f-958565bd8a0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_d_violet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57c5e0c6-85c7-4c21-b68b-b7f4028b840c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03d9468-8e50-4ac7-9e7f-958565bd8a0c",
            "compositeImage": {
                "id": "84fe3044-2b22-4c80-b19a-62114dcc7cf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57c5e0c6-85c7-4c21-b68b-b7f4028b840c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f2ee58d-4175-4478-840e-ab0c661ac365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57c5e0c6-85c7-4c21-b68b-b7f4028b840c",
                    "LayerId": "eb6cb752-e98b-43e2-8e52-67d7aff93700"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "eb6cb752-e98b-43e2-8e52-67d7aff93700",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d03d9468-8e50-4ac7-9e7f-958565bd8a0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}