{
    "id": "f3b7a142-a420-4b6c-adab-a44b69162295",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_fist_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da5d9ede-8aaf-44be-a607-40e1ad6a8739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b7a142-a420-4b6c-adab-a44b69162295",
            "compositeImage": {
                "id": "42a034d2-2318-4ef0-a795-435c802cdcd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da5d9ede-8aaf-44be-a607-40e1ad6a8739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a407fd3-a60b-4306-bd67-a0f963021732",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da5d9ede-8aaf-44be-a607-40e1ad6a8739",
                    "LayerId": "74c4dc8f-b171-4e28-9976-6f06df45fb9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "74c4dc8f-b171-4e28-9976-6f06df45fb9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3b7a142-a420-4b6c-adab-a44b69162295",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 11,
    "yorig": 13
}