{
    "id": "c1f1a328-d910-4cb5-aafd-e5dde9c5ee54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_btn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 0,
    "bbox_right": 261,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07c58eff-5dbd-4e37-a09b-7cba97481035",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1f1a328-d910-4cb5-aafd-e5dde9c5ee54",
            "compositeImage": {
                "id": "48fed0ea-6b76-4b41-80e8-a6ab30c3e54e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07c58eff-5dbd-4e37-a09b-7cba97481035",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ae17403-9c74-4931-8a88-15d3efa895b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07c58eff-5dbd-4e37-a09b-7cba97481035",
                    "LayerId": "720d0bb2-c851-4353-bf79-effca7616580"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "720d0bb2-c851-4353-bf79-effca7616580",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1f1a328-d910-4cb5-aafd-e5dde9c5ee54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 262,
    "xorig": 131,
    "yorig": 32
}