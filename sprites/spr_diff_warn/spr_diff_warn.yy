{
    "id": "a9beb8b7-67e2-4771-b234-805d910d71ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_diff_warn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 0,
    "bbox_right": 85,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "463b78ac-f5bb-4cff-a77e-d53699c9d6c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9beb8b7-67e2-4771-b234-805d910d71ca",
            "compositeImage": {
                "id": "734fd53b-d3a3-4543-99e4-2df4b672566f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "463b78ac-f5bb-4cff-a77e-d53699c9d6c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b1d612-3e1a-4fa9-847c-ff764c1d0d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "463b78ac-f5bb-4cff-a77e-d53699c9d6c3",
                    "LayerId": "80bb4939-612f-412f-b0e1-37844ee4515f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "80bb4939-612f-412f-b0e1-37844ee4515f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9beb8b7-67e2-4771-b234-805d910d71ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 86,
    "xorig": 43,
    "yorig": 33
}