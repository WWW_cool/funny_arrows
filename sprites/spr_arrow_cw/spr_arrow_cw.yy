{
    "id": "b0f5287a-60ab-4d42-b8dd-8225c1c5fe8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow_cw",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 36,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b713594-1f53-4bed-b623-1fa3fc71e1c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0f5287a-60ab-4d42-b8dd-8225c1c5fe8c",
            "compositeImage": {
                "id": "6774bda6-bd5e-47bb-ad55-268e7afe3d35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b713594-1f53-4bed-b623-1fa3fc71e1c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d34bdf6-085a-48fe-abbc-4cdd8fa96c05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b713594-1f53-4bed-b623-1fa3fc71e1c6",
                    "LayerId": "8a31993e-bc89-43d2-86c8-340ff347cf54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "8a31993e-bc89-43d2-86c8-340ff347cf54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0f5287a-60ab-4d42-b8dd-8225c1c5fe8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 8,
    "yorig": 8
}