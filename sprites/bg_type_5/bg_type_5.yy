{
    "id": "63bf0b5f-bd4e-4eca-bdef-ccfe11c03b86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_type_5",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a61b02a-5807-4f85-ae53-0ec892a323ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63bf0b5f-bd4e-4eca-bdef-ccfe11c03b86",
            "compositeImage": {
                "id": "8edb798e-d284-4331-bf67-8830afb172d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a61b02a-5807-4f85-ae53-0ec892a323ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b63c0901-d986-4c17-b997-fe1963842e28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a61b02a-5807-4f85-ae53-0ec892a323ce",
                    "LayerId": "f8dce60d-aa5e-4178-b7dd-86b51b938397"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "f8dce60d-aa5e-4178-b7dd-86b51b938397",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63bf0b5f-bd4e-4eca-bdef-ccfe11c03b86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 0,
    "yorig": 0
}