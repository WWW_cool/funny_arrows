{
    "id": "216a8ac1-2ec9-4f9f-b7ce-98d00bbea6bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_fist_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30b55bee-fcca-4111-b15b-5c3b2b711669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "216a8ac1-2ec9-4f9f-b7ce-98d00bbea6bb",
            "compositeImage": {
                "id": "62fc2e1f-6932-4c63-a475-99b12950e953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30b55bee-fcca-4111-b15b-5c3b2b711669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53f98207-283b-4728-9e59-a6c9f4ddb742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30b55bee-fcca-4111-b15b-5c3b2b711669",
                    "LayerId": "d70b5c6e-30fe-47a2-873b-f99fcbd1e026"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "d70b5c6e-30fe-47a2-873b-f99fcbd1e026",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "216a8ac1-2ec9-4f9f-b7ce-98d00bbea6bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 11,
    "yorig": 13
}