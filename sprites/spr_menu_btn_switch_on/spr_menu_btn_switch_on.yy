{
    "id": "01745a9f-91a8-4720-9577-c97973104a7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_btn_switch_on",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 108,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c80215c-b579-44f9-87d2-ae5de41911ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01745a9f-91a8-4720-9577-c97973104a7b",
            "compositeImage": {
                "id": "66c5beee-76c8-40ab-abb0-04d0e4b0c993",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c80215c-b579-44f9-87d2-ae5de41911ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b80785de-6be3-4dfb-8ab1-b28eb08ce7aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c80215c-b579-44f9-87d2-ae5de41911ca",
                    "LayerId": "4a5d80ae-6e6f-4b18-9740-4d0b8d32bcd4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "4a5d80ae-6e6f-4b18-9740-4d0b8d32bcd4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01745a9f-91a8-4720-9577-c97973104a7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 109,
    "xorig": 54,
    "yorig": 29
}