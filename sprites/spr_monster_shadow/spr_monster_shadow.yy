{
    "id": "4db60249-31ea-4171-ab85-ea5c5bf8cbca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_monster_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 171,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "091eedc8-19cb-4c99-a09d-04892cf0bc1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db60249-31ea-4171-ab85-ea5c5bf8cbca",
            "compositeImage": {
                "id": "f7fa03ef-ae36-48d1-a5c9-f7fcc0cb8fdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "091eedc8-19cb-4c99-a09d-04892cf0bc1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43ac8ec4-1ab0-4a84-84f7-6193ff466aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "091eedc8-19cb-4c99-a09d-04892cf0bc1a",
                    "LayerId": "308758e1-a6c0-4ef5-b5bb-e46a0aec77d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "308758e1-a6c0-4ef5-b5bb-e46a0aec77d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4db60249-31ea-4171-ab85-ea5c5bf8cbca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 172,
    "xorig": 86,
    "yorig": 11
}