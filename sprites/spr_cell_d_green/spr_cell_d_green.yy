{
    "id": "301681f1-f405-4822-9016-eb50f67f44ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_d_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfe21396-d65a-4621-803e-98fdac270b5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "301681f1-f405-4822-9016-eb50f67f44ea",
            "compositeImage": {
                "id": "71626731-5b0d-4063-af73-74f6e64820e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfe21396-d65a-4621-803e-98fdac270b5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd545651-502b-481e-b4b7-39ca2e87c1ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfe21396-d65a-4621-803e-98fdac270b5e",
                    "LayerId": "8095564c-e577-404c-9269-c13535e1d225"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "8095564c-e577-404c-9269-c13535e1d225",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "301681f1-f405-4822-9016-eb50f67f44ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}