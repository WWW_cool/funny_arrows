{
    "id": "128f5753-b1b2-47b3-8925-a81434f3df22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_star_on",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1dedb08f-2559-4946-8229-164aefa87c20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "128f5753-b1b2-47b3-8925-a81434f3df22",
            "compositeImage": {
                "id": "8620ef87-c73c-4270-bf30-efd8369b32bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dedb08f-2559-4946-8229-164aefa87c20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cfffc4b-b550-45a4-90e5-b6cbfcea8dc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dedb08f-2559-4946-8229-164aefa87c20",
                    "LayerId": "33f7065f-50d0-49ab-a5c2-c6dc4efeadac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "33f7065f-50d0-49ab-a5c2-c6dc4efeadac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "128f5753-b1b2-47b3-8925-a81434f3df22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 29
}