{
    "id": "bf46e399-00fa-46f4-b9e3-2521d8a72b07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_point",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4174309-328b-4701-a6a0-9b615f799f1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf46e399-00fa-46f4-b9e3-2521d8a72b07",
            "compositeImage": {
                "id": "f7a962d3-0113-443b-b0de-39dde8612c3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4174309-328b-4701-a6a0-9b615f799f1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "348a2cec-86a3-4c2b-b5a5-077749159ce2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4174309-328b-4701-a6a0-9b615f799f1c",
                    "LayerId": "e455994a-5008-43be-b3de-505f7a9f3537"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "e455994a-5008-43be-b3de-505f7a9f3537",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf46e399-00fa-46f4-b9e3-2521d8a72b07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}