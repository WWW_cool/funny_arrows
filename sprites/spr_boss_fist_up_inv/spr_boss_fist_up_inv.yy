{
    "id": "e02b0f2b-e16d-4927-833f-1a41a5cec828",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_fist_up_inv",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "197cd914-6c35-4edd-bba4-f211d9c6c948",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e02b0f2b-e16d-4927-833f-1a41a5cec828",
            "compositeImage": {
                "id": "c07aa816-d7f7-4f00-a88d-f30cc94dd0d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "197cd914-6c35-4edd-bba4-f211d9c6c948",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dde4ce30-80bb-4ee0-85b4-4818e58771a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "197cd914-6c35-4edd-bba4-f211d9c6c948",
                    "LayerId": "637a0398-4330-4853-b810-a68e24b877c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "637a0398-4330-4853-b810-a68e24b877c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e02b0f2b-e16d-4927-833f-1a41a5cec828",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 11,
    "yorig": 13
}