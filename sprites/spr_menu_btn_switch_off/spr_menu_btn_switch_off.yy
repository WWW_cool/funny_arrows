{
    "id": "27348fe8-a96c-4a0d-a6d0-916aa293897b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_btn_switch_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 108,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1986d693-5bf9-424d-803b-56253c048a2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27348fe8-a96c-4a0d-a6d0-916aa293897b",
            "compositeImage": {
                "id": "00753376-7e5a-448e-b1dc-f01da2dfd13c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1986d693-5bf9-424d-803b-56253c048a2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27d1e0c8-c81b-4f6e-84af-81c4572f7c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1986d693-5bf9-424d-803b-56253c048a2d",
                    "LayerId": "b9f08aab-a5e1-415b-8d3e-a044cd9f80df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "b9f08aab-a5e1-415b-8d3e-a044cd9f80df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27348fe8-a96c-4a0d-a6d0-916aa293897b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 109,
    "xorig": 54,
    "yorig": 29
}