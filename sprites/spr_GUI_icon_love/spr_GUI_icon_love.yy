{
    "id": "b255f547-ad38-4db8-8981-335564231835",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_love",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "264c7e7f-3286-4370-a594-c06ffd51f57b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b255f547-ad38-4db8-8981-335564231835",
            "compositeImage": {
                "id": "547bdd3b-667d-4ea9-bab3-d9ce58791db1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "264c7e7f-3286-4370-a594-c06ffd51f57b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "843921aa-e9f9-40a4-87be-209dea54c590",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "264c7e7f-3286-4370-a594-c06ffd51f57b",
                    "LayerId": "7bed5311-d655-4cf2-9c5a-19c681b196d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "7bed5311-d655-4cf2-9c5a-19c681b196d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b255f547-ad38-4db8-8981-335564231835",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 14
}