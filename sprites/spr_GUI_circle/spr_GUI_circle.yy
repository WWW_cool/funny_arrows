{
    "id": "b180550c-ad99-495b-afe4-bf2b545820f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_circle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03d80c4b-0671-40e7-98fe-04d9ec32e3c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b180550c-ad99-495b-afe4-bf2b545820f5",
            "compositeImage": {
                "id": "16cf9120-c285-4bd0-88b8-7208140ed2ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03d80c4b-0671-40e7-98fe-04d9ec32e3c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15719227-d9f5-4998-83cc-873060a895e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03d80c4b-0671-40e7-98fe-04d9ec32e3c2",
                    "LayerId": "2e9c6bef-e9ba-405b-87f7-9860662f59d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "2e9c6bef-e9ba-405b-87f7-9860662f59d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b180550c-ad99-495b-afe4-bf2b545820f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 16
}