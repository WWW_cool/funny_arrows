{
    "id": "40158e40-062b-4294-98fa-5303f6c8fa6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_reverse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf10dc6b-7d00-47a6-a643-060ab1c1985a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40158e40-062b-4294-98fa-5303f6c8fa6c",
            "compositeImage": {
                "id": "daaaee0f-a812-4d41-8efd-b6ff795ed5c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf10dc6b-7d00-47a6-a643-060ab1c1985a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59b305aa-c183-4095-8d47-431341cffadd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf10dc6b-7d00-47a6-a643-060ab1c1985a",
                    "LayerId": "cec79629-3773-4172-b213-97ffb658407a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "cec79629-3773-4172-b213-97ffb658407a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40158e40-062b-4294-98fa-5303f6c8fa6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 11
}