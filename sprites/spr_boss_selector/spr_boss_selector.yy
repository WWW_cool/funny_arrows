{
    "id": "b42eb9f3-04e6-45d3-a40b-05aa8e0ec131",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_selector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 0,
    "bbox_right": 117,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17f067ff-88d8-40b5-b820-e6e4a0bae28b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b42eb9f3-04e6-45d3-a40b-05aa8e0ec131",
            "compositeImage": {
                "id": "ddd15251-deb0-4af2-b7cf-32efc03f7aa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17f067ff-88d8-40b5-b820-e6e4a0bae28b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4e15f11-5820-479d-ae7f-2bed30200764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17f067ff-88d8-40b5-b820-e6e4a0bae28b",
                    "LayerId": "7670e97e-3ace-4931-8c98-001a14558c71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 117,
    "layers": [
        {
            "id": "7670e97e-3ace-4931-8c98-001a14558c71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b42eb9f3-04e6-45d3-a40b-05aa8e0ec131",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 118,
    "xorig": 59,
    "yorig": 58
}