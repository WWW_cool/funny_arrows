{
    "id": "ee1f747f-9e66-44c3-b41d-fa2dc5b0dfa8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_btn_arrow_L",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a5e9164-8572-4552-a53f-90e89303ee52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee1f747f-9e66-44c3-b41d-fa2dc5b0dfa8",
            "compositeImage": {
                "id": "5080164c-9505-4185-ad48-f4302710e90f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a5e9164-8572-4552-a53f-90e89303ee52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c20106b-968c-4668-b6d5-1faad1f38842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a5e9164-8572-4552-a53f-90e89303ee52",
                    "LayerId": "38584634-5a00-4c27-a408-3b48681a6dee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "38584634-5a00-4c27-a408-3b48681a6dee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee1f747f-9e66-44c3-b41d-fa2dc5b0dfa8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 19,
    "yorig": 18
}