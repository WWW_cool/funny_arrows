{
    "id": "de666b28-73c7-48dc-962a-5cd1f0044efa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_bg_big",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 415,
    "bbox_left": 0,
    "bbox_right": 334,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9655b37-16a8-47c9-8d0f-65ba9011b8b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de666b28-73c7-48dc-962a-5cd1f0044efa",
            "compositeImage": {
                "id": "148b45ae-dd2a-42dc-9d1f-2a749a112c9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9655b37-16a8-47c9-8d0f-65ba9011b8b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cb082db-0edb-487d-bd2e-19db08ec17f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9655b37-16a8-47c9-8d0f-65ba9011b8b1",
                    "LayerId": "d0e0a9cd-c629-4ce4-80ac-385ca402e262"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 416,
    "layers": [
        {
            "id": "d0e0a9cd-c629-4ce4-80ac-385ca402e262",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de666b28-73c7-48dc-962a-5cd1f0044efa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 335,
    "xorig": 167,
    "yorig": 208
}