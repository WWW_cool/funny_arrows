{
    "id": "57d9ae13-13f0-4926-bd73-adafd8a86935",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_hpBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dcff17c2-855c-4eec-85e7-5d7ff0e22dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57d9ae13-13f0-4926-bd73-adafd8a86935",
            "compositeImage": {
                "id": "ab19d5c0-2fcd-482c-b2c1-f49afdd92ba9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcff17c2-855c-4eec-85e7-5d7ff0e22dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08ab74f6-b77d-4fd1-bbd4-a2c033d5b925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcff17c2-855c-4eec-85e7-5d7ff0e22dbd",
                    "LayerId": "e9af5abb-89be-4359-8a6d-528a65b306b3"
                }
            ]
        },
        {
            "id": "4c60892a-7505-41fa-987d-e30e7cc1791c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57d9ae13-13f0-4926-bd73-adafd8a86935",
            "compositeImage": {
                "id": "9e6647f1-5779-456d-8242-008c3c36159d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c60892a-7505-41fa-987d-e30e7cc1791c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d25e59f8-fd83-46f0-a882-55a8872eaa5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c60892a-7505-41fa-987d-e30e7cc1791c",
                    "LayerId": "e9af5abb-89be-4359-8a6d-528a65b306b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "e9af5abb-89be-4359-8a6d-528a65b306b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57d9ae13-13f0-4926-bd73-adafd8a86935",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}