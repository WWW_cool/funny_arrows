{
    "id": "3ae71181-edbd-42f7-a61b-540ac5633e5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_vortex",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 0,
    "bbox_right": 64,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d10d1df-4f84-4227-afae-a6e88544723d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ae71181-edbd-42f7-a61b-540ac5633e5f",
            "compositeImage": {
                "id": "98df982e-6b01-4040-871c-4f2dc6e4b64a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d10d1df-4f84-4227-afae-a6e88544723d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36546d14-c9bc-408f-a8f1-9cd770810a69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d10d1df-4f84-4227-afae-a6e88544723d",
                    "LayerId": "92d261ae-cbba-4776-bba6-12ccdb130383"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "92d261ae-cbba-4776-bba6-12ccdb130383",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ae71181-edbd-42f7-a61b-540ac5633e5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 32,
    "yorig": 32
}