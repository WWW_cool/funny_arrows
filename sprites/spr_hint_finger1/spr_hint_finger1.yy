{
    "id": "c0ba233c-8757-4dd4-9c2f-2747a4f6b233",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hint_finger1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9681d63b-bc7f-4e25-b086-f8224c69ec25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0ba233c-8757-4dd4-9c2f-2747a4f6b233",
            "compositeImage": {
                "id": "88391cd4-6383-4fce-8780-32c3afe32035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9681d63b-bc7f-4e25-b086-f8224c69ec25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db0ebf54-d75a-4e46-8f94-953c2cd3ded1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9681d63b-bc7f-4e25-b086-f8224c69ec25",
                    "LayerId": "d7908eef-6f71-48b0-8e37-99061c5a6ffb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "d7908eef-6f71-48b0-8e37-99061c5a6ffb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0ba233c-8757-4dd4-9c2f-2747a4f6b233",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 6,
    "yorig": 55
}