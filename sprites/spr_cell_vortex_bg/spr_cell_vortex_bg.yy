{
    "id": "b1f39454-6f12-4dc4-a638-5a309475b5f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_vortex_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 0,
    "bbox_right": 64,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a72160d-1af3-4815-b5bd-78a4e3ad5050",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f39454-6f12-4dc4-a638-5a309475b5f3",
            "compositeImage": {
                "id": "cbbdb5ba-686e-4edb-93ba-0387f43d8e43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a72160d-1af3-4815-b5bd-78a4e3ad5050",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a9fc572-97c5-4c0c-a64e-c3e324bd4666",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a72160d-1af3-4815-b5bd-78a4e3ad5050",
                    "LayerId": "c4a46294-623c-4bc1-ae2b-df1890527549"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "c4a46294-623c-4bc1-ae2b-df1890527549",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1f39454-6f12-4dc4-a638-5a309475b5f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 32,
    "yorig": 32
}