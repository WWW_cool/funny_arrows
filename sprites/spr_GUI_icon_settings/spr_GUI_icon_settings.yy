{
    "id": "fcff1e4d-3208-46e6-951f-6ae0bd75f417",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_settings",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 0,
    "bbox_right": 34,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ecc7256-c14c-4789-928d-afb6b0a46699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcff1e4d-3208-46e6-951f-6ae0bd75f417",
            "compositeImage": {
                "id": "744521db-d823-4701-bc4b-32760f710c2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ecc7256-c14c-4789-928d-afb6b0a46699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c3be293-c382-4258-b340-bb7028590370",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ecc7256-c14c-4789-928d-afb6b0a46699",
                    "LayerId": "d0092b56-5ee4-41e3-a180-b016b1dd7afa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "d0092b56-5ee4-41e3-a180-b016b1dd7afa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fcff1e4d-3208-46e6-951f-6ae0bd75f417",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 35,
    "xorig": 17,
    "yorig": 17
}