{
    "id": "e4f23ce0-1d97-4af0-9177-7b0ca5b9685e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 172,
    "bbox_left": 0,
    "bbox_right": 267,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a23a2de8-a739-467a-84c3-8651bb11f524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4f23ce0-1d97-4af0-9177-7b0ca5b9685e",
            "compositeImage": {
                "id": "1a9965ad-230c-40d1-b5a0-43a6e53df650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a23a2de8-a739-467a-84c3-8651bb11f524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1433d1f1-63c7-40bb-a3d0-d29f6920cd03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a23a2de8-a739-467a-84c3-8651bb11f524",
                    "LayerId": "f1a76b7f-6f9f-4b57-b655-7849c9f89695"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 173,
    "layers": [
        {
            "id": "f1a76b7f-6f9f-4b57-b655-7849c9f89695",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4f23ce0-1d97-4af0-9177-7b0ca5b9685e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 268,
    "xorig": 134,
    "yorig": 86
}