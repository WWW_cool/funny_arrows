{
    "id": "e1c7b009-ed3a-484e-bfcb-54c787913a58",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_main",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 759,
    "bbox_left": 0,
    "bbox_right": 404,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a815c784-a4e0-4418-a44b-6aaea5394944",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1c7b009-ed3a-484e-bfcb-54c787913a58",
            "compositeImage": {
                "id": "5aae5fc9-be80-4575-adb4-40390de8f3f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a815c784-a4e0-4418-a44b-6aaea5394944",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d039f5ca-abd8-4ddf-98be-b6b3a86c89ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a815c784-a4e0-4418-a44b-6aaea5394944",
                    "LayerId": "1b89014a-96ca-4fda-a11f-f2b1ee6eaeb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 760,
    "layers": [
        {
            "id": "1b89014a-96ca-4fda-a11f-f2b1ee6eaeb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1c7b009-ed3a-484e-bfcb-54c787913a58",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 405,
    "xorig": 0,
    "yorig": 0
}