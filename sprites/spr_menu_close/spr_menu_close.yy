{
    "id": "beb02229-b3db-4d11-9656-a63fbf0ada49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_close",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 56,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1610f8bb-026d-4f20-ae0a-e975c2155f16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb02229-b3db-4d11-9656-a63fbf0ada49",
            "compositeImage": {
                "id": "1e0c6f96-451a-4573-961e-8f8c30e4b9c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1610f8bb-026d-4f20-ae0a-e975c2155f16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81b6dd29-be81-4a9d-b4e4-98fd1bcdbc46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1610f8bb-026d-4f20-ae0a-e975c2155f16",
                    "LayerId": "4def812e-a0ca-4f82-b246-ea624b68c8c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "4def812e-a0ca-4f82-b246-ea624b68c8c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "beb02229-b3db-4d11-9656-a63fbf0ada49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 57,
    "xorig": 28,
    "yorig": 31
}