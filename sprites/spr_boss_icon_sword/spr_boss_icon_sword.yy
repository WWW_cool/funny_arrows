{
    "id": "204251ba-d9fa-4769-97e6-370b825061a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_icon_sword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3c29924-e51b-4833-8aae-9108e35a26dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "204251ba-d9fa-4769-97e6-370b825061a7",
            "compositeImage": {
                "id": "ca3c7319-ec85-4a22-8e9e-38b94ad66eb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3c29924-e51b-4833-8aae-9108e35a26dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fec2bdee-4a35-4f97-945e-47c1024ca647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3c29924-e51b-4833-8aae-9108e35a26dd",
                    "LayerId": "5a10e5ee-99c1-41a0-8f24-b000aadf913c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "5a10e5ee-99c1-41a0-8f24-b000aadf913c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "204251ba-d9fa-4769-97e6-370b825061a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 14
}