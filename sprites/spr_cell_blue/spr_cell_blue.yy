{
    "id": "34ed8a9e-2d4a-4a12-b90a-28c9d855b168",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 70,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a084bc37-6931-4e91-9a59-6fb88f82871e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34ed8a9e-2d4a-4a12-b90a-28c9d855b168",
            "compositeImage": {
                "id": "798680be-ea09-4686-835a-052a155e7bf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a084bc37-6931-4e91-9a59-6fb88f82871e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3774a6ea-b295-4b31-8122-51903b48764e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a084bc37-6931-4e91-9a59-6fb88f82871e",
                    "LayerId": "72c440a0-9019-4f10-b935-ad27048dc242"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "72c440a0-9019-4f10-b935-ad27048dc242",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34ed8a9e-2d4a-4a12-b90a-28c9d855b168",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 71,
    "xorig": 35,
    "yorig": 33
}