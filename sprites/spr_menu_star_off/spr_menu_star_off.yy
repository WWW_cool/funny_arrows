{
    "id": "4aba9ac3-77de-4382-807d-d230991901f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_star_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 1,
    "bbox_right": 54,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dda51cf9-e350-4898-b9e3-c9a68eaf23f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aba9ac3-77de-4382-807d-d230991901f1",
            "compositeImage": {
                "id": "55b530c9-de01-4de7-bea2-9cb6801ca0d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dda51cf9-e350-4898-b9e3-c9a68eaf23f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf37025e-cd84-49e9-9e6c-941efb4e2965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dda51cf9-e350-4898-b9e3-c9a68eaf23f7",
                    "LayerId": "e2443326-7892-4f62-9d2e-13fa2ec365ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "e2443326-7892-4f62-9d2e-13fa2ec365ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4aba9ac3-77de-4382-807d-d230991901f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 29
}