{
    "id": "d5cc1cc4-00f9-4d73-9522-4537ee59f6b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_d_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5c1c3f9-c8ed-485c-a591-79d33e09051e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5cc1cc4-00f9-4d73-9522-4537ee59f6b6",
            "compositeImage": {
                "id": "37509638-f2d1-4985-a500-c79a6d75f1a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5c1c3f9-c8ed-485c-a591-79d33e09051e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3bdd617-3861-456e-9b5e-54265981052d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5c1c3f9-c8ed-485c-a591-79d33e09051e",
                    "LayerId": "f2550472-7880-44e5-88cf-af4d553db5d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "f2550472-7880-44e5-88cf-af4d553db5d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5cc1cc4-00f9-4d73-9522-4537ee59f6b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}