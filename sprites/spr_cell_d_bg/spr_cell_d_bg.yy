{
    "id": "419d7736-5cac-4197-beab-a8aadd078f44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_d_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d67d290-e10f-4aae-8d2f-a97d20c6ea78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419d7736-5cac-4197-beab-a8aadd078f44",
            "compositeImage": {
                "id": "b25c3d0c-07dc-4825-aed7-de4ae85eeaa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d67d290-e10f-4aae-8d2f-a97d20c6ea78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "044454bd-1a07-4154-8476-ebed99b40150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d67d290-e10f-4aae-8d2f-a97d20c6ea78",
                    "LayerId": "f758cab7-15ae-451f-9db7-b895019d33cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f758cab7-15ae-451f-9db7-b895019d33cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "419d7736-5cac-4197-beab-a8aadd078f44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}