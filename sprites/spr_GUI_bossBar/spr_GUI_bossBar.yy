{
    "id": "61fb30de-ec15-4269-850d-078757d7f67d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_bossBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 263,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54318792-ba2b-4853-9c73-f48d974faad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61fb30de-ec15-4269-850d-078757d7f67d",
            "compositeImage": {
                "id": "855f5ec3-32ea-4523-8734-4c44c40c80f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54318792-ba2b-4853-9c73-f48d974faad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd75f23b-82b6-4e1a-a5ef-65b6240541fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54318792-ba2b-4853-9c73-f48d974faad8",
                    "LayerId": "6ef6d01b-759c-404f-8c48-03f59f0435db"
                }
            ]
        },
        {
            "id": "da25a0e7-994a-4396-93f3-7052105ede8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61fb30de-ec15-4269-850d-078757d7f67d",
            "compositeImage": {
                "id": "04757f6a-7f3c-4812-86f1-d9a2c14b93c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da25a0e7-994a-4396-93f3-7052105ede8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4339fd26-249f-4b8b-a0b1-c8e3f8e1453d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da25a0e7-994a-4396-93f3-7052105ede8e",
                    "LayerId": "6ef6d01b-759c-404f-8c48-03f59f0435db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "6ef6d01b-759c-404f-8c48-03f59f0435db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61fb30de-ec15-4269-850d-078757d7f67d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 264,
    "xorig": 0,
    "yorig": 0
}