{
    "id": "1b5e081d-b5d8-4599-a3df-2c56fea98ac3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_bg_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 181,
    "bbox_left": 0,
    "bbox_right": 335,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f71d1ac-5ec0-4333-843b-284c11b97117",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b5e081d-b5d8-4599-a3df-2c56fea98ac3",
            "compositeImage": {
                "id": "4fa8fed5-46db-4715-ba5d-dd2acf931078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f71d1ac-5ec0-4333-843b-284c11b97117",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b37427ae-ed7c-4d99-8083-496e5819da74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f71d1ac-5ec0-4333-843b-284c11b97117",
                    "LayerId": "66f46b20-051e-433b-b11c-4574b904457a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 182,
    "layers": [
        {
            "id": "66f46b20-051e-433b-b11c-4574b904457a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b5e081d-b5d8-4599-a3df-2c56fea98ac3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 336,
    "xorig": 168,
    "yorig": 91
}