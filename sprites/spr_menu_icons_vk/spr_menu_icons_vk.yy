{
    "id": "219f0354-216f-4cd3-932e-7096a81e4b51",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_icons_vk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cca02a1f-32ca-4139-aedc-f46ab9df1f13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "219f0354-216f-4cd3-932e-7096a81e4b51",
            "compositeImage": {
                "id": "49018f67-723d-4830-848d-76ae32200e44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cca02a1f-32ca-4139-aedc-f46ab9df1f13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f7fb743-7cd2-42ea-bf7a-cf4d70b27683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cca02a1f-32ca-4139-aedc-f46ab9df1f13",
                    "LayerId": "fbb68460-abc4-4a44-9ec1-a1fa57c254a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "fbb68460-abc4-4a44-9ec1-a1fa57c254a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "219f0354-216f-4cd3-932e-7096a81e4b51",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 27
}