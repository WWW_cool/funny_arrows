{
    "id": "af9c81a3-1243-4270-9d8d-a7829d280b62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39ebf2d4-115a-4791-8abd-ce63a12d086a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af9c81a3-1243-4270-9d8d-a7829d280b62",
            "compositeImage": {
                "id": "b51d4c17-dc51-4872-9f12-562aaf7ddf5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39ebf2d4-115a-4791-8abd-ce63a12d086a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae78a94e-acaf-4fe1-912e-dcf3b09e734e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39ebf2d4-115a-4791-8abd-ce63a12d086a",
                    "LayerId": "b4535299-e615-42f4-916a-50160365a53f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "b4535299-e615-42f4-916a-50160365a53f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af9c81a3-1243-4270-9d8d-a7829d280b62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 11
}