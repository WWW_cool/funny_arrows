{
    "id": "97526c24-b757-4a26-ae83-7d29b450a013",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_foto_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e71e809-707b-43d1-8a6b-ae6aaa42ecd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97526c24-b757-4a26-ae83-7d29b450a013",
            "compositeImage": {
                "id": "a896937c-e9f4-4db0-982d-74bec1c9ed2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e71e809-707b-43d1-8a6b-ae6aaa42ecd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c3af5f0-e705-4506-ba83-836524794007",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e71e809-707b-43d1-8a6b-ae6aaa42ecd7",
                    "LayerId": "b48ca30e-b71d-44db-8949-176c75600964"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "b48ca30e-b71d-44db-8949-176c75600964",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97526c24-b757-4a26-ae83-7d29b450a013",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 36,
    "yorig": 36
}