{
    "id": "d1a396df-609c-40d9-9613-c9b3ce3fbfe1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_next",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 36,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccace355-b9a2-4e68-b902-4954a0e2e017",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1a396df-609c-40d9-9613-c9b3ce3fbfe1",
            "compositeImage": {
                "id": "da166af8-23e6-4d63-aacb-6e4d8e719b3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccace355-b9a2-4e68-b902-4954a0e2e017",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fb29831-934f-4357-93ee-3999aa011c53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccace355-b9a2-4e68-b902-4954a0e2e017",
                    "LayerId": "7ee57c4b-a027-4449-b424-620b6fb01ca8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "7ee57c4b-a027-4449-b424-620b6fb01ca8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1a396df-609c-40d9-9613-c9b3ce3fbfe1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 18,
    "yorig": 15
}