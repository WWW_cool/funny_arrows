{
    "id": "046c1f3a-7201-4bf6-802e-8675e0d05441",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_icon_share",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12d93ecf-6741-4bdc-8e87-9d0bbb164249",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "046c1f3a-7201-4bf6-802e-8675e0d05441",
            "compositeImage": {
                "id": "f2db4aec-30c6-4b97-b140-e31fdfc2956f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12d93ecf-6741-4bdc-8e87-9d0bbb164249",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3797e965-573c-4629-9e4a-569a0fbcffb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12d93ecf-6741-4bdc-8e87-9d0bbb164249",
                    "LayerId": "02b819a6-eb42-4395-97a1-84004ff8f751"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "02b819a6-eb42-4395-97a1-84004ff8f751",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "046c1f3a-7201-4bf6-802e-8675e0d05441",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 14
}