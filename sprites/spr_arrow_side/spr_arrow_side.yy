{
    "id": "f1d55c2d-fbc1-4f01-ac4b-98bc97279d1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 56,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75baab00-910e-4365-b772-40f1cda19d53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1d55c2d-fbc1-4f01-ac4b-98bc97279d1b",
            "compositeImage": {
                "id": "ce976ba6-9016-418d-9eb0-bdc76e95e459",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75baab00-910e-4365-b772-40f1cda19d53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b80af96c-7f73-4590-a495-5bd37dfd70f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75baab00-910e-4365-b772-40f1cda19d53",
                    "LayerId": "16d688dc-45ea-4160-a78c-fabb30a308c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "16d688dc-45ea-4160-a78c-fabb30a308c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1d55c2d-fbc1-4f01-ac4b-98bc97279d1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 57,
    "xorig": 28,
    "yorig": 8
}