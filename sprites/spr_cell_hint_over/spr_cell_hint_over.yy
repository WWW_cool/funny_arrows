{
    "id": "a8ee7190-e213-4080-be14-b7bdb4568918",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_hint_over",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 84,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9964174d-11ba-44f7-a3dd-b6a078183b50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ee7190-e213-4080-be14-b7bdb4568918",
            "compositeImage": {
                "id": "08391d9c-a270-457d-a4f5-084b5a2b7ee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9964174d-11ba-44f7-a3dd-b6a078183b50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5999559e-75a0-4288-ac4b-7808a699a9a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9964174d-11ba-44f7-a3dd-b6a078183b50",
                    "LayerId": "d2585486-0cb2-44ec-9c23-934a658cfdcb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "d2585486-0cb2-44ec-9c23-934a658cfdcb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8ee7190-e213-4080-be14-b7bdb4568918",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 85,
    "xorig": 42,
    "yorig": 42
}