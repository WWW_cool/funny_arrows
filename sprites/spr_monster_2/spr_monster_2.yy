{
    "id": "d8a73e5c-b155-48fb-aef5-54f6a40272e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_monster_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 190,
    "bbox_left": 0,
    "bbox_right": 141,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bcfa39be-b0e6-465c-bb02-186d26372940",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a73e5c-b155-48fb-aef5-54f6a40272e0",
            "compositeImage": {
                "id": "0fbc4a10-288b-4867-9731-8dec8a401b04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcfa39be-b0e6-465c-bb02-186d26372940",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5955b16e-43b6-403d-a5f6-3efc883267f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcfa39be-b0e6-465c-bb02-186d26372940",
                    "LayerId": "2b83c166-47ca-468f-9c58-bf54adbd28e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 191,
    "layers": [
        {
            "id": "2b83c166-47ca-468f-9c58-bf54adbd28e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8a73e5c-b155-48fb-aef5-54f6a40272e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 142,
    "xorig": 71,
    "yorig": 191
}