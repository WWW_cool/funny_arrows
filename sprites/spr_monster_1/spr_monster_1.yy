{
    "id": "d0f773ba-3fd4-45f1-8e07-d907d21de994",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_monster_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 168,
    "bbox_left": 0,
    "bbox_right": 152,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2cf213f6-0946-4064-bddf-7ba01fd29d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0f773ba-3fd4-45f1-8e07-d907d21de994",
            "compositeImage": {
                "id": "93d6dd2c-2582-4567-a571-5dcd8158cb1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cf213f6-0946-4064-bddf-7ba01fd29d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2eb29f3-a92b-43a4-a399-462cce1416d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cf213f6-0946-4064-bddf-7ba01fd29d56",
                    "LayerId": "50166216-888d-486a-b36e-636f4af3e006"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 169,
    "layers": [
        {
            "id": "50166216-888d-486a-b36e-636f4af3e006",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0f773ba-3fd4-45f1-8e07-d907d21de994",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 153,
    "xorig": 76,
    "yorig": 169
}