{
    "id": "26abaddb-b160-4bb5-8f86-dc966fd77cef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_part_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "921f9fa5-39f9-4c5d-b20a-c5648cb24229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26abaddb-b160-4bb5-8f86-dc966fd77cef",
            "compositeImage": {
                "id": "ef49e4e8-df42-4e26-9ce9-078b9f7737df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "921f9fa5-39f9-4c5d-b20a-c5648cb24229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "362e5ff9-bdef-44ba-8b7b-1443610848ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "921f9fa5-39f9-4c5d-b20a-c5648cb24229",
                    "LayerId": "dc3c0f53-05da-498b-82f4-e2f40d0e6d56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "dc3c0f53-05da-498b-82f4-e2f40d0e6d56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26abaddb-b160-4bb5-8f86-dc966fd77cef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 9
}