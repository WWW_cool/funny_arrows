{
    "id": "58982232-8c6a-44d7-911d-be2cabbc88f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_monster_lock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "954d8781-2fb8-4fb1-ba72-a6568a5086bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58982232-8c6a-44d7-911d-be2cabbc88f5",
            "compositeImage": {
                "id": "a39607a1-c413-4f1a-9e13-5c40569c17f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "954d8781-2fb8-4fb1-ba72-a6568a5086bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c0d2681-120a-45dd-af30-12122cadfde6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "954d8781-2fb8-4fb1-ba72-a6568a5086bc",
                    "LayerId": "81b839c2-0107-4345-97aa-6e97fa50916b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "81b839c2-0107-4345-97aa-6e97fa50916b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58982232-8c6a-44d7-911d-be2cabbc88f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 40
}