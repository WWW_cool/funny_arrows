{
    "id": "b099302c-92de-48e8-afd4-6865974dc91d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_fist_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a229c4d1-c6c1-4495-a41d-194386e8c264",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b099302c-92de-48e8-afd4-6865974dc91d",
            "compositeImage": {
                "id": "5068baef-969b-450c-a89d-13b92849d0b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a229c4d1-c6c1-4495-a41d-194386e8c264",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92654017-4e40-4172-8a0f-ae5f4dad606a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a229c4d1-c6c1-4495-a41d-194386e8c264",
                    "LayerId": "bf0fc748-4153-4313-a860-109c81dd9c6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "bf0fc748-4153-4313-a860-109c81dd9c6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b099302c-92de-48e8-afd4-6865974dc91d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 11
}