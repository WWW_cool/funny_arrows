{
    "id": "76315f8d-ac89-407c-8ee0-543239ed1a4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUI_icon_like",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94a57a4d-d442-4c23-9040-dd645561c0e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76315f8d-ac89-407c-8ee0-543239ed1a4a",
            "compositeImage": {
                "id": "e8b82b02-6dee-4b78-b7ce-f13398d875e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94a57a4d-d442-4c23-9040-dd645561c0e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f84a0f19-1218-4bce-ba07-9ce9cd84a576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94a57a4d-d442-4c23-9040-dd645561c0e0",
                    "LayerId": "67c4bd4f-f6ac-4af1-91e4-3a9459d815f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "67c4bd4f-f6ac-4af1-91e4-3a9459d815f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76315f8d-ac89-407c-8ee0-543239ed1a4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 14,
    "yorig": 20
}