_GLOBAL_INIT();
if(global._def_glb_var_default_resolution_pc)
{
        var tmp_x = 288
        var tmp_y = 132
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 1
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 288
        var tmp_y = 204
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 3
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 0
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 288
        var tmp_y = 276
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 1
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 4
            a_next_dir      = 2
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 360
        var tmp_y = 204
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 3
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 360
        var tmp_y = 276
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 2
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 360
        var tmp_y = 348
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 3
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 0
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 360
        var tmp_y = 420
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 4
            a_next_dir      = 0
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 432
        var tmp_y = 492
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 2
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 432
        var tmp_y = 420
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 4
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 0
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 432
        var tmp_y = 348
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 432
        var tmp_y = 276
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 3
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 2
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 504
        var tmp_y = 420
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 504
        var tmp_y = 348
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
}
else
{
        var tmp_x = 92
        var tmp_y = 180
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 1
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 92
        var tmp_y = 252
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 3
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 0
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 92
        var tmp_y = 324
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 1
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 4
            a_next_dir      = 2
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 164
        var tmp_y = 252
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 3
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 164
        var tmp_y = 324
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 2
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 164
        var tmp_y = 396
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 3
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 0
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 164
        var tmp_y = 468
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 4
            a_next_dir      = 0
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 236
        var tmp_y = 540
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 2
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 236
        var tmp_y = 468
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 4
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 0
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 236
        var tmp_y = 396
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 236
        var tmp_y = 324
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 3
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 2
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 308
        var tmp_y = 468
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 308
        var tmp_y = 396
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
}