_GLOBAL_INIT();
if(global._def_glb_var_default_resolution_pc)
{
        var tmp_x = 396
        var tmp_y = 456
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 4
            a_colour        = 3
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 5
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 3
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 5
        }
        var tmp_x = 396
        var tmp_y = 384
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 2
            a_colour        = 6
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 0
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 396
        var tmp_y = 312
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 1
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 1
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 6
            a_next_dir      = 0
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 396
        var tmp_y = 240
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 4
            a_next_dir      = 0
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 396
        var tmp_y = 168
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 3
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 468
        var tmp_y = 240
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 2
            a_colour        = 1
            a_dir           = 2
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 2
            a_next_dir      = 0
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 468
        var tmp_y = 312
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 2
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 1
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 540
        var tmp_y = 240
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 4
            a_colour        = 3
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 2
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 0
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 324
        var tmp_y = 384
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 4
            a_colour        = 3
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 2
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 0
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 324
        var tmp_y = 312
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 1
            a_angle         = 1
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 1
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 324
        var tmp_y = 240
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 4
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 5
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 1
            a_next_colour   = 1
            a_next_dir      = 0
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 324
        var tmp_y = 168
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 2
            a_colour        = 1
            a_dir           = 1
            a_angle         = 3
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 0
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 252
        var tmp_y = 384
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 2
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 2
            a_next_dir      = 0
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
}
else
{
        var tmp_x = 204
        var tmp_y = 504
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 4
            a_colour        = 3
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 5
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 3
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 5
        }
        var tmp_x = 204
        var tmp_y = 432
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 2
            a_colour        = 6
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 0
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 204
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 1
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 1
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 6
            a_next_dir      = 0
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 204
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 4
            a_next_dir      = 0
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 204
        var tmp_y = 216
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 3
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 276
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 2
            a_colour        = 1
            a_dir           = 2
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 2
            a_next_dir      = 0
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 276
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 2
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 1
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 348
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 4
            a_colour        = 3
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 2
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 0
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 132
        var tmp_y = 432
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 4
            a_colour        = 3
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 2
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 0
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 132
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 1
            a_angle         = 1
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 1
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 132
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 4
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 5
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 1
            a_next_colour   = 1
            a_next_dir      = 0
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 132
        var tmp_y = 216
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 2
            a_colour        = 1
            a_dir           = 1
            a_angle         = 3
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 0
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 60
        var tmp_y = 432
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 2
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 2
            a_next_dir      = 0
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
}