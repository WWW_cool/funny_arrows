_GLOBAL_INIT();
if(global._def_glb_var_default_resolution_pc)
{
        var tmp_x = 384
        var tmp_y = 300
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 1
            a_colour        = 2
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 2
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 2
            a_next_dir      = 3
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 384
        var tmp_y = 228
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 2
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 1
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 312
        var tmp_y = 228
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 2
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 312
        var tmp_y = 300
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 1
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 2
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 2
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 312
        var tmp_y = 372
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 1
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 1
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 384
        var tmp_y = 372
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 2
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 2
            a_next_dir      = 0
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 456
        var tmp_y = 300
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 1
            a_colour        = 4
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 3
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 3
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 456
        var tmp_y = 228
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 4
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 1
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 456
        var tmp_y = 372
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 1
            a_colour        = 4
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 6
            a_next_dir      = 2
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 528
        var tmp_y = 300
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 4
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 2
            a_next_dir      = 2
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
}
else
{
        var tmp_x = 168
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 1
            a_colour        = 2
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 2
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 2
            a_next_dir      = 3
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 168
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 2
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 1
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 96
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 2
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 96
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 1
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 2
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 2
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 96
        var tmp_y = 432
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 1
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 1
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 168
        var tmp_y = 432
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 2
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 1
            a_next_type     = 0
            a_next_colour   = 2
            a_next_dir      = 0
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 240
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 1
            a_colour        = 4
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 3
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 3
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 240
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 4
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 1
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 240
        var tmp_y = 432
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 1
            a_colour        = 4
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 6
            a_next_dir      = 2
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        var tmp_x = 312
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 4
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 2
            a_next_dir      = 2
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
}