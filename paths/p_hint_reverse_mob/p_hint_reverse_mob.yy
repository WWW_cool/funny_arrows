{
    "id": "75fae969-471b-443c-9ad1-8d1d82828b88",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "p_hint_reverse_mob",
    "closed": false,
    "hsnap": 2,
    "kind": 0,
    "points": [
        {
            "id": "8ef59d08-beca-46b7-9c57-550fc6c02896",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 132,
            "y": 360,
            "speed": 100
        },
        {
            "id": "0ed58bf7-4d28-4f9e-aa78-ef9b596adae1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 204,
            "y": 360,
            "speed": 100
        },
        {
            "id": "0b01959a-72b7-4262-993a-600350a434d6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 204,
            "y": 288,
            "speed": 100
        },
        {
            "id": "7791fb5a-cec5-4dc2-848a-f731078dd047",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 276,
            "y": 288,
            "speed": 100
        },
        {
            "id": "48af9085-4540-4ad8-b4ac-5725931f40bc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 276,
            "y": 360,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 2
}