{
    "id": "5d344108-cf72-46f1-badf-a1e4a7414e56",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "p_hint_reverse",
    "closed": false,
    "hsnap": 2,
    "kind": 0,
    "points": [
        {
            "id": "09a22a13-79a7-4268-b5e6-6296a99e8263",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 328,
            "y": 336,
            "speed": 100
        },
        {
            "id": "5877a51d-7a0f-40cb-aaf6-2d1ad02f66da",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 400,
            "y": 336,
            "speed": 100
        },
        {
            "id": "0ef53bcb-e425-4dda-b28c-76ec37617984",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 400,
            "y": 264,
            "speed": 100
        },
        {
            "id": "994f9fa9-0ae9-4f2b-af0c-9959afeabb86",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 472,
            "y": 264,
            "speed": 100
        },
        {
            "id": "e3359d54-0784-4052-9a76-81208a1a7c13",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 472,
            "y": 336,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 2
}