{
    "id": "80f36a46-9833-427a-913f-bbfe00f2abc3",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "p_heart",
    "closed": false,
    "hsnap": 8,
    "kind": 0,
    "points": [
        {
            "id": "aa678504-5b00-4065-9643-b85723daa3c2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 128,
            "speed": 100
        },
        {
            "id": "95f11392-460a-439d-a2de-2733c6aa8feb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 148,
            "speed": 100
        },
        {
            "id": "db83c362-3723-4284-b20c-a8c8f65eb265",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 128,
            "speed": 100
        },
        {
            "id": "cef2daeb-d8b1-46b5-a038-ace74e76c030",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 128,
            "speed": 100
        },
        {
            "id": "53243591-bc4c-476a-9bab-f479f324e1e4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 112,
            "y": 148,
            "speed": 100
        },
        {
            "id": "be7706c9-30d9-4279-98f4-68a49aa1eb70",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 128,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 4
}