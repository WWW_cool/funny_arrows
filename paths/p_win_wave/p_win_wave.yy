{
    "id": "9d8d944f-25d2-484d-8327-165e4b0f6a04",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "p_win_wave",
    "closed": false,
    "hsnap": 8,
    "kind": 0,
    "points": [
        {
            "id": "c149910f-ed97-4f35-ac47-7b14651292c4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 128,
            "speed": 100
        },
        {
            "id": "c727f5e5-7122-4e72-9765-0d4757c02a62",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 120,
            "speed": 100
        },
        {
            "id": "6cae5e90-9b8d-4c96-8c08-d718cb1b98c2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 136,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 4
}