{
    "id": "47dc9b22-b58f-40cd-8aab-44c865f67c1b",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "p_hint_wave",
    "closed": false,
    "hsnap": 8,
    "kind": 0,
    "points": [
        {
            "id": "13e9f5fa-9bb5-4b12-9804-62b402c9f561",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 188,
            "speed": 100
        },
        {
            "id": "e1df1c77-ba83-4e9b-9217-149d4a686498",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 176,
            "speed": 100
        },
        {
            "id": "b8daac1a-aea6-46d6-8662-02f1bf63c750",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 192,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 4
}