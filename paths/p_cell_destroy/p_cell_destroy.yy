{
    "id": "08fbebc0-cc13-4a66-bc36-c37ce065d3b3",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "p_cell_destroy",
    "closed": false,
    "hsnap": 8,
    "kind": 0,
    "points": [
        {
            "id": "0c025627-5786-43b2-b78b-b97d0f832b2e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 96,
            "speed": 100
        },
        {
            "id": "9dcf10f1-75a7-4cd1-8c89-1f68179722e5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 128,
            "speed": 100
        },
        {
            "id": "35d0f5a3-dab4-4de9-9f72-c8909d7b7d2f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 48,
            "y": -8,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 4
}