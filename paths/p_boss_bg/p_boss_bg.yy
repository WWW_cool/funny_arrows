{
    "id": "9f64d147-3ce7-46bb-b0b7-8676ad2f24e7",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "p_boss_bg",
    "closed": true,
    "hsnap": 8,
    "kind": 0,
    "points": [
        {
            "id": "150054e7-4928-4fdd-a8c2-b56dcda6b208",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 56,
            "y": 128,
            "speed": 100
        },
        {
            "id": "d8ef4688-99d1-40aa-96db-f319c32cf19f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 120,
            "speed": 100
        },
        {
            "id": "5b201108-d468-4915-a404-6ce17afd6fde",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 136,
            "speed": 100
        },
        {
            "id": "3e25a6cd-6a74-460e-9a38-b295fb55c778",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 120,
            "speed": 100
        },
        {
            "id": "3cf47922-a750-40df-953c-7dcf808d8769",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 104,
            "y": 128,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 4
}