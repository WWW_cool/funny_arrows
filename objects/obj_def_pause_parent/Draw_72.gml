
if(global._def_glb_var_pause)
{
    var tmp_spr = global._def_glb_var_pause_sprite;
    if(sprite_exists(tmp_spr) && !_def_p_draw_after_create)
    {
        if(global._def_glb_var_g_resolution_w_prev != 0 && 
        global._def_glb_var_g_resolution_w_prev != global._def_glb_var_g_resolution_w)
        {
            var k_w = global._def_glb_var_default_resolution_w/global._def_glb_var_g_resolution_w_prev;
            var k_h = global._def_glb_var_default_resolution_h/global._def_glb_var_g_resolution_h_prev;
            
            draw_sprite_ext(tmp_spr,0,
            __view_get( e__VW.XView, global._def_glb_var_view_current ),
            __view_get( e__VW.YView, global._def_glb_var_view_current ),
            k_w,
            k_h,0,c_white,1);
        }
        else
        {
            draw_sprite_ext(tmp_spr,0,
            __view_get( e__VW.XView, global._def_glb_var_view_current ),
            __view_get( e__VW.YView, global._def_glb_var_view_current ),
            1/global._def_GUI_xscale,
            1/global._def_GUI_yscale,0,c_white,1);
        }
    }
    var _def_tmp_alpha = draw_get_alpha();
    draw_set_alpha(0.2);
    draw_set_colour(c_black);
    draw_rectangle(__view_get( e__VW.XView, global._def_glb_var_view_current ),
    __view_get( e__VW.YView, global._def_glb_var_view_current ),
    __view_get( e__VW.XView, global._def_glb_var_view_current ) + __view_get( e__VW.WView, global._def_glb_var_view_current ),
    __view_get( e__VW.YView, global._def_glb_var_view_current ) + __view_get( e__VW.HView, global._def_glb_var_view_current ),
    false);
    draw_set_alpha(_def_tmp_alpha);
}

