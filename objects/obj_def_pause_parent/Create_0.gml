/// @description  init parent obj for all pause in game

// default behavior = simple pause
enum _EnPauseType{SIMPLE = 1,MENU,HINT,LVL_END,ACHIEV};
_def_p_draw_after_create = 0;
_def_p_owner = noone;
_def_p_type = 0;
_def_p_first_start = 0;

var ind = 0;
_def_p_need_objs[ind++] = global.inst_glb;
_def_p_need_objs[ind++] = global._v_btns_owner;
_def_p_need_objs[ind++] = global._v_info_owner;
_def_p_need_objs[ind++] = global.inst_back;

scr_def_show_debug_message("_def_pause_parent INIT (sucsess)","info");


