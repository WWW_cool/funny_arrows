/// @description draw screen back

if(global._def_glb_var_pause)
{
    
    //if(!sprite_exists(global._def_glb_var_pause_sprite))
    if(!_def_p_first_start)
    {
        _def_p_draw_after_create = 1;
        _def_p_first_start = 1;
        /*global._def_glb_var_pause_sprite = sprite_create_from_surface(application_surface, 0, 0,
        view_wview[global._def_glb_var_view_current], view_hview[global._def_glb_var_view_current],
        0, 0, 0, 0);*/
        instance_deactivate_all(true);
        scr_def_instance_activate_base();
        var i;
        for(i = 0; i < array_length_1d(_def_p_need_objs); i += 1)
        {
            instance_activate_object(_def_p_need_objs[i]);
        }
        
        instance_activate_object(_def_p_owner);
        
        for(var i = 0; i < 8; i += 1)
        {
           // background_visible[i] = 0;
        }
        global._def_glb_var_g_resolution_w_prev = global._def_glb_var_g_resolution_w;
        global._def_glb_var_g_resolution_h_prev = global._def_glb_var_g_resolution_h; // in game resolution
    }
    
    /*if(sprite_exists(global._def_glb_var_pause_sprite) && _def_p_draw_after_create)
    {
        _def_p_draw_after_create = 0;
        draw_sprite_ext(global._def_glb_var_pause_sprite,0,
        view_xview[global._def_glb_var_view_current],
        view_yview[global._def_glb_var_view_current],
        1/global._def_GUI_xscale,
        1/global._def_GUI_yscale,0,c_white,1);
    }*/
}

/* */
/*  */
