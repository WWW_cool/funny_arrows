{
    "id": "828ce2f9-d683-442c-8a47-bba4d4bf6427",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_def_pause_parent",
    "eventList": [
        {
            "id": "6f143db0-d082-4bc2-b439-ccf8e2b024d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "828ce2f9-d683-442c-8a47-bba4d4bf6427"
        },
        {
            "id": "3b2eac1c-8f6b-4424-9658-c6dcdfb3f2ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "828ce2f9-d683-442c-8a47-bba4d4bf6427"
        },
        {
            "id": "dbac5d2c-aba6-492f-8317-486d8cf17a0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "828ce2f9-d683-442c-8a47-bba4d4bf6427"
        },
        {
            "id": "175b75bd-c3e6-4ed8-acd0-92370b1b3717",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 8,
            "m_owner": "828ce2f9-d683-442c-8a47-bba4d4bf6427"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}