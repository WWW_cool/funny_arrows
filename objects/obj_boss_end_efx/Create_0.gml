/// @description  init
randomize();

type = 0; // win = 1, lose = 0;

life_time = 4;

lose_scale = 0;
lose_scale_speed = 0.3;
lose_msg = "Ха-ха!!!# Время закончилось!";

win_radius = 130;
win_r_time_min = 50;
win_r_time_max = 250;
win_r_time = 0;
win_r_count = irandom_range(15,25);

anim_res[0] = 0;

alarm[0] = 1;

