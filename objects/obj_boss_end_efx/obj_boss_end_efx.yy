{
    "id": "9f10b516-689b-4bb9-817c-8a64042a4022",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss_end_efx",
    "eventList": [
        {
            "id": "a444ecbd-c350-4f51-90f3-e400665db02f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9f10b516-689b-4bb9-817c-8a64042a4022"
        },
        {
            "id": "df8280aa-1398-458f-930d-0f7b7094b62e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "9f10b516-689b-4bb9-817c-8a64042a4022"
        },
        {
            "id": "dd03966d-50b2-4a16-8198-82a7e9f708dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9f10b516-689b-4bb9-817c-8a64042a4022"
        },
        {
            "id": "48770a8a-5901-4bb2-bc15-c14694bbac7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9f10b516-689b-4bb9-817c-8a64042a4022"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}