/// @description  draw 

if(type) // win
{
    win_r_time -= delta_time/1000;
    if(win_r_time <= 0  && win_r_count)
    {
        win_r_count--;
        win_r_time = irandom_range(win_r_time_min,win_r_time_max);
        var rnd_rad = irandom(win_radius);
        var rnd_angle = irandom(360);
        var rnd_x = x + lengthdir_x(rnd_rad,rnd_angle);
        var rnd_y = y + lengthdir_y(rnd_rad,rnd_angle);
        instance_create(rnd_x,rnd_y,obj_boss_efx_hit);
    }
}
else // lose
{
    if(lose_scale < 1)
    {
        lose_scale = v_lerp(anim_res,lose_scale,1,lose_scale_speed,lose_scale_speed/5);
    }
    draw_sprite_ext(spr_boss_msg,0,x,y,lose_scale,lose_scale,0,c_white,1);
    
    draw_set_font(f_text);
    draw_set_valign(fa_middle);
    draw_set_halign(fa_center);
    draw_set_colour(c_black);
    draw_text_transformed(x,y,string_hash_to_newline(lose_msg),lose_scale,lose_scale,0);
}

life_time -= delta_time/1000000;
if(life_time <= 0)
{
    instance_destroy();
}



