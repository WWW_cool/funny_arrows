/// @description  init

enum EnColourTypes{EMPTY = 0,BLUE,ORANGE,YELLOW,RED,GREEN,VIOLET,LENGTH};
enum EnDirTypes{NONE = 0,SIDE,CCW,CW,LENGTH};
enum EnCellTypes{NONE = 0,SHIELD,LIFT,TIME,DOUBLE,VORTEX,MULTI,LOCK,LENGTH};
enum EnCellStates{IDLE = 0,SELECTED,CREATE,DESTROY,ANIM,ANIM_END,REVERSE,LENGTH};
enum EnCellAnimTypes{CREATE = 0,DESTROY,LIFT,MOVE,COMMON,FIRST,LENGTH};

if(room != rm_lvlCreate)
{
    a_state = EnCellStates.CREATE;
}
else
{
    a_state = EnCellStates.IDLE;
}

a_anim_state = EnCellAnimTypes.COMMON;

a_colour = EnColourTypes.EMPTY;
a_dir = EnDirTypes.NONE;    //irandom(EnDirTypes.LENGTH - 1);
a_angle = 0;    //irandom(3);
a_type = EnCellTypes.NONE;
a_type_prev = EnCellTypes.NONE;

a_lock = 0;
a_selected = 0;
a_first = 0;
a_create_time = random_range(0.1,0.4);

a_inst_prev = noone;
a_inst_next = noone;

a_point_dist = sprite_get_width(sprite_index)/4;

a_destroy = 0;

a_next = false;
a_next_res[0] = 0;

a_next_delta = 0.01;

a_next_delta_scale = 1;//0.9;
a_next_scale_speed = (1 - a_next_delta_scale )/5;

a_next_delta_center = sprite_get_width(sprite_index)*(1 - 0.82)/2;
a_next_center_speed = a_next_delta_center/5;

a_next_dx = 0;
a_next_dy = 0;
a_next_scale = 0;

a_next_colour = irandom_range(1,EnColourTypes.LENGTH - 1);
a_next_dir = irandom(EnDirTypes.LENGTH - 1);
a_next_angle = irandom(3);
a_next_type = EnCellTypes.NONE;

a_next_p_shield     = 1;
a_next_p_turn_count = 1;
a_next_p_double_colour = EnColourTypes.RED;
a_next_p_mover_inst = noone;

// инициализация данных для возвращения действий.
alarm[0] = 1;

a_dx[0] = 1; a_dx[1] = 1; a_dx[2] =-1; a_dx[3] =-1;
a_dy[0] =-1; a_dy[1] = 1; a_dy[2] = 1; a_dy[3] =-1;

a_first_idle = 1;
a_back_die = 0;


///anim


// destroy

anim_res[0]         = false;
anim_point[0]       = 0;
anim_scale          = 1;
anim_angle          = 0;
anim_scale_speed    = 0.10;
anim_scale_delta    = 0.01;

//// анимация переворота -- lift
anim_l_state = 0;
anim_l_alpha = 1;
anim_l_alpha_speed = 0.1;
anim_l_dx_init = a_next_delta_center - 1;
anim_l_dy_init = a_next_delta_center - 1;
anim_l_dx = anim_l_dx_init;
anim_l_dy = anim_l_dy_init;

amin_reverse_dir = 0;


// подсказка первого хода
anim_f_delay_div = 0.1;
anim_f_delay = 0;
anim_f_delay_time = 0;
anim_f_dir = 0;
anim_f_alpha = 0;
anim_f_alpha_speed = 0.1;
anim_f_scale = 0.8;

// lock

anim_lock_alpha = 0;
anim_lock_alpha_speed = 0.3;
anim_lock_back = 0;

anim_state_prev_for_snd = 0;


///cell add params

a_p_shield = 1; // shield present

a_p_turn_count = 1; // time this cell undistractable

a_p_double_colour = EnColourTypes.RED; // to change to
a_p_double_alpha = 0;
a_p_double_speed = 0.2;

a_p_vortex_colour = EnColourTypes.EMPTY; // new color
a_p_vortex_alpha = 0;
a_p_vortex_speed = 0.2;

a_p_moving = false;
a_p_mover_inst = noone;

a_p_first_turn = 0;


a_move_id = -1;


script_execute(gui_scope_default,obj_a_parent,0,0,0,0);
