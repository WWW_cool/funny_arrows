/// @description  anim states

if(a_type != a_type_prev)
{
    if(a_type_prev == EnCellTypes.LOCK)
    {
        scr_a_lock(0);
    }
    a_type_prev = a_type;
    if(a_type == EnCellTypes.LOCK)
    {
        scr_a_lock(1);
    }
}

switch(a_state)
{
   case EnCellStates.CREATE:
        if(a_create_time > 0)
        {
            a_create_time -= delta_time/1000000;
        }
        else
        {
            a_next_scale = v_increment(a_next_res,a_next_scale,
            1,0.2,a_next_delta);
            
            scr_a_draw_cell();
            
            if(a_next_res[0])
            {
                a_state = EnCellStates.IDLE;
            }
        }
    break;
   case EnCellStates.IDLE:
        scr_a_draw_next_cell();
        
        a_next_scale = v_increment(a_next_res,a_next_scale,
        1,0.2,a_next_delta);
        
        scr_a_draw_cell();
        scr_a_lock_check();
    break; 
    case EnCellStates.SELECTED:
        a_next_scale = v_increment(a_next_res,a_next_scale,
            1,0.2,a_next_delta);
        scr_a_draw_next_cell();
        scr_a_draw_cell();
        draw_sprite_ext(scr_a_get_col_spr(a_colour),a_colour,x - a_next_dx,y - a_next_dy,a_next_scale,a_next_scale,0,c_gray,0.4);
    break;
    case EnCellStates.DESTROY:
        a_next_scale = v_path_anim(anim_res,a_next_scale,p_cell_destroy,
        anim_point,anim_scale_speed,anim_scale_delta);
        //draw_sprite_ext(scr_a_get_col_spr(a_colour),a_colour,x,y,anim_scale,anim_scale,0,c_white,anim_scale);
        scr_a_draw_cell();
        if(anim_res[0])
        {
            if(a_next)
            {
                var next_inst = instance_create(x,y,obj_a_parent);
                next_inst.a_colour  = a_next_colour;
                next_inst.a_dir     = a_next_dir;
                next_inst.a_angle   = a_next_angle;
                next_inst.a_type    = a_next_type;
                next_inst.sprite_index = scr_a_get_col_spr(a_next_colour);
                next_inst.a_next_scale = 0;
                next_inst.a_state = EnCellStates.IDLE;
                
                next_inst.a_p_shield        = a_next_p_shield;
                next_inst.a_p_turn_count    = a_next_p_turn_count;
                next_inst.a_p_double_colour = a_next_p_double_colour;
                next_inst.a_p_mover_inst    = a_next_p_mover_inst;
                
                if(instance_exists(a_next_p_mover_inst))
                {
                    a_next_p_mover_inst.m_inst = next_inst;
                }
                
                if(next_inst.a_type == EnCellTypes.LIFT)
                {
                    next_inst.a_type = EnCellTypes.NONE;
                }
            }
            scr_a_move_tile_destroy(id);
            if(a_type == EnCellTypes.LOCK)
            {
                scr_a_lock(0);
                with(obj_a_parent)
                {
                    if(a_type == EnCellTypes.LOCK && id != other.id)
                    {
                        scr_a_lock(1);
                    }
                }
            }
            //fed("tile destroy -- " + s(id));
            instance_destroy();
            if(!a_back_die)scr_glob_points_add();
        }
    break;
    case EnCellStates.ANIM:
        if(scr_a_draw_anim_check())
        {
            var res = scr_a_draw_anim();
            if(res)
            {
                if(global.a_hint_reverse)
                {
                    a_state = EnCellStates.REVERSE;
                }
                else
                {
                    a_state = EnCellStates.ANIM_END;
                }
            }
        }
        else
        {
            a_state = EnCellStates.IDLE;
        }
    break;
    case EnCellStates.REVERSE:
        if(global.a_hint_reverse)
        {
            scr_a_draw_cell_ext(0,a_type,x + anim_l_dx,y + anim_l_dy,1,0,1);
            scr_a_draw_cell_ext(1,a_next_type,x,y,1,0,1);
        }
        else
        {
            var res = scr_a_draw_anim();
            if(res)
            {
                a_state = EnCellStates.IDLE;
            }
        }
    break;
    case EnCellStates.ANIM_END:
        scr_a_anim_end();
        a_state = EnCellStates.IDLE;
        scr_a_draw_next_cell();
        scr_a_draw_cell();
    break;
}



