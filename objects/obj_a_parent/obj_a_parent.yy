{
    "id": "b50af0a4-6678-4621-a140-198f86a6df59",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_a_parent",
    "eventList": [
        {
            "id": "58c731a1-09d6-4eb4-9e93-944fc9aadcf8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b50af0a4-6678-4621-a140-198f86a6df59"
        },
        {
            "id": "7f15d38f-e176-4038-8bc3-e773e3195e11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b50af0a4-6678-4621-a140-198f86a6df59"
        },
        {
            "id": "62ff1fa1-26de-4711-96f8-d4dcf043d113",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b50af0a4-6678-4621-a140-198f86a6df59"
        },
        {
            "id": "f4242b0f-609b-4e7e-92c4-dd82ffc9bc40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "b50af0a4-6678-4621-a140-198f86a6df59"
        },
        {
            "id": "53b603e6-36ff-448b-9878-28ee56bdaedc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b50af0a4-6678-4621-a140-198f86a6df59"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3fceae30-b371-4d87-8d85-8099ae8fa3a7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b1f39454-6f12-4dc4-a638-5a309475b5f3",
    "visible": true
}