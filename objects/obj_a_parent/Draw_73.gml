/// @description selected line

switch(a_state)
{
    case EnCellStates.SELECTED:
        if(instance_exists(a_inst_next))
        {
           var _dir = point_direction(x,y,a_inst_next.x,a_inst_next.y);
           draw_sprite_ext(spr_cell_line,0,x,y,1,1,_dir,c_white,1); 
        }
        draw_sprite(spr_cell_point,0,x,y);
    break;
}

if(anim_state_prev_for_snd != a_state)
{
    if(anim_state_prev_for_snd == EnCellStates.SELECTED
    && a_state == EnCellStates.IDLE
    && !audio_is_playing(global.a_sound_click_off))
    {
        global.a_sound_click_off = scr_def_play_snd(snd_tile_click_off);
    }
    anim_state_prev_for_snd = a_state;
}

/*
if(a_type == EnCellTypes.LOCK)
{
    for(var i = 0; i < 4; i += 1)
    {
        draw_circle_colour(x + 1.1*sprite_get_width(sprite_index)*a_dx[i],
        y + 1.1*sprite_get_width(sprite_index)*a_dy[i],15,c_red,c_red,false);
    }
}
*/


/* */
/*  */
