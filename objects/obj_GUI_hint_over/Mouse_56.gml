/// @description  click event

if(h_need_event)
{
    h_wait_for_event = 1;
    instance_destroy();
}
else
{
    if(h_click_delay <= 0)
    {
        if(h_state == 2)
        {
            if(ds_exists(h_targets,ds_type_list))
            {
                if(ds_list_size(h_targets) > 0)
                {
                    h_targets_length = v_twink(h_text_res,h_text_state,h_targets_length,0,25,h_targets_speed);
                    for(var i = 0; i < ds_list_size(h_targets); i++)
                    {
                        var _arrow = h_targets[| i];
                        if(instance_exists(_arrow))
                        {
                            if(_arrow.state + 1 < _arrow.state_max)
                            {
                                _arrow.state += 1;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            h_state += 1;
            h_action = 0;
            h_time_start = h_time_start_init;
        }
        h_click_delay = h_click_delay_init;
    }
}


