/// @description init
startfinger_time = 1.2 // скорость прокрутки пальца помогатора)
num=0
finger_time = startfinger_time
finger_time1 = startfinger_time

h_state = 0;
h_wait_for_event = 0;
h_need_event = 0;

if(global._def_glb_var_default_resolution_pc)
{
    h_path_id = p_hint;
}
else
{
    h_path_id = p_hint_mob;
}

h_path_pos = 0;
h_path_speed_init = 4;
h_path_speed = 0.05;

h_time_start_init = 5;
h_time_start = h_time_start_init;
h_time_init = 0.5;
h_time = h_time_init;
h_time_flag = 1;

h_back = instance_create(1,1,obj_hint_back)
h_back.back_alpha = 0.55;
h_back.depth = -5;

h_action = 0;
h_text_scale = 1;
h_text_scale_speed = 0.15;
h_text_scale_max = 1.2;
h_text_scale_min = 0.8;
h_text_state[0] = true;
h_text_res[0] = false;

h_targets = ds_list_create();
h_targets_length = 0;
h_targets_speed = 0.3;

h_click_delay_init = 0.5;
h_click_delay = h_click_delay_init;



