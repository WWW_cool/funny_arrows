/// @description  draw hint overlay
if (live_call()) return live_result;

//num+=100000*current_time/delta_time/get_timer()




if(h_click_delay > 0)
{
    h_click_delay -= delta_time/1000000;
}

if(!h_wait_for_event)
{
    draw_set_font(f_text);
    draw_set_halign(fa_center);
    draw_set_valign(fa_middle);
    draw_set_colour(c_white);
    
    //draw hint
    switch(h_state)
    {
        case 0:
            var hint_x = __view_get( e__VW.XView, 0 ) + __view_get( e__VW.WView, 0 )/2;
            var hint_y = 0.2*room_height;
            draw_set_font(f_H2);
            h_text_scale = v_twink(h_text_res,h_text_state,h_text_scale,h_text_scale_min,h_text_scale_max,h_text_scale_speed);
            draw_text_transformed(hint_x,hint_y,string_hash_to_newline("Очистите поле!"),h_text_scale,h_text_scale,0);
            global.a_hint_mouse_block = 1;
        break; 
        case 1:
            var hint_x = __view_get( e__VW.XView, 0 ) + __view_get( e__VW.WView, 0 )/2;
            var hint_y = 0.2*room_height;
            global.a_hint_mouse_block = 0;
            if(!h_action)
            {
                h_action = 1;
                for(var i = 0; i < path_get_number(h_path_id); i++)
                {
                    var _x = path_get_point_x(h_path_id,i);
                    var _y = path_get_point_y(h_path_id,i);
                    var _inst = collision_point(_x,_y,obj_a_parent,false,true);
                    if(instance_exists(_inst))
                    {
                        _inst.depth = -10;
                    }
                }
            }
            draw_set_font(f_H2);
            h_text_scale = v_twink(h_text_res,h_text_state,h_text_scale,h_text_scale_min,h_text_scale_max,h_text_scale_speed);
            draw_text_transformed(hint_x,hint_y,string_hash_to_newline("Нажмите и ведите!"),h_text_scale,h_text_scale,0);
            draw_set_font(f_text);
            h_path_speed = h_path_speed_init/path_get_length(h_path_id);
            
            hint_x = path_get_x(h_path_id,h_path_pos);
            hint_y = path_get_y(h_path_id,h_path_pos);


draw_text(50,10,current_time)
draw_text(50,20,get_timer())
draw_text(50,30,delta_time)
draw_text(50,40,num)


			if finger_time > 0
			{
				finger_time-=delta_time/1000000
				draw_sprite(spr_hint_finger,0,hint_x,hint_y+56);
			}else
			{
				if h_path_pos < 0.90
				{
					h_path_pos += h_path_speed*delta_time/10000
					h_path_pos %= 1;
				
					draw_sprite(spr_hint_finger1,0,hint_x,hint_y+56);
					h_need_event = 1;
					if h_path_pos = 0
						{
							finger_time = startfinger_time
						}
				}else
				{
					draw_sprite(spr_hint_finger,0,hint_x,hint_y+56);
					if finger_time1 > 0
					{
						finger_time1-=delta_time/1000000
					}else
					{
						finger_time1 = startfinger_time
						h_path_pos = 0
						finger_time = startfinger_time
					}
				}
				
			}
        break;
        case 2: // show restart or move back choise
            h_need_event = 1;
            if(ds_exists(h_targets,ds_type_list))
            {
                if(ds_list_size(h_targets) > 0)
                {
                    h_targets_length = v_twink(h_text_res,h_text_state,h_targets_length,0,25,h_targets_speed);
                    for(var i = 0; i < ds_list_size(h_targets); i++)
                    {
                        var _arrow = h_targets[| i];
                        if(instance_exists(_arrow))
                        {
                            if(_arrow.target_btn >= 0)v_btns_draw_btn(_arrow.target_btn);
                            var _x = _arrow.x + lengthdir_x(h_targets_length,_arrow.image_angle + 180);
                            var _y = _arrow.y + lengthdir_y(h_targets_length,_arrow.image_angle + 180);
                            if(is_array(_arrow.target_text))
                            {
                                if(_arrow.state < array_length_1d(_arrow.target_text))
                                {
                                    var _text = _arrow.target_text[_arrow.state];
                                    var _str_h = (string_height(string_hash_to_newline(_text)) div 30);
                                    if(!_str_h){_str_h = 50;}else{_str_h = (_str_h + 1)*30;}
                                    
                                    var _w = sprite_get_width(_arrow.sprite_index) + _str_h;
                                    var _text_x = _arrow.x + lengthdir_x(_w,_arrow.image_angle + 180);
                                    var _text_y = _arrow.y + lengthdir_y(_w,_arrow.image_angle + 180);
                                    v_draw_text(_text_x,_text_y,_text,
                                    _arrow.target_text_halign[_arrow.state],fa_middle,EnText.H2);
                                }
                            }
                            draw_sprite_ext(_arrow.sprite_index,0,_x,_y,1,1,_arrow.image_angle,c_white,1);
                            
                            if(path_exists(_arrow.tile_path))
                            {
                                scr_hint_show_on_path(_arrow.tile_path);
                            }
                            if(h_need_event)
                            {
                                if(_arrow.state + 1 < _arrow.state_max)
                                {
                                    h_need_event = 0;
                                }
                            }
                        }
                    }
                }
            }
        break;
        default:
            h_need_event = 1;
        break;
    }
    
    // draw click to continue
    h_time_start -= delta_time/1000000;
    draw_set_halign(fa_center);
    draw_set_valign(fa_middle);
    draw_set_font(f_text);
    if(h_time_start <= 0)
    {
        h_time -= delta_time/1000000;
        if(h_time > 0)
        {
            if(h_time_flag)
            {
                draw_text(__view_get( e__VW.XView, 0 ) + __view_get( e__VW.WView, 0 )/2,__view_get( e__VW.YView, 0 ) + 0.8*__view_get( e__VW.HView, 0 ),string_hash_to_newline("Нажмите, чтобы продолжить!"));
            }
        }
        else
        {
            h_time = h_time_init;
            h_time_flag = !h_time_flag;
        }
    }
    else
    {
        draw_text(__view_get( e__VW.XView, 0 ) + __view_get( e__VW.WView, 0 )/2,__view_get( e__VW.YView, 0 ) + 0.8*__view_get( e__VW.HView, 0 ),string_hash_to_newline("Нажмите, чтобы продолжить!"));
    }
}






