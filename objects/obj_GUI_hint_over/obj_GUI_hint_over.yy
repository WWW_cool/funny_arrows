{
    "id": "d7862cc8-830f-47fa-a95a-c5a389a72ea8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_hint_over",
    "eventList": [
        {
            "id": "345e22ad-c791-452f-b293-4a2153fccef7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d7862cc8-830f-47fa-a95a-c5a389a72ea8"
        },
        {
            "id": "b8201347-a58a-483a-b090-6561e00bd07c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d7862cc8-830f-47fa-a95a-c5a389a72ea8"
        },
        {
            "id": "d0a9c9d6-829a-470d-afbc-babae09e1227",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "d7862cc8-830f-47fa-a95a-c5a389a72ea8"
        },
        {
            "id": "46e98cfd-5636-44e4-99dd-54de58740862",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "d7862cc8-830f-47fa-a95a-c5a389a72ea8"
        },
        {
            "id": "ff2d4b09-a3f8-4b3f-ae81-b828d3e6b32e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d7862cc8-830f-47fa-a95a-c5a389a72ea8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}