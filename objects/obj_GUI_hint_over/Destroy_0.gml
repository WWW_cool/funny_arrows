if(instance_exists(h_back))
{
    h_back.back_destroy = 1;
}

if(ds_exists(h_targets,ds_type_list))
{
    if(ds_list_size(h_targets) > 0)
    {
        for(var i = 0; i < ds_list_size(h_targets); i++)
        {
            var _arrow = h_targets[| i];
            if(instance_exists(_arrow))
            {
                with(_arrow)
                {
                    instance_destroy();
                }
            }
        }
    }
    ds_list_destroy(h_targets);
}


