/// @description  mem free

var list = global.lvlObjList;

if(ds_exists(list,ds_type_list))
{
    ds_list_destroy(list);
}

__view_set( e__VW.WView, global._def_glb_var_view_current, old_view_wview );
__view_set( e__VW.WPort, global._def_glb_var_view_current, old_view_wport );
__view_set( e__VW.HView, global._def_glb_var_view_current, old_view_hview );
__view_set( e__VW.HPort, global._def_glb_var_view_current, old_view_hport );


surface_resize(application_surface,
__view_get( e__VW.WPort, global._def_glb_var_view_current ),
__view_get( e__VW.HPort, global._def_glb_var_view_current ));

window_set_size(__view_get( e__VW.WPort, global._def_glb_var_view_current ),
__view_get( e__VW.HPort, global._def_glb_var_view_current ));


