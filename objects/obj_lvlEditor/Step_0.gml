/// @description GUI step

// Updated items
gui_set_value(guid, "FPS", fps);
gui_scope_update(0);

// Interact with gui
gui_check(guid, __view_get( e__VW.WView, 0 )-243, 0, mouse_x-__view_get( e__VW.XView, 0 ), mouse_y-__view_get( e__VW.YView, 0 ));

// Update values
gui_scope_update(1);
//grid = gui_get_value(guid,"GRID",grid);
grid_show = gui_get_value(guid,"Show GRID",grid_show);
layer = gui_get_value(guid,"Layer",layer);
debug_show = gui_get_value(guid,"Show debug",debug_show);


/// move all units

var _grid = grid;
/***************************************************
  move all inst on stage
 ***************************************************/
if(keyboard_check(vk_up))
{
    with(obj_in_game)
    {
        scope_vars[3,1] -= _grid;
    }
}
if(keyboard_check(vk_down))
{
    with(obj_in_game)
    {
        scope_vars[3,1] += _grid;
    }
}

if(keyboard_check(vk_left))
{
    with(obj_in_game)
    {
        scope_vars[2,1] -= _grid;
    }
}
if(keyboard_check(vk_right))
{
    with(obj_in_game)
    {
        scope_vars[2,1] += _grid;
    }
}
/***************************************************
  choose function file
 ***************************************************/
 
if(keyboard_check_pressed(ord("S")))
{
    if(is_array(file_list))
    {
        file_index = clamp(file_index + 1,0,array_length_1d(file_list) - 1);
    }
}

if(keyboard_check_pressed(ord("W")))
{
    if(is_array(file_list))
    {
        file_index = clamp(file_index - 1,0,array_length_1d(file_list) - 1);
    }
}




/* */
/*  */
