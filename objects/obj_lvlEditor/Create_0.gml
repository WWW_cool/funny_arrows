/// @description  create gui menu
lvl_name = "0";
lvl_num = real(lvl_name);
GUI_width = 243;
GUI_height = 250;

global.lvlObjList = noone;

scope_inst = noone;
grid = 36;
grid_show = 0;
debug_show = 1;
layer = 0;

guid = gui_create_gui();
gui_create_scope_header(guid);

old_view_wview = global._def_glb_var_view_wview;
old_view_wport = global._def_glb_var_view_wport;
old_view_hview = global._def_glb_var_view_hview;
old_view_hport = global._def_glb_var_view_hport;


__view_set( e__VW.WView, global._def_glb_var_view_current, old_view_wview + GUI_width );
__view_set( e__VW.WPort, global._def_glb_var_view_current, old_view_wport + GUI_width );
__view_set( e__VW.HView, global._def_glb_var_view_current, old_view_hview + GUI_height );
__view_set( e__VW.HPort, global._def_glb_var_view_current, old_view_hport + GUI_height );


surface_resize(application_surface,
__view_get( e__VW.WPort, global._def_glb_var_view_current ),
__view_get( e__VW.HPort, global._def_glb_var_view_current ));

window_set_size(__view_get( e__VW.WPort, global._def_glb_var_view_current ),
__view_get( e__VW.HPort, global._def_glb_var_view_current ));

gui_w = window_get_width();
gui_h = window_get_height();

file_strings[0] = "";

file = file_find_first("*.func", 0);
var _ind = 0;
file_list = 0;
file_index = -1;
use_file_name = "";
while(file != "")
{
    file_list[_ind++] = file;
    file = file_find_next();
}

if(_ind == 0)
{
    fed( "lvlEditor | no file founded...");
}




