/// @description draw GUI

draw_set_font(f_gGUI);
draw_set_halign(fa_left);
draw_set_valign(fa_middle);
draw_set_colour(c_white);
draw_rectangle(0,0,global._def_glb_var_default_resolution_w,
global._def_glb_var_default_resolution_h,true);
draw_line(global._def_glb_var_default_resolution_w/2,0,
global._def_glb_var_default_resolution_w/2,global._def_glb_var_default_resolution_h);
draw_line(0,global._def_glb_var_default_resolution_h/2,
global._def_glb_var_default_resolution_w,global._def_glb_var_default_resolution_h/2);

if(grid_show)
{
    draw_set_colour(c_white);
    var h_line_count = 1 + __view_get( e__VW.HView, 0 )/grid div 1;
    var v_line_count = 1 + __view_get( e__VW.WView, 0 )/grid div 1;
    
    for(var i = 0; i < h_line_count; i += 1)
    {
        draw_line(0,i*grid,__view_get( e__VW.WView, 0 ),i*grid);
    }
    
    for(var i = 0; i < v_line_count; i += 1)
    {
        draw_line(i*grid,0,i*grid,__view_get( e__VW.HView, 0 ));
    }
}
if(gui_w != window_get_width() || gui_h != window_get_height())
{
    gui_w = window_get_width();
    gui_h = window_get_height();
    display_set_gui_size(__view_get( e__VW.WView, 0 ), __view_get( e__VW.HView, 0 ));
}
gui_draw(guid, __view_get( e__VW.WView, 0 ) - 243, 0);

if(instance_exists(scope_inst))
{
    with(scope_inst)
    {
        //draw_sprite_ext(sprite_index,0,x,y,1,1,image_angle,c_yellow,0.6);
        var x1 = x - sprite_get_width(sprite_index)/2;
        var x2 = x + sprite_get_width(sprite_index)/2;
        var y1 = y - sprite_get_height(sprite_index)/2;
        var y2 = y + sprite_get_height(sprite_index)/2;
        draw_rectangle_colour(x1,y1,x2,y2,c_red,c_red,c_red,c_red,true);
    }
}

if(debug_show)
{
    scr_level_show(50,50);
}



