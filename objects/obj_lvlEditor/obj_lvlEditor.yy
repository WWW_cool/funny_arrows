{
    "id": "ffe94eaa-a4d6-49e2-b651-08f707eede0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lvlEditor",
    "eventList": [
        {
            "id": "2213de7e-4c34-4018-b039-b7cf4a7e3e2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ffe94eaa-a4d6-49e2-b651-08f707eede0c"
        },
        {
            "id": "de146617-ad1c-464c-85bc-38891ce931cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "ffe94eaa-a4d6-49e2-b651-08f707eede0c"
        },
        {
            "id": "91315f19-3fcb-4744-9bd7-0440186d31fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ffe94eaa-a4d6-49e2-b651-08f707eede0c"
        },
        {
            "id": "55692224-925b-4825-ae02-88d3ead4dfcd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "ffe94eaa-a4d6-49e2-b651-08f707eede0c"
        },
        {
            "id": "78877178-75a3-41d2-8bff-c11e81bccae8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ffe94eaa-a4d6-49e2-b651-08f707eede0c"
        },
        {
            "id": "00cffe5f-e98e-49a7-904b-61f5a20db767",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 116,
            "eventtype": 9,
            "m_owner": "ffe94eaa-a4d6-49e2-b651-08f707eede0c"
        },
        {
            "id": "fb03b135-caa0-4318-b960-509fa83cb906",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 46,
            "eventtype": 9,
            "m_owner": "ffe94eaa-a4d6-49e2-b651-08f707eede0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}