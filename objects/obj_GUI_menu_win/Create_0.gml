/// @description  init
var ind = 0;
OPT_btn_spr = spr_menu_btn;
OPT_back = spr_menu_bg;
OPT_btn_font = f_text;
OPT_need_close = 0;
var OPT_btn_spr_tmp = OPT_btn_spr;
var OPT_back_tmp = OPT_back;
var OPT_btn_font_tmp = OPT_btn_font;

x = __view_get( e__VW.WView, 0 )/2 + __view_get( e__VW.XView, 0 );
y = __view_get( e__VW.HView, 0 )/2 + __view_get( e__VW.YView, 0 );;
OPT_y_tmp = y + sprite_get_height(OPT_back) - sprite_get_height(OPT_btn_spr) - 80;

glob = noone;
player_lvl = 0;
lvl_type = -1;
if(instance_exists(global.inst_glb))
{
    glob = global.inst_glb;
}

OPT_controls_type[ind++] = v_btns_add(0,0,spr_menu_btn, id,0, 
              NO_SCRIPT,
              NO_SCRIPT,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
              
v_btns_set_text(OPT_controls_type[ind - 1],0,-2,
"Следующий",1,c_black,0,f_H2,f_H2,1);

OPT_controls_type[ind++] = v_btns_add(0,0,spr_menu_btn, id,0, 
              NO_SCRIPT,
              NO_SCRIPT,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
v_btns_set_text(OPT_controls_type[ind - 1],0,-2,
"Репост",1,c_black,0,f_H2,f_H2,1);                        
ind = 0;        
OPT_controls_x[ind] = x;
OPT_controls_y[ind++] = y - 35;  
OPT_controls_x[ind] = x;
OPT_controls_y[ind++] = y + 35;
//////////////////////////////////////////////////////////////////////////////////////////
event_inherited();
//////////////////////////////////////////////////////////////////////////////////////////

OPT_btn_spr = OPT_btn_spr_tmp;
OPT_back = OPT_back_tmp;
OPT_btn_font = OPT_btn_font_tmp;




/// game logic

award_star_count = 3;
award_star_inst = noone;
/*
for(var i = 1; i < 4; i += 1)
{
    if(instance_exists(glob))
    {
        with(glob)
        {
            if(game_time >= game_time_need[game_lvl,i - 1])
            {
                other.award_star_count = 3 - i;
                break;
            }
        }
    }
}*/
award_star_count_save = award_star_count;

star_count          = 1;
anim_res[0]         = false;
anim_point[0]       = 0;
anim_scale          = 0;
anim_scale_speed    = 0.14;
anim_scale_delta    = 0.01;


add_res[0]          = false;
add_is              = 0;
add_start           = 0;
add_inst            = noone;
add_delay_start     = 0.5;
add_delay_end       = 0.5;
if(instance_exists(glob))
{
    /*if(award_star_count_save > glob.game_star_count[glob.game_lvl])
    {
        glob.game_star_count[glob.game_lvl] = award_star_count_save;
        glob.game_win = 1;
    }*/
    if(os_browser == browser_not_a_browser)
    {
        if(global.user_lvl_ok < glob.game_lvl && !global.user_open_all)
        {
            global.user_lvl_ok = glob.game_lvl;
            add_is = 1;
        }
    }
    else
    {
        if(global.user_lvl_ok <= glob.game_lvl && !global.user_open_all)
        {
            global.user_lvl_ok = glob.game_lvl;
            add_is = 1;
        }
    }
}


award_msg = noone;//irandom(sprite_get_number(spr_win_msg) - 1);
scr_def_play_snd(snd_lvl_victory);




/* */
/*  */
