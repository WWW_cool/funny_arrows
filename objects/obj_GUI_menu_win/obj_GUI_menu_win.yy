{
    "id": "a017f78e-b3b7-4ff7-a7a5-ff0128a97082",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_menu_win",
    "eventList": [
        {
            "id": "ab8005e9-979c-4eda-8794-886a57a03411",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a017f78e-b3b7-4ff7-a7a5-ff0128a97082"
        },
        {
            "id": "751ffeb6-a261-478c-83c3-843316ac7b75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a017f78e-b3b7-4ff7-a7a5-ff0128a97082"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0f12a9c8-0859-422b-865b-2665b2ddf681",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}