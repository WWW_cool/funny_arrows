{
    "id": "b63ca112-f77b-469b-8f18-b8e192e3745d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_efx_shield",
    "eventList": [
        {
            "id": "f77e7c86-db1b-49e3-a5a1-ea062dba7062",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b63ca112-f77b-469b-8f18-b8e192e3745d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6fc41001-0fc1-4de0-95a9-bf904d16ba79",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "822a2c81-b3cd-4f69-bd00-fed9495f3f2f",
    "visible": true
}