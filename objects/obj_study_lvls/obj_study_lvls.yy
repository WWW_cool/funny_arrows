{
    "id": "1e0df373-fbdc-4e12-8cb3-ccb9d90b1a27",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_study_lvls",
    "eventList": [
        {
            "id": "70243626-8aa2-4ffb-86c2-c4354421ad96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1e0df373-fbdc-4e12-8cb3-ccb9d90b1a27"
        },
        {
            "id": "73eff378-898a-442d-adbb-0a62ccafe273",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1e0df373-fbdc-4e12-8cb3-ccb9d90b1a27"
        },
        {
            "id": "bcff37b0-265e-4108-b9e1-995b946f50f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1e0df373-fbdc-4e12-8cb3-ccb9d90b1a27"
        },
        {
            "id": "319a6df3-e3e2-4386-b095-ca9b61ee4787",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "1e0df373-fbdc-4e12-8cb3-ccb9d90b1a27"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}