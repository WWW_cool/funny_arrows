/// @description  move

//fed("view_xview = " + s(view_xview));

if(!st_destroy)
{
    if(st_level != st_level_prev)
    {
        st_ismove = true;
        var val_to = st_start_x + st_level*st_x_dist;
        var _dx = v_lerp(a_res,__view_get( e__VW.XView, 0 ),val_to,st_x_speed,5);
        v_btns_move(btn_back,_dx - __view_get( e__VW.XView, 0 ),0);
        __view_set( e__VW.XView, 0, _dx );
        if(a_res[0])
        {
            scr_study_init(st_level,st_level_prev);
            st_level_prev = st_level;
            st_ismove = false;
            if(st_target_type != -1)
            {
                st_hint_number = st_target_type;
                st_target_type = -1
                st_level++;
            }
        }
    }
}
else
{
    if(!st_destroy_start)
    {
        st_destroy_start = 1;
        scr_study_deinit();
        ds_list_destroy(st_objects);
        if(st_pause_init)
        {
            global.a_study_inst = noone;
            scr_def_g_pause();
        }
    }
    st_ismove = true;
    __view_set( e__VW.XView, 0, v_lerp(a_res,__view_get( e__VW.XView, 0 ),st_start_x,st_x_speed,5 ));
    if(a_res[0])
    {
        instance_destroy();
        st_ismove = false;
    }
}




