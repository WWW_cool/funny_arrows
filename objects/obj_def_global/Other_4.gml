/// @description reInit game vars
//scr_JS_barHide();
if(_def_glb_first_start)
{
    _def_glb_first_start = 0;
    scr_def_view_init();
}
scr_def_view_update();
randomize();
//show_debug_overlay(true);
//////////////////////////////////////////////////////////
/////////////////// Message INFO /////////////////////////
//////////////////////////////////////////////////////////
global._def_glb_var_pause = false;
_def_glb_show_msg_win = 0;
_def_glb_show_msg_lose = 0;
global.a_hint_first_turn = 0;
_def_glb_lvl_start_delay = 1;



/// game logic init event

if(room != rm_Start)
{
    scr_gui_life(-1);
    scr_g_save();
}

randomize();
scr_a_rnd_col_spr();
game_lvl = -1;

if(os_browser == browser_not_a_browser)
{
    if(room == rm_Start)
    {
        game_lvl_to_load = game_lvl_ok;
    }
    else
    {
        if(game_lvl_to_load && room == rm_load)
        {
            if(scr_level_get_count() >= game_lvl_to_load)
            {
                scr_level_load_selected(game_lvl_to_load);
                game_lvl  = game_lvl_to_load;
                if(instance_exists(global.a_selector))
                {
                    fed("lvl start #" + s(game_lvl - 1));
                    scr_create_header("Уровень " + s(game_lvl));
                    scr_audio_rnd(snd_lvl_begin_1,snd_lvl_begin_2);
                    //global.a_selector.s_attempts = game_attempts[game_lvl];
                }
            }
            if(game_lvl_to_load == 1)
            {
                var hint = instance_create(1,1,obj_GUI_hint_over);
            }
        }
        else
        {
            if(room > rm_lvlCreate)
            {
                game_lvl = room - rm_lvlCreate;
                scr_audio_rnd(snd_lvl_begin_1,snd_lvl_begin_2);
                var inst = scr_create_header("Уровень " + s(game_lvl));
                inst.h_diff = scr_lvl_get_diff(game_lvl);
                if(game_lvl == 1)
                {
                    var hint = instance_create(1,1,obj_GUI_hint_over);
                }
            }
        }
    }
    if(room == rm_boss)
    {
        scr_create_header("БОСС");
    } 
}
else
{
    /*if(room > rm_lvls)
    {
        game_lvl = room - rm_alvl_1;//rm_lvl_2;
        if(!((game_lvl + 1)%10))
        {
            JS_vk_invite();
        }
    }*/
    if(room != rm_Start)global.v_db_read_first = 0;
    if(room > rm_lvlCreate)
    {
        game_lvl = room - rm_lvlCreate;
        //fed("game_lvl = " + s(game_lvl));
        scr_audio_rnd(snd_lvl_begin_1,snd_lvl_begin_2);
        var inst = scr_create_header("Уровень " + s(game_lvl));
        inst.h_diff = scr_lvl_get_diff(game_lvl);
        if(game_lvl == 1)
        {
            var hint = instance_create(1,1,obj_GUI_hint_over);
        }
    }
    /*if(room == rm_Start)
    {
        if(global.user_lvl > 0)
        {
            instance_create(1,1,obj_GUI_menu_API);
        }
    }*/
}


var _rnd = irandom(4);
var _bg,_vs,_hs;
switch(_rnd)
{
    case 0:
        _bg = bg_type_1;
        _vs = 1;
        _hs = 1;
    break;
    case 1:
        _bg = bg_type_2;
        _vs = 0;
        _hs = 1;
    break;
    case 2:
        _bg = bg_type_3;
        _vs = 1;
        _hs = 0;
    break;
    case 3:
        _bg = bg_type_4;
        _vs = -1;
        _hs = -1;
    break;
    default:
        _bg = bg_type_5;
        _vs = 1;
        _hs = -1;
}
__background_set( e__BG.Index, 0, _bg );
__background_set( e__BG.VSpeed, 0, _vs );
__background_set( e__BG.HSpeed, 0, _hs );

global.a_hint_mouse_block = 0;
game_lose = 0;
win_time = 0;
game_time = 0;
game_award_lvl = 0;
game_current_lvl_points = 0;

var need_study = 0;

if(game_lvl_prev != game_lvl)
{
    game_lvl_prev = game_lvl;
    game_need_repost++;
}

if(STUDY_ADD)
{
    if(game_study_lvl <= game_lvl)
    {
        if(is_array(global.a_st_unlock_lvl))
        {
            for(var i = 0; i < array_length_1d(global.a_st_unlock_lvl); i++)
            {
                if(global.a_st_unlock_lvl[i] == game_lvl && game_lvl != 1)
                {
                    game_study_type = i;
                    break;
                }
            }
        }
        game_study_lvl = game_lvl + 1;
        v_a_event("new level","level - " + s(game_lvl),"started");
    }
}

scr_check_reverse_need();



/* */
/// analytics

room_time = get_timer()/1000;





/* */
/*  */
