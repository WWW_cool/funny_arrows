/// @description  mem free

if(sprite_exists(global._def_glb_var_pause_sprite))
{
    sprite_delete(global._def_glb_var_pause_sprite);
}

if(surface_exists(global._tmp_surf))
{
    surface_free(global._tmp_surf);
}   

if(font_exists(global.fnt_spr_hp_xp))
{
    font_delete(global.fnt_spr_hp_xp);
}

if(font_exists(global.fnt_spr_lvl))
{
    font_delete(global.fnt_spr_lvl);
}

if(font_exists(global.fnt_spr_wave))
{
    font_delete(global.fnt_spr_wave);
}

if(font_exists(global.fnt_spr_crit))
{
    font_delete(global.fnt_spr_crit);
}

if(font_exists(global.fnt_spr_money))
{
    font_delete(global.fnt_spr_money);
}

if(room == rm_Start && global.user_need_reset)scr_g_save();


/// analytics

var tick = get_timer()/1000 - room_time;
var name = room_get_name(room);

if(room != rm_Start)
{
    v_a_time(name,"passed",tick);
}


