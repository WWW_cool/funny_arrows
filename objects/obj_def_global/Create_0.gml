/// @description param init

scr_def_glob_var_init();


//////////DEBUG OBJECT
if(EnMSGType.enDEBUG)
{
    if(!instance_exists(global._def_glb_var_obj_debug))
    {
        instance_create(1,1,global._def_glb_var_obj_debug);
    }
}
_def_glb_first_start = 1;
_def_glb_current_level = 0;
_def_glb_state = "noone"; // "retry" "next_lvl" "next_lvl_with_ads" 

_def_glb_show_msg_win = 0;
_def_glb_show_msg_lose = 0; 
_def_glb_lvl_start_delay = 0;
//ini_open(global._def_glb_var_save_ini);
//global._def_glb_var_audio_on = ini_read_real("save","audio",1);
//ini_close();

audio_master_gain(global._def_glb_var_audio_gain);
/*
if(global._def_glb_var_audio_on)
{
    if(audio_exists(global._def_glb_var_audio_back))
    {
        audio_play_sound(global._def_glb_var_audio_back,0,1);
    }
}
*/
scr_def_show_debug_message("_def_global INIT (sucsess)","info");

room_time = 0;


/* */
/// db init 

glb_db_req_list = ds_list_create();

var ind = 0;
glb_user_xp[ind++] = 0;
glb_user_xp[ind++] = 10;
glb_user_xp[ind++] = 20;
glb_user_xp[ind++] = 50;
glb_user_xp[ind++] = 75;

global.user_lvl_amounts = glb_user_xp;

ind = 0;
JS_vk_loader();
glb_db_keys[ind++] = "id";
glb_db_keys[ind++] = "all_ok";
glb_db_keys[ind++] = "lvl_ok";
glb_db_keys[ind++] = "hint_fstep";
glb_db_keys[ind++] = "hint_back";
glb_db_keys[ind++] = "live_count";
glb_db_keys[ind++] = "live_time";
glb_db_keys[ind++] = "price_time";
glb_db_keys[ind++] = "game_time";
glb_db_keys[ind++] = "game_points";
glb_db_keys[ind++] = "game_lvl_try";
glb_db_keys[ind++] = "sound";
glb_db_keys[ind++] = "music";
glb_db_keys[ind++] = "reserve_1";
glb_db_keys[ind++] = "reserve_2";
glb_db_keys[ind++] = "reserve_3";
glb_db_keys[ind++] = "reserve_4";

enum EnDbKeys{  ID,ALL_OK,LVL_OK,HINT_FSTEP,HINT_BACK,LIVE_COUNT,LIVE_TIME,
                PRICE_TIME,GAME_TIME,GAME_POINTS,GAME_LVL_TRY,SOUND,
                MUSIC,REV_1,REV_2,REV_3,REV_4,LENGTH};

for(var i = 0; i < array_length_1d(glb_db_keys); i += 1)
{
    glb_db_key_update[i] = 0;
    glb_db_key_val[i] = 0;
}


var user_str = JS_vk_get_param("viewer_id");
if(is_string(user_str))
{
    game_user_id = user_str;
    fed("viewer_id = " + user_str);
    JS_vk_settings();
    JS_vk_AdInit(user_str);
}
else
{
    fed("play demo");
    game_user_id = "12578423"; // my vk
}








/* */
/// общие для комнат параметры - они же поддерживают сохранение загрузку
//todo callback from API for what leng
//scr_update_localization("en");
game_leng = "en";
//JS_getTxt(working_directory + "Levels.txt");
//alarm[0] = 1;

price_time = 0;
price_time_need = 360;

live_count_max = 5;
live_count = 5;
live_time = 0;
live_time_need = 360;

hint_fstep_count = 3;
hint_moveback_count = 4;

win_d_time = 0;
win_d_time_max = 0.4;
win_time = 0;
win_time_need = 2;

game_lvl_to_load = 0;
game_lvl  = 0;
game_lvl_prev  = 0;
game_time = 0;
game_award_lvl = 0;

game_lvl_ok = 0;

game_win = 0;
game_lose = 0;
game_random = 0;

enum EnGameType{simple = 0,sprint = 1}; 

room_hint = "";

if(os_browser == browser_not_a_browser)
{
    ini_open("saves");
    game_lvl_ok = ini_read_real("game","lvl_ok",0);
    ini_close();
}

game_boss_index = 0;
game_boss_max = 5;
game_boss_lvl_count = 20;
game_boss_next = 0;
for(var i = 0; i < 6; i += 1)
{
    game_boss_lock[i] = 1;
}
game_boss_lock[0] = 0;
game_points = 0;
game_current_lvl_try = 0;
game_current_lvl_points = 0;
game_points_per_tile = 20;

game_study_type = -1;
game_study_lvl = -1;
game_study_delay_init = 0.7;
game_study_delay = game_study_delay_init;

game_hint_reverse = 0;
game_hint_study = 0;
game_hint_fstep = 0;

audio_bg_pause_first = 1;
audio_bg_pause_init = 25;
audio_bg_pause_chance = 0.15;
audio_bg_pause = 0;
audio_bg_pause_was = 0;
audio_bg_pause_double = 0;

game_time_price = 0;
game_need_repost = 0;
game_time_adv = price_time_need/2;

v_a_event("Game start","system","started");

ads_timeout_init = 5;
ads_timeout = ads_timeout_init;


/* */
/// site lock

var ind = 0;;
s_lock[ind++] = "vk.com";
s_lock[ind++] = "naogames.ru";
s_lock[ind++] = "127.0.0.1";

if(os_browser != browser_not_a_browser)
{
    var _str = url_get_domain();
    var _res = 0;
    for(var i = 0; i < array_length_1d(s_lock); i++)
    {
        if(_str == s_lock[i])
        {
            _res = 1;
        }
    }
    if(!_res)
    {
        fed("site lock");
        room_goto(rm_lock);
    }
}



/* */
/*  */
