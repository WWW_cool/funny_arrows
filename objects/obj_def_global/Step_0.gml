with (obj_def_global) {
///view check

if(os_browser != browser_not_a_browser)
{
    if(scr_def_view_check())
    {
        //fed("view changed...");
        scr_def_view_init();
    }
}


}
///timers
/*
if(JS_vk_get_status())
{
    JS_vk_reset_status();
    v_db_read(game_user_id);
}

scr_db_user_lvl(EnUserLvlCmd.CHECK_AMOUNT);
*/

if(_def_glb_lvl_start_delay > 0)
{
    _def_glb_lvl_start_delay -= delta_time/1000000;
}

if(win_time == 0)
{
    /*if(instance_number(obj_w_puzzle) == 1)
    {
        win_time = 2;
    }*/
    //win_time = -1;
}
else
{
    if(win_time > 0)
    {
        //win_d_time += delta_time/1000000;
        win_time -= delta_time/1000000;
        /*if(win_d_time >= win_d_time_max)
        {
            win_d_time = 0;
            repeat(irandom_range(1,4))
            {
                instance_create(1,1,obj_firework);
            }
        }*/
        if(win_time < 0)
        {
            win_time = -1;
            scr_def_rm_goto_next();
            //instance_create(1,1,obj_GUI_menu_win);
        }
    }
}


if(price_time)
{
    price_time -= delta_time/1000000;
    if(price_time < 0)
    {
        price_time = 0;
    }
}

if(live_count < live_count_max)
{
    live_time += delta_time/1000000;
    if(live_time >= live_time_need)
    {
        live_time = 0;
        scr_gui_price_efx(EnPriceType.Life_clock,1);
    }
}
else
{
    live_time = 0;
}

if(game_study_type != -1)
{
    // если уже перешли на обучение - отменяем
    if(instance_number(obj_study_lvls) > 0)
    {
        game_study_type = -1;
    }
    // если уже начали играть - отменяем
    for(var i = 0; i < instance_number(obj_a_parent); i += 1)
    {
        var inst = instance_find(obj_a_parent,i);
        if(instance_exists(inst))
        {
            if(inst.a_state == EnCellStates.SELECTED)
            {
                game_study_type = -1;
                break;
            }
        }
    }
    // если закончились оверлей и хедер
    if(instance_number(obj_GUI_hint_over) == 0
        && instance_number(obj_GUI_header) == 0)
    {
        game_study_delay -= delta_time/1000000;
        if(game_study_delay <= 0)
        {
            var inst = scr_s_msg(EnMsgTypes.Study);
            inst.sh_study_index = game_study_type;
            game_study_type = -1;
        }
    }
    else
    {
        game_study_delay = game_study_delay_init;
    }
}


/*
if(!instance_exists(global.lvlEdit))
{    
    if(!global._def_glb_var_pause)
    {    
        game_time += delta_time/1000000;
        if(game_time >= game_time_need[game_lvl,0])
        {
            game_award_lvl = 1;
        }
        if(game_time >= game_time_need[game_lvl,1])
        {
            game_award_lvl = 2;
        }
        if(game_time >= game_time_need[game_lvl,2])
        {
            game_time = 0;
            if(instance_number(obj_tile_parent) > 0)
            {
                instance_create(1,1,obj_GUI_menu_lose);
            }
        }
    }
}
*/

/* */
/// audio

if((audio_sound_get_track_position(global.a_sound_bg) >= 24.9 
    || !audio_is_playing(global.a_sound_bg)
    ) && audio_bg_pause <= 0)
{
    var rnd = random(1);
    if(rnd < audio_bg_pause_chance && !audio_bg_pause_first)
    {
        audio_bg_pause = audio_bg_pause_init;
        if(audio_bg_pause_was)
        {
            audio_bg_pause_double = 1;
        }
    }
    if(!audio_bg_pause_double)
    {
        
        if(audio_bg_pause_was || audio_bg_pause_first)
        {
            scr_audio_start_bg();
            
        }
        else
        {
            global.a_sound_bg = scr_def_play_snd(scr_audio_get_bg(),0);
            audio_sound_gain(global.a_sound_bg, 0.2, 0);
        }
        audio_bg_pause_was = 0;
    }
    audio_bg_pause_double = 0;
    audio_bg_pause_first = 0;
}

if(audio_bg_pause > 0)
{
    
    if((audio_sound_get_track_position(global.a_sound_bg) >= 23.9 
        || !audio_is_playing(global.a_sound_bg)) 
        && !audio_bg_pause_was)
    {
        audio_sound_gain(global.a_sound_bg, 0, 1000);
        audio_bg_pause_was = 1;
    }
    else
    {
        if(audio_bg_pause_was)audio_bg_pause -= delta_time/1000000;
    }
}



/* */
/// vk order status check

if(JS_vk_get_status())
{
    fed("update user data after order success");
    JS_vk_reset_status();
    v_db_read(game_user_id);
}

if(ads_timeout > 0)ads_timeout -= delta_time/1000000;
var ads_state = JS_vk_AdCheck();
if((ads_state == 2 || ads_state == 0) && ads_timeout <= 0)
{
    ads_timeout = ads_timeout_init;
    JS_vk_AdInit();
}


/* */
/*  */
