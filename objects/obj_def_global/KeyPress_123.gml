/// @description create/destroy level designer
if(os_browser == browser_not_a_browser)
{
    if(room == rm_lvlCreate)
    {
        if(instance_exists(global.lvlEdit))
        {
            with(global.lvlEdit)
            {
                instance_destroy();
            }
        }
        else
        {
            global.lvlEdit = instance_create(1,1,obj_lvlEditor);
        }
    }
    else
    {
        game_lvl_to_load = 0;
        scr_def_rm_goto_room(rm_lvlCreate);
    }
}


