/// @description  load data from server

if ds_map_find_value(async_load, "id") == global.v_db_request_id
{
    //fed("Event - " + s(global.v_db_request_id));
    global.v_db_request_id = noone;
    if ds_map_find_value(async_load, "status") >= 0
    {
        request_result = ds_map_find_value(async_load, "result");
        if(is_string(request_result))
        {
            
            var resultMap = json_decode(request_result);
            //fed("REQUEST_Types - " + global.v_db_request_type);
            //fed("OK - " + request_result);
            if(ds_exists(resultMap,ds_type_map))
            {
                //fed("map " + request_result);
                if(ds_map_exists(resultMap,"message"))
                {
                    //fed("receive message");
                    switch(global.v_db_request_type)
                    {
                        case V_DB_REQUEST_Types.PUT:
                        case V_DB_REQUEST_Types.GET:
                            v_db_add(global.v_db_request_p_id);
                        break;       
                    }
                }
                else
                {
                    if(ds_exists(global.v_db_request_map,ds_type_map))
                    {
                        ds_map_clear(global.v_db_request_map);
                        ds_map_copy(global.v_db_request_map,resultMap);
                    }
                    else
                    {
                        global.v_db_request_map = ds_map_create();
                        ds_map_copy(global.v_db_request_map,resultMap);
                    }
                    switch(global.v_db_request_type)
                    {
                        case V_DB_REQUEST_Types.GET:
                            scr_db_user_update();
                            scr_g_load();
                            break;
                        case V_DB_REQUEST_Types.PUT:
                            //fed("rewrite from http");
                            v_db_rewrite(global.v_db_request_p_id);
                        break;
                    }
                }
                ds_map_destroy(resultMap);
            }
            else
            {
                fed("NOT map " + request_result);
                switch(global.v_db_request_type)
                {
                    case V_DB_REQUEST_Types.PUT:
                    case V_DB_REQUEST_Types.GET:
                        v_db_add(global.v_db_request_p_id);
                    break;                 
                }
            }
        }
    }
    else
    {
        request_result = "null";
    }
    if(global.v_db_request_id == noone)
    {
        v_req_resend();
    }
}



