{
    "id": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_def_global",
    "eventList": [
        {
            "id": "46657181-d376-4e82-8fb6-fc4eacfd0fb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "5e55fb1a-4d3e-4f2e-adba-c9f057c5ebff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "b65c1e35-e1ba-4e93-88f6-a7b9530ebcf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "7668d87e-2178-4086-881b-f1840ced0f9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "ee0ea32f-fcd5-468c-82e7-849b8f5f17cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "1ca1891a-75fd-4499-b35d-beb05601c9ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 54,
            "eventtype": 6,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "321ff36a-35b1-4af4-90d2-4b76a8549122",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 62,
            "eventtype": 7,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "13db135f-ecf7-418a-a2b9-cd341179fce0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "e24013b2-c490-48f3-ad04-c8f747056ef1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "703c2def-c7b2-4be0-959c-3173e6beba2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "179cdf11-d4f5-418c-bffb-668300c982b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "fad5da90-6196-465a-9337-0f09412f4790",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "932b506e-1727-436d-8171-003aec94abde",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "01ea47dc-abbe-4922-adbb-2712f793f91b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 123,
            "eventtype": 9,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "67c0367b-1476-4671-838b-1110e00c55a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 114,
            "eventtype": 9,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "3e1333a3-b4fa-41d0-a360-b4633777ab39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 113,
            "eventtype": 9,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "9da958f4-deef-4847-ae18-20b24807d506",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "68762162-90ad-441b-8d7e-9920357fa4af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        },
        {
            "id": "201b4eb2-d484-4649-b64a-37b0f1844d22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "2b6a56a0-e22a-417a-ac5e-08abdd019a6b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "645e2d30-66e5-4108-8641-3075e7e5a0ac",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "c3f22388-9d87-49c5-bd79-f8c90fe38512",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 140,
            "y": 140
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}