/// @description  init sound levels

ini_open("mixer.ini");
ini_close();


/// init sys vars
hide = false;
space_size = 50;
separate = 45;
size_x = 150;
size_y = 6;

rnd_size_x = 100;
arrow_size_x = 150;

size_volume = 25;
size_volume_min = 0;
size_volume_max = ((room_height - y)/separate div 1) - 2;

if(size_volume_max > size_volume)
{
    size_volume_max = size_volume;
}

for(i = 0; i < size_volume; i += 1)
{
    xx[i] = x+size_x; 
    on_off[i] = 1; 
    volume[i,0] = 0;
    a[i] = false;
    b[i] = false;
    color_bars[i] = $FF9900;
    colli_area[i] = 0;
    colli_rnd_area[i] = 0;
    colli_button[i] = 0;
    colli_rnd_button[i] = 0;
    colli_switch[i] = 0;
    colli_play[i] = 0;
    colli_rArri[i] = 0;
    colli_lArri[i] = 0;
    percent[i] = 0;
    looped[i] = 0;
    play_loop_ind[i] = 0;
    looped_delta_time[i] = 0; 
}

max_lenght = 0;

audio_config();
//show_debug_message(string(max_lenght));

size_label = array_length_1d(label);

for(i = 0; i < size_volume; i += 1)
{
 size_lists[i] = array_length_2d(volume,i);
}

spr_button = sBoton;
spr_switch = sSwitch_007;
spr_play = sSwitch_Car;
spr_play_loop = sMarker;
spr_scroll = sScroll;
spr_rArr = sArrow_right;
spr_lArr = sArrow_left;

mixer_font = noone;//fnt_Hint_text;

xbar = 30 + x + max_lenght;

xplay = xbar + size_x + space_size + sprite_get_width(spr_play)/2;    
xrnd = xplay + space_size + sprite_get_width(spr_play)/2;
xarrow = xrnd + rnd_size_x + space_size;
xarrow_L = xarrow + sprite_get_width(spr_lArr);
xarrow_R = xarrow + arrow_size_x - sprite_get_width(spr_rArr);
xof = xarrow + arrow_size_x + space_size + sprite_get_width(spr_play)/2 + sprite_get_width(spr_switch)/2;      
v_scroll_init();

scroll_id = v_scroll_add(sScroll,size_volume_max,
separate,xof + sprite_get_width(spr_switch)/2 + 25,y,noone);

for(i = 0; i < size_volume; i += 1)
{
    xx[i] = xbar + size_x;
    rnd_xx[i] = xrnd + rnd_size_x;
    loop_repeat_time[i] = 1;
    rnd_percent[i] = 0.10;
}

loop_repeat_time_div = 0.1;
loop_repeat_time_div_2 = 1;

scroll_up_ind = 0; 
scroll_down_ind = 0; 


arrow_ind = 0;





