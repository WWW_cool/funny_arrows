/// @description System

for(var i=size_volume_min; i < size_volume_min + size_volume_max; i++)
{
    var k = i - size_volume_min;
    percent[i] = (xx[i] - (xbar)) /size_x;
    rnd_percent[i] = ((rnd_xx[i] - (xrnd)) /rnd_size_x)/2;
    
    if(hide == false){
        if(!on_off[i])
        {
           for(c=0; c < size_lists[i]; c++)
           {
            music[c] = volume[i,c];
            audio_stop_sound(music[c]);
           }
        }
        visible = true;    
        colli_area[i] = point_in_rectangle(mouse_x,mouse_y,xbar,(y-size_y)+(separate*k),xbar+size_x,(y+size_y)+(separate*k));
        colli_rnd_area[i] = point_in_rectangle(mouse_x,mouse_y,xrnd,(y-size_y)+(separate*k),xrnd+rnd_size_x,(y+size_y)+(separate*k));
        colli_button[i] = point_in_rectangle(mouse_x,mouse_y,xx[i]-sprite_get_xoffset(spr_button),(y-sprite_get_yoffset(spr_button))+(separate*k),xx[i]+sprite_get_xoffset(spr_button),(y+sprite_get_yoffset(spr_button))+(separate*k));
        colli_rnd_button[i] = point_in_rectangle(mouse_x,mouse_y,rnd_xx[i]-sprite_get_xoffset(spr_button),(y-sprite_get_yoffset(spr_button))+(separate*k),rnd_xx[i]+sprite_get_xoffset(spr_button),(y+sprite_get_yoffset(spr_button))+(separate*k));
        colli_switch[i] = point_in_rectangle(mouse_x,mouse_y,xof-sprite_get_xoffset(spr_switch),(y-sprite_get_yoffset(spr_switch))+(separate*k),xof+sprite_get_xoffset(spr_switch),(y+sprite_get_yoffset(spr_switch))+(separate*k));
        colli_play[i] = point_in_rectangle(mouse_x,mouse_y,xplay-sprite_get_xoffset(spr_play),(y-sprite_get_yoffset(spr_play))+(separate*k),xplay+sprite_get_xoffset(spr_play),(y+sprite_get_yoffset(spr_play))+(separate*k));
        
        colli_rArri[i] = point_in_rectangle(mouse_x,mouse_y,xarrow_R-sprite_get_xoffset(spr_rArr),(y-sprite_get_yoffset(spr_rArr))+(separate*k),xarrow_R+sprite_get_xoffset(spr_rArr),(y+sprite_get_yoffset(spr_rArr))+(separate*k));
        colli_lArri[i] = point_in_rectangle(mouse_x,mouse_y,xarrow_L-sprite_get_xoffset(spr_lArr),(y-sprite_get_yoffset(spr_lArr))+(separate*k),xarrow_L+sprite_get_xoffset(spr_lArr),(y+sprite_get_yoffset(spr_lArr))+(separate*k));
        
        if(mouse_check_button_pressed(mb_left))
        {
            if(on_off[i]&& (colli_button[i] || colli_area[i])){a[i] = true;}
            if(on_off[i] && (colli_rnd_button[i] || colli_rnd_area[i])){b[i] = true;}
        } 
        if(mouse_check_button(mb_right))
        {
            if(colli_rArri[i])
            {
                loop_repeat_time[i] += loop_repeat_time_div_2;
                
            }
            if(colli_lArri[i])
            {
                loop_repeat_time[i] -= loop_repeat_time_div_2;
                if(loop_repeat_time[i] < 1)loop_repeat_time[i] = 1;
            }
        }
        if(mouse_check_button(mb_left))
        {
            if(colli_rArri[i])
            {
                loop_repeat_time[i] += loop_repeat_time_div;
                
            }
            if(colli_lArri[i])
            {
                loop_repeat_time[i] -= loop_repeat_time_div;
                if(loop_repeat_time[i] < 0.1)loop_repeat_time[i] = 0.1;
            }
        } 
        if(mouse_check_button_released(mb_left))
        {
         if(a[i])
         {
            //audio_sound_gain(soundTest,percent[i],0); 
            //audio_play_sound(soundTest,0,0);
            for(c=0; c < size_lists[i]; c++)
               {
                music[c] = volume[i,c];
                audio_sound_gain(music[c],percent[i],0);
               }
         }
         a[i] = false;
         b[i] = false;
        }
        
        var slip;
        if(a[i])
        {
            slip = clamp (mouse_x, xbar, xbar + size_x);
            xx[i] = slip;
        }
        if(b[i])
        {
            slip = clamp (mouse_x, xrnd, xrnd + rnd_size_x);
            rnd_xx[i] = slip;
        }
    }else{
        visible = false;
    }
}

if(y+separate*size_volume > room_height)
{
   size_volume_min = v_scroll_step(scroll_id,size_volume);
}




