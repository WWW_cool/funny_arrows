{
    "id": "cad53ae1-4542-47ee-818f-5c7f9894d16f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Mixer",
    "eventList": [
        {
            "id": "5b7ada61-8940-474b-86ba-93cf57fc44b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cad53ae1-4542-47ee-818f-5c7f9894d16f"
        },
        {
            "id": "fc7df6ec-803e-42d4-b28e-946c456ff958",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cad53ae1-4542-47ee-818f-5c7f9894d16f"
        },
        {
            "id": "575a54c9-16ef-4ffa-8483-dc3dbb9a9a53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "cad53ae1-4542-47ee-818f-5c7f9894d16f"
        },
        {
            "id": "036e1970-a44e-4044-b868-f1bd39f791a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "cad53ae1-4542-47ee-818f-5c7f9894d16f"
        },
        {
            "id": "d642f249-9c67-4b62-a5c1-0f492008b005",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cad53ae1-4542-47ee-818f-5c7f9894d16f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "1ace5ad8-5c1e-437f-8bee-f4b51bd9d6d7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 0
        },
        {
            "id": "a571d244-aafd-4a5d-a9cf-78dfb170291c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 80
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}