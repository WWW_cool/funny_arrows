
c_first = draw_get_color();

//draw_sprite(sBase_Automatic,0,x,y - 20);

draw_set_alpha(0.8);
draw_set_color(c_black);
draw_rectangle(x - 20,y - 60,xof + sprite_get_width(spr_switch) + 20,y + (separate*(size_volume_max-1))+ sprite_get_height(spr_switch)/2 + 25,0);
draw_set_alpha(1);


for(var i=size_volume_min; i < size_volume_min + size_volume_max; i++)
{
    var k = i - size_volume_min;
    var sub_img = 0; 
    for(c=0; c < size_lists[i]; c++)
    {
     music[c] = volume[i,c];
     if(audio_is_playing(music[c]))
     {
        sub_img = 1;
     }
    }
//======================================================| DRAW BASES |=====================================================

    draw_set_color(c_black);
    draw_roundrect_ext(xbar,(y-size_y)+(separate*k),xbar + size_x,(y+size_y)+(separate*k),10,10,0);
    draw_set_color(c_first);
    
    draw_set_color(c_black);
    draw_roundrect_ext(xrnd,(y-size_y)+(separate*k),xrnd + rnd_size_x,(y+size_y)+(separate*k),10,10,0);
    draw_set_color(c_first);
    
    if(on_off[i])
    {
        if(sub_img || looped[i])
        {
            draw_set_color(c_lime);
        }
        else
        {
            draw_set_color(color_bars[i]);
        }
    }
    else
    {
        draw_set_color(c_gray)
    }
    draw_roundrect_ext(xbar,(y-size_y)+(separate*k),xx[i],(y+size_y)+(separate*k),10,10,0);
    draw_roundrect_ext(xrnd,(y-size_y)+(separate*k),rnd_xx[i],(y+size_y)+(separate*k),10,10,0);
    draw_set_color(c_first);
    if(colli_rArri[i])
    {
        arrow_ind += 1;
        arrow_ind %= sprite_get_number(spr_rArr);
        draw_sprite(spr_rArr,arrow_ind,xarrow_R,y+(separate*k));
    }
    else
    {
        draw_sprite(spr_rArr,0,xarrow_R,y+(separate*k));
    }
    if(colli_lArri[i])
    {
        arrow_ind += 1;
        arrow_ind %= sprite_get_number(spr_lArr);
        draw_sprite(spr_lArr,arrow_ind,xarrow_L,y+(separate*k));
    }
    else
    {
        draw_sprite(spr_lArr,0,xarrow_L,y+(separate*k));
    }
//=====================================================| DRAW BUTTONS |====================================================

    draw_sprite_ext(spr_button,0,xx[i],y+(separate*k),1,1,image_angle,c_white,1);
    draw_sprite_ext(spr_button,0,rnd_xx[i],y+(separate*k),1,1,image_angle,c_white,1);
    if(colli_switch[i])
    {
        colored2 = c_ltgray;  
        if(mouse_check_button_pressed(mb_left))
        {
            on_off[i] = !on_off[i];
        }
    }
    else
    { 
        colored2 = c_white;
    }
    draw_sprite_ext(spr_switch,on_off[i],xof,y+(separate*k),1,1,image_angle,colored2,1);   

    if(colli_play[i])
    {
        colored2 = c_ltgray;  
        if(mouse_check_button_pressed(mb_left))
        {
            for(c=0; c < size_lists[i]; c++)
            {
             music[c] = volume[i,c];
             if(sub_img == 1)
             {
                audio_stop_sound(music[c]);
             }
             else
             {
                 if!(audio_is_playing(music[c]))
                 {
                    if(on_off[i])audio_sound_gain(music[c],percent[i] -(percent[i]*random_range(0,rnd_percent[i])),0);
                    else audio_sound_gain(music[c],0,0);
                    audio_play_sound(music[c],0,0);
                 }
             }
            }
        }
        if(mouse_check_button_pressed(mb_right))
        {
            if(looped[i])
            {
                looped[i] = 0;
                for(c=0; c < size_lists[i]; c++)
                {
                 music[c] = volume[i,c];
                 audio_stop_sound(music[c]);
                }
            }
            else
            {
                looped[i] = 1;
            }
        }
    }
    else
    { 
        colored2 = c_white;
    }
    
    if(looped[i])
    {
        looped_delta_time[i] += delta_time/1000000;
        if(sub_img == 0 || looped_delta_time[i] > loop_repeat_time[i])
        {
            //show_debug_message(string(looped_delta_time[i]));
            looped_delta_time[i] = 0;
            sub_img = 1;
            for(c=0; c < size_lists[i]; c++)
            {
                music[c] = volume[i,c];
                if(on_off[i])audio_sound_gain(music[c],percent[i] -(percent[i]*random_range(0,rnd_percent[i])),0);
                else audio_sound_gain(music[c],0,0);
                audio_play_sound(music[c],0,0);
            }
        }
        play_loop_ind[i] += 1;
        play_loop_ind[i] %= sprite_get_number(spr_play_loop);
        draw_sprite(spr_play_loop,play_loop_ind[i],xplay,y+(separate*k));
    } 
    draw_sprite_ext(spr_play,sub_img,xplay,y+(separate*k),1,1,image_angle,colored2,1);  
//}
//=======================================================| DRAW TEXTS |====================================================
/**/    
    draw_set_valign(fa_middle);
    draw_set_halign(fa_center);
    draw_set_color(c_white);
    //draw_set_font(mixer_font);
    if(i == size_volume_min)
    {
        var tmp_y = y - 50;
        draw_set_color(c_red);
        draw_text(xbar + size_x/2 - 20,tmp_y,string_hash_to_newline(" GLOBAL vol = " + string(audio_get_master_gain(0))));
        draw_set_color(c_white);
        draw_text(x + (xbar - x)/2,tmp_y,string_hash_to_newline("In game sounds names"));
        draw_text(xplay,tmp_y,string_hash_to_newline("Play"));
        draw_text(xof,tmp_y,string_hash_to_newline("On/Off"));
        draw_text(xarrow + arrow_size_x/2,tmp_y,string_hash_to_newline("Reloop time"));
        draw_text(xrnd + rnd_size_x/2,tmp_y,string_hash_to_newline("Volume range"));
    }
    draw_text(xx[i],y+(separate*k) - 20,string_hash_to_newline(string(round(percent[i]*100)) + "%"));
    draw_text(rnd_xx[i],y+(separate*k) - 20,string_hash_to_newline(string(round(rnd_percent[i]*100)) + "%"));
    draw_text(xarrow + arrow_size_x/2,y+(separate*k) - 20,string_hash_to_newline("" + string(loop_repeat_time[i]) + " second(s)"));
    draw_set_halign(fa_left);
    if(i < size_label){draw_text(x,y-size_y/2+(separate*k),string_hash_to_newline(string(i+1)+". "+string(label[i])));}
    //draw_set_font(-1);
    draw_set_color(c_first);
    draw_set_valign(fa_top);
    draw_set_halign(fa_middle);
}

if(y+separate*size_volume > room_height)
{
    v_scroll_draw(scroll_id,size_volume);
    var y_offset = (((size_volume_max - 1)*separate)/(size_volume - size_volume_max)) div 1;
 
    if(size_volume_min > 0)
    {
        scroll_up_ind += 1;
        scroll_up_ind %= sprite_get_number(sArrow_up);
        draw_sprite(sArrow_up,scroll_up_ind,xscroll,y);
    }
    if(size_volume_min < size_volume - size_volume_max)
    {
        scroll_down_ind += 1;
        scroll_down_ind %= sprite_get_number(sArrow_down);
        draw_sprite(sArrow_down,scroll_down_ind,xscroll,y + y_offset*(size_volume - size_volume_max));
    }
}


/* */
/*  */
