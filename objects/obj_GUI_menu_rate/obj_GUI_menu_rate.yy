{
    "id": "97c1b716-6f22-453d-8c96-cb17c33f1267",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_menu_rate",
    "eventList": [
        {
            "id": "8e204cd3-b158-46b4-994b-0d8090e44830",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97c1b716-6f22-453d-8c96-cb17c33f1267"
        },
        {
            "id": "38b21c58-5719-4f01-89b5-8ed6045124a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "97c1b716-6f22-453d-8c96-cb17c33f1267"
        },
        {
            "id": "2fe2d4fd-d336-44f2-a38a-8313be26f705",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "97c1b716-6f22-453d-8c96-cb17c33f1267"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0f12a9c8-0859-422b-865b-2665b2ddf681",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}