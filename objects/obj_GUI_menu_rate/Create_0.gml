/// @description  init
var ind = 0;
OPT_btn_spr = spr_menu_btn_small;
OPT_back = spr_menu_bg_small;
OPT_btn_font = f_text;
OPT_need_close = 0;
var OPT_btn_spr_tmp = OPT_btn_spr;
var OPT_back_tmp = OPT_back;
var OPT_btn_font_tmp = OPT_btn_font;

x = __view_get( e__VW.WView, 0 )/2 + __view_get( e__VW.XView, 0 );
y = __view_get( e__VW.HView, 0 )/2 + __view_get( e__VW.YView, 0 );;
OPT_y_tmp = y + sprite_get_height(OPT_back) - sprite_get_height(OPT_btn_spr) - 80;

OPT_header_x = x - 145;
OPT_header_y = y - 55;
OPT_header_text = "Оцените игру!";
OPT_header_icon = spr_GUI_icon_happy;



glob = noone;
if(instance_exists(global.inst_glb))
{
    glob = global.inst_glb;
}

var _shift = 60;

for(var i = 0; i < 5; i += 1)
{
    OPT_controls_type[i] = v_btns_add(0,0,spr_menu_star_off, id,0, 
              NO_SCRIPT,
              NO_SCRIPT,
              NO_SCRIPT, 
              scr_s_rate_lay_on,
              NO_SCRIPT
              );
    
    OPT_controls_x[i] = x - 118 + i*_shift;
    OPT_controls_y[i] = y + 30; 
}

//////////////////////////////////////////////////////////////////////////////////////////
event_inherited();
//////////////////////////////////////////////////////////////////////////////////////////

OPT_btn_spr = OPT_btn_spr_tmp;
OPT_back = OPT_back_tmp;
OPT_btn_font = OPT_btn_font_tmp;




