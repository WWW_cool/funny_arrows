/// @description  draw shop

if(!OPT_cantDraw)
{
    var tmp_alpha = draw_get_alpha();
    draw_set_alpha(OPT_alpha);
    draw_sprite(OPT_back,0,x,y);
    for(var i = 0; i < array_length_1d(OPT_controls_type); i += 1)
    {
        v_btns_draw_btn(OPT_controls_type[i]);        
    }
    draw_set_colour(c_black);
    if(sprite_exists(OPT_header_icon))
    {
        draw_sprite(OPT_header_icon,0,x,y - 125);
    }
    v_draw_text(OPT_header_x,OPT_header_y,OPT_header_text,fa_left,fa_middle,EnText.H2);
    draw_set_alpha(tmp_alpha);
    if(OPT_alpha < 1)OPT_alpha += OPT_alpha_speed;
}



