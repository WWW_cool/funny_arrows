/// @description  cell selector
//if(!scr_s_check_anim())
if(!global.a_hint_mouse_block)
{
    if(mouse_check_button(mb_left))
    {
        if(mouse_check_button_pressed(mb_left))
        {
            var inst = collision_point(mouse_x,mouse_y,obj_a_parent,false,true);
            if(instance_exists(inst))
            {
                if(inst.a_state != EnCellStates.DESTROY && scr_a_check_select(inst))
                {
                    s_seq_count = 0;
                    s_start_seq = 1;
                    inst.a_state = EnCellStates.SELECTED;
                    inst.a_first = 1;
                    s_seq_items[s_seq_count++] = inst;
                    scr_def_play_snd(snd_tile_click_on);
                }
            }
        }
        else
        {
            var inst = collision_point(mouse_x,mouse_y,obj_a_parent,false,true);
            if(instance_exists(inst))
            {
                if(s_start_seq)
                {
                    if(scr_a_check_cell(inst) && scr_a_check_select(inst))
                    {
                        
                        scr_def_play_snd(snd_tile_click_on);
                        scr_a_add_point(inst);
                        scr_a_create_path();
                    }
                }
                else
                {
                    if(inst.a_state != EnCellStates.DESTROY && scr_a_check_select(inst))
                    {
                        s_seq_count = 0;
                        s_start_seq = 1;
                        inst.a_state = EnCellStates.SELECTED;
                        inst.a_first = 1;
                        s_seq_items[s_seq_count++] = inst;
                    }
                }
            }
        }
    }
    else
    {
        s_start_seq = 0;
        if(is_array(s_seq_items))
        {
            if(s_seq_count > 1)
            {
                for(var i = 0; i < s_seq_count; i += 1)
                {
                    if(instance_exists(s_seq_items[i]))
                    {
                        scr_a_check_destroy(s_seq_items[i]);
                    }
                }
                scr_def_play_snd(snd_tile_success);
                global.a_hint_first_turn = 0;
                scr_a_next_step();
            }
            else
            {
                for(var i =0; i < array_length_1d(s_seq_items); i += 1)
                {
                    if(instance_exists(s_seq_items[i]))
                    {
                        s_seq_items[i].a_state = EnCellStates.IDLE;
                    }
                }
            }
        }
        s_seq_items = 0;
    }
    
    scr_s_check_state();
}



