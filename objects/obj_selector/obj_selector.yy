{
    "id": "d0735997-e78d-4609-924f-8021d31c0646",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_selector",
    "eventList": [
        {
            "id": "e405c55a-a3f9-443c-a5e7-8cea52e257f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d0735997-e78d-4609-924f-8021d31c0646"
        },
        {
            "id": "286a22b8-711a-4c59-b2a2-830894fed62a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d0735997-e78d-4609-924f-8021d31c0646"
        },
        {
            "id": "61d16614-14d3-41ec-abf0-efa2ce2b7a34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d0735997-e78d-4609-924f-8021d31c0646"
        },
        {
            "id": "6f28a668-38fd-4bcd-9e46-8d8dcc0e1e24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d0735997-e78d-4609-924f-8021d31c0646"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}