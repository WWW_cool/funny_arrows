/// @description draw gui
/*
draw_set_font(f_Attempts);
draw_set_valign(fa_top);
draw_set_halign(fa_center);
draw_set_colour(c_white);
draw_text(150,8,s(s_attempts));
if(s_attemt_anim || s_lose_anim)
{
    draw_set_font(f_Attempts_large);
    draw_set_valign(fa_middle);
    draw_text_transformed(room_width/2,room_height/2,s(s_attempts),s_at_scale,s_at_scale,0);
}

*/
if(is_array(s_seq_items))
{
    if(s_seq_count > 0)
    {
        if(instance_exists(s_seq_items[s_seq_count - 1]))
        {
            var _x = s_seq_items[s_seq_count - 1].x;
            var _y = s_seq_items[s_seq_count - 1].y;
            var _w = point_distance(_x,_y,mouse_x,mouse_y);
            var _dir = point_direction(_x,_y,mouse_x,mouse_y);
            _w = clamp(_w/sprite_get_width(spr_cell_line),0,1);
            draw_sprite_ext(spr_cell_line,0,_x,_y,_w,1,_dir,c_white,1);
            
        }
    }
}




/* */
/*  */
