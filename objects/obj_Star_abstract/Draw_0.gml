if(AM_start_pulse)
{
    if(!AM_flag_pulse_up)
    {
        AM_scale += AM_Speed_pulse;
        if(AM_scale >= AM_scale_max)
        {
            AM_flag_pulse_up = 1;
            AM_scale  = AM_scale_max;
            AM_start_ef_1 = 1;
        }
    }
    else
    {
        if(!AM_flag_pulse_down)
        {
            AM_scale -= AM_Speed_pulse;
            if(AM_scale <= AM_pulse_min)
            {
                AM_flag_pulse_down = 1;
                AM_scale = AM_pulse_min;
                AM_start_ef_2 = 1;
            }
        }
    }
}
else
{
    AM_scale -= p_scalse_speed;
    if(AM_scale <= AM_pulse_min)AM_scale = AM_pulse_min;
}
draw_sprite_ext(spr_Star_G,AM_ef_2_ind,x,y,AM_scale,AM_scale,0,c_white,1);

if(AM_start_ef_1)
{
    draw_sprite_ext(spr_ef_star,AM_ef_1_ind,x + 75*AM_scale,y - 71*AM_scale,AM_scale,AM_scale,0,c_white,1);
    AM_ef_1_ind += 1;
    if(AM_ef_1_ind > 16)
    {
        AM_start_ef_1 = 0;
    }
}
if(AM_start_ef_2)
{
    AM_ef_2_ind += 1;
    if(AM_ef_2_ind > 18)
    {
        AM_start_ef_2 = 0;
        AM_ef_2_ind = 0;
    }
}


