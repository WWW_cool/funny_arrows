{
    "id": "73731ccc-5e7a-4502-97c9-df67794706f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Star_abstract",
    "eventList": [
        {
            "id": "9773329f-829a-43d0-bd90-ad5798a093b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73731ccc-5e7a-4502-97c9-df67794706f1"
        },
        {
            "id": "5d9383c9-d683-41d3-b82c-c38ca6dfb64c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "73731ccc-5e7a-4502-97c9-df67794706f1"
        },
        {
            "id": "c1fec094-6fb6-4dd9-868a-a0511ff6d445",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "73731ccc-5e7a-4502-97c9-df67794706f1"
        },
        {
            "id": "5e60c666-3e74-49c3-8748-74fa4b77fd0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "73731ccc-5e7a-4502-97c9-df67794706f1"
        },
        {
            "id": "8cfd4031-1aa2-4549-8ccf-a0f905265580",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "73731ccc-5e7a-4502-97c9-df67794706f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "923abbd7-a985-4b1c-879e-f4a41351200e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 74,
            "y": 70
        },
        {
            "id": "4e8f0689-b88d-4d7b-bc1c-fdbb9cd1cabf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 74,
            "y": 74
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}