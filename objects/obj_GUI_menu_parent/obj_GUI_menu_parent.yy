{
    "id": "0f12a9c8-0859-422b-865b-2665b2ddf681",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_menu_parent",
    "eventList": [
        {
            "id": "cf9dfb47-abe3-422f-975c-1ea84ffd679e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0f12a9c8-0859-422b-865b-2665b2ddf681"
        },
        {
            "id": "39d05294-d7f7-4be7-9a48-465fc1362d26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "0f12a9c8-0859-422b-865b-2665b2ddf681"
        },
        {
            "id": "e69be328-7212-4b9a-b57e-5d0bc4fce2ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "0f12a9c8-0859-422b-865b-2665b2ddf681"
        },
        {
            "id": "27998696-33b5-401e-950e-f5a49e3a4920",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0f12a9c8-0859-422b-865b-2665b2ddf681"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}