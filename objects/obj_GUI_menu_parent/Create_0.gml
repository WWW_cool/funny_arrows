/// @description init

OPT_owner = noone;

OPT_cantDraw = 0;
OPT_back = noone;
OPT_alpha = 0;
OPT_alpha_speed = 0.2;

OPT_btn_spr = noone;
OPT_btn_font = noone;

OPT_btn_close_spr = noone;
OPT_btn_close_x = x + sprite_get_width(OPT_back) - sprite_get_width(OPT_btn_close_spr);
OPT_btn_close_y = y;

var ind = 0;
if(is_array(OPT_controls_type))
{
    if(array_length_1d(OPT_controls_type))
    {
        ind = array_length_1d(OPT_controls_type);
    } 
    if(OPT_need_close)
    {
        OPT_controls_type[ind] = v_btns_add(0,0,OPT_btn_close_spr, id,0, 
                  NO_SCRIPT,
                  scr_btn_menu_close,
                  NO_SCRIPT, 
                  NO_SCRIPT,
                  NO_SCRIPT
                  );
        OPT_controls_x[ind] = OPT_btn_close_x;
        OPT_controls_y[ind++] = OPT_btn_close_y; 
    }                       
    for(var i = 0; i < array_length_1d(OPT_controls_type); i += 1)
    {
        v_btns_set_param(OPT_controls_type[i],"cantDraw",1);
    }
    
    event_user(0);
}

scr_def_g_pause(id,"on"); 


