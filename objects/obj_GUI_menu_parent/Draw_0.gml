/// @description  draw gui of start room

if(!OPT_cantDraw)
{
    var tmp_alpha = draw_get_alpha();
    draw_set_alpha(OPT_alpha);
    if(sprite_exists(OPT_back))
    {
        draw_sprite(OPT_back,0,x,y);
    }
    for(var i = 0; i < array_length_1d(OPT_controls_type); i += 1)
    {
        v_btns_draw_btn(OPT_controls_type[i]);
    }
    draw_set_alpha(tmp_alpha);
    if(OPT_alpha < 1)OPT_alpha += OPT_alpha_speed;
}





