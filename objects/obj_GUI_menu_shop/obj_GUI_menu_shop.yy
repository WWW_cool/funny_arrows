{
    "id": "8da66bf6-d5eb-4f02-a52f-8d6aacb9cada",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_menu_shop",
    "eventList": [
        {
            "id": "ba4a0856-aa8a-48e6-9c65-fce45840ba5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8da66bf6-d5eb-4f02-a52f-8d6aacb9cada"
        },
        {
            "id": "c28a0276-6c4f-4e11-abef-18950ab13ef3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8da66bf6-d5eb-4f02-a52f-8d6aacb9cada"
        },
        {
            "id": "cd752876-9f73-4c57-ab8e-72b3c944d12b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8da66bf6-d5eb-4f02-a52f-8d6aacb9cada"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0f12a9c8-0859-422b-865b-2665b2ddf681",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}