/// @description  draw shop

if(!OPT_cantDraw)
{
    var tmp_alpha = draw_get_alpha();
    draw_set_alpha(OPT_alpha);
    draw_sprite_ext(OPT_back,0,x,y,0.9,1.15,0,c_white,1);
    draw_set_font(OPT_btn_font);
    draw_set_halign(fa_left);
    draw_set_valign(fa_middle);
    draw_set_colour(c_red);
    for(var i = 0; i < array_length_1d(OPT_controls_type); i += 1)
    {
        v_btns_draw_btn(OPT_controls_type[i]);        
    }
    v_draw_text(OPT_header_x,OPT_header_y,OPT_header_text,fa_left,fa_middle,EnText.H2);
    draw_set_alpha(tmp_alpha);
    if(OPT_alpha < 1)OPT_alpha += OPT_alpha_speed;
}



