/// @description V_BTNS_step

var i;
var releaseFree = true;
var releaseFree_id = -1;
for (i = 0; i < _v_btns_count; i += 1)
{
    //Если кнопка существует
    if (!_v_btns_deleted[i] && !_v_btns_colddown[i] && _v_btns_active[i])
    {
        //Если мышка попала на кнопку
        if( (mouse_x > _v_btns_xpos[i] - _v_btns_spr_x_offset[i])                       &&
            (mouse_x < _v_btns_xpos[i] - _v_btns_spr_x_offset[i] + _v_btns_spr_w[i])    &&
            (mouse_y > _v_btns_ypos[i] - _v_btns_spr_y_offset[i])                       &&
            (mouse_y < _v_btns_ypos[i] - _v_btns_spr_y_offset[i] + _v_btns_spr_h[i]) 
          )
        {
            //то:
            _v_btns_spr_index[i] = 1; 
            
            if(_v_btns_mouse_lay_on_flag[i] == false) _v_btns_lay_on_count++;            
            _v_btns_mouse_lay_on_flag[i] = true;
            
            if(_v_btns_spr_sc[i] < _v_btns_spr_sc_add[i])
            {
                _v_btns_spr_sc[i] = v_lerp(0,_v_btns_spr_sc[i],_v_btns_spr_sc_add[i],
                _v_btns_spr_sc_speed[i],_v_btns_spr_sc_speed[i]/5);
            }
            
            if(_v_btns_2click_start[i])
            {
                _v_btns_2click_time[i] += delta_time;
                if(_v_btns_2click_time[i] > 350*1000)//Dclick_time)
                {
                    _v_btns_2click_release[i] = 0;
                    _v_btns_2click_start[i] = 0;
                    _v_btns_2click_time[i] = 0;
                }
            }   
            //Mouse lay on callback
            if(script_exists(_v_btns_callback_mouse_lay_on[i]))
            {
                if(instance_exists(_v_btns_owner[i]))
                {
                    with(_v_btns_owner[i])
                    {
                        script_execute(other._v_btns_callback_mouse_lay_on[i],i);
            }}}
 
            //Mouse pressed callback execute
            if(mouse_check_button_pressed(mb_left))
            {
                if(script_exists(_v_btns_callback_mouse_pressed[i]))
                {
                    if(_v_btns_2click_cb[i] != -1)
                    {
                        if(!_v_btns_2click_start[i])
                        {
                            _v_btns_2click_time[i] = 0;
                            _v_btns_2click_start[i] = 1;
                            _v_btns_2click_time[i] += delta_time;
                        }
                        else
                        {
                            
                            if(instance_exists(_v_btns_owner[i]))
                            {
                                with(_v_btns_owner[i])
                                {
                                    script_execute(other._v_btns_2click_cb[i],i); 
                                }
                            }
                            _v_btns_2click_time[i] = 0;
                            _v_btns_2click_start[i] = 1;
                        }
                    }
                    ///Pressed callback
                    if(instance_exists(_v_btns_owner[i]))
                    {
                        if(_v_btns_colddown_time_init[i] > 0)
                        {
                            _v_btns_colddown[i] = 1;
                            _v_btns_colddown_time[i] = _v_btns_colddown_time_init[i];
                        }

                        with(_v_btns_owner[i])
                        {
                            script_execute(other._v_btns_callback_mouse_pressed[i],i); 
                        }
                        _v_btns_spr_index[i] = 0;
                        
                        if(_v_btns_sound_in_array[i])
                        {
                            if(is_array(_v_btns_sound_array))
                            {
                                var _rnd = irandom(array_length_1d(_v_btns_sound_array) - 1)
                                scr_def_play_snd(_v_btns_sound_array[_rnd]); 
                            }
                            else
                            {
                                scr_def_play_snd(global._def_glb_var_audio_click);
                            }
                        }
                        else
                        {
                            if(_v_btns_sound[i] != noone)
                            {
                                scr_def_play_snd(_v_btns_sound[i]);
                            }
                            else
                            {
                                scr_def_play_snd(global._def_glb_var_audio_click);
                            }
                        }
                        if(_v_btns_analytics_id[i] != "")
                        {
                            v_a_event(_v_btns_analytics_id[i],"btn","pressed");
                        }
                    }
            }}
                
            //Mouse released callback execute
            if(mouse_check_button_released(mb_left))
            {
                if(script_exists(_v_btns_callback_mouse_released[i]))
                {
                    if(instance_exists(_v_btns_owner[i]))
                    {
                        with(_v_btns_owner[i])
                        {
                            script_execute(other._v_btns_callback_mouse_released[i],i);
                        }
                    }
                }
                releaseFree = false;
            }
        }
        else
        {           
            _v_btns_spr_index[i] = 0;
             
            if(_v_btns_2click_start[i])
            {
                    _v_btns_2click_start[i] = 0;
                    _v_btns_2click_time[i] = 0;
                    _v_btns_2click_release[i] = 0;
            }
            
            //Если предыдущее состояние флага - 1, это значит, что мы поймали момент перехода с 1 в 0, т оесть событие lay_off
            if(_v_btns_mouse_lay_on_flag[i])
            {
                 //Mouse lay off callback
                if(script_exists(_v_btns_callback_mouse_lay_off[i]))
                {
                    if(instance_exists(_v_btns_owner[i]))
                    {
                        with(_v_btns_owner[i])
                        {
                            script_execute(other._v_btns_callback_mouse_lay_off[i],i);
                }}} 
                
               _v_btns_lay_on_count--;
               if(_v_btns_lay_on_count<0)
               { 
                _v_btns_lay_on_count = 0;
                error("obj_v_btns_step","_v_btns_lay_on_count < 0 error.."); 
               }          
            }
            if(_v_btns_spr_sc[i] > 0)
            {
                _v_btns_spr_sc[i] = v_lerp(0,_v_btns_spr_sc[i],0,
                _v_btns_spr_sc_speed[i],_v_btns_spr_sc_speed[i]/5);
            }
            _v_btns_mouse_lay_on_flag[i] = false;         
        } 
        if(script_exists(_v_btns_callback_mouse_release_free[i]))
        {
            releaseFree_id = i;
        }   
    }        
} 

if(releaseFree && releaseFree_id != -1 && mouse_check_button_released(mb_left))
{
    if(script_exists(_v_btns_callback_mouse_release_free[releaseFree_id]))
    {
        if(instance_exists(_v_btns_owner[releaseFree_id]))
        {
            with(_v_btns_owner[releaseFree_id])
            {
                script_execute(other._v_btns_callback_mouse_release_free[releaseFree_id],releaseFree_id);
            }
        }
    }
}








