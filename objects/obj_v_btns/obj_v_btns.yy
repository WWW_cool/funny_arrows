{
    "id": "9a50fb07-3a41-4e10-9e26-5ea97c501813",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_v_btns",
    "eventList": [
        {
            "id": "53ab6801-7eea-4f7a-9a3a-6d8cdb5c95f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9a50fb07-3a41-4e10-9e26-5ea97c501813"
        },
        {
            "id": "b1225b93-3f1e-42d3-91d0-d10f8be68873",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9a50fb07-3a41-4e10-9e26-5ea97c501813"
        },
        {
            "id": "45492dd1-dfdc-4aa6-bc7c-84ebd054f481",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9a50fb07-3a41-4e10-9e26-5ea97c501813"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}