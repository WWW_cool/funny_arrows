

event_inherited();
t_type      = EnTileTypes.CROSS;
t_sprite    = spr_block_cross;
t_side_allow[EnTileAngles.RIGHT]    = 0;
t_side_allow[EnTileAngles.UP]       = 90;
t_side_allow[EnTileAngles.LEFT]     = 180;
t_side_allow[EnTileAngles.DOWN]     = 270;


