/// @description OnCreate

//В какую комнату хотим попасть
if(room_exists(global._def_glb_var_room_start))
{
    _def_rm_destination = global._def_glb_var_room_start;
}
else
{
    _def_rm_destination = noone;
}
//Стави главное меню, чтобы если что не выпасть с ошибкой

//факт перехода между комнатами
_def_rm_was_transition = 0;

//С какой прозрачностью начинаем рисовать черный экран
_def_rm_current_alpha = 0;

//Скорость затемнения/осветления 
_def_rm_alpha_rate = 3/room_speed; 

scr_def_show_debug_message("_def_rm_transition INIT (sucsess)","info");
scr_def_show_debug_message("_def_ view_current = " + string(view_current),"debug");

//instance_deactivate_all(true);
scr_def_instance_activate_base();


