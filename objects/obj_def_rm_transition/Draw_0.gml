/// @description OnDraw
if(view_current == global._def_glb_var_view_current)
{
    var _def_rm_save_alpha = draw_get_alpha();
    var trans_color = c_black;
    draw_set_alpha(_def_rm_current_alpha);
    var _def_wview = __view_get( e__VW.WView, global._def_glb_var_view_current );
    var _def_hview = __view_get( e__VW.HView, global._def_glb_var_view_current );
    var _def_xview = __view_get( e__VW.XView, global._def_glb_var_view_current );
    var _def_yview = __view_get( e__VW.YView, global._def_glb_var_view_current );
    draw_rectangle_colour(_def_xview,_def_yview,_def_wview,_def_hview,
    trans_color,trans_color,trans_color,trans_color,false);
    draw_set_alpha(_def_rm_save_alpha);
    if(!_def_rm_was_transition)
    {
        if(_def_rm_current_alpha >= 1)
             {
                _def_rm_was_transition = 1; 
                if(room_exists(_def_rm_destination)) 
                {  
                    room_goto(_def_rm_destination);
                }
                else
                {
                    scr_def_show_debug_message("Room not found!", "error");
                }
             }
        _def_rm_current_alpha += _def_rm_alpha_rate;     
    }
    else 
    {
        if(_def_rm_current_alpha<=0) 
        {
            instance_activate_all();
            instance_destroy();     
        }   
        _def_rm_current_alpha -= _def_rm_alpha_rate;  
    }
}




