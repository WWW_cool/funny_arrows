/// @description  draw below
v_draw_alpha();

draw_set_color(c_white);

var col = global.col_main;
var rm_width = global._def_glb_var_default_resolution_w;

draw_rectangle_colour(__view_get( e__VW.XView, 0 ),__view_get( e__VW.YView, 0 ),__view_get( e__VW.XView, 0 ) + rm_width,0,
col,col,col,col,false);
v_draw_tiled(spr_gui_bg,0,__view_get( e__VW.XView, 0 ),0,EnTiled.Hor);

if(!global.user_open_all)
{       
    draw_sprite(spr_GUI_circle,0,GUI_back_count_x,GUI_back_count_y);
    draw_set_colour(global.col_main);
    v_draw_text(GUI_back_count_x,GUI_back_count_y + 2,s(glob.hint_moveback_count),fa_center,fa_middle,EnText.Text);
    
    draw_sprite(spr_GUI_circle,0,GUI_FT_count_x,GUI_FT_count_y);
    draw_set_colour(global.col_main);
    v_draw_text(GUI_FT_count_x,GUI_FT_count_y + 2,s(glob.hint_fstep_count),fa_center,fa_middle,EnText.Text);
}

draw_set_colour(c_white);
//v_draw_text(GUI_FT_count_x - 100,GUI_FT_count_y + 2,s(glob.game_current_lvl_points),fa_left,fa_middle,EnText.Text);
//v_draw_text(GUI_FT_count_x - 200,GUI_FT_count_y + 2,"try = " + s(glob.game_current_lvl_try),fa_left,fa_middle,EnText.Text);
//v_draw_text(GUI_FT_count_x - 300,GUI_FT_count_y + 2,"all = " + s(glob.game_points),fa_left,fa_middle,EnText.Text);

if(instance_exists(_selector))
{
    if(_selector.a_move_index > 0)
    {
        v_btns_freeze(_btn_moveback,-1,cb_gui_moveBack);
        v_btns_freeze(_btn_fturn,-1,-1);
        v_btns_set_param(_btn_moveback,"icon",spr_GUI_icon_back);
        v_btns_set_param(_btn_fturn,"icon",spr_GUI_icon_fstep_off);
        v_btns_set_param(_btn_moveback,"scalability",0.1);
        v_btns_set_param(_btn_fturn,"scalability",0);
    }
    else
    {
        v_btns_freeze(_btn_moveback,-1,-1);
        v_btns_freeze(_btn_fturn,-1,cb_gui_hint_first);
        v_btns_set_param(_btn_moveback,"icon",spr_GUI_icon_back_off);
        v_btns_set_param(_btn_fturn,"icon",spr_GUI_icon_fstep);
        v_btns_set_param(_btn_moveback,"scalability",0);
        v_btns_set_param(_btn_fturn,"scalability",0.1);
    }
}

if(room != rm_lvlCreate)
{
    if(global.a_hint_reverse_need)
    {
        v_btns_freeze(_btn_reverse,-1,cb_gui_reverse);
        v_btns_set_param(_btn_reverse,"icon",spr_GUI_icon_swap);
        v_btns_set_param(_btn_reverse,"scalability",0.1);
    }
    else
    {
        v_btns_freeze(_btn_reverse,-1,-1);
        v_btns_set_param(_btn_reverse,"icon",spr_GUI_icon_swap_off);
        v_btns_set_param(_btn_reverse,"scalability",0);
    }
}

if(instance_exists(global.inst_glb))
{
    if(global.inst_glb.game_lose)
    {
        a_retry_efx_time -= delta_time/1000000;
        if(a_retry_efx_time <= 0)
        {
            a_retry_efx_time = a_retry_efx_time_init;
            a_retry_hint += 1;
            instance_create(105,50,obj_boss_efx_click);
            instance_create(v_btns_get_param(_btn_moveback,"xpos"),v_btns_get_param(_btn_moveback,"ypos"),obj_boss_efx_click);
            if(a_retry_hint == 2)
            {
                var hint = instance_create(1,1,obj_GUI_hint_over);
                hint.h_state = 2;
                if(ds_exists(hint.h_targets,ds_type_list))
                {
                    var _x = v_btns_get_param(_btn_retry,"xpos");
                    var _y = v_btns_get_param(_btn_retry,"ypos") + 50;
                    var _arrow = instance_create(_x,_y,obj_hint_arrow);
                    _arrow.sprite_index = spr_hint_arrow_right;
                    _arrow.image_angle = 90;
                    _arrow.target_btn = _btn_retry;
                    _arrow.target_text[0] = "Начать заново";
                    ds_list_add(hint.h_targets,_arrow);
                    _x = v_btns_get_param(_btn_moveback,"xpos");
                    _y = v_btns_get_param(_btn_moveback,"ypos") + 50;
                    _arrow = instance_create(_x,_y,obj_hint_arrow);
                    _arrow.sprite_index = spr_hint_arrow_right;
                    _arrow.image_angle = 90;
                    _arrow.target_btn = _btn_moveback;
                    _arrow.target_text[0] = "Ход назад";
                    ds_list_add(hint.h_targets,_arrow);
                }
            }
        }
    }
    
    draw_set_colour(c_white);
    draw_sprite_ext(spr_GUI_hpBar,0,levelBar_x,levelBar_y,-1,-1,0,c_white,1);
    if(os_browser == browser_not_a_browser)
    {
        v_draw_text(levelBar_x - sprite_get_width(spr_GUI_hpBar)/2 - sprite_get_width(spr_GUI_btn_circle)/4,
                levelBar_y - sprite_get_height(spr_GUI_hpBar)/2,
                room_get_name(room),fa_center,fa_middle,EnText.Text);
    }
    else
    {
        v_draw_text(levelBar_x - sprite_get_width(spr_GUI_hpBar)/2 - sprite_get_width(spr_GUI_btn_circle)/4,
                levelBar_y - sprite_get_height(spr_GUI_hpBar)/2,
                "Уровень",fa_center,fa_middle,EnText.Text);
    }
    draw_sprite(spr_GUI_btn_circle,0,levelIcon_x,levelIcon_y);
    draw_set_colour(c_black);
    v_draw_text(levelIcon_x,
                levelIcon_y,
                s(global.inst_glb.game_lvl),fa_center,fa_middle,EnText.H2);


    if(ds_exists(a_event_list,ds_type_list))
    {
        var delta = delta_time/1000000;
        global.inst_glb.game_time_price -= delta;
        global.inst_glb.game_time_adv -= delta;
        if(global.inst_glb.game_time_price <= 0)
        {
            scr_event_list_add(EnEventType.Price);
        }
        if(global.inst_glb.game_time_adv <= 0)
        {
            scr_event_list_add(EnEventType.Advert);
        }
        if(global.inst_glb.game_need_repost >= 7)
        {
            global.inst_glb.game_need_repost = 0;
            scr_event_list_add(EnEventType.Repost);
        }
        
        if(ds_list_size(a_event_list) > 0)
        {
            for(var i = 0; i < ds_list_size(a_event_list); i++)
            {
                var _y = v_btns_get_param(a_event_list[| i],"ypos");
                if(_y != a_event_y_target[i])
                {
                    _y = v_increment(a_res,_y,a_event_y_target[i],a_event_y_speed,a_event_y_speed);
                    v_btns_set_param(a_event_list[| i],"ypos",_y);
                    if(a_res[0])
                    {
                        var _x = v_btns_get_param(a_event_list[| i],"xpos");
                        instance_create(_x,_y,obj_boss_efx_click);
                        scr_audio_rnd(snd_lvl_begin_1,snd_lvl_begin_2);
                    }
                }
            }
        }
    }
}
scr_gui_draw_life(heart_x,heart_y,a_heart);



action_inherited();
/// draw over

v_draw_alpha(0);


