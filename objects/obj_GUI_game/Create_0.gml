/// @description Init befor

GUI_xpos = 0;
GUI_ypos = 0;

point[EnPoint.X] = 0;
point[EnPoint.Y] = 0;
var ind = 0;
//settings                                                                          
v_choose(point,45,50,45,50);
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_circle,id,0,
                            -1,scr_s_settings,-1,-1,-1);
v_btns_set_param(GUI_controls[ind - 1],"icon",spr_GUI_icon_settings);
v_btns_set_param(GUI_controls[ind - 1],"analytick_id","gameSettings");

//price                                                                          
v_choose(point,45,115,45,115);
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_circle,id,0,
                            -1,scr_gui_game_btn,-1,-1,-1);
v_btns_set_param(GUI_controls[ind - 1],"icon",spr_GUI_icon_price);
v_btns_set_param(GUI_controls[ind - 1],"active",0);
v_btns_set_param(GUI_controls[ind - 1],"audio_in_array",0);
v_btns_set_param(GUI_controls[ind - 1],"audio_sound",snd_click_gift);
v_btns_set_param(GUI_controls[ind - 1],"analytick_id","gamePrice");
btn_price = GUI_controls[ind - 1];
//interest                                                                         
v_choose(point,45,180,45,180);
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_circle,id,0,
                            -1,scr_gui_game_btn,-1,-1,-1);
v_btns_set_param(GUI_controls[ind - 1],"icon",spr_GUI_icon_love);
v_btns_set_param(GUI_controls[ind - 1],"active",0);
v_btns_set_param(GUI_controls[ind - 1],"analytick_id","gameLike");
btn_repost = GUI_controls[ind - 1];
//ads offer                                                                         
v_choose(point,45,245,45,245);
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_circle,id,0,
                            -1,scr_gui_game_btn,-1,-1,-1);
v_btns_set_param(GUI_controls[ind - 1],"icon",spr_menu_icon_advert);
v_btns_set_param(GUI_controls[ind - 1],"active",0);
btn_advert = GUI_controls[ind - 1];

//retry                                                                         
v_choose(point,105,50,105,50);
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_circle,id,0,
                            -1,scr_win_retry,-1,-1,-1);
v_btns_set_param(GUI_controls[ind - 1],"icon",spr_GUI_icon_reverse);
_btn_retry = GUI_controls[ind - 1];
v_btns_set_param(GUI_controls[ind - 1],"analytick_id","gameRetry");
// reverse
v_choose(point,757,50,362,50);                            
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_circle,id,0,
                     -1,cb_gui_reverse,-1,-1,-1);
v_btns_set_param(GUI_controls[ind - 1],"icon",spr_GUI_icon_swap);
_btn_reverse = GUI_controls[ind - 1];

v_btns_set_param(GUI_controls[ind - 1],"audio_in_array",0);
v_btns_set_param(GUI_controls[ind - 1],"audio_sound",snd_tile_rewind);
v_btns_set_param(GUI_controls[ind - 1],"analytick_id","gameReverse");
//moveback                            
v_choose(point,697,50,302,50);                            
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_circle,id,0,
                     -1,cb_gui_moveBack,-1,-1,-1);   
v_btns_set_param(GUI_controls[ind - 1],"icon",spr_GUI_icon_back);
v_btns_set_param(GUI_controls[ind - 1],"analytick_id","gameMoveback");

_btn_moveback = GUI_controls[ind - 1];

GUI_back_count_x = point[EnPoint.X] - 47;
GUI_back_count_y = point[EnPoint.Y] - 5;

//first turn                           
v_choose(point,600,50,205,50);                            
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_circle,id,0,
                     -1,cb_gui_hint_first,-1,-1,-1);   
v_btns_set_param(GUI_controls[ind - 1],"icon",spr_GUI_icon_fstep);
v_btns_set_param(GUI_controls[ind - 1],"analytick_id","gameFStep");
_btn_fturn = GUI_controls[ind - 1];

GUI_FT_count_x = point[EnPoint.X] - 47;
GUI_FT_count_y = point[EnPoint.Y] - 5;

//heart
v_choose(point,45,560,45,680);
heart_x = point[EnPoint.X];
heart_y = point[EnPoint.Y];
// +
GUI_controls[ind++] = v_btns_add(heart_x + 138,heart_y,spr_GUI_btn_circle_mini,id,0,
                                    -1,scr_s_btn_add_xp,-1,-1,-1);
v_btns_set_text(GUI_controls[ind - 1],0,-2,"+",1,c_black,0,f_H2,f_H2,1);

if(STUDY_ADD)
{
    //study                                                                         
    v_choose(point,45,560,45,680);
    GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_circle,id,0,
                                -1,scr_study_btn_cb,-1,-1,-1);
    v_btns_set_param(GUI_controls[ind - 1],"icon",spr_GUI_icon_question);
    v_btns_set_param(GUI_controls[ind - 1],"analytick_id","gameStudy");
    _btn_study = GUI_controls[ind - 1];
    
    instance_create(point[EnPoint.X],point[EnPoint.Y],obj_boss_efx_click);
}

_selector = noone;
if(instance_exists(global.a_selector))
{
    _selector = global.a_selector;
}

// level bar
v_choose(point,755,560,360,680);
levelIcon_x = point[EnPoint.X];
levelIcon_y = point[EnPoint.Y];
levelBar_x = levelIcon_x;
levelBar_y = levelIcon_y + sprite_get_height(spr_GUI_hpBar)/2;





action_inherited();
///Anim

a_res[0] = 0;
a_point[0] = 0;
for(var i = 0; i < EnAnimParam.LENGTH; i++)
{
    a_heart[i] = 0;
}

a_heart[EnAnimParam.initVal] = 1;
a_heart[EnAnimParam.Val] = a_heart[EnAnimParam.initVal];

a_retry_efx_time_init = 3;
a_retry_efx_time = 1;
a_retry_hint = 0;

a_event_time_max = 90;
a_event_time_min = 30;
a_event_timer = irandom_range(a_event_time_min,a_event_time_max);

a_event_list = ds_list_create();

a_event_chance[EnEventType.Price] = 0.4; // price
a_event_chance[EnEventType.Advert] = 0.75; // advert
a_event_chance[EnEventType.Repost] = 1; // repost

a_event_inst[EnEventType.Price] = btn_price;
a_event_inst[EnEventType.Advert] = btn_advert;
a_event_inst[EnEventType.Repost] = btn_repost;

a_event_y_speed = 6;
a_event_y_init = __view_get( e__VW.HView, 0 )/2 + __view_get( e__VW.YView, 0 );
a_event_y_target[0] = 115;
a_event_y_target[1] = 180;
a_event_y_target[2] = 245;

/*var _need_hint = 0;
if(os_browser == browser_not_a_browser)
{
    if(room - rm_lvlCreate == 4)// || glob.game_lvl_to_load == 4)
    {
        _need_hint = 1;
    }
}
else
{
    if(room - rm_lvlCreate == 4)
    {
        _need_hint = 1;
    }
}*/

if(room - rm_lvlCreate == 11 && !glob.game_hint_reverse)
{
    var hint = instance_create(1,1,obj_GUI_hint_over);
    hint.h_state = 2;
    if(ds_exists(hint.h_targets,ds_type_list))
    {
        var _x = v_btns_get_param(_btn_reverse,"xpos");
        var _y = v_btns_get_param(_btn_reverse,"ypos") + 50;
        var _arrow = instance_create(_x,_y,obj_hint_arrow);
        _arrow.sprite_index = spr_hint_arrow_right;
        _arrow.image_angle = 90;
        _arrow.target_btn = _btn_reverse;
        _arrow.target_text[0] = "Посмотреть #нижний уровень";
        _arrow.target_text_halign[0] = fa_right;
        _arrow.target_text[1] = "Еще раз!";
        _arrow.target_text_halign[1] = fa_right;
        _arrow.state_max = array_length_1d(_arrow.target_text);
        if(global._def_glb_var_default_resolution_pc)
        {
            _arrow.tile_path = p_hint_reverse;
        }
        else
        {
            _arrow.tile_path = p_hint_reverse_mob;
        }

        ds_list_add(hint.h_targets,_arrow);
    }
    glob.game_hint_reverse = 1;
}
else
{
    if(room - rm_lvlCreate == 2 && !glob.game_hint_study && STUDY_ADD)
    {
        var hint = instance_create(1,1,obj_GUI_hint_over);
        hint.h_state = 2;
        if(ds_exists(hint.h_targets,ds_type_list))
        {
            var _x = v_btns_get_param(_btn_study,"xpos") + 50;
            var _y = v_btns_get_param(_btn_study,"ypos");
            var _arrow = instance_create(_x,_y,obj_hint_arrow);
            _arrow.sprite_index = spr_hint_arrow_right;
            _arrow.image_angle = 180;
            _arrow.target_btn = _btn_study;
            _arrow.target_text[0] = "Все о том #как играть";
            _arrow.target_text_halign[0] = fa_left;
            _arrow.state_max = array_length_1d(_arrow.target_text);    
            ds_list_add(hint.h_targets,_arrow);
        }
        glob.game_hint_study = 1;
    }
    else
    {
        if(!(glob.game_current_lvl_try % 3) && glob.game_current_lvl_try != 0 &&
                glob.game_hint_fstep == 0)
        {
            var hint = instance_create(1,1,obj_GUI_hint_over);
            hint.h_state = 2;
            if(ds_exists(hint.h_targets,ds_type_list))
            {
                var _x = v_btns_get_param(_btn_fturn,"xpos");
                var _y = v_btns_get_param(_btn_fturn,"ypos") + 50;
                var _arrow = instance_create(_x,_y,obj_hint_arrow);
                _arrow.sprite_index = spr_hint_arrow_right;
                _arrow.image_angle = 90;
                _arrow.target_btn = _btn_fturn;
                _arrow.target_text[0] = "Подсказка#первого хода";
                ds_list_add(hint.h_targets,_arrow);
            }
        }
    }
}



/* */
/*  */
