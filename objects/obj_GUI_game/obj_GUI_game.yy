{
    "id": "34cb6453-2c82-4632-9938-67687cb37721",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_game",
    "eventList": [
        {
            "id": "687abd2d-b897-459a-aadc-69f6983d1751",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "34cb6453-2c82-4632-9938-67687cb37721"
        },
        {
            "id": "bc411c35-1946-435a-a730-fc6d416d0708",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "34cb6453-2c82-4632-9938-67687cb37721"
        },
        {
            "id": "bfac7394-275d-49b8-83f2-ad23cb85727e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "34cb6453-2c82-4632-9938-67687cb37721"
        },
        {
            "id": "75b124f2-b15b-4a33-8e36-2378dece4520",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "34cb6453-2c82-4632-9938-67687cb37721"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d6960cef-2d41-4b43-b2e6-96945303e7a7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}