/// @description  draw shop

if(!OPT_cantDraw)
{
    anim_scale = v_path_anim(anim_res,anim_scale,p_hint_wave,
    anim_point,anim_scale_speed,anim_scale_delta);
    
    if(sprite_exists(OPT_back))
    {
        draw_sprite_ext(OPT_back,0,x,y,anim_scale,anim_scale,0,c_white,1);
    }
    if(anim_res[0])
    {
        var tmp_alpha = draw_get_alpha();
        draw_set_alpha(OPT_alpha);
        for(var i = 0; i < array_length_1d(OPT_controls_type); i += 1)
        {
            v_btns_draw_btn(OPT_controls_type[i]);
        }
        draw_set_font(f_Game_timer);
        draw_set_halign(fa_center);
        draw_set_valign(fa_middle);
        draw_set_colour(global.col_main);
        var str = h_str[h_index];
        var spr = h_sprite[h_index];
        var str_w = string_height(string_hash_to_newline("QWERTYUIOPASDFGHJKLZXCVBNM"));
        draw_text_ext(x,y - 65,string_hash_to_newline(str),str_w,350);
        draw_sprite(spr,0,OPT_img_x,OPT_img_y);
        draw_set_alpha(tmp_alpha);
        if(OPT_alpha < 1)OPT_alpha += OPT_alpha_speed;
    }
}



