/// @description  init
var ind = 0;
OPT_btn_spr = spr_end_Next;
OPT_back = spr_hint_back;
OPT_btn_font = f_End_timer;
OPT_need_close = 0;
var OPT_btn_spr_tmp = OPT_btn_spr;
var OPT_back_tmp = OPT_back;
var OPT_btn_font_tmp = OPT_btn_font;

x = 184 + __view_get( e__VW.WView, 0 )/2 - sprite_get_width(OPT_back)/2 + __view_get( e__VW.XView, 0 );
y = 139 + __view_get( e__VW.HView, 0 )/2 - sprite_get_height(OPT_back)/2 + __view_get( e__VW.YView, 0 );;
OPT_y_tmp = y + sprite_get_height(OPT_back) - sprite_get_height(OPT_btn_spr) - 80;

glob = noone;
player_lvl = 0;
lvl_type = -1;
if(instance_exists(global.inst_glb))
{
    glob = global.inst_glb;
}

OPT_controls_type[ind++] = v_btns_add(0,0,spr_btn_ok, id,0, 
              NO_SCRIPT,
              scr_win_ok,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
OPT_controls_type[ind++] = v_btns_add(0,0,spr_end_Next, id,0, 
              NO_SCRIPT,
              scr_win_hint_next,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );  
OPT_controls_type[ind++] = v_btns_add(0,0,spr_end_Prev, id,0, 
              NO_SCRIPT,
              scr_win_hint_prev,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );                         
ind = 0;        
OPT_controls_x[ind] = x;
OPT_controls_y[ind++] = y + 140; 
OPT_controls_x[ind] = x + 100;
OPT_controls_y[ind++] = y + 120; 
OPT_controls_x[ind] = x - 150;
OPT_controls_y[ind++] = y + 120; 
//////////////////////////////////////////////////////////////////////////////////////////
event_inherited();
//////////////////////////////////////////////////////////////////////////////////////////

OPT_btn_spr = OPT_btn_spr_tmp;
OPT_back = OPT_back_tmp;
OPT_btn_font = OPT_btn_font_tmp;

OPT_img_x = x;
OPT_img_y = y - 10;

var i = 0;
h_str[i] = "Goal: make all the tiles light at the same time.#Tip: use Diamond tiles to light up all other tiles.";
h_sprite[i++] = spr_hint_1;
h_str[i] = "Tip: to discover hidden tile, light the tile nearby.";
h_sprite[i++] = spr_hint_2; 
h_str[i] = "Tip: sometimes you can face fixed tiles, they look different and couldn`t be turned.";
h_sprite[i++] = spr_hint_3; 
h_str[i] = "Tip: there are multi-input tiles, that means they require more than one energy to light up.";
h_sprite[i++] = spr_hint_4; 
h_str[i] = "Tip: some tiles can hide a light source inside, once lighted up they become a source.";
h_sprite[i++] = spr_hint_5; 

h_index = 0;


/// game logic


anim_res[0]         = false;
anim_point[0]       = 0;
anim_scale          = 0;
anim_scale_speed    = 0.14;
anim_scale_delta    = 0.01;




