
if(global.user_open_all)instance_destroy();

if(alpha_start)
{
    var tmp_alpha = draw_get_alpha();
    draw_set_alpha(xp_alpha);
    v_btns_freeze(xp_add_btn,0,NO_SCRIPT);
}

draw_self();

if(global.user_lvl >= 0 && global.user_lvl < array_length_1d(global.user_lvl_amounts) - 1)
{
    xp_amount = (global.user_lvl_amount - global.user_lvl_amounts[global.user_lvl])/
    (global.user_lvl_amounts[global.user_lvl + 1] - global.user_lvl_amounts[global.user_lvl]);
}
else
{
    xp_amount = 1;
}

draw_sprite_part(sprite_index,1,0,0,(sprite_get_width(sprite_index) - 30)*xp_amount,sprite_get_height(sprite_index),x,y);
draw_sprite_ext(spr_xp_icon,0,x - sprite_get_width(spr_xp_icon)*0.2/2,y + sprite_get_height(sprite_index)/2,0.35,0.35,0,c_white,xp_alpha);
draw_set_font(f_a_user_lvl_small);
draw_set_colour(c_white);
draw_set_valign(fa_middle);
draw_set_halign(fa_center);
draw_text(x- sprite_get_width(spr_xp_icon)*0.2/2,y + sprite_get_height(sprite_index)/2,string_hash_to_newline(s(global.user_lvl + 1)));
if(!alpha_start)
{
    v_btns_draw_btn(xp_add_btn);
}
else
{
    draw_sprite(spr_xp_add_icon,0,x + sprite_get_width(sprite_index) + 10,y + sprite_get_height(sprite_index)/2);
}

if(alpha_start)
{
    draw_set_alpha(tmp_alpha);
    if(xp_alpha < 1)xp_alpha += xp_alpha_speed;
}

