{
    "id": "f0308e96-3ed3-41f6-9ed0-e5d1815f57b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_def_btn_play",
    "eventList": [
        {
            "id": "dea0e36d-26e9-4626-858c-74e98c73548f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "f0308e96-3ed3-41f6-9ed0-e5d1815f57b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e5a4ac43-08ab-4d0f-b785-5a926b071609",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "dd4f6510-9cf2-41e0-871d-2d0499bfb299",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "63eaa3dd-a572-4d8a-81a0-972837e2ea1f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 960
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}