{
    "id": "1ea591a4-e22f-4ec5-8b6d-7254b77870ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_start",
    "eventList": [
        {
            "id": "5b8e8d00-e9ec-4265-af59-b7ca3424b321",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1ea591a4-e22f-4ec5-8b6d-7254b77870ce"
        },
        {
            "id": "13f2f5c0-4add-40e9-a436-d05adb1c30af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1ea591a4-e22f-4ec5-8b6d-7254b77870ce"
        },
        {
            "id": "b20a753b-be09-4839-b4fd-3d326b0597bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "1ea591a4-e22f-4ec5-8b6d-7254b77870ce"
        },
        {
            "id": "b8d7fa42-ab8d-47bc-9b5c-87631f256448",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1ea591a4-e22f-4ec5-8b6d-7254b77870ce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}