/// @description  draw shop

if(!OPT_cantDraw)
{
    anim_scale = v_path_anim(anim_res,anim_scale,p_win_wave,
    anim_point,anim_scale_speed,anim_scale_delta);
    
    if(sprite_exists(OPT_back))
    {
        draw_sprite_ext(OPT_back,0,x,y,anim_scale,anim_scale,0,c_white,1);
    }
    if(anim_res[0])
    {
        var tmp_alpha = draw_get_alpha();
        draw_set_alpha(OPT_alpha);
        for(var i = 0; i < array_length_1d(OPT_controls_type); i += 1)
        {
            v_btns_draw_btn(OPT_controls_type[i]);
        }
        v_draw_text(OPT_header_x,OPT_header_y,OPT_header_text,fa_center,fa_middle,EnText.H2);
        draw_sprite(OPT_lose_icon_spr,0,OPT_lose_icon_x,OPT_lose_icon_y);   
        draw_set_alpha(tmp_alpha);
        if(OPT_alpha < 1)OPT_alpha += OPT_alpha_speed;
    }
}



