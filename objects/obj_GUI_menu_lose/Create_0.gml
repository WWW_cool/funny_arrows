/// @description  init
var ind = 0;
OPT_btn_spr = spr_menu_btn_small;
OPT_back = spr_menu_bg_small;
OPT_btn_font = f_text;
OPT_need_close = 0;
var OPT_btn_spr_tmp = OPT_btn_spr;
var OPT_back_tmp = OPT_back;
var OPT_btn_font_tmp = OPT_btn_font;



x = __view_get( e__VW.WView, 0 )/2 + __view_get( e__VW.XView, 0 );
y = __view_get( e__VW.HView, 0 )/2 + __view_get( e__VW.YView, 0 );;

OPT_header_x = x;
OPT_header_y = y - 60;
OPT_header_font = f_H2;
OPT_header_text = "Переиграть?"

OPT_lose_icon_x = x;
OPT_lose_icon_y = y - 130;
OPT_lose_icon_spr = spr_GUI_icon_fail;

OPT_y_tmp = y + sprite_get_height(OPT_back) - sprite_get_height(OPT_btn_spr) - 80;

glob = noone;
player_lvl = 0;
lvl_type = -1;
if(instance_exists(global.inst_glb))
{
    glob = global.inst_glb;
}

OPT_controls_type[ind++] = v_btns_add(0,0,OPT_btn_spr, id,0, 
              NO_SCRIPT,
              scr_win_retry,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
v_btns_set_param(OPT_controls_type[ind - 1],"icon",spr_GUI_icon_next);              
//v_btns_set_text(OPT_controls_type[ind - 1],0,-2,
//"Переиграть",1,c_black,0,f_H2,f_H2,1);

OPT_controls_type[ind++] = v_btns_add(0,0,OPT_btn_spr, id,0, 
              NO_SCRIPT,
              scr_s_btn_menu,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
              
v_btns_set_param(OPT_controls_type[ind - 1],"icon",spr_GUI_icon_menu);              
//v_btns_set_text(OPT_controls_type[ind - 1],0,-2,
//"Ход назад (15)",1,c_black,0,f_H2,f_H2,1);
                       
ind = 0;        
OPT_controls_x[ind] = x + 80;
OPT_controls_y[ind++] = y + 35;

OPT_controls_x[ind] = x - 80;
OPT_controls_y[ind++] = y + 35; 

//////////////////////////////////////////////////////////////////////////////////////////
event_inherited();
//////////////////////////////////////////////////////////////////////////////////////////

OPT_btn_spr = OPT_btn_spr_tmp;
OPT_back = OPT_back_tmp;
OPT_btn_font = OPT_btn_font_tmp;




/// game logic


anim_res[0]         = false;
anim_point[0]       = 0;
anim_scale          = 0;
anim_scale_speed    = 0.14;
anim_scale_delta    = 0.01;

scr_def_play_snd(snd_lvl_lose);


