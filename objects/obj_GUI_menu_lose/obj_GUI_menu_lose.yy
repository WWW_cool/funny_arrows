{
    "id": "1f1747ba-ecbd-4387-a8eb-8c1d519775e4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_menu_lose",
    "eventList": [
        {
            "id": "a7232784-22ab-4505-9b31-c667d45b2524",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f1747ba-ecbd-4387-a8eb-8c1d519775e4"
        },
        {
            "id": "36494fde-1cb3-4033-b4e2-68d5b65716ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1f1747ba-ecbd-4387-a8eb-8c1d519775e4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0f12a9c8-0859-422b-865b-2665b2ddf681",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}