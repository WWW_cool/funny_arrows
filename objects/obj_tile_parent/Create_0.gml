/// @description  tile init
image_speed = 0;

enum EnTileTypes{HALL = 0,CORNER,T,CROSS,LENGTH};
enum EnTileStates{IDLE = 0,HIDED,ROTATE,LENGTH};
enum EnTileAngles{RIGHT = 0,UP,LEFT,DOWN,LENGTH};

t_type      = EnTileTypes.HALL;
t_sprite    = noone;
t_isdraw    = true;

t_atr_fixed         = 0;
t_atr_light_need    = 1;
t_atr_source        = 0;
t_atr_hiden_source  = 0;
t_atr_rotated       = 0;
t_atr_boss          = 0;
t_atr_invisible     = 0;

t_state = EnTileStates.IDLE;
t_angle = image_angle;
t_angle_speed       = 0.3;
t_angle_target      = 0;
t_angle_delta       = 5;

t_lerp_res[0]       = 0;

t_alpha             = 0;
t_alpha_toggle      = 1;
t_alpha_speed_up    = 0.1;
t_alpha_speed_down  = 0.05;
t_alpha_delta       = 0.02;

t_unhide_anim       = 0;
t_hide_scale        = 1;
t_hide_scale_speed  = 0.3;
t_hide_scale_delta  = 0.1;
t_hide_alpha        = 0;
t_hide_alpha_speed  = 0.2;
t_hide_alpha_delta  = 0.05;

t_sys_anim          = 0;
t_sys_uncontrol     = 0;
t_sys_uncontrol_time = room_speed*2;
t_sys_lighted       = 0;
t_sys_master_check  = 0;
t_sys_master_count  = 0;
t_sys_range = sprite_get_width(spr_block_Back);

t_side_allow[EnTileAngles.RIGHT]    = 0;
t_side_allow[EnTileAngles.UP]       = 90;
t_side_allow[EnTileAngles.LEFT]     = 180;
t_side_allow[EnTileAngles.DOWN]     = 270;




///t_source anim


t_src_core_rot          = 0;
t_src_core_rot_speed    = 1;

t_src_anim_pause = 5*room_speed;
t_src_anim_up           = 1;
t_src_anim_down         = 0;

t_src_scale = 1;
t_src_scale_up = 0.2;
t_src_scale_init = 1;

t_src_scale_speed_up = 0.03;
t_src_scale_speed_down = 0.03;

t_src_anim_ind = 0;
t_src_anim_ind_max = 6;
t_src_anim_ind_speed = 0.5;




script_execute(gui_scope_default,0,0,0,0,0);
