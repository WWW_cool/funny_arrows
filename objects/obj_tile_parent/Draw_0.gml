/// @description Рисуем все режимы плитки + вычисления
var light_spr = scr_get_light_spr(t_atr_light_need);
var light_state = scr_get_light_state(t_atr_light_need,0,t_sys_master_count);
if(t_isdraw)
{
    if(!t_atr_source)
    {
        draw_sprite_ext(spr_block_Back,t_atr_fixed,x,y,1,1,t_angle,c_white,1);
        draw_sprite_ext(t_sprite, 0,x,y,1,1,t_angle,c_white,1);
        draw_sprite_ext(t_sprite, 1,x,y,1,1,t_angle,c_white,t_alpha);
        draw_sprite_ext(light_spr,light_state,x,y,1,1,t_angle,c_white,1);
        draw_sprite_ext(light_spr,scr_get_light_state(t_atr_light_need,1,t_sys_master_count),x,y,1,1,t_angle,c_white,t_alpha);
        draw_sprite_ext(light_spr,scr_get_light_state(t_atr_light_need,2,t_sys_master_count),x,y,1,1,t_angle,c_white,t_alpha);
    }
    else
    {
        draw_sprite_ext(spr_block_Back,t_atr_fixed,x,y,1,1,t_angle,c_white,1);
        draw_sprite_ext(t_sprite, 0,x,y,1,1,t_angle,c_white,1);
        draw_sprite_ext(t_sprite, 1,x,y,1,1,t_angle,c_white,t_alpha);
        draw_sprite_ext(spr_source_back,t_src_anim_ind,x,y,t_src_scale,t_src_scale, t_angle,c_white,1);
        draw_sprite_ext(spr_source_core,0,x,y,t_src_scale,t_src_scale,t_angle + t_src_core_rot,c_white,1);
    }
}
else
{
    if(!t_atr_invisible)
    {
        draw_sprite_ext(spr_block_Back,t_atr_fixed,x,y,1,1,t_angle,c_white,1);
        draw_sprite_ext(t_sprite, 0,x,y,1,1,t_angle,c_white,t_hide_alpha);
        draw_sprite_ext(light_spr,light_state,x,y,1,1,t_angle,c_white,t_hide_alpha);
        draw_sprite_ext(spr_block_hiden,0,x,y,t_hide_scale,t_hide_scale,0,c_white,1);
    }
}



