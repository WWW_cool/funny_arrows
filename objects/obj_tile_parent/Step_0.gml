t_isdraw = true;
switch(t_state)
{
    case EnTileStates.HIDED:
        t_isdraw = false;
        if(t_unhide_anim)
        {
            var unhide_res = true;
            t_hide_scale = v_lerp(t_lerp_res,t_hide_scale,0,t_hide_scale_speed,t_hide_scale_delta);
            if(!t_lerp_res[0])unhide_res = false;
            t_hide_alpha = v_lerp(t_lerp_res,t_hide_alpha,1,t_hide_alpha_speed,t_hide_alpha_delta);
            if(!t_lerp_res[0])unhide_res = false;
            if(unhide_res || t_atr_invisible)
            {
                t_state = EnTileStates.IDLE;
            }
        }
    break;
    case EnTileStates.IDLE:
        if(t_sys_lighted)
        {
            if(t_alpha_toggle)
            {
                t_alpha = v_lerp(t_lerp_res,t_alpha,1,t_alpha_speed_up,t_alpha_delta);
                if(t_lerp_res[0])t_alpha_toggle = 0;
            }
            else
            {
                t_alpha = v_lerp(t_lerp_res,t_alpha,0.5,t_alpha_speed_down,t_alpha_delta);
                if(t_lerp_res[0])t_alpha_toggle = 1;
            }
        }
        else
        {
            t_alpha = v_lerp(t_lerp_res,t_alpha,0,t_alpha_speed_up,t_alpha_delta);
            t_alpha_toggle = 1;
        }
        
        if(t_atr_source)
        {
            t_src_scale_init = v_lerp(t_lerp_res,t_src_scale_init,0.7,0.1,0.05);
            t_src_core_rot += t_src_core_rot_speed; 
            if(t_src_anim_up)
            {
                if(t_src_scale < t_src_scale_init + t_src_scale_up)
                {
                    t_src_scale += t_src_scale_speed_up; 
                }
                else 
                {
                    t_src_scale = t_src_scale_init + t_src_scale_up;
                    if(t_src_scale == t_src_scale_init + t_src_scale_up)
                    {
                        t_src_anim_up = 0;
                        t_src_anim_down = 1;
                        t_src_anim_ind = 0;
                        //scr_Play_snd(snd_Main_down);
                    }
                }
            }
            else
            {
                if(t_src_anim_down)
                {
                    if(t_src_scale > t_src_scale_init)
                    {
                        t_src_scale -= t_src_scale_speed_down; 
                    }
                    else t_src_scale = t_src_scale_init;
                    t_src_anim_ind += t_src_anim_ind_speed;
                    t_src_anim_ind %= t_src_anim_ind_max;
                    
                    if(!t_src_anim_ind)
                    {
                        if(t_src_scale == t_src_scale_init)
                        {
                            t_src_anim_ind = 0;
                            t_src_anim_down = 0;
                            alarm[2] = irandom_range(t_src_anim_pause/2,t_src_anim_pause);
                        }
                    }
                }
            }
        }
    break;
    case EnTileStates.ROTATE:
        t_angle = v_lerp(t_lerp_res,t_angle,t_angle_target,t_angle_speed,t_angle_delta);
        if(t_lerp_res[0])
        {
            t_angle = t_angle div 90;
            t_angle *= 90;
            t_angle %= 360;
            scr_T_update();
            t_state = EnTileStates.IDLE;
        }
    break;
}

image_angle = t_angle;



script_execute(gui_scope_step,0,0,0,0,0);
