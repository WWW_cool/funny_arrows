{
    "id": "043ac27a-7f84-4c0a-8536-50ffa9c8b21a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tile_parent",
    "eventList": [
        {
            "id": "8212afce-270e-489b-851d-331eab65e477",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "043ac27a-7f84-4c0a-8536-50ffa9c8b21a"
        },
        {
            "id": "d8f46969-e525-488a-8642-76ee094cfda2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "043ac27a-7f84-4c0a-8536-50ffa9c8b21a"
        },
        {
            "id": "669707ef-13e0-4cc8-9fe4-e49a496e62b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "043ac27a-7f84-4c0a-8536-50ffa9c8b21a"
        },
        {
            "id": "397a71f8-faa1-4330-bd93-1a20a7f69458",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "043ac27a-7f84-4c0a-8536-50ffa9c8b21a"
        },
        {
            "id": "2e492e0c-b542-47fd-ae93-ebfb69bccde0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "043ac27a-7f84-4c0a-8536-50ffa9c8b21a"
        },
        {
            "id": "38f98264-e730-4320-ac5a-95b5b0ae54f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "043ac27a-7f84-4c0a-8536-50ffa9c8b21a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}