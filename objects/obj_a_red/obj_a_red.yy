{
    "id": "493ca7b9-e7c2-4aae-b471-c9ec70d9149e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_a_red",
    "eventList": [
        {
            "id": "e53f1af5-8415-45cc-b19c-cb1df327c2ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "493ca7b9-e7c2-4aae-b471-c9ec70d9149e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b50af0a4-6678-4621-a140-198f86a6df59",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c390555-84ff-4ad8-9ee3-b05942997892",
    "visible": true
}