/// @description DEBUG MESSAGES
if(DEBUG)
{
    draw_text_colour(5,300,string_hash_to_newline("Debug: fps = " + string(fps) + " fps real = " + string(fps_real)),
    c_black,c_black,c_black,c_black,1);
    draw_text_colour(5,315,string_hash_to_newline("Room: " + room_get_name(room)),
    c_black,c_black,c_black,c_black,1);
}

