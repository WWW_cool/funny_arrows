{
    "id": "23b1fb37-6905-433e-bcbd-8f2ec060f1f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_def_debug",
    "eventList": [
        {
            "id": "1bb3ad1d-6872-4e4f-8305-41dd0af70480",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "23b1fb37-6905-433e-bcbd-8f2ec060f1f7"
        },
        {
            "id": "17b237c5-769f-4893-bbfe-4e09f810a9fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "23b1fb37-6905-433e-bcbd-8f2ec060f1f7"
        },
        {
            "id": "0aedb226-e592-45f4-9377-751854633bde",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 80,
            "eventtype": 9,
            "m_owner": "23b1fb37-6905-433e-bcbd-8f2ec060f1f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}