{
    "id": "da02ced4-5d25-4470-874c-e6f757952565",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_a_multi",
    "eventList": [
        {
            "id": "2b62235b-8f4c-4558-8b26-fe658746e61a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "da02ced4-5d25-4470-874c-e6f757952565"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b50af0a4-6678-4621-a140-198f86a6df59",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "03c11e00-5e3d-4f49-8bb0-367bb9adfb79",
    "visible": true
}