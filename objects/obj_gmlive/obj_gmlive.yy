{
    "id": "33634c94-b392-4387-9e8a-14a6330a1256",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gmlive",
    "eventList": [
        {
            "id": "f52d0cec-e340-4004-a9eb-dcb28e689c6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "33634c94-b392-4387-9e8a-14a6330a1256"
        },
        {
            "id": "92ac39e8-f361-4112-9053-9f608ba037ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "33634c94-b392-4387-9e8a-14a6330a1256"
        },
        {
            "id": "d4fb21ef-57be-4cc8-a777-0aaf2f2b76e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 62,
            "eventtype": 7,
            "m_owner": "33634c94-b392-4387-9e8a-14a6330a1256"
        },
        {
            "id": "75c8f044-3431-4a76-809d-6e690b2f6129",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "33634c94-b392-4387-9e8a-14a6330a1256"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}