{
    "id": "d0831018-656e-4809-add5-f7eee6e89b33",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_menu_API",
    "eventList": [
        {
            "id": "6c63fa65-8cde-4509-b00f-cd0678214772",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d0831018-656e-4809-add5-f7eee6e89b33"
        },
        {
            "id": "756dd2be-563f-47bd-a9ed-7b3ad69fe050",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d0831018-656e-4809-add5-f7eee6e89b33"
        },
        {
            "id": "2c5e64ee-f013-433d-975b-2dd85f871a59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d0831018-656e-4809-add5-f7eee6e89b33"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0f12a9c8-0859-422b-865b-2665b2ddf681",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}