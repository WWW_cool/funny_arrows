{
    "id": "4c8f3e04-0542-4666-83b8-cdcfdccbe609",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_preBoss",
    "eventList": [
        {
            "id": "6f570ce3-643a-4da3-abcf-f9141acc1d9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4c8f3e04-0542-4666-83b8-cdcfdccbe609"
        },
        {
            "id": "f9520d4d-4236-4163-a3a2-c2104900dbfc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4c8f3e04-0542-4666-83b8-cdcfdccbe609"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d6960cef-2d41-4b43-b2e6-96945303e7a7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}