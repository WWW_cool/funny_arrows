/// @description Init

randomize();

GUI_xpos = x;
GUI_ypos = y;

point[EnPoint.X] = 0;
point[EnPoint.Y] = 0;

var ind = 0;

/// btn start
v_choose(point,400,300,202,350);
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_hit,id,0,
                            -1,scr_boss_start,-1,-1,-1);            
v_btns_set_text(GUI_controls[ind - 1],0,0,
"Начать",1,c_black,0,f_H2,f_H2,1);

/// btn skip
v_choose(point,400,380,202,430);
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_hit,id,0,
                            -1,scr_win_next,-1,-1,-1);            
v_btns_set_text(GUI_controls[ind - 1],0,0,
"Пропустить",1,c_black,0,f_H2,f_H2,1);

// price icons
price_count = 3;
price_max = 3;
price_icons[0] = spr_GUI_icon_heart;
price_icons[1] = spr_GUI_icon_rating;
price_icons[2] = spr_GUI_icon_back;

var x_delta = 110;
price_x[0] = -x_delta;
price_x[1] = 0;
price_x[2] = x_delta;

v_choose(point,400,200,202,250);
var _index = 0;
for(var i = 0; i < price_count; i += 1)
{
    price_item_count[i] = irandom_range(1,price_max);
    if(price_item_count[i])
    {
        GUI_controls[ind++] = v_btns_add(point[EnPoint.X]+ price_x[_index],point[EnPoint.Y],spr_GUI_btn_circle,id,0,
                            -1,-1,-1,-1,-1);
        price_btn[i] = GUI_controls[ind - 1];
        v_btns_set_param(GUI_controls[ind - 1],"icon",price_icons[i]);
        _index++
    }
}

v_choose(point,400,120,202,170);
header_x = point[EnPoint.X];
header_y = point[EnPoint.Y];





action_inherited();
