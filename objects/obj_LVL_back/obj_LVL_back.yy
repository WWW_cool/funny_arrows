{
    "id": "8d3b4bc5-8b9f-4940-a525-c666a0918aec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_LVL_back",
    "eventList": [
        {
            "id": "fc9e5387-4bc7-4648-901f-c526bf5d4bc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d3b4bc5-8b9f-4940-a525-c666a0918aec"
        },
        {
            "id": "a5372bb4-6fe3-4f48-b36b-456be2e60615",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8d3b4bc5-8b9f-4940-a525-c666a0918aec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "34973a45-be29-4bd4-ba9b-698102ded637",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 1,
            "y": 1
        },
        {
            "id": "af7d63ad-bc46-46c4-8609-7d23539bfd65",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 1,
            "y": 1
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}