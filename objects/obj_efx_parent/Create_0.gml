/// @description init

efx_res[0] = 1;

efx_alpha = 1;
efx_alpha_speed = 0.1;

efx_scale = 0;
efx_scale_speed = 0.1;
efx_scale_max = 3;

image_speed = 0;

efx_type = 0;
efx_angle = 0;
efx_angle_speed = 0.3;
efx_angle_max = irandom_range(360,720);

//efx_life_time = random_range(1,2);
efx_child_max = 3;
efx_child_count = irandom_range(1,efx_child_max);

for(var i = 0; i < efx_child_max; i++)
{
    efx_child[i] = noone;
}




