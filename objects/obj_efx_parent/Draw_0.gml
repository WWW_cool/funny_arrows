/// @description  draw efx

if(sprite_exists(sprite_index))
{
    switch(efx_type)
    {
        case 1:
            if(efx_child_count)
            {
                for(var i = 0; i < efx_child_max; i++)
                {
                    if(!instance_exists(efx_child[i]))
                    {
                        efx_child[i] = instance_create(x,y,obj_boss_efx_circle);
                        efx_child_count--;
                    }
                }
            }
            efx_angle = v_lerp(efx_res,efx_angle,efx_angle_max,efx_angle_speed,efx_angle_speed/5);
            efx_scale = v_increment(efx_res,efx_scale,1,efx_scale_speed,efx_scale_speed/5);
            if(efx_res[0])
            {
                efx_alpha = v_increment(efx_res,efx_alpha,0,efx_alpha_speed,efx_alpha_speed/5);
                if(efx_res[0])
                {
                    for(var i = 0; i < efx_child_max; i++)
                    {
                        if(instance_exists(efx_child[i]))
                        {
                            with(efx_child[i])
                            {
                                instance_destroy();
                            }
                        }
                    }
                    instance_destroy();
                }
            }
        break;
        default:
            efx_scale = v_increment(efx_res,efx_scale,efx_scale_max,efx_scale_speed,efx_scale_speed/5);
            if(efx_scale > 1)
            {
                efx_alpha = v_increment(efx_res,efx_alpha,0,efx_alpha_speed,efx_alpha_speed/5);
                if(efx_res[0])
                {
                    instance_destroy();
                }
            }
    }
    
    draw_sprite_ext(sprite_index,image_index,x,y,efx_scale,efx_scale,efx_angle,c_white,efx_alpha);
}



