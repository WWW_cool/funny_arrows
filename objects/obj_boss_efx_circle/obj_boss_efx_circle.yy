{
    "id": "9f870642-2fc4-4704-a525-1e21858f5402",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss_efx_circle",
    "eventList": [
        {
            "id": "34e986ef-13e2-474e-a053-91d0a8ba1c0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9f870642-2fc4-4704-a525-1e21858f5402"
        },
        {
            "id": "f67abb05-7eac-490a-a727-6e6a0c4bfea1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9f870642-2fc4-4704-a525-1e21858f5402"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6fc41001-0fc1-4de0-95a9-bf904d16ba79",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}