/// @description Init parent obj for all btns objects
image_speed = 0;

_def_btn_x = 0;
_def_btn_y = 0;

_def_btn_spr = noone;
_def_btn_spr_index_init = 0;
_def_btn_spr_index = 0;
_def_btn_scale_x = 1;
_def_btn_scale_y = 1;

if(sprite_index != noone)_def_btn_spr = sprite_index;

scr_def_show_debug_message("_def_new_btn INIT (sucsess)","debug");
scr_def_show_debug_message("_def_new_btn x = " + string(x),"debug");
scr_def_show_debug_message("_def_new_btn y = " + string(y),"debug");
scr_def_show_debug_message("_def_new_btn spr = " + sprite_get_name(sprite_index),"debug");

