{
    "id": "e5a4ac43-08ab-4d0f-b785-5a926b071609",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_def_btn_abs",
    "eventList": [
        {
            "id": "453db96e-8829-4e55-be6f-7b573e28a1b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e5a4ac43-08ab-4d0f-b785-5a926b071609"
        },
        {
            "id": "0d7bcc33-54ba-47c9-ac4e-dba8917bb209",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "e5a4ac43-08ab-4d0f-b785-5a926b071609"
        },
        {
            "id": "a9f09368-4060-4a5a-b902-7c01f2695347",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "e5a4ac43-08ab-4d0f-b785-5a926b071609"
        },
        {
            "id": "782bd4fe-42f7-405b-be20-3f39781f80b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "e5a4ac43-08ab-4d0f-b785-5a926b071609"
        },
        {
            "id": "82abd475-3a8b-49ec-bb87-0c6adf44240c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e5a4ac43-08ab-4d0f-b785-5a926b071609"
        },
        {
            "id": "58ed260f-8cf4-4748-8a73-fa386923404b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "e5a4ac43-08ab-4d0f-b785-5a926b071609"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "24e3c6af-0947-42d6-8910-259efa43b77f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "e65af177-be57-4c21-99d5-85dd25a91d28",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 960
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}