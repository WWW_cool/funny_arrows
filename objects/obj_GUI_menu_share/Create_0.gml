/// @description  init
var ind = 0;
OPT_btn_spr = spr_menu_btn_small;
OPT_back = spr_menu_bg;
OPT_btn_font = f_text;
OPT_need_close = 0;
var OPT_btn_spr_tmp = OPT_btn_spr;
var OPT_back_tmp = OPT_back;
var OPT_btn_font_tmp = OPT_btn_font;

x = __view_get( e__VW.WView, 0 )/2 + __view_get( e__VW.XView, 0 );
y = __view_get( e__VW.HView, 0 )/2 + __view_get( e__VW.YView, 0 );;
OPT_y_tmp = y + sprite_get_height(OPT_back) - sprite_get_height(OPT_btn_spr) - 80;

OPT_header_x = x - 127;
OPT_header_y = y - 115;
OPT_header_text = "УСПЕХ!";
OPT_header_icon = spr_GUI_icon_happy;


OPT_text_x = x ;
OPT_text_y = y - 10;
OPT_text = "Понравилась игра? Поделитесь ей со своими друзьями!";

OPT_type = EnMsgTypes.Share;
OPT_msg_type = 0;

glob = noone;
if(instance_exists(global.inst_glb))
{
    glob = global.inst_glb;
}

OPT_controls_type[ind++] = v_btns_add(0,0,spr_menu_close, id,0, 
              NO_SCRIPT,
              scr_vk_shop_close,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
scr_audio_init_close(OPT_controls_type[ind - 1]);
alarm[0] = 1;
                     
ind = 0;        
OPT_controls_x[ind] = x + 140 - 18;
OPT_controls_y[ind++] = y - 131; 

//////////////////////////////////////////////////////////////////////////////////////////
event_inherited();
//////////////////////////////////////////////////////////////////////////////////////////

OPT_btn_spr = OPT_btn_spr_tmp;
OPT_back = OPT_back_tmp;
OPT_btn_font = OPT_btn_font_tmp;




/// price text

price_count = 3;
price_max = 2;
price_names[0] = "Жизнь";
price_names[1] = "Первый ход";
price_names[2] = "Ход назад";
price_text = "";

for(var i = 0; i < price_count; i += 1)
{
    price_item_count[i] = irandom(price_max);
    if(price_item_count[i])
    {
        price_text += "+" + s(price_item_count[i]) + " " + price_names[i] + "#";
    }
}

 



/// additionals params

sh_study_index = -1;

h_back = noone;

h_text_scale = 1;
h_text_scale_speed = 0.15;
h_text_scale_max = 1.2;
h_text_scale_min = 0.8;
h_text_state[0] = true;
h_text_res[0] = false;


