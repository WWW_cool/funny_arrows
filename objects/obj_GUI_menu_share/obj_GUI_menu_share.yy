{
    "id": "6f494a7e-b437-43b8-90d8-5449c627d701",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_menu_share",
    "eventList": [
        {
            "id": "45d42905-93ae-41fb-b6ec-3efdde75f1c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6f494a7e-b437-43b8-90d8-5449c627d701"
        },
        {
            "id": "d0e97ea3-a648-445e-9bbf-ed767ec50aca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6f494a7e-b437-43b8-90d8-5449c627d701"
        },
        {
            "id": "fa43c6bf-b3fe-4307-bb00-4f92f84eb5f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6f494a7e-b437-43b8-90d8-5449c627d701"
        },
        {
            "id": "f55ac682-8ba7-44a8-9f96-27308870db74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6f494a7e-b437-43b8-90d8-5449c627d701"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0f12a9c8-0859-422b-865b-2665b2ddf681",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}