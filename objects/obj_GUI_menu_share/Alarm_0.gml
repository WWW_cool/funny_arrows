/// @description  init msg type btn

h_back = instance_create(1,1,obj_hint_back)
h_back.back_alpha = 0.55;
h_back.depth = -5;

var ind = array_length_1d(OPT_controls_type);
OPT_controls_type[ind] = v_btns_add(x,y + 101,OPT_btn_spr, id,0, 
              NO_SCRIPT,
              NO_SCRIPT,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
v_btns_set_param(OPT_controls_type[ind],"cantDraw",1);
var btn_buy_y = 101;
var btn_buy_x = 90;
var okAnalytics = 0, type = EnVkShop.ALL, text = "";              
switch(OPT_type)
{
    case EnMsgTypes.Share:
        OPT_controls_type[ind + 1] = v_btns_add(x + btn_buy_x,y + btn_buy_y,OPT_btn_spr, id,0, 
              NO_SCRIPT,
              scr_vk_inv,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
        v_btns_set_param(OPT_controls_type[ind + 1],"cantDraw",1);
        v_btns_set_param(OPT_controls_type[ind + 1],"analytick_id","game_invite");
        v_btns_set_text(OPT_controls_type[ind + 1],0,0,
            "Пригласить #друзей",1,c_black,0,f_text,f_text,1);
        v_btns_set_param(OPT_controls_type[ind + 1],"text_halign",fa_center);
        
        v_btns_set_param(OPT_controls_type[ind],"analytick_id","game_repost");
        v_btns_set_param(OPT_controls_type[ind],"icon",spr_menu_icon_share);
        var _x = v_btns_get_param(OPT_controls_type[ind], "xpos");
        v_btns_set_param(OPT_controls_type[ind],"xpos",_x - btn_buy_x);
        v_btns_set_cb(OPT_controls_type[ind],"press",scr_win_repost_2);
    break;
    case EnMsgTypes.Moveback:
        OPT_controls_type[ind + 1] = v_btns_add(x + btn_buy_x,y + btn_buy_y,OPT_btn_spr, id,0, 
              NO_SCRIPT,
              NO_SCRIPT,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
        v_btns_set_param(OPT_controls_type[ind + 1],"cantDraw",1);
        v_btns_set_param(OPT_controls_type[ind + 1],"analytick_id","buy_addMoveback");
        v_btns_set_param(OPT_controls_type[ind],"analytick_id","addMoveback");
        okAnalytics = 1;
        type = EnVkShop.BACK_L;
        text = " 50";
    case EnMsgTypes.FStep:
        if(!okAnalytics)
        {
            OPT_controls_type[ind + 1] = v_btns_add(x + btn_buy_x,y + btn_buy_y,OPT_btn_spr, id,0, 
              NO_SCRIPT,
              NO_SCRIPT,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
            v_btns_set_param(OPT_controls_type[ind + 1],"cantDraw",1);
            v_btns_set_param(OPT_controls_type[ind + 1],"analytick_id","buy_addFStep");
            v_btns_set_param(OPT_controls_type[ind],"analytick_id","addFStep");
            okAnalytics = 1;
            type = EnVkShop.FSTEP_L;
            text = " 50";
        }
    case EnMsgTypes.Lives: // lives
        
        if(!okAnalytics)
        {
            OPT_controls_type[ind + 1] = v_btns_add(x + btn_buy_x,y + btn_buy_y,OPT_btn_spr, id,0, 
              NO_SCRIPT,
              NO_SCRIPT,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
            v_btns_set_param(OPT_controls_type[ind + 1],"cantDraw",1);
            v_btns_set_param(OPT_controls_type[ind + 1],"analytick_id","buy_addLives");
            v_btns_set_param(OPT_controls_type[ind],"analytick_id","addLives");
            okAnalytics = 1;
            type = EnVkShop.LIFE_L;
            text = " 25";
        }
        
        scr_vk_init_btn(OPT_controls_type[ind + 1], type, "Купить" + text, -35, 0);
        v_btns_set_text(OPT_controls_type[ind],-55,0,
            "Получить",1,c_black,0,f_text,f_text,1);
        v_btns_set_param(OPT_controls_type[ind],"text_halign",fa_left);
        v_btns_set_param(OPT_controls_type[ind],"icon",spr_menu_icon_advert);
        v_btns_set_param(OPT_controls_type[ind],"icon_x", + 45);
        var _x = v_btns_get_param(OPT_controls_type[ind], "xpos");
        v_btns_set_param(OPT_controls_type[ind],"xpos",_x - btn_buy_x);
        v_btns_set_cb(OPT_controls_type[ind],"press",scr_gui_shop);
    break;
    case EnMsgTypes.BTime: // boss time
    break;
    case EnMsgTypes.BDMG: // boss dmg
    break;
    case EnMsgTypes.BLVL: // boss lvl
    break;
    case EnMsgTypes.Price: // boss lvl
        OPT_text += "##" + price_text;
        v_btns_set_text(OPT_controls_type[ind],0,-2,
        "Забрать",1,c_black,0,f_text,f_text,1);
        v_btns_set_cb(OPT_controls_type[ind],"press",scr_gui_price);
    break;
    case EnMsgTypes.BossPrice: // boss lvl
        v_btns_set_text(OPT_controls_type[ind],0,-2,
        "Забрать",1,c_black,0,f_text,f_text,1);
        v_btns_set_cb(OPT_controls_type[ind],"press",scr_gui_price);
    break;
    case EnMsgTypes.Advert: // Advert
        v_btns_set_param(OPT_controls_type[ind],"icon",spr_menu_icon_advert);
        v_btns_set_cb(OPT_controls_type[ind],"press",scr_s_ads);
    break;
    case EnMsgTypes.Demo: // Demo
        v_btns_set_param(OPT_controls_type[ind],"icon",spr_menu_icon_share);
        v_btns_set_cb(OPT_controls_type[ind],"press",scr_vk_inv_group);
        v_btns_set_param(OPT_controls_type[ind],"analytick_id","FinishDemo");
    break;
    case EnMsgTypes.Study: // Study
        v_btns_set_text(OPT_controls_type[ind],0,-2,
        "Начать",1,c_black,0,f_text,f_text,1);
        v_btns_set_cb(OPT_controls_type[ind],"press",scr_study_btn_start_cb);
    break;
}




