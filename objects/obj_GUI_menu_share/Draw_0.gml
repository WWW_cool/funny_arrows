/// @description  draw shop

if(!OPT_cantDraw)
{
    var tmp_alpha = draw_get_alpha();
    draw_set_alpha(OPT_alpha);
    
    draw_sprite(spr_hint_arrow_up, 0, 150, 50);
    var hint_x = __view_get( e__VW.XView, 0 ) + 150;
    var hint_y = 0.2*room_height;
    draw_set_font(f_H2);
    draw_set_colour(c_white);
    h_text_scale = v_twink(h_text_res,h_text_state,h_text_scale,h_text_scale_min,h_text_scale_max,h_text_scale_speed);
    draw_text_transformed(hint_x,hint_y,string_hash_to_newline("Оцените приложение!"),h_text_scale,h_text_scale,0);
    
    draw_set_font(f_text);
    draw_sprite_ext(OPT_back,0,x,y,0.9,1.15,0,c_white,1);
    draw_set_font(OPT_btn_font);
    draw_set_halign(fa_left);
    draw_set_valign(fa_middle);
    draw_set_colour(c_red);
    for(var i = 0; i < array_length_1d(OPT_controls_type); i += 1)
    {
        v_btns_draw_btn(OPT_controls_type[i]);        
    }
    draw_set_colour(c_black);
    if(sprite_exists(OPT_header_icon))
    {
        draw_sprite(OPT_header_icon,0,x,y - 180);
    }
    v_draw_text(OPT_header_x,OPT_header_y,OPT_header_text,fa_left,fa_middle,EnText.H2);
    v_draw_text_ext(OPT_text_x,OPT_text_y,OPT_text,fa_center,fa_middle,EnText.H2,250);
    draw_set_alpha(tmp_alpha);
    if(OPT_alpha < 1)OPT_alpha += OPT_alpha_speed;
}



