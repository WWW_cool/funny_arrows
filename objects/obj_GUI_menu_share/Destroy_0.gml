
if(instance_exists(h_back))
{
    h_back.back_destroy = 1;
}

switch(OPT_type)
{
    case EnMsgTypes.Lives:
        if(instance_exists(glob))
        {
            if(glob.live_count > 0)
            {
                if(!OPT_msg_type)
                {
                    scr_win_next();
                }
                else
                {
                    scr_win_retry();
                }
            }
            else
            {
                scr_def_rm_goto_room(rm_Start);
            }
        }
    exit;
    case EnMsgTypes.Share:
    case EnMsgTypes.Advert:
    case EnMsgTypes.Moveback:
    case EnMsgTypes.FStep:
    case EnMsgTypes.Demo:
    case EnMsgTypes.Study:
        event_inherited();
    exit;
    case EnMsgTypes.BossPrice:
        scr_win_next();
    exit;
}


if(os_browser == browser_not_a_browser)
{
    if(room == rm_load)
    {
        if(instance_exists(glob))
        {   
            //glob.game_lvl_to_load = 0;
            scr_def_rm_goto_room(rm_Start);
        }
    }
    else event_inherited();
}
else
{
    if(room > rm_lvlCreate)
    {
        scr_def_rm_goto_room(rm_Start);
    }
    else event_inherited();
}

