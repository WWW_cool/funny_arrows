{
    "id": "024d053e-1fde-4c7d-b1b0-a11057f3a5ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_lvl_select",
    "eventList": [
        {
            "id": "7bc76446-8b6d-4104-81cc-134b5bbcf8dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "024d053e-1fde-4c7d-b1b0-a11057f3a5ec"
        },
        {
            "id": "4960bddd-1003-464a-82f8-9d228e9c3fd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "024d053e-1fde-4c7d-b1b0-a11057f3a5ec"
        },
        {
            "id": "c4fe87ec-91f2-47fa-9e50-cdd9d536b9df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "024d053e-1fde-4c7d-b1b0-a11057f3a5ec"
        },
        {
            "id": "9167a489-546d-4f1a-b90d-60bbcda1b1be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 61,
            "eventtype": 6,
            "m_owner": "024d053e-1fde-4c7d-b1b0-a11057f3a5ec"
        },
        {
            "id": "aa40d678-2972-41d2-9240-84ba3b2976cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 60,
            "eventtype": 6,
            "m_owner": "024d053e-1fde-4c7d-b1b0-a11057f3a5ec"
        },
        {
            "id": "9356963d-7afe-4d68-a5ee-61490691acda",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "024d053e-1fde-4c7d-b1b0-a11057f3a5ec"
        },
        {
            "id": "4ac5858b-b3e4-4c9b-af02-d51db5c459d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "024d053e-1fde-4c7d-b1b0-a11057f3a5ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}