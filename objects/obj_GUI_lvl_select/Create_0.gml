/// @description page anim init

p_anim = 0;
p_num = 0;
p_num_max = 2;



///init

OPT_owner = noone;

OPT_btn_spr = noone;//spr_end_Next;
OPT_back = noone;//spr_Menu_back;
OPT_btn_font = f_End_timer;

OPT_cantDraw = 0;
OPT_alpha = 0;
OPT_alpha_speed = 0.2;

x = __view_get( e__VW.XView, 0 );
y = __view_get( e__VW.YView, 0 );

if(instance_exists(global.inst_glb))
{
    glob = global.inst_glb;
}

var ind = 0;
/*OPT_controls_type[ind++] = v_btns_add(0,0,spr_LS_arrow_L, id,0, 
          NO_SCRIPT,
          scr_lvl_arror_l,
          NO_SCRIPT, 
          NO_SCRIPT,
          NO_SCRIPT
          );
OPT_controls_type[ind++] = v_btns_add(0,0,spr_LS_arrow_R, id,0, 
          NO_SCRIPT,
          scr_lvl_arror_r,
          NO_SCRIPT, 
          NO_SCRIPT,
          NO_SCRIPT
          );
          */
                 
OPT_controls_x[ind]     = 60;
OPT_controls_y[ind++]   = 220; 
OPT_controls_x[ind]     = 640;
OPT_controls_y[ind++]   = 220; 
OPT_controls_lvl_count = 15;
var str_length = 3;
OPT_lvl_icon = spr_lvls_unlock;
ind = 0;  
for(var i = ind; i < ind + OPT_controls_lvl_count*p_num_max; i += 1)
{
    OPT_controls_type[i] = v_btns_add(0,0,OPT_lvl_icon, id,0, 
          NO_SCRIPT,
          scr_lvl_btn_select,
          NO_SCRIPT, 
          NO_SCRIPT,
          NO_SCRIPT
          );     
    var old_i = i -  ind;
    OPT_controls_x[i] = 33 + 103*(old_i%str_length) + sprite_get_width(OPT_lvl_icon)/2;
    var tmp_high = (old_i)/str_length div 1;
    OPT_controls_y[i] = 158 + 103*(tmp_high)  + sprite_get_height(OPT_lvl_icon)/2;
    
    OPT_controls_block[old_i] = 0; 
    OPT_controls_star[old_i] = 0;
    OPT_controls_block_lvl[old_i] = 0; 
}
scr_lvl_check_lock();
open_all = global.user_open_all;

OPT_star_x[0] = -29;     
OPT_star_y[0] = -44; 
OPT_star_x[1] = 0;     
OPT_star_y[1] = -54; 
OPT_star_x[2] = 29;     
OPT_star_y[2] = -44; 
           
for(var i = 0; i < array_length_1d(OPT_controls_type); i += 1)
{
    v_btns_set_param(OPT_controls_type[i],"cantDraw",1);
}

OPT_slider_x = 356;
OPT_slider_y = 88;
OPT_slider_start = 0;
OPT_slider_delta = 0;
OPT_slider_pos_slider = 0;
OPT_slider_pos = 0;
OPT_slider_pos_old = 0;
OPT_slider_pos_max = 512 - 150;
OPT_slider_wheel = 0;


OPT_slider_index = 0;
OPT_slider_pos_delta = str_length*OPT_slider_pos_max/(p_num_max*OPT_controls_lvl_count)

OPT_dark_x = 0;
OPT_dark_y = 673;

event_user(0);



/* */
/*  */
