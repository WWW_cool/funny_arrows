/// @description  draw gui of start room

if(!OPT_cantDraw)
{
    var tmp_alpha = draw_get_alpha();
    draw_set_alpha(OPT_alpha);
    if(sprite_exists(OPT_back))
    {
        draw_sprite(OPT_back,0,x,y);
    }
    for(var i = 0; i < array_length_1d(OPT_controls_type); i += 1)
    {
        
        var slide_alpha = tmp_alpha;
        if(OPT_controls_y[i] - OPT_slider_pos > 158 + sprite_get_height(OPT_lvl_icon)/2 - 5 
        && OPT_controls_y[i] - OPT_slider_pos < 573 - sprite_get_height(OPT_lvl_icon)/2)
        {
            slide_alpha = tmp_alpha;
        }
        else
        {
            var alpha_speed = 3;
            if(OPT_controls_y[i] - OPT_slider_pos <= 158 + sprite_get_height(OPT_lvl_icon)/2 - 5)
            {
                slide_alpha = tmp_alpha - clamp(abs(((158 + sprite_get_height(OPT_lvl_icon)/2 - 5)) - (OPT_controls_y[i] - OPT_slider_pos))
                /OPT_slider_pos_delta,0,alpha_speed)/alpha_speed;
            }
            else
            {
                slide_alpha = tmp_alpha - clamp(abs((OPT_controls_y[i] - OPT_slider_pos) - 
                (573 - sprite_get_height(OPT_lvl_icon)/2))/OPT_slider_pos_delta,0,alpha_speed)/alpha_speed;
            }
            
        }
        if(slide_alpha > 0)
        {
            draw_set_alpha(slide_alpha);
            if(OPT_controls_block_lvl[i] > global.user_lvl)
            {
                v_btns_freeze(OPT_controls_type[i],1,NO_SCRIPT);
                draw_sprite(spr_lvls_unlock,5,OPT_controls_x[i],
                         OPT_controls_y[i] - OPT_slider_pos);       
                draw_sprite_ext(spr_xp_icon,0,OPT_controls_x[i],
                     OPT_controls_y[i] - OPT_slider_pos,0.35,0.35,0,c_white,slide_alpha);
                draw_sprite(spr_lvls_lock,0,OPT_controls_x[i] - 45,
                         OPT_controls_y[i] - OPT_slider_pos - 45); 
                
                draw_set_font(f_a_user_lvl_small);
                draw_set_colour(c_white);
                draw_set_valign(fa_middle);
                draw_set_halign(fa_center);
                
                draw_text(  OPT_controls_x[i],
                            OPT_controls_y[i] - OPT_slider_pos,
                            string_hash_to_newline(s(OPT_controls_block_lvl[i] + 1)));
            }
            else
            {
                v_btns_draw_btn(OPT_controls_type[i]);
                draw_set_font(f_Attempts);
                draw_set_colour(c_white);
                draw_set_halign(fa_center);
                draw_set_valign(fa_middle);
                draw_text_transformed(OPT_controls_x[i], OPT_controls_y[i] - 10 - OPT_slider_pos, string_hash_to_newline(s(i + 1)),
                0.5,0.5,0);
                if(!OPT_controls_block[i])
                {
                    v_btns_freeze(OPT_controls_type[i],-1,scr_lvl_btn_select);
                    if(OPT_controls_star[i])
                    {
                        draw_sprite(spr_lvls_passed,2,OPT_controls_x[i],
                             OPT_controls_y[i] - OPT_slider_pos);
                    }
                    else
                    {
                         draw_sprite(spr_lvls_unlock,2,OPT_controls_x[i],
                         OPT_controls_y[i] - OPT_slider_pos);  
                    }
                }
                else
                {
                    v_btns_freeze(OPT_controls_type[i],3,NO_SCRIPT);
                    draw_sprite(spr_lvls_unlock,4,OPT_controls_x[i],
                         OPT_controls_y[i] - OPT_slider_pos);
                }
            }
        }
        else
        {
            v_btns_freeze(OPT_controls_type[i],1,NO_SCRIPT);
        }
        
    }
    draw_set_alpha(tmp_alpha);
    draw_sprite(spr_lvls_slider,1,OPT_slider_x,OPT_slider_y);
    draw_sprite(spr_lvls_slider,0,OPT_slider_x,OPT_slider_y + OPT_slider_pos_slider);
    
    draw_set_alpha(tmp_alpha);
    if(OPT_alpha < 1)OPT_alpha += OPT_alpha_speed;
}





