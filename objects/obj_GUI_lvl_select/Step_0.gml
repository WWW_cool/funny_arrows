// slider control

if(open_all != global.user_open_all)
{
    open_all = global.user_open_all;
    scr_lvl_check_lock();
}

if(mouse_x > OPT_slider_x - 20)
{
    if(mouse_y > OPT_slider_y - 10)
    {
        if(mouse_check_button_pressed(mb_left))
        {
            OPT_slider_start = 1;
            OPT_slider_pos_old = OPT_slider_pos_slider;
            OPT_slider_delta = mouse_y;
        }
    }
}

if(mouse_check_button_released(mb_left) || !mouse_check_button(mb_left))
{
    OPT_slider_start = 0;
}

if(OPT_slider_start)
{
    OPT_slider_pos_slider = clamp(OPT_slider_pos_old + mouse_y - OPT_slider_delta,
    0,OPT_slider_pos_max);
    OPT_slider_pos = p_num_max*OPT_slider_pos_slider*0.96;
    //OPT_slider_index = OPT_slider_pos/(OPT_slider_pos_delta);
    event_user(0);
}



