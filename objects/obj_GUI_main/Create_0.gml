/// @description Init befor

GUI_xpos = 0;
GUI_ypos = 0;

point[EnPoint.X] = 0;
point[EnPoint.Y] = 0;
var ind = 0;
//heart
v_choose(point,55,50,55,50);
heart_x = point[EnPoint.X];
heart_y = point[EnPoint.Y];
// HP bar
//hpBar_x = heart_x + 10;
//hpBar_y = heart_y - sprite_get_height(spr_GUI_hpBar)/2;

// +
v_choose(point,193,50,193,50);
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_circle_mini,id,0,
-1,scr_s_btn_add_xp,-1,-1,-1);
v_btns_set_text(GUI_controls[ind - 1],0,-2,
"+",1,c_black,0,f_H2,f_H2,1);
// rating
v_choose(point,737,55,342,55);                            
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_circle,id,0,
                     -1,scr_gui_btn_ads,-1,-1,-1);
v_btns_set_param(GUI_controls[ind - 1],"icon",spr_GUI_icon_rating);
//settings                                                                          
v_choose(point,75,550,75,670);
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_circle,id,0,
                            -1,scr_s_settings,-1,-1,-1);
v_btns_set_param(GUI_controls[ind - 1],"icon",spr_GUI_icon_settings);
v_btns_set_param(GUI_controls[ind - 1],"analytick_id","settings");
//price
v_choose(point,75,350,200,670);
price_x = point[EnPoint.X];
price_y = point[EnPoint.Y];
price_btn = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_price,id,0,
                            -1,scr_s_price,-1,-1,-1);
GUI_controls[ind++] = price_btn;

v_btns_set_param(GUI_controls[ind - 1],"audio_in_array",0);
v_btns_set_param(GUI_controls[ind - 1],"audio_sound",snd_click_gift);
v_btns_set_param(GUI_controls[ind - 1],"analytick_id","price");

//social                            
v_choose(point,725,550,330,670);                            
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_circle,id,0,
                     -1,scr_s_social,-1,-1,-1);   
v_btns_set_param(GUI_controls[ind - 1],"icon",spr_GUI_icon_like);
v_btns_set_param(GUI_controls[ind - 1],"analytick_id","social");

// Hit!
v_choose(point,400,550,202,580);
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_hit,id,0,
-1,scr_wait_start,-1,-1,-1);
main_play = GUI_controls[ind - 1];
v_btns_set_param(GUI_controls[ind - 1],"audio_in_array",0);
v_btns_set_param(GUI_controls[ind - 1],"audio_sound",snd_click_startbtn);
scr_boss_play(main_play);
// Arrow R!
v_choose(point,533,420,335,450);
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_arrow_R,id,0,
-1,scr_boss_next,-1,-1,-1);
main_arrow_r = GUI_controls[ind - 1];

// Arrow L!
v_choose(point,267,420,70,450);
GUI_controls[ind++] = v_btns_add(point[EnPoint.X],point[EnPoint.Y],spr_GUI_btn_arrow_L,id,0,
-1,scr_boss_prev,-1,-1,-1);
main_arrow_l = GUI_controls[ind - 1];

scr_boss_check_btns();

// Logo
v_choose(point,400,140,202,170);
logo_x = point[EnPoint.X];
logo_y = point[EnPoint.Y];

// Boss bar
v_choose(point,400 - sprite_get_width(spr_GUI_bossBar)/2,265,202 - sprite_get_width(spr_GUI_bossBar)/2,295);
bossBar_x = point[EnPoint.X];
bossBar_y = point[EnPoint.Y];

// rating bar
v_choose(point,715 - sprite_get_width(spr_GUI_rateBar),
50 - sprite_get_height(spr_GUI_hpBar)/2,
320 - sprite_get_width(spr_GUI_rateBar),
50 - sprite_get_height(spr_GUI_hpBar)/2);
rateBar_x = point[EnPoint.X];
rateBar_y = point[EnPoint.Y];

// Boss name
v_choose(point,400,bossBar_y - 15,202,
bossBar_y - 15);
bossName_x = point[EnPoint.X];
bossName_y = point[EnPoint.Y];

// Boss
v_choose(point,400,500,202,
530);
boss_x = point[EnPoint.X];
boss_y = point[EnPoint.Y];

//price_time_init = 360;
//price_time = 0;

lvl_count = scr_level_get_count();




action_inherited();
///Anim

a_res[0] = 0;
a_point[0] = 0;
for(var i = 0; i < EnAnimParam.LENGTH; i++)
{
    a_heart[i] = 0;
}

a_heart[EnAnimParam.initVal] = 1;
a_heart[EnAnimParam.Val] = a_heart[EnAnimParam.initVal];

a_boss_next = noone;
a_boss_anim = 0;
a_boss_anim_alpha = 0;
a_boss_alpha = 1;
a_boss_alpha_speed = 0.05;
a_boss_amount = 1;
a_boss_amount_init = 0.15;
a_boss_amount_speed = 0.01;

a_boss_price_time = 0.3;
a_boss_price_timer = a_boss_price_time;
if(glob.game_boss_next)
{
    glob.game_boss_next = 0;
    a_boss_next = instance_create(boss_x,boss_y - sprite_get_height(scr_boss_get_spr(glob.game_boss_index))/2,obj_boss_end_efx);
    a_boss_next.type = 1;
    a_boss_anim = 1;
    a_boss_amount = a_boss_amount_init;
    v_btns_set_param(main_play,"active",0);
}



