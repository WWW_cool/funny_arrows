{
    "id": "4560df7d-70a5-493d-8d9c-645961e97a40",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_main",
    "eventList": [
        {
            "id": "0d3c1b1b-8c31-4439-8812-e2f8825172a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4560df7d-70a5-493d-8d9c-645961e97a40"
        },
        {
            "id": "9cb60139-c0a9-445a-8450-65e1a691c7dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4560df7d-70a5-493d-8d9c-645961e97a40"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d6960cef-2d41-4b43-b2e6-96945303e7a7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}