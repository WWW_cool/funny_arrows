/// @description  draw below
v_draw_alpha();

scr_boss_check_btns();

draw_set_color(c_white);

var col = global.col_main;
var rm_width = global._def_glb_var_default_resolution_w;

draw_rectangle_colour(__view_get( e__VW.XView, 0 ),__view_get( e__VW.YView, 0 ),__view_get( e__VW.XView, 0 ) + rm_width,0,
col,col,col,col,false);
v_draw_tiled(spr_gui_bg,0,__view_get( e__VW.XView, 0 ),0,EnTiled.Hor);

draw_sprite(spr_logo,0,logo_x,logo_y);
v_draw_progBar(spr_GUI_bossBar,scr_boss_get_amount()*a_boss_amount,bossBar_x,bossBar_y,true);

draw_sprite(spr_GUI_rateBar,0,rateBar_x,rateBar_y);
v_draw_text(rateBar_x + 10,
            rateBar_y + sprite_get_height(spr_GUI_rateBar)/2,
            string(19347),fa_left,fa_middle,EnText.Text);

scr_gui_draw_life(heart_x,heart_y,a_heart);
/*
if(glob.live_time)
{
    v_draw_progBar(spr_GUI_hpBar,(glob.live_time/glob.live_time_need),hpBar_x,hpBar_y,2,glob.live_time_need - glob.live_time);
}
else
{
    v_draw_progBar(spr_GUI_hpBar,(glob.live_time/glob.live_time_need),hpBar_x,hpBar_y,false);
}

v_anim(a_heart,EnAnim.Heart,3);
draw_sprite_ext(spr_GUI_icon_heart,0,heart_x,heart_y,
a_heart[EnAnimParam.Val],a_heart[EnAnimParam.Val],0,c_white,1);

v_draw_text(heart_x - 1,heart_y,
            string(glob.live_count),fa_center,fa_middle,EnText.H2);  
*/
var _boss_spr = scr_boss_get_spr(glob.game_boss_index);
draw_sprite(spr_monster_shadow,0,boss_x,boss_y);  
draw_sprite(_boss_spr,0,boss_x,boss_y);
if(glob.game_boss_lock[glob.game_boss_index])
{
    draw_sprite_ext(_boss_spr,0,boss_x,boss_y,1,1,0,c_black,0.9*a_boss_alpha);
    draw_sprite_ext(spr_monster_lock,0,boss_x,boss_y - sprite_get_height(_boss_spr)/2 - 100*(1 - a_boss_alpha),1,1,0,c_white,a_boss_alpha*2); 
}
else
{
    //draw_set_color(global.col_main);
    v_draw_text(bossName_x,bossName_y,
               scr_boss_get_name(glob.game_boss_index),fa_center,fa_middle,EnText.Text);         
}

if(glob.price_time)
{
    v_btns_freeze(price_btn,2,-1);
    if(glob.price_time % 60 < 10)
    {
        var _time = s((glob.price_time/60) div 1) + ":0" + s((glob.price_time % 60) div 1);
    }
    else
    {
        var _time = s((glob.price_time/60) div 1) + ":" + s((glob.price_time % 60) div 1);
    }
    v_draw_text(price_x,price_y + 45,_time,fa_center,fa_middle,EnText.Text);
}
else
{
    v_btns_freeze(price_btn,-1,scr_s_price);
}

if(a_boss_anim)
{
    if(!instance_exists(a_boss_next))
    {
        glob.game_boss_index = clamp(glob.game_boss_index + 1,0,glob.game_boss_max);
        if(glob.game_boss_lock[glob.game_boss_index])
        {
            a_boss_anim_alpha = 1;
            scr_boss_check_btns();
        }
        a_boss_anim = 0;
    }
    a_boss_amount += a_boss_amount_speed;
    if(a_boss_amount > 1)a_boss_amount = 1;
    
    a_boss_price_timer -= delta_time/1000000;
    if(a_boss_price_timer <= 0)
    {
        a_boss_price_timer = a_boss_price_time;
        scr_gui_price_efx(irandom(EnPriceType.LENGTH - 1),1);
    }
}

if(a_boss_anim_alpha)
{
    a_boss_alpha = v_increment(a_res,a_boss_alpha,0,a_boss_alpha_speed,a_boss_alpha_speed);
    if(a_res[0])
    {
        glob.game_boss_lock[glob.game_boss_index] = 0;
        a_boss_anim_alpha = 0;
        a_boss_alpha = 1;
        a_boss_amount = 1;
        v_btns_set_param(main_play,"active",1);
    }
}




/* */
action_inherited();
/// draw over

v_draw_alpha(0);


/* */
/*  */
