/// @description v_info_draw
var i;
var tmp_alpha = draw_get_alpha()

for (i = 0; i < _v_info_count; i += 1)
{
   //msg(s(i));
   if(!_v_info_deleted[i])
   {
        //Отрисовываем стандартные запчасти панели с инфой
        
        draw_set_alpha(_v_info_alpha[i]);
        var xxx = _v_info_xpos[i];
        var yyy = _v_info_ypos[i];
        
        //Сначала рисуем прямоугольник бэкграунда
        var cl = make_colour_rgb(63, 57, 45);
        if(sprite_exists(_v_info_bg[i]))
        {
            draw_sprite(_v_info_bg[i],0,xxx,yyy);
        }
        else
        {
            draw_sprite(spr_tip_win,0,xxx,yyy);
        }
                            
       //Вызываем колбэк отрисовки инфы в рамке
        //Specific info callback
        if(script_exists(_v_info_callback[i]))
        {
            if(instance_exists(_v_info_owner[i]))
            {
                with(_v_info_owner[i])
                {
                    script_execute(other._v_info_callback[i],i);
        }}}  
        if(_v_info_alpha[i] >= 1)_v_info_alpha[i] = 1;
        else 
        {
            _v_info_alpha[i] += 1/(_v_info_timeStart[i]);
        }                                                   
   }
}

draw_set_alpha(tmp_alpha);

