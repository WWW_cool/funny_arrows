
var _tmp_aplha = draw_get_alpha();
draw_set_alpha(promo_alpha);

draw_rectangle(__view_get( e__VW.XView, 0 ),__view_get( e__VW.YView, 0 ),__view_get( e__VW.WView, 0 ),__view_get( e__VW.HView, 0 ),false);
draw_sprite_ext(spr_logo_nao,0,__view_get( e__VW.XView, 0 ) + __view_get( e__VW.WView, 0 )/2,
__view_get( e__VW.YView, 0 ) + __view_get( e__VW.HView, 0 )/2,1,1,0,c_white,promo_alpha*logo_alpha);

draw_set_alpha(_tmp_aplha);

if(!logo_on)
{
    logo_alpha += logo_alpha_speed;
    if(logo_alpha >= 1)
    {
        logo_on = 1;
        logo_alpha = 1;
    }
}
else
{
    promo_time -= delta_time/1000000;
    if(promo_time <= 0)
    {
        promo_time = 0;
        logo_alpha -= logo_alpha_speed;
        if(logo_alpha <= 0.2)
        {
            promo_alpha -= promo_alpha_speed;
            if(promo_alpha <= 0)
            {
                promo_alpha = 0;
                instance_destroy();
            }
        }
    }
}

