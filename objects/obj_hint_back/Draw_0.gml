//draw back rect

var tmp_alpha = draw_get_alpha();
draw_set_alpha(alpha);
draw_set_colour(c_black);
draw_rectangle(__view_get( e__VW.XView, 0 ),__view_get( e__VW.YView, 0 ),__view_get( e__VW.WView, 0 ),__view_get( e__VW.HView, 0 ),false);
draw_set_alpha(tmp_alpha);

if(!back_destroy)
{
    if(alpha < back_alpha)
    {
        alpha = v_increment(a_res,alpha,back_alpha,alpha_speed,alpha_speed);
    }
}
else
{
    if(alpha > 0)
    {
        alpha = v_increment(a_res,alpha,0,alpha_speed,alpha_speed);
    }
    else
    {
        instance_destroy();
    }
}


