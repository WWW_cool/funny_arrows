/// @description  init
h_colour = global.col_main;
h_init_v = 150;
h_line_max = 3;

for(var i = 0; i < h_line_max; i++)
{
    h_center_x[i] = __view_get( e__VW.WView, 0 );
    h_center_x_end[i] = 0;
    h_center_v[i] = h_init_v;
    h_center_width[i] = 0;
    h_center_speed[i] = 0;
}
var i = 0;
h_center_x[i] = 0;
h_center_x_end[i] = h_center_x[i];
h_center_v[i] += 0;
h_center_width[i] = 5;
h_center_speed[i++] = 60;

h_center_x[i] = 0 + __view_get( e__VW.WView, 0 );
h_center_x_end[i] = h_center_x[i];
h_center_v[i] += 5 + 7;
h_center_width[i] = 45;
h_center_speed[i++] = -50;

h_center_x[i] = 0;
h_center_x_end[i] = h_center_x[i];
h_center_v[i] += 12 + 45 + 7;
h_center_width[i] = 13;
h_center_speed[i++] = 55;

h_text = "БОСС";
h_text_init_x = __view_get( e__VW.WView, 0 )/2 + 45;
h_text_init_y = h_center_v[1] + h_center_width[1]/2;
h_text_colour = c_white;
h_text_alpha_start = 0;
h_text_alpha = h_text_alpha_start;
h_text_alpha_speed_up = 0.25;
h_text_alpha_speed_down = 0.2;
h_text_alpha_dir = 0;
h_text_move_speed = 3;
h_text_time = 1;

a_res[0] = 0;

h_win = 0;
h_diff = 0;


