
var _colour = draw_get_colour();
draw_set_colour(h_colour);
for(var i = 0; i < h_line_max; i++)
{
    if(h_text_alpha_dir <= 1)
    {
        if(h_center_speed[i] > 0)
        {
            draw_rectangle(__view_get( e__VW.XView, 0 ) + h_center_x[i], h_center_v[i],__view_get( e__VW.XView, 0 ),h_center_v[i] + h_center_width[i], false);
            if(h_center_x[i] < __view_get( e__VW.WView, 0 ))h_center_x[i] += h_center_speed[i];
        }
        else
        {
            draw_rectangle(__view_get( e__VW.XView, 0 ) + h_center_x[i], h_center_v[i],__view_get( e__VW.XView, 0 ) + __view_get( e__VW.WView, 0 ),h_center_v[i] + h_center_width[i], false);
            if(h_center_x[i] > 0)h_center_x[i] += h_center_speed[i];
        }
    }
    else
    {
        if(h_center_speed[i] > 0)
        {
            draw_rectangle(__view_get( e__VW.XView, 0 ) + h_center_x[i], h_center_v[i],__view_get( e__VW.XView, 0 ) + h_center_x_end[i],h_center_v[i] + h_center_width[i], false);
            if(h_center_x_end[i] < __view_get( e__VW.WView, 0 ))h_center_x_end[i] += h_center_speed[i];
        }
        else
        {
            draw_rectangle(__view_get( e__VW.XView, 0 ) + h_center_x[i], h_center_v[i],__view_get( e__VW.XView, 0 ) + h_center_x_end[i],h_center_v[i] + h_center_width[i], false);
            if(h_center_x_end[i] > 0)h_center_x_end[i] += h_center_speed[i];
            else
            {
                if(h_win)
                {
                    scr_win_next();
                }
                instance_destroy();
            }
        }
    }
}

draw_set_colour(h_text_colour);
var _alpha = draw_get_alpha();
draw_set_alpha(h_text_alpha);
draw_set_font(f_H1);
switch(h_text_alpha_dir)
{
    case 0:
        h_text_alpha = v_lerp(a_res,h_text_alpha,1,h_text_alpha_speed_up,h_text_alpha_speed_up/5);
        if(a_res[0])
        {
            h_text_alpha_dir = 1;
        }
    break;
    case 1:
        h_text_time -= delta_time/1000000;
        if(h_text_time <= 0)
        {
            h_text_alpha_dir = 2;
        }
    break;
    case 2:
        h_text_alpha = v_lerp(a_res,h_text_alpha,0,h_text_alpha_speed_down,h_text_alpha_speed_down/5);
    break;
}
draw_set_halign(fa_center);
draw_set_valign(fa_middle);

draw_text(__view_get( e__VW.XView, 0 ) + h_text_init_x,h_text_init_y,string_hash_to_newline(h_text));
if(h_text_alpha_dir == 1)
{
    h_text_init_x -= h_text_move_speed/5;
}
else
{
    h_text_init_x -= h_text_move_speed;
}

if(h_diff)
{
    var _shift = 160;
    draw_sprite(spr_diff_warn_small,0,__view_get( e__VW.XView, 0 ) + h_text_init_x - _shift,h_text_init_y);
    draw_sprite(spr_diff_warn_small,0,__view_get( e__VW.XView, 0 ) + h_text_init_x + _shift,h_text_init_y);
}
draw_set_alpha(_alpha);
draw_set_colour(_colour);



