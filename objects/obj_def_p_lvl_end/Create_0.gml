/// @description  inherite init 

event_inherited();

alarm[0] = 1; // init in 1 step of obj life

///////////////////////////////////////
/// vars to init
///////////////////////////////////////
_def_p_msg_type     = noone; // "win" "lose"
_def_p_msg_spr      = noone;
_def_p_msg_x        = 0;
_def_p_msg_y        = 0;
_def_p_msg_scale    = 0.5;

var i;
_def_p_msg_controls_max = 5;

for(i = 0; i < _def_p_msg_controls_max; i += 1)
{
    _def_p_msg_controls_type[i] = noone;
}


