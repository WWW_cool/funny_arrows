event_inherited();
var i;

for(i = 0; i < _def_p_msg_controls_max; i += 1)
{
    if(instance_exists(_def_p_msg_controls_type[i]))
    {
        with(_def_p_msg_controls_type[i])
        {
            instance_destroy();
        }
    }   
}

