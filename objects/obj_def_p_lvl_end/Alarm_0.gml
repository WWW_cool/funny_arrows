/// @description  params init

switch(_def_p_msg_type)
{
    case "win":
    if(sprite_exists(global._def_glb_var_msg_win))
    {
        _def_p_msg_spr = global._def_glb_var_msg_win;
    }
    if(!instance_exists(_def_p_msg_controls_type[1]))
    {
        _def_p_msg_controls_type[1]  = instance_create(_def_p_msg_x + 290, _def_p_msg_y + 330, obj_def_btn_lvl_next);
    } 
    if(!instance_exists(_def_p_msg_controls_type[0]))
    {
        _def_p_msg_controls_type[0] = instance_create(_def_p_msg_x + 410, _def_p_msg_y + 330, obj_def_btn_lvl_retry);
    }
    break;
    case "lose":
    if(sprite_exists(global._def_glb_var_msg_lose))
    {
        _def_p_msg_spr = global._def_glb_var_msg_lose;
    }
    if(!instance_exists(_def_p_msg_controls_type[0]))
    {
        _def_p_msg_controls_type[0] = instance_create(_def_p_msg_x + 410, _def_p_msg_y + 330, obj_def_btn_lvl_retry);
    }
    break;
}


