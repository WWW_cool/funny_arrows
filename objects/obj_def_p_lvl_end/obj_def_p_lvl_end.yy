{
    "id": "a9375045-2f19-4869-bbcd-c9aecab5c7c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_def_p_lvl_end",
    "eventList": [
        {
            "id": "e99ca4e7-59aa-4b6a-89f2-1c521511d19e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a9375045-2f19-4869-bbcd-c9aecab5c7c4"
        },
        {
            "id": "d99b3ec2-5539-4c04-8197-364f8a4f8d1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "a9375045-2f19-4869-bbcd-c9aecab5c7c4"
        },
        {
            "id": "e41de2e4-398d-43f1-97ed-2e8f0800871d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a9375045-2f19-4869-bbcd-c9aecab5c7c4"
        },
        {
            "id": "f3cccadf-307d-45f3-878b-c7206d475726",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a9375045-2f19-4869-bbcd-c9aecab5c7c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "828ce2f9-d683-442c-8a47-bba4d4bf6427",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}