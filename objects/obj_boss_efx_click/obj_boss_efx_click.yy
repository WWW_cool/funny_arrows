{
    "id": "19982dbc-af3c-4ed4-889f-4faed4e08d5a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss_efx_click",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6fc41001-0fc1-4de0-95a9-bf904d16ba79",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b42eb9f3-04e6-45d3-a40b-05aa8e0ec131",
    "visible": true
}