{
    "id": "2eae0294-95ac-4246-b0f0-6a202a7424f5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_a_yellow",
    "eventList": [
        {
            "id": "afd65de4-c0de-4ab0-b0db-87b42b1b50d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2eae0294-95ac-4246-b0f0-6a202a7424f5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b50af0a4-6678-4621-a140-198f86a6df59",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9dae5ca2-0f9c-4e75-8441-eb7f864a3307",
    "visible": true
}