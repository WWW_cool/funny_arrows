/// @description  draw shop

anim_atart_time -= delta_time/1000000;
if(anim_atart_time > 0)exit;

y -= 1;
var _x = x - 25*anim_scale;

anim_scale = v_path_anim(anim_res,anim_scale,p_win_wave,
anim_point,anim_scale_speed,anim_scale_delta);
if(anim_life_time > 0)
{
    if(sprite_exists(OPT_icon))draw_sprite_ext(spr_GUI_btn_bg,0,x,y,anim_scale,anim_scale,0,c_white,1);
    if(sprite_exists(OPT_back))
    {
        draw_sprite_ext(OPT_back,0,x,y,anim_scale,anim_scale,0,c_white,1);
        _x = x - (3 + sprite_get_width(OPT_back)/2)*anim_scale;
    }
    if(sprite_exists(OPT_icon))
    {
        draw_sprite_ext(OPT_icon,0,x,y,anim_scale,anim_scale,0,c_white,1);
    }
    
    draw_set_font(OPT_btn_font);
    draw_set_halign(fa_right);
    draw_set_valign(fa_middle);
    draw_set_colour(c_white);
    draw_text_transformed(_x,y,string_hash_to_newline(OPT_text),anim_scale,anim_scale,0);
    
    if(anim_res[0])
    {
        anim_life_time -= delta_time/1000000;
        
        if(anim_life_time <= 0)
        {
            OPT_alpha_speed = -OPT_alpha_speed;
            OPT_alpha = 0.99;
        }
    }
}
else
{
    var tmp_alpha = draw_get_alpha();
    draw_set_alpha(OPT_alpha);
    if(sprite_exists(OPT_icon))draw_sprite_ext(spr_GUI_btn_bg,0,x,y,anim_scale,anim_scale,0,c_white,1);
    if(sprite_exists(OPT_back))
    {
        draw_sprite_ext(OPT_back,0,x,y,anim_scale,anim_scale,0,c_white,OPT_alpha);
        _x = x - (3 + sprite_get_width(OPT_back)/2)*anim_scale;
    }
    if(sprite_exists(OPT_icon))
    {
        draw_sprite_ext(OPT_icon,0,x,y,anim_scale,anim_scale,0,c_white,1);
    }
    draw_set_font(OPT_btn_font);
    draw_set_halign(fa_right);
    draw_set_valign(fa_middle);
    draw_set_colour(c_white);
    draw_text_transformed(_x,y,string_hash_to_newline(OPT_text),anim_scale,anim_scale,0);
    draw_set_alpha(tmp_alpha);
    if(OPT_alpha < 1)
    {
        OPT_alpha += OPT_alpha_speed;
        if(OPT_alpha <= 0)
        {
            instance_destroy();
        }
    }
}



