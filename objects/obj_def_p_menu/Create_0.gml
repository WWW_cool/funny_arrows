/// @description  inherite init 

event_inherited();

///////////////////////////////////////
/// vars to init
///////////////////////////////////////
_def_p_type = _EnPauseType.MENU;
_def_p_alpha = 0;
_def_p_alpha_speed = 0.2;

x = __view_get( e__VW.WView, 0 )/2 - 379;
y = 150;

_def_next_x = x;
_def_next_y = y;

_def_p_msg_controls_type[0] = v_btns_add(0,0,spr_m_p_btn_continue, id,0, 
              NO_SCRIPT,
              GAME_options_quit,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
v_btns_set_text(_def_p_msg_controls_type[0],sprite_get_width(spr_m_p_btn_continue)/2,sprite_get_height(spr_m_p_btn_continue)/2 + 14,
"Continue",1,global.col_main,0,f_btn_text,f_btn_text,0);
_def_p_msg_controls_type[1] = v_btns_add(0,0,spr_m_p_btn_options, id,0, 
              NO_SCRIPT,
              scr_m_opt_cb,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
v_btns_set_text(_def_p_msg_controls_type[1],sprite_get_width(spr_m_p_btn_options)/2,sprite_get_height(spr_m_p_btn_options)/2 - 2,
"Options",1,global.col_main,0,f_btn_text,f_btn_text,0);              
_def_p_msg_controls_type[2] = v_btns_add(0,0,spr_m_p_btn_exit, id,0, 
              NO_SCRIPT,
              _GLOBAL_GAME_END,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
v_btns_set_text(_def_p_msg_controls_type[2],sprite_get_width(spr_m_p_btn_exit)/2,sprite_get_height(spr_m_p_btn_exit)/2 - 14,
"Exit",1,global.col_main,0,f_btn_text,f_btn_text,0);               
_def_p_msg_controls_type[3] = v_btns_add(0,0,spr_inv_btn_close, id,0, 
              NO_SCRIPT,
              GAME_options_quit,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
              
event_user(0);
       
for(var i = 0; i < array_length_1d(_def_p_msg_controls_type); i += 1)
{
    v_btns_set_param(_def_p_msg_controls_type[i],"cantDraw",1);
}

              

