{
    "id": "34eb315c-3697-4ef1-b97b-eca37be938fc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_def_p_menu",
    "eventList": [
        {
            "id": "bfc99408-cf7f-4bf3-8cab-5f695be7e20c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "34eb315c-3697-4ef1-b97b-eca37be938fc"
        },
        {
            "id": "4160564a-7e9a-4385-bafd-b0691560a030",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "34eb315c-3697-4ef1-b97b-eca37be938fc"
        },
        {
            "id": "36faf0be-a36e-473f-97cf-bdef870b0406",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "34eb315c-3697-4ef1-b97b-eca37be938fc"
        },
        {
            "id": "89bae589-bbf1-4c5c-8bb6-11e38c292316",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "34eb315c-3697-4ef1-b97b-eca37be938fc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "828ce2f9-d683-442c-8a47-bba4d4bf6427",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}