/// @description event inherite

//event_inherited();
if(sprite_exists(global._def_glb_var_pause_sprite))
{
    var tmp_alpha = draw_get_alpha();
    draw_set_alpha(_def_p_alpha);
    draw_sprite(spr_m_p_back,0,x,y);
    //draw_set_halign(fa_center);
    draw_set_colour(global.col_main);
    //draw_set_valign(fa_middle);
    draw_set_font(f_slot_name);
    draw_text(x + 375,y + 53,string_hash_to_newline(scr_localization_get("Pause","No text")));
    for(var i = 0; i < array_length_1d(_def_p_msg_controls_type); i += 1)
    {
        v_btns_draw_btn(_def_p_msg_controls_type[i]);
    }
    draw_set_alpha(tmp_alpha);
    if(_def_p_alpha < 1)_def_p_alpha += _def_p_alpha_speed;
}

