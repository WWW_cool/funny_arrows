/// @description move to new pivot point

_def_p_alpha = 0;

M_btn_x = x + 379 - sprite_get_width(spr_m_p_btn_continue)/2;
M_btn_y = y + 182;

M_close_x = x + 747 - 39;
M_close_y = y + 38;


v_btns_set_param(_def_p_msg_controls_type[0],"xpos",M_btn_x);
v_btns_set_param(_def_p_msg_controls_type[0],"ypos",M_btn_y);

v_btns_set_param(_def_p_msg_controls_type[1],"xpos",M_btn_x);
v_btns_set_param(_def_p_msg_controls_type[1],"ypos",M_btn_y + 185);

v_btns_set_param(_def_p_msg_controls_type[2],"xpos",M_btn_x);
v_btns_set_param(_def_p_msg_controls_type[2],"ypos",M_btn_y + 343);

v_btns_set_param(_def_p_msg_controls_type[3],"xpos",M_close_x);
v_btns_set_param(_def_p_msg_controls_type[3],"ypos",M_close_y);




