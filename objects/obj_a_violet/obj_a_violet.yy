{
    "id": "1819a6bf-15f7-4253-81a6-558196e708c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_a_violet",
    "eventList": [
        {
            "id": "e936a968-4b7a-4384-a771-04bbaaaa4801",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1819a6bf-15f7-4253-81a6-558196e708c9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b50af0a4-6678-4621-a140-198f86a6df59",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "edd286ae-c6d5-4d36-9e1d-d3b28a3a8b6d",
    "visible": true
}