/// @description draw tips
switch(state)
{
    case "idle":
        break;
    case "show":
        var tmp_alpha = draw_get_alpha();
        if(!surface_exists(global._tmp_surf))
        {
            global._tmp_surf = surface_create(__view_get( e__VW.WView, 0 ),__view_get( e__VW.HView, 0 ));
        }
        if(surface_exists(global._tmp_surf))
        {
            surface_set_target(global._tmp_surf);
            draw_set_colour(c_black);
            draw_rectangle(0,0,__view_get( e__VW.WView, 0 ),__view_get( e__VW.HView, 0 ),0);
            draw_set_blend_mode(bm_subtract);
            if(tip_ind < tip_max)
            {
                draw_rectangle(t_x1[tip_ind],t_y1[tip_ind],
                t_x2[tip_ind],t_y2[tip_ind],0);
            }
            draw_set_blend_mode(bm_normal);
            surface_reset_target();
            draw_set_alpha(0.5);
            draw_surface(global._tmp_surf,0,0);
        }
        draw_set_alpha(tmp_alpha);
        break;
    case "create":
        var tmp_alpha = draw_get_alpha();
        if(surface_exists(global._tmp_surf))
        {
            surface_set_target(global._tmp_surf);
            draw_rectangle(0,0,__view_get( e__VW.WView, 0 ),__view_get( e__VW.HView, 0 ),0);
            draw_set_blend_mode(bm_subtract);
            for(var i = 0; i < tip_count; i += 1)
            {
                draw_rectangle(t_x1[i],t_y1[i],t_x2[i],t_y2[i],0);
            }
            draw_set_blend_mode(bm_normal);
            surface_reset_target();
            draw_set_alpha(0.5);
            draw_surface(global._tmp_surf,0,0);
        }
        draw_set_alpha(tmp_alpha);
        break;
}



