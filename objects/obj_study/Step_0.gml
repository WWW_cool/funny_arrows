/// @description  step tip

switch(state)
{
    case "idle":
        break;
    case "show":
        if(!first_show)
        {
            first_show = 1;
            tip_ind = tip_min;
            if(tip_max > tip_count)
            {
                tip_max = tip_count;
            }
            if(tip_ind < tip_max)
            {
                var tmp_x = min(t_x1[tip_ind],t_x2[tip_ind]) 
                + abs(t_x1[tip_ind] - t_x2[tip_ind])/2 - sprite_get_width(spr_tip_win)/2;
                var tmp_y = min(t_y1[tip_ind],t_y2[tip_ind]) - 350;
                if(tmp_y < 0)
                {
                    tmp_y = max(t_y1[tip_ind],t_y2[tip_ind]) + 50;
                }
                s_show_info = v_info_add( tmp_x,tmp_y,id,
                                    scr_v_info_cb_skill_hint); 
                v_info_set_type(s_show_info, tip_type[tip_ind]);
            }
        }
        break;
    case "create":
        if(mb_l_pressed)
        {
            t_x2[tip_count - 1] = mouse_x;
            t_y2[tip_count - 1] = mouse_y;
        }
        break;
}


