
switch(state)
{
    case "idle":
        break;
    case "show":
        tip_ind += 1;
        if(tip_ind < tip_max)
        {
            var tmp_x = min(t_x1[tip_ind],t_x2[tip_ind]) 
            + abs(t_x1[tip_ind] - t_x2[tip_ind])/2 - sprite_get_width(spr_tip_win)/2;
            var tmp_y = min(t_y1[tip_ind],t_y2[tip_ind]) - 350;
            if(tmp_y < 0)
            {
                tmp_y = max(t_y1[tip_ind],t_y2[tip_ind]) + 50;
            }
            v_info_delete(s_show_info);
            s_show_info = v_info_add( tmp_x,tmp_y,id,
                                scr_v_info_cb_skill_hint); 
            v_info_set_type(s_show_info, tip_type[tip_ind]);
        }
        else
        {
            v_info_delete(s_show_info);
            instance_destroy();
        }
        break;
    case "create":
        mb_l_pressed = 1;
        t_x1[tip_count] = mouse_x;
        t_y1[tip_count] = mouse_y;
        tip_count += 1;
        break;
}



