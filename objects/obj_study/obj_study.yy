{
    "id": "0ed57033-2eab-4795-905e-783d61cb0f67",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_study",
    "eventList": [
        {
            "id": "217dae96-cace-400c-a237-008fe05e1ae1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0ed57033-2eab-4795-905e-783d61cb0f67"
        },
        {
            "id": "8904e0f5-3d44-4b72-b23d-74be711048c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "0ed57033-2eab-4795-905e-783d61cb0f67"
        },
        {
            "id": "bf1ed67c-b13a-4ccd-b860-764f4b19bc66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0ed57033-2eab-4795-905e-783d61cb0f67"
        },
        {
            "id": "c60e5017-f971-4b40-b4f3-154206fc313a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "0ed57033-2eab-4795-905e-783d61cb0f67"
        },
        {
            "id": "cbe383e5-3f27-4dc4-a4cb-447b74105a83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "0ed57033-2eab-4795-905e-783d61cb0f67"
        },
        {
            "id": "2fbf30a9-13c4-4a93-a4b9-b87996f37bf0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0ed57033-2eab-4795-905e-783d61cb0f67"
        },
        {
            "id": "4a91f27e-cca6-4d96-9574-d600c2e0efc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 9,
            "m_owner": "0ed57033-2eab-4795-905e-783d61cb0f67"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}