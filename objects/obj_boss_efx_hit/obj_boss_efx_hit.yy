{
    "id": "76feeabf-6368-430d-97ac-f1f48d55771d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss_efx_hit",
    "eventList": [
        {
            "id": "1ecfe2be-ef77-43a4-a7e0-5888b245df44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "76feeabf-6368-430d-97ac-f1f48d55771d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6fc41001-0fc1-4de0-95a9-bf904d16ba79",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0dd2b392-b03d-4ce8-bb64-8e999cf8f0fa",
    "visible": true
}