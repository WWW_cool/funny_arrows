{
    "id": "69758ebd-b0ca-4b6a-953f-c84713d464ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_a_blue",
    "eventList": [
        {
            "id": "33793520-35e2-4f98-ac21-f41a46c9fc56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "69758ebd-b0ca-4b6a-953f-c84713d464ee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b50af0a4-6678-4621-a140-198f86a6df59",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "34ed8a9e-2d4a-4a12-b90a-28c9d855b168",
    "visible": true
}