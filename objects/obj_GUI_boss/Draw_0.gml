/// @description  draw below
v_draw_alpha();

draw_set_color(c_white);
var col = global.col_main;

boss_time -= delta_time/1000000;
if(!boss_lvl_end)
{
    if(boss_time <= 0)
    {
        boss_lvl_end = 1;
        var inst = instance_create(boss_x,boss_y,obj_boss_end_efx);
        a_boss_gui_state = 2;
    }
    
    if(boss_dmg >= boss_dmg_max)
    {
        boss_lvl_end = 1;
        var inst = instance_create(boss_x,boss_y,obj_boss_end_efx);
        a_boss_gui_state = 2;
        inst.type = 1;
        boss_win = 1;
    }
}

switch(a_boss_gui_state)
{
    case 0:
        a_boss_gui_alpha = v_increment(a_res,a_boss_gui_alpha,1,a_boss_gui_alpha_speed,a_boss_gui_alpha_speed);
        
        if(a_res[0])
        {
            a_boss_gui_state = 1;
        }
    break;
    case 1:
    break;
    case 2:
        a_boss_gui_alpha = v_increment(a_res,a_boss_gui_alpha,0,a_boss_gui_alpha_speed,a_boss_gui_alpha_speed);
        
        if(a_res[0])
        {
            //a_boss_gui_state = 1;
        }
    break;
}


var _alpha = draw_get_alpha();
draw_set_alpha(a_boss_gui_alpha);

draw_sprite(spr_boss_icon_time,0,time_icon_x,time_icon_y*a_boss_gui_alpha); 
draw_sprite(spr_boss_icon_sword,0,dmg_icon_x,dmg_icon_y*a_boss_gui_alpha);
 
v_draw_progBar(spr_boss_time,boss_time/boss_time_init,time_bar_x,time_bar_y*a_boss_gui_alpha,false);
v_draw_progBar(spr_boss_dmg,boss_dmg/boss_dmg_max,dmg_bar_x,dmg_bar_y*a_boss_gui_alpha,false);
draw_sprite_part_ext(spr_boss_dmg,2,0,0,
sprite_get_width(spr_boss_dmg)*(hit_number/hit_number_max),
sprite_get_height(spr_boss_dmg),dmg_bar_x,dmg_bar_y*a_boss_gui_alpha,1,1,c_white,0.3*a_boss_gui_alpha);


v_anim(a_bg,EnAnim.Boss_bg,0);
draw_sprite_ext(spr_boss_bg,0,boss_bg_x,boss_bg_y,
a_bg[EnAnimParam.Val],a_bg[EnAnimParam.Val],a_bg_rot,c_white,a_bg[EnAnimParam.Val]*a_boss_gui_alpha); 
a_bg_rot -= a_bg_rot_speed;

//v_anim(a_selector,EnAnim.Selector,0);
a_sel_scale = v_lerp(a_res,a_sel_scale,a_sel_scale_max,a_sel_scale_speed,a_sel_scale_speed/5);

for(var i = 0; i < array_length_1d(btn_x); i++)
{
    if(i == hit_pos)
    {
        v_btns_set_param(GUI_controls[i],"scale",a_sel_scale);
    }
    v_btns_draw_btn(GUI_controls[i]);
}

draw_set_alpha(_alpha);


if(a_res[0])
{
    if(a_sel_scale_max == 1.2)
    {
        a_sel_scale_max = 0.9;
    }
    else
    {
        a_sel_scale_max = 1.2;
    }
}

if(boss_lvl_end && boss_win)
{
    v_draw_text(price_x_init,(price_y_init - 100)*(1 - a_boss_gui_alpha),"Награда",fa_center,fa_top,EnText.H1);
    var _index = 0;
    for(var i = 0; i < price_count; i += 1)
    {
        if(boss_price_item[i])
        {
            var _x = price_x_init + price_x[_index];
            var _y = price_y_init*(1 - a_boss_gui_alpha);
            draw_sprite_ext(spr_GUI_btn_circle,0,_x,_y,1,1,0,c_white,1 - a_boss_gui_alpha);
            draw_sprite_ext(price_icons[i],0,_x,_y,1,1,0,c_white,1 - a_boss_gui_alpha);
            _y += 35*(1 - a_boss_gui_alpha);
            v_draw_text(_x,_y,"+" + s(boss_price_item[i]),fa_center,fa_top,EnText.H2);
            _index++
        }
    }
}

/*
if(a_res[0])
{
    a_sel_alpha = v_lerp(a_res,a_sel_alpha,0,a_sel_alpha_speed,a_sel_alpha_speed/5);
    if(a_res[0])
    {
        a_sel_scale = 0.5;
        a_sel_alpha = 1;
    }
}
for(var i = 0; i < array_length_1d(btn_x); i++)
{
    if(i == hit_pos)
    {
        draw_sprite_ext(spr_boss_selector,0,boss_bg_x + btn_x[i],boss_bg_y + btn_y[i],
        a_sel_scale,a_sel_scale,0,c_white,a_sel_alpha); 
    }
}
*/


draw_sprite(spr_monster_shadow,0,boss_x,boss_y);  
draw_sprite(spr_monster_1,0,boss_x,boss_y);





/* */
/// draw over

v_draw_alpha(0);


/* */
/*  */
