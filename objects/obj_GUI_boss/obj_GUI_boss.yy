{
    "id": "20267d90-e0aa-420d-b76e-1414c24df951",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI_boss",
    "eventList": [
        {
            "id": "55f6f5b2-f47a-4e06-b848-4f5189c6d36b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20267d90-e0aa-420d-b76e-1414c24df951"
        },
        {
            "id": "b8b27cf7-9ca2-4a6a-b8a1-d1626a9ab728",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "20267d90-e0aa-420d-b76e-1414c24df951"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d6960cef-2d41-4b43-b2e6-96945303e7a7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}