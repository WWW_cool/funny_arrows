/// @description Init befor

boss_win = 0;

randomize();
boss_time_init = 20;//20;
boss_time = boss_time_init;
boss_dmg_max = 50;//100;
boss_dmg = 0;

hit_count = 0;

hit_pos = 0;
hit_number_max = 0;
hit_number = hit_number_max;

hit_min = 7;
hit_max = 13;


GUI_xpos = 0;
GUI_ypos = 0;

point[EnPoint.X] = 0;
point[EnPoint.Y] = 0;

btn_x[0] = 0;
btn_y[0] = -150;
btn_x[1] = 150;
btn_y[1] = 0;
btn_x[2] = 0;
btn_y[2] = 150;
btn_x[3] = -150;
btn_y[3] = 0;

btn_icon[0] = spr_boss_fist_down;
btn_icon[1] = spr_boss_fist_right;
btn_icon[2] = spr_boss_fist_up;
btn_icon[3] = spr_boss_fist_left;

btn_icon_inv[0] = spr_boss_fist_down_inv;
btn_icon_inv[1] = spr_boss_fist_right_inv;
btn_icon_inv[2] = spr_boss_fist_up_inv;
btn_icon_inv[3] = spr_boss_fist_left_inv;

var ind = 0;
                                                                         
// Boss
v_choose(point,400,400,202,470);
boss_x = point[EnPoint.X];
boss_y = point[EnPoint.Y];

boss_bg_x = boss_x;
boss_bg_y = boss_y - 80;

for(var i = 0; i < 4; i++)
{
    GUI_controls[ind++] = v_btns_add(boss_bg_x + btn_x[i],boss_bg_y + btn_y[i],spr_GUI_btn_circle,id,0,
                            -1,scr_boss_hit,-1,-1,-1);
    v_btns_set_param(GUI_controls[ind - 1],"icon",btn_icon[i]);
    v_btns_set_param(GUI_controls[ind - 1],"scalability",0);
}

v_choose(point,220,40,45,50);

time_icon_x = point[EnPoint.X];
time_icon_y = point[EnPoint.Y];

time_bar_x = time_icon_x + 30;
time_bar_y = time_icon_y - 15;

v_choose(point,220,80,45,90);

dmg_icon_x = point[EnPoint.X];
dmg_icon_y = point[EnPoint.Y];

dmg_bar_x = dmg_icon_x + 30;
dmg_bar_y = dmg_icon_y - 15;

scr_boss_hit_init();

/*v_choose(point,400,550,202,670);
btn_skip_x = point[EnPoint.X];
btn_skip_y = point[EnPoint.Y];
btn_skip = v_btns_add(btn_skip_x,btn_skip_y,spr_GUI_btn_hit,id,0,
                            -1,scr_win_next,-1,-1,-1);
//v_btns_set_param(btn_skip,"icon",spr_GUI_btn_arrow_R);               
v_btns_set_text(btn_skip,0,0,
"Пропустить",1,c_black,0,f_H2,f_H2,1);
*/



/* */
action_inherited();
///Anim

a_res[0] = 0;
a_point[0] = 0;

for(var i = 0; i < EnAnimParam.LENGTH; i++)
{
    a_bg[i] = 0;
    a_selector[i] = 0;
}

a_bg[EnAnimParam.initVal] = 1;
a_bg[EnAnimParam.Val] = a_bg[EnAnimParam.initVal];

a_selector[EnAnimParam.initVal] = 1;
a_selector[EnAnimParam.Val] = a_selector[EnAnimParam.initVal];

a_bg_rot = 0;
a_bg_rot_speed = 0.5;

a_sel_scale = 0.9;
a_sel_scale_speed = 0.2;
a_sel_scale_max = 1.2;

a_sel_alpha = 1;
a_sel_alpha_speed = 0.5;

a_boss_gui_state = 0;

a_boss_gui_alpha = 0;
a_boss_gui_alpha_speed = 0.1;

// price icons
price_count = 3;
price_max = 3;
price_icons[0] = spr_GUI_icon_heart;
price_icons[1] = spr_GUI_icon_rating;
price_icons[2] = spr_GUI_icon_back;

var x_delta = 110;
price_x[0] = -x_delta;
price_x[1] = 0;
price_x[2] = x_delta;

v_choose(point,400,150,202,200);
price_x_init = point[EnPoint.X];
price_y_init = point[EnPoint.Y];



/* */
/// logic

boss_lvl_end = 0;

boss_price_count = 3;
boss_price_item = 0;



/* */
/*  */
