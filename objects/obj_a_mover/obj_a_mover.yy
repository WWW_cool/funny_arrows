{
    "id": "6c4ad9cc-96b2-4f4d-8f58-00ca7173abc2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_a_mover",
    "eventList": [
        {
            "id": "4f68a5d5-1ba5-4f55-8ab7-a6fd3df47cc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6c4ad9cc-96b2-4f4d-8f58-00ca7173abc2"
        },
        {
            "id": "5d960580-3525-4129-aa0e-0d6eb66bade6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6c4ad9cc-96b2-4f4d-8f58-00ca7173abc2"
        },
        {
            "id": "29790ad6-5f5a-48ea-9d25-70348fa8e0c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6c4ad9cc-96b2-4f4d-8f58-00ca7173abc2"
        },
        {
            "id": "d9c5473d-a4ef-4d28-b1dc-1a45a1f49608",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6c4ad9cc-96b2-4f4d-8f58-00ca7173abc2"
        },
        {
            "id": "ee5abaf2-67df-4bb9-98f5-d20b0a92bd22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6c4ad9cc-96b2-4f4d-8f58-00ca7173abc2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3fceae30-b371-4d87-8d85-8099ae8fa3a7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "be3d5f53-cfba-46a7-afc3-d9574449c7c6",
    "visible": true
}