script_execute(gui_scope_step,0,0,0,0,0);
/// move cells
if(!instance_exists(global.lvlEdit))
{
    if(m_first_step)
    {
        m_first_step = 0;
        scr_a_check_mover_inst();
        if(path_exists(m_path))
        {
            path_add_point(m_path,x,y,100);
            path_set_closed(m_path,false);
        
            var _angle = image_angle;
            var _x = x + 72*dcos(_angle);
            var _y = y - 72*dsin(_angle);
            
            path_add_point(m_path,_x,_y,100);
        }
    }
    else
    {
        if(!instance_exists(m_inst))
        {
            /*fed("step mover info");
            fed("m_inst = " + s(m_inst));
            if(!instance_exists(m_inst))fed("m_inst NOT exist");
            fed("m_pos = " + s(m_pos));
            fed("m_move_speed = " + s(m_move_speed));
            fed("m_anim_state = " + s(m_anim_state));*/
            scr_a_check_mover_inst();
        }
        else
        {
            if(m_inst.a_state == EnCellStates.DESTROY && !m_inst.a_next)
            {
                m_anim_state = EnMoverStates.DESTROY;
            }
            scr_a_move();
        }
    }
}
else
{
    m_anim_alpha = 1;
    m_anim_state = EnMoverStates.IDLE;
}

/* */
/*  */
