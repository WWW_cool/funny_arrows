/// @description  init

enum EnMoverStates{IDLE = 0,MOVE,CREATE,DESTROY,LENGTH};

m_first_step = 1;
m_inst = noone;

m_anim_start_delay = random_range(0.2,0.4);
m_anim_state = EnMoverStates.CREATE;
m_anim_alpha = 0;
m_anim_res[0] = 0;


m_move_back = 0;
m_move_speed_init = 0.03;
m_move_speed = m_move_speed_init;
m_move_boost = 0.2;

m_path = path_add();
m_pos = 0;

//late init
alarm[0] = 1;

m_move_id = -1;


script_execute(gui_scope_default,obj_a_mover,0,0,0,0);
