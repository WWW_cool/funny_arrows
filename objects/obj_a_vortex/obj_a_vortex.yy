{
    "id": "66868404-e4b1-443c-b5f9-02f7e3e00a1f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_a_vortex",
    "eventList": [
        {
            "id": "79181891-dd98-49e4-8d53-83cf2a688e6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "66868404-e4b1-443c-b5f9-02f7e3e00a1f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b50af0a4-6678-4621-a140-198f86a6df59",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3ae71181-edbd-42f7-a61b-540ac5633e5f",
    "visible": true
}