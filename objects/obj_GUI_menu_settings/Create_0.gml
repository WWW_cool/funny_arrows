/// @description  text

var ind = 0;

shop_msg[ind++] = "Звуки";
shop_msg[ind++] = "Музыка";
shop_msg[ind++] = "Сбросить прогресс игры";
shop_msg[ind++] = "Выйти в меню";





/// init
var ind = 0;
OPT_btn_spr = spr_menu_btn;
OPT_back = spr_menu_bg;
OPT_btn_font = f_text;
OPT_need_close = 0;
var OPT_btn_spr_tmp = OPT_btn_spr;
var OPT_back_tmp = OPT_back;
var OPT_btn_font_tmp = OPT_btn_font;

x = __view_get( e__VW.WView, 0 )/2 + __view_get( e__VW.XView, 0 );
y = __view_get( e__VW.HView, 0 )/2 + __view_get( e__VW.YView, 0 );;
OPT_y_tmp = y + sprite_get_height(OPT_back) - sprite_get_height(OPT_btn_spr) - 80;

OPT_header_x = x - 127;
OPT_header_y = y - 115;
OPT_header_text = "НАСТРОЙКИ";

glob = noone;
if(instance_exists(global.inst_glb))
{
    glob = global.inst_glb;
}

OPT_controls_type[ind++] = v_btns_add(0,0,spr_menu_close, id,0, 
              NO_SCRIPT,
              scr_vk_shop_close,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
scr_audio_init_close(OPT_controls_type[ind - 1]);
OPT_controls_type[ind++] = v_btns_add(0,0,scr_def_get_snd_sprite(global._def_glb_var_audio_on), id,0, 
              NO_SCRIPT,
              scr_def_snd_switch,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
v_btns_set_text(OPT_controls_type[ind - 1],-205,-2,
shop_msg[0],1,c_black,0,f_H2,f_H2,1);
v_btns_set_param(OPT_controls_type[ind - 1],"text_halign",fa_left);
v_btns_set_param(OPT_controls_type[ind - 1],"scalability",0);
v_btns_set_param(OPT_controls_type[ind - 1],"analytick_id","systemSound");

OPT_controls_type[ind++] = v_btns_add(0,0,scr_def_get_snd_sprite(global._def_glb_var_audio_on_bg), id,0, 
              NO_SCRIPT,
              scr_def_music_switch,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
              
v_btns_set_text(OPT_controls_type[ind - 1],-205,-2,
shop_msg[1],1,c_black,0,f_H2,f_H2,1);
v_btns_set_param(OPT_controls_type[ind - 1],"text_halign",fa_left);
v_btns_set_param(OPT_controls_type[ind - 1],"scalability",0);
v_btns_set_param(OPT_controls_type[ind - 1],"analytick_id","systemMusic");

OPT_controls_type[ind++] = v_btns_add(0,0,OPT_btn_spr, id,0, 
              NO_SCRIPT,
              NO_SCRIPT,
              NO_SCRIPT, 
              NO_SCRIPT,
              NO_SCRIPT
              );
if(room != rm_Start)
{
    v_btns_set_text(OPT_controls_type[ind - 1],-110,-2,
    shop_msg[3],1,c_black,0,f_text,f_text,1);
    v_btns_set_cb(OPT_controls_type[ind - 1],"press",scr_win_start);
    v_btns_set_param(OPT_controls_type[ind - 1],"analytick_id","systemMainScreen");
}
else
{
    v_btns_set_text(OPT_controls_type[ind - 1],-110,-2,
    shop_msg[2],1,c_black,0,f_text,f_text,1);
    v_btns_set_cb(OPT_controls_type[ind - 1],"press",scr_g_reset);
    v_btns_set_param(OPT_controls_type[ind - 1],"analytick_id","systemReset");
}
v_btns_set_param(OPT_controls_type[ind - 1],"text_halign",fa_left);

                     
ind = 0;        
OPT_controls_x[ind] = x + 140 - 18;
OPT_controls_y[ind++] = y - 131; 

var _y_delta = 4;

OPT_controls_x[ind] = x + 75;
OPT_controls_y[ind++] = y - 35 - _y_delta; 
OPT_controls_x[ind] = x + 75;
OPT_controls_y[ind++] = y + 35 - _y_delta; 
OPT_controls_x[ind] = x;
OPT_controls_y[ind++] = y + 105 - _y_delta; 

//////////////////////////////////////////////////////////////////////////////////////////
event_inherited();
//////////////////////////////////////////////////////////////////////////////////////////

OPT_btn_spr = OPT_btn_spr_tmp;
OPT_back = OPT_back_tmp;
OPT_btn_font = OPT_btn_font_tmp;




