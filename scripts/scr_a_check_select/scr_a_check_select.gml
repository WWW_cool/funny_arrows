/// @description scr_a_check_select(inst)

var _inst = argument0;

if(!instance_exists(_inst))exit;

if(global.a_hint_reverse)
{
    cb_gui_reverse();
}

var is_destroed = false;

if(_inst.a_lock)return false;

for(var i = 0; i < instance_number(obj_a_parent); i++)
{
    var tmp_ = instance_find(obj_a_parent,i);
    if(tmp_.a_state == EnCellStates.DESTROY)
    {
        is_destroed = true;
        break;
    }
}

var res = true;

switch(_inst.a_type)
{
    case EnCellTypes.TIME:
        if(_inst.a_p_turn_count > 0)res = false;
    break;
    default:
        if(is_destroed)
        {
            res = false;
        }
        else
        {
            res = true;
        }     
}

return res;

