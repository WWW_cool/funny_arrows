/// @description scr_a_anim_end
switch(a_anim_state)
{
    case EnCellAnimTypes.COMMON:
        switch(a_type)
        {
            case EnCellTypes.NONE:
            break;
            case EnCellTypes.SHIELD:
            break;
            case EnCellTypes.LIFT:
                scr_a_anim_end_lift();
            break;
            case EnCellTypes.TIME:
            break;
            case EnCellTypes.DOUBLE:
                var _a_colour   = a_colour;
                a_colour        = a_p_double_colour;
                a_p_double_colour  = _a_colour;
            break;
        }
    break;
    case EnCellAnimTypes.LIFT:
        scr_a_anim_end_lift();
    break;
}

