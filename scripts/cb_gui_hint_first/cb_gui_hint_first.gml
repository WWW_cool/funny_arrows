/// @description cb_gui_hint_first

if(global.a_hint_first_turn)
{
    global.a_hint_first_turn = 0;
}
else
{
    if(glob.hint_fstep_count || global.user_open_all || glob.game_hint_fstep)
    {
        if(glob.game_hint_fstep == 0)
        {
            glob.hint_fstep_count -= 1;
            glob.game_current_lvl_try += 0.1;
        }
        global.a_hint_first_turn = 1;
        glob.game_hint_fstep = 1;
    }
    else
    {
        var _inst = scr_s_msg(EnMsgTypes.FStep);
        if(instance_exists(_inst))
        {
            _inst.OPT_msg_type = 0;
        }
    }
}



