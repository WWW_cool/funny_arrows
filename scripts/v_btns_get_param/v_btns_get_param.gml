/// @description v_btns_get_param(btn_id, p_name)

var tmp_id = argument0;
var tmp_name = argument1;
var res = -1;
var inst = global._v_btns_owner;
if(instance_exists(inst))
with(inst)
{
    switch(tmp_name)
    {
        case "lay_on":
        res = _v_btns_mouse_lay_on_flag[tmp_id];
        //fed("res = " + string(res));
        break;
        case "xpos":
            res = _v_btns_xpos[tmp_id];
        break;
        case "ypos":
            res = _v_btns_ypos[tmp_id];
        break;
        case "spr":
            res = _v_btns_spr[tmp_id];
        break;
    }
}
return res;

