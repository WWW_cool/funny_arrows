/// @description scr_level_load_obj(type,section,num)

var obj_type    = argument0;
var tmp_section = argument1;
var obj_num     = argument2;
var inst = noone;
var tmp_x,tmp_y;

switch(obj_type)
{
    case "obj_a_parent":
        tmp_x = ini_read_real(tmp_section,"x"+string(obj_num),0);
        tmp_y = ini_read_real(tmp_section,"y"+string(obj_num),0);
        inst = instance_create(tmp_x,tmp_y,asset_get_index(obj_type));
        if(instance_exists(inst))
        {
            inst.scope_create = 0;
            inst.scope_obj_type     = asset_get_index(obj_type);
            
            var ind = 0;
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"GRID"+string(obj_num),32);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Layer"+string(obj_num),0);
            ind+= 2;
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Type"+string(obj_num),0);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Colour"+string(obj_num),0);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Dir"+string(obj_num),0);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Angle"+string(obj_num),0);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Shield"+string(obj_num),0);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Unlock"+string(obj_num),0);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Double"+string(obj_num),0);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"First"+string(obj_num),0);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Child_IS"+string(obj_num),0);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Child_Type"+string(obj_num),0);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Child_Colour"+string(obj_num),0);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Child_Dir"+string(obj_num),0);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Child_Angle"+string(obj_num),0);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Child_Shield"+string(obj_num),0); 
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Child_Unlock"+string(obj_num),0); 
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Child_Double"+string(obj_num),0); 
            ind = 4;
            with(inst)
            {
                a_type          = scope_vars[ind++,1];
                a_colour        = scope_vars[ind++,1];
                a_dir           = scope_vars[ind++,1];
                a_angle         = scope_vars[ind++,1];
                a_p_shield         = scope_vars[ind++,1];
                a_p_turn_count     = scope_vars[ind++,1];
                a_p_double_colour  = scope_vars[ind++,1];
                a_p_first_turn  = scope_vars[ind++,1];
                
                a_next          = scope_vars[ind++,1];
                a_next_type     = scope_vars[ind++,1];
                a_next_colour   = scope_vars[ind++,1];
                a_next_dir      = scope_vars[ind++,1];
                a_next_angle    = scope_vars[ind++,1];
                a_next_p_shield         = scope_vars[ind++,1];
                a_next_p_turn_count     = scope_vars[ind++,1];
                a_next_p_double_colour  = scope_vars[ind++,1];
            }
            scr_level_add_obj(inst);
            grid = inst.scope_vars[0,1];
            if(instance_exists(global.lvlEdit))
            {
                with(global.lvlEdit)
                {
                    gui_set_value(guid,"GRID",grid);
                    //gui_scope_init(inst);
                }
            }
        }   
    break;
    case "obj_a_mover":
        tmp_x = ini_read_real(tmp_section,"x"+string(obj_num),0);
        tmp_y = ini_read_real(tmp_section,"y"+string(obj_num),0);
        inst = instance_create(tmp_x,tmp_y,asset_get_index(obj_type));
        if(instance_exists(inst))
        {
            inst.scope_create = 0;
            inst.scope_obj_type     = asset_get_index(obj_type);
            
            var ind = 0;
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"GRID"+string(obj_num),32);
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Layer"+string(obj_num),0);
            ind+= 2;
            inst.scope_vars[ind++,1] = ini_read_real(tmp_section,"Angle"+string(obj_num),0);
            ind = 4;
            with(inst)
            {
                image_angle = scope_vars[ind++,1]*90;
            }
            scr_level_add_obj(inst);
            grid = inst.scope_vars[0,1];
            if(instance_exists(global.lvlEdit))
            {
                with(global.lvlEdit)
                {
                    gui_set_value(guid,"GRID",grid);
                    //gui_scope_init(inst);
                }
            }
        }  
    break;
}

