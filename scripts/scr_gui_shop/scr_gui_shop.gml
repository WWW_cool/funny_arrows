/// @description scr_gui_shop(type)

var _type = argument0;
var res = false;
if(!instance_exists(global.inst_glb)) exit;

scr_vk_tryAd();

switch(OPT_type)
{
    case EnMsgTypes.Moveback:
        global.inst_glb.hint_moveback_count += 6;
        res = true;
    break;
    case EnMsgTypes.FStep:
        global.inst_glb.hint_fstep_count += 3;
        res = true;
    break;
    case EnMsgTypes.Lives:
        global.inst_glb.live_count = global.inst_glb.live_count_max;
        res = true;
    break;
    default:
        fed("DNG | unknown shop item");
}

if(res)
{
    scr_vk_shop_close();
}


