/// @description v_anim_ext(params[],period(sec),path,speed,delta,random)
/// @param params[]
/// @param period(sec

var _params = argument0;
var _period = argument1;

var _path = argument2;
var _speed = argument3;
var _delta = argument4;
var _random = argument5;

var initVal = _params[EnAnimParam.initVal];
var val = _params[EnAnimParam.Val];
var point = _params[EnAnimParam.Point];
var time = _params[EnAnimParam.Time];

if(_params[EnAnimParam.Time] == 0)
{
    a_point[@ 0] = _params[EnAnimParam.Point];
    _params[@ EnAnimParam.Val] = v_path_anim_ext(a_res,_params[EnAnimParam.Val],_path,a_point,_speed,_delta,_random);
    _params[@ EnAnimParam.Point] = a_point[0];
    if(a_res[0])
    {
        if(_period)
        {
            _params[@ EnAnimParam.Time] = _period;
        }
        else
        {
            _params[@ EnAnimParam.Time] = 0;
            _params[@ EnAnimParam.Point] = 0;
            _params[@ EnAnimParam.Val] = initVal;
            a_res[@ 0] = 0;
        }
    }
}
else
{
    _params[@ EnAnimParam.Time] -= delta_time/1000000;
    if(_params[EnAnimParam.Time] < 0)
    {
        _params[@ EnAnimParam.Time] = 0;
        _params[@ EnAnimParam.Point] = 0;
        _params[@ EnAnimParam.Val] = initVal;
        a_res[@ 0] = 0;
    }
}
