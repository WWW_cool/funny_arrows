/// @description scr_gui_price_efx(type,val)

var _type = argument0;
var _val = argument1;

if(!instance_exists(global.inst_glb)) exit;

var _text = "+" + s(_val);
var _back = noone;
var _icon = noone;

switch(_type)
{
    case EnPriceType.Life:
        _back = spr_GUI_btn_circle;
        _icon = spr_GUI_icon_heart;
        global.inst_glb.live_count += _val;
    break;
    case EnPriceType.FirstTurn:
        _back = spr_GUI_btn_circle;
        _icon = spr_GUI_icon_fstep;
        global.inst_glb.hint_fstep_count += _val;
    break;
    case EnPriceType.MoveBack:
        _back = spr_GUI_btn_circle;
        _icon = spr_GUI_icon_back;
        global.inst_glb.hint_moveback_count += _val;
    break;
    case EnPriceType.Life_clock:
        _back = spr_GUI_icon_heart;
        _icon = spr_GUI_icon_clock;
        global.inst_glb.live_count += _val;
    break;
}
var _inst = instance_create(1,1,obj_GUI_life_efx);
_inst.OPT_text = _text;
_inst.OPT_back = _back;
_inst.OPT_icon = _icon;
_inst.anim_life_time = 1;
_inst.anim_atart_time = random(0.7);
_inst._angle = 0;

var _angle_list = ds_list_create();
for(var i = 0; i < 7; i++)
{
    ds_list_add(_angle_list,45*i);
}

for(var i = 0; i < instance_number(obj_GUI_life_efx); i++)
{
    var _efx = instance_find(obj_GUI_life_efx,i);
    if(_efx != _inst)
    {
        var _ind = ds_list_find_index(_angle_list,_efx._angle);
        if(_ind >= 0)
        {
            ds_list_delete(_angle_list,_ind);
        }
    }
}
var _rnd_dir = _angle_list[| irandom(ds_list_size(_angle_list) - 1)];
if(_rnd_dir != undefined)
{
    _inst._angle = _rnd_dir;
}
else
{
    _rnd_dir = irandom(360);
}

ds_list_destroy(_angle_list);
var _dist = _inst.OPT_rnd_pos_w;
_dist = irandom_range(_dist*0.5,_dist);

var _x = lengthdir_x(_dist,_rnd_dir);
var _y = lengthdir_y(_dist,_rnd_dir);

_inst.x += _x;
_inst.y += _y;


