/// @description scr_g_save
//fed("save game");
if(instance_exists(global.inst_glb))
{
    with(global.inst_glb)
    {         
        if(room == rm_load || room > rm_lvlCreate)
        {
            if(game_win)
            {
                game_win = 0;
                if(game_lvl_ok < game_lvl)
                {
                    game_lvl_ok = game_lvl;
                    fed("lvl ok = " + s(game_lvl_ok));
                    v_db_invoke(game_user_id + ";" + s(game_lvl_ok), V_DB_REQUEST_Types.INVOKE);
                }
            }
        }
        var game_time = date_get_second_of_year(date_current_datetime());
        if(os_browser == browser_not_a_browser || game_user_id == "demo")
        {
            ini_open("saves");
            ini_write_real("game","lvl_ok",game_lvl_ok);
            ini_write_real("game","hint_fstep",hint_fstep_count);
            ini_write_real("game","hint_back",hint_moveback_count);
            ini_write_real("game","live_count",live_count);
            ini_write_real("game","live_time",live_time);
            ini_write_real("game","price_time",price_time);
            ini_write_real("game","game_time",game_time); 
            ini_write_real("game","game_points",game_points);
            ini_write_real("game","game_lvl_try",game_current_lvl_try);
            ini_write_real("system","sound",global._def_glb_var_audio_on);
            ini_write_real("system","music",global._def_glb_var_audio_on_bg);
            ini_close();
            fed("game saved");
        }
        else
        {
            scr_db_lvl_ok(game_lvl_ok);
            scr_db_save_param(EnDbKeys.HINT_FSTEP,hint_fstep_count);
            scr_db_save_param(EnDbKeys.HINT_BACK,hint_moveback_count);
            scr_db_save_param(EnDbKeys.LIVE_COUNT,live_count);
            scr_db_save_param(EnDbKeys.LIVE_TIME,live_time);
            scr_db_save_param(EnDbKeys.GAME_TIME,game_time);
            scr_db_save_param(EnDbKeys.GAME_POINTS,game_points);
            scr_db_save_param(EnDbKeys.GAME_LVL_TRY,game_current_lvl_try);
            //fed("rewrite from g_save live_count - " + s(live_count));
            v_db_rewrite(game_user_id);
            ini_open("saves");
            ini_write_real("game","lvl_ok",game_lvl_ok);
            ini_write_real("game","hint_fstep",hint_fstep_count);
            ini_write_real("game","hint_back",hint_moveback_count);
            ini_write_real("game","live_count",live_count);
            ini_write_real("game","live_time",live_time);
            ini_write_real("game","price_time",price_time);
            ini_write_real("game","game_time",game_time); 
            ini_write_real("game","game_points",game_points);
            ini_write_real("game","game_lvl_try",game_current_lvl_try);
            ini_write_real("system","sound",global._def_glb_var_audio_on);
            ini_write_real("system","music",global._def_glb_var_audio_on_bg);
            ini_close();
        }
    }
}



