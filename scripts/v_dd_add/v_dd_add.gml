/// @description v_dd_add(x,y,type,owner,spr_in,spr_out,cb_take,cb_put)

var tmp_x = argument[0];
var tmp_y = argument[1];

var tmp_type        = argument[2]; // source/dest
var tmp_owner       = argument[3]; 
var tmp_spr_in      = argument[4]; // my for v_btn
var tmp_spr_out     = argument[5]; // when drag

var tmp_cb_take  = argument[6]; 
var tmp_cb_put   = argument[7]; 
for (var i = 0; i < _dd_count; i += 1)
{
    if (_dd_deleted[i])
    {
        _dd_xpos[i]     = tmp_x;
        _dd_ypos[i]     = tmp_y;
        _dd_x_shift[i]  = 0;
        _dd_y_shift[i]  = 0;
        _dd_type[i]     = tmp_type;
        _dd_owner[i]    = tmp_owner;
        _dd_spr_in[i]   = tmp_spr_in;
        _dd_spr_mid[i]  = noone;
        _dd_spr_out[i]  = tmp_spr_out;
        _dd_cb_take[i]  = tmp_cb_take;
        _dd_cb_put[i]   = tmp_cb_put;
        _dd_cb_draw[i]  = noone;
        _dd_v_btn[i]    = v_btns_add(tmp_x,tmp_y,tmp_spr_in,
                                tmp_owner,0,-1,v_dd_cb_take,v_dd_cb_put,-1,-1);
        v_btns_set_param(_dd_v_btn[i],"cantDraw",1);
        //РІРЅСѓС‚СЂРµРЅРЅРёРµ РїРµСЂРµРјРµРЅРЅС‹Рµ СЃРѕСЃС‚РѕСЏРЅРёР№
        _dd_active[i]   = 1;
        if(sprite_exists(tmp_spr_out))
        {
            _dd_state[i]    = "full";
        }
        else
        {
            _dd_state[i]    = "empty";
        }
        _dd_mouse_x_delta[i] = 0;
        _dd_mouse_y_delta[i] = 0;
        _dd_mouse_x_init[i] = 0;
        _dd_mouse_y_init[i] = 0;
        _dd_deleted[i] = 0;
        return i;
    }
}
    _dd_xpos[_dd_count]     = tmp_x;
    _dd_ypos[_dd_count]     = tmp_y;
    _dd_x_shift[_dd_count]  = 0;
    _dd_y_shift[_dd_count]  = 0;
    _dd_type[_dd_count]     = tmp_type;
    _dd_owner[_dd_count]    = tmp_owner;
    _dd_spr_in[_dd_count]   = tmp_spr_in;
    _dd_spr_mid[_dd_count]  = noone;
    _dd_spr_out[_dd_count]  = tmp_spr_out;
    _dd_cb_take[_dd_count]  = tmp_cb_take;
    _dd_cb_put[_dd_count]   = tmp_cb_put;
    _dd_cb_draw[_dd_count]  = noone;
    _dd_v_btn[_dd_count]    = v_btns_add(tmp_x,tmp_y,tmp_spr_in,
                            tmp_owner,0,-1,v_dd_cb_take,v_dd_cb_put,-1,-1);
    v_btns_set_param(_dd_v_btn[_dd_count],"cantDraw",1);
    //РІРЅСѓС‚СЂРµРЅРЅРёРµ РїРµСЂРµРјРµРЅРЅС‹Рµ СЃРѕСЃС‚РѕСЏРЅРёР№
    _dd_active[_dd_count]   = 1;
    if(sprite_exists(tmp_spr_out))
    {
        _dd_state[_dd_count]    = "full";
    }
    else
    {
        _dd_state[_dd_count]    = "empty";
    }
    _dd_mouse_x_delta[_dd_count] = 0;
    _dd_mouse_y_delta[_dd_count] = 0;
    _dd_mouse_x_init[_dd_count] = 0;
    _dd_mouse_y_init[_dd_count] = 0;
        
    _dd_deleted[_dd_count] = 0;
    
    _dd_count += 1;
    return _dd_count - 1;


