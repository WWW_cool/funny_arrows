/// @description v_path_anim(res[],val,path,point[],speed,delta)

var res         = argument0;
var val         = argument1;
var path        = argument2;
var point       = argument3;
var v_speed     = argument4;
var delta       = argument5;

var increment   = 128;

if(path_exists(path))
{
    var path_length = path_get_number(path);
    if(point[0] < path_length)
    {
        if(point[0] > 0)
        {
            var dif = abs(path_get_point_y(path,point[0]) - increment)/path_get_point_y(path,0);
            if(dif != 0)
            v_speed *= dif*3;
            //fed("v_speed = " + s(v_speed));
        }
        val = v_increment(res,val,path_get_point_y(path,point[0])/increment,v_speed,delta);
        if(res[0])
        {
            point[@ 0] += 1;
            res[@ 0] = false;
            return val;
        }
        else
        {
            res[@ 0] = false;
            return val;
        }
    }
    else
    {
        res[@ 0] = true;
        return val;
    }
}
else
{
    res[@ 0] = false;
    return val;
}


