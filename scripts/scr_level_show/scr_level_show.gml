/// @description scr_level_show(x,y)

var tmp_x = argument0;
var tmp_y = argument1;

var list = global.lvlObjList;

if(ds_exists(list,ds_type_list))
{
    for(var i = 0; i < ds_list_size(list); i += 1)
    {
        if(instance_exists(list[|i]))
        {
            if(list[|i] == scope_inst)draw_set_colour(c_red);
            else draw_set_colour(c_green);
            draw_text(tmp_x,tmp_y + 15*i,
            string_hash_to_newline(string(i + 1) + " : " 
            + object_get_name(list[|i].scope_obj_type) 
            + " " + string(list[|i]) + " layer : " 
            + string(list[|i].scope_vars[1,1])));
        }
    }
}

tmp_y += 300;

if(is_array(file_list) && lvl_num = 0)
{
    for(var i = 0; i < array_length_1d(file_list); i += 1)
    {
        if(i == file_index)draw_set_colour(c_red);
        else draw_set_colour(c_green);
        draw_text(tmp_x,tmp_y + 15*i,
        string_hash_to_newline(string(i + 1) + " : " + file_list[i]));
    }
}
