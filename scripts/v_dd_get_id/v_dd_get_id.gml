/// @description v_dd_get_id(v_btn_id)
var tmp_ind = argument0;

if(_dd_count > 0)
{
    for(var i = 0; i < _dd_count; i += 1)
    {
        if (!_dd_deleted[i])
        {
            if(_dd_v_btn[i] == tmp_ind)
            {
                return i;
            }
        }
    }
}
return -1;


