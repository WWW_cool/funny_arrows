/// @description scr_boss_start

var _inst = instance_create(1,1,obj_GUI_boss);

if(instance_exists(_inst))
{
    _inst.boss_price_count = array_length_1d(price_item_count);
    for(var i = 0; i < array_length_1d(price_item_count); i += 1)
    {
        if(price_item_count[i])
        {
            _inst.boss_price_item[i] = price_item_count[i];
        }
    }
}

instance_destroy();
