/// @description v_sld_cb_release

/// system callback
var tmp_id = -1;
var tmp_obj = noone;
if(argument_count > 0)
{
    tmp_obj = global.v_sld_src_obj
    tmp_id = global.v_sld_src_id;

    if(instance_exists(tmp_obj))
    {
        if(tmp_id != -1)
        {
            with(tmp_obj)
            {
                v_sld_state[tmp_id] = "idle";
                v_sld_value[tmp_id] = v_sld_get_val(tmp_id);
                if(script_exists(v_sld_callback[tmp_id]))
                {
                    script_execute(v_sld_callback[tmp_id],v_sld_value[tmp_id]);
                }
            }
        }
    }
}



