/// @description scr_boss_hit

if(argument_count > 0)
{
    var btn_id = argument[0];
    for(var i = 0; i < array_length_1d(GUI_controls); i++)
    {
        if(btn_id == GUI_controls[i])
        {
            if(i == hit_pos)
            {
                a_sel_scale = 0.9;
                hit_number--;
                if(hit_number <= 0)
                {
                    scr_boss_hit_init();
                    boss_dmg += irandom_range(hit_min,hit_max);
                    
                    var _x = irandom_range(-50,50);
                    var _y = irandom_range(-50,50);
                    
                    instance_create(boss_bg_x + _x,boss_bg_y + _y,obj_boss_efx_hit);
                }
                hit_count++;
                if(!hit_count%4)
                {
                    boss_dmg += irandom_range(hit_min,hit_max);
                }
            }
            instance_create(other._v_btns_xpos[btn_id],other._v_btns_ypos[btn_id],obj_boss_efx_click);
        }
    }
}


