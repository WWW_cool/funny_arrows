/// @description cb_gui_moveBack
if(glob.hint_moveback_count || global.user_open_all)
{
    glob.hint_moveback_count -= 1;
    glob.game_current_lvl_try += 0.05;
    scr_a_move_back_tiles();
    scr_a_move_back_mover();
    if(instance_exists(global.a_selector))
    {
        global.a_selector.s_lvl_win = 0;
        glob.game_lose = 0;
    }
}
else
{
    var _inst = scr_s_msg(EnMsgTypes.Moveback);
    if(instance_exists(_inst))
    {
        _inst.OPT_msg_type = 1;
    }
}

