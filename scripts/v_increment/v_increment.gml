/// @description v_increment(res[],val_from,val_to,v_speed,delta)

var res         = argument0;
var val_from    = argument1;
var val_to      = argument2;
var v_speed     = argument3;
var delta       = argument4;


if(abs(val_from - val_to) > delta)
{
    res[@ 0] = false;
    if(val_to > val_from)
    {
        val_from += v_speed;
        if(val_from > val_to) val_from = val_to;
    }
    else
    {
        val_from -= v_speed;
        if(val_from < val_to) val_from = val_to;   
    }
    return val_from;
}
else
{
    res[@ 0] = true;
    return val_to;
}
