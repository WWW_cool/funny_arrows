/// @description v_map_clear(map)
var map = argument[0];

/**
    This function will clear the (previously created) v_map of all 
    key/value pairs, but it will not delete the v_map itself from memory. 
    For that you should use the function v_map_destroy. 
**/

if(is_array(map))
{
    //Проходим по всему массиву
    var length = array_height_2d(map); 
    for(var i=0; i<length; i++)
    { 
        //И очищаем к чертям
        map[@ i,0] = -1;
        map[@ i,1] = -1;        
    }
    return true;  
}

return false;

