/// @description  gui_destroy_gui(gui);
/// @param gui
/*
    Destroy gui
*/

n = ds_list_size(argument0);

// Free arrays
for (var i = 0; i < n; i++ ) {
    argument0[|i] = 0;
}
ds_list_clear(argument0);
// Destroy gui
ds_list_destroy(argument0);
