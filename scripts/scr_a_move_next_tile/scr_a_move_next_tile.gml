/// @description scr_a_move_next_tile(inst)

var _inst = argument0;
if(instance_exists(global.a_study_inst))exit;

if(instance_exists(global.a_selector))
{
    with(global.a_selector)
    {
        a_move_tile_xpos[_inst.a_move_id,a_move_index] = _inst.x;
        a_move_tile_ypos[_inst.a_move_id,a_move_index] = _inst.y;
        
        a_move_tile_state[_inst.a_move_id,a_move_index]          = _inst.a_state;
        
        a_move_tile_colour[_inst.a_move_id,a_move_index]         = _inst.a_colour;
        a_move_tile_dir[_inst.a_move_id,a_move_index]            = _inst.a_dir;
        a_move_tile_angle[_inst.a_move_id,a_move_index]          = _inst.a_angle;
        a_move_tile_type[_inst.a_move_id,a_move_index]           = _inst.a_type;
        a_move_tile_first[_inst.a_move_id,a_move_index]          = _inst.a_first;
        a_move_tile_p_shield[_inst.a_move_id,a_move_index]       = _inst.a_p_shield; 
        a_move_tile_p_turn_count[_inst.a_move_id,a_move_index]   = _inst.a_p_turn_count;
        a_move_tile_p_double_colour[_inst.a_move_id,a_move_index]= _inst.a_p_double_colour; 
        a_move_tile_p_mover_inst[_inst.a_move_id,a_move_index]   = _inst.a_p_mover_inst;
        a_move_tile_p_first_turn[_inst.a_move_id,a_move_index]   = _inst.a_p_first_turn;
        
        a_move_tile_next_colour[_inst.a_move_id,a_move_index]    = _inst.a_next_colour;
        a_move_tile_next_dir[_inst.a_move_id,a_move_index]       = _inst.a_next_dir;
        a_move_tile_next_angle[_inst.a_move_id,a_move_index]     = _inst.a_next_angle;
        a_move_tile_next_type[_inst.a_move_id,a_move_index]      = _inst.a_next_type;
        
        a_move_tile_next_p_shield[_inst.a_move_id,a_move_index]  = _inst.a_next_p_shield;
        a_move_tile_next_p_turn_count[_inst.a_move_id,a_move_index]      = _inst.a_next_p_turn_count;
        a_move_tile_next_p_double_colour[_inst.a_move_id,a_move_index]   = _inst.a_next_p_double_colour;
        a_move_tile_next_p_mover_inst[_inst.a_move_id,a_move_index]      = _inst.a_next_p_mover_inst;
    }
}






