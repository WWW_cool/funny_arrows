/// @description scr_boss_prev

if(instance_exists(global.inst_glb))
{
    with(global.inst_glb)
    {
        if(game_boss_index > 0)
        {
            game_boss_index -= 1;
            if(game_boss_lock[game_boss_index])
            {
                v_btns_set_param(other.main_play,"active",0);
            }
            else
            {
                v_btns_set_param(other.main_play,"active",1);
                scr_boss_play(other.main_play);
            }
            if(game_boss_index == 0)
            {
                v_btns_set_param(other.main_arrow_l,"active",0);
            }
            else
            {
                v_btns_set_param(other.main_arrow_r,"active",1);
            }
        }
    }
}





