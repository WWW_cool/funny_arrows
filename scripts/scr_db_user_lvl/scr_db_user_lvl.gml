/// @description scr_db_user_lvl(cmd_type)

enum EnUserLvlCmd{CHECK_XP = 0,CHECK_LVL,CHECK_AMOUNT,
CHECK_XP_INIT,CHECK_LVL_INIT,LENGTH};

var cmd = argument0;

if(global.user_open_all)
{
    return 0;
}


if(instance_exists(global.inst_glb))
{
    switch(cmd)
    {
        case EnUserLvlCmd.CHECK_XP:
            if(global.user_xp_new > global.user_xp)
            {
                global.user_xp = global.user_xp_new;
                scr_db_user_lvl(EnUserLvlCmd.CHECK_LVL);
                scr_db_update_amount();
            }
        break;
        case EnUserLvlCmd.CHECK_LVL:
            if(global.user_lvl < array_length_1d(global.user_lvl_amounts) - 1)
            {
                if(!global.v_db_read_first)
                {
                    global.user_lvl_amount_new = global.user_xp;
                }
                else
                {
                    global.v_db_read_first = 0;
                    global.user_lvl_amount_new = global.user_xp;
                    global.user_lvl_amount = global.user_xp;
                    for(var i = 1; i < array_length_1d(global.user_lvl_amounts); i += 1)
                    {
                        if(global.user_lvl_amount >= global.user_lvl_amounts[i])
                        {
                            if(global.user_lvl < i)
                            {
                                global.user_lvl = i;
                            }
                        }
                    }
                }
            }
            else
            {
                global.user_lvl = array_length_1d(global.user_lvl_amounts) - 1;
                global.user_lvl_amount = 0;
                global.user_lvl_amount_new = 0;
            }
        break;
        case EnUserLvlCmd.CHECK_AMOUNT:
            if(global.user_lvl_amount != global.user_lvl_amount_new)
            {
                global.user_lvl_amount = v_lerp(noone,global.user_lvl_amount,
                                                global.user_lvl_amount_new,0.2,0.05);
                for(var i = 1; i < array_length_1d(global.user_lvl_amounts); i += 1)
                {
                    if(global.user_lvl_amount >= global.user_lvl_amounts[i])
                    {
                        if(global.user_lvl < i)
                        {
                            // new user lvl event;
                            fed("New LEVEL! Your lvl = " + s(i));
                            global.user_lvl = i;
                            //instance_create(1,1,obj_GUI_lvl_up);
                            //JS_vk_lvl_up(global.user_lvl + 1);
                        }
                    }
                }
            }
        break;
        case EnUserLvlCmd.CHECK_XP_INIT:
            
        break;
        case EnUserLvlCmd.CHECK_LVL_INIT:
            
        break;
    }
}













