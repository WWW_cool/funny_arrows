/// @description v_draw_text_ext(x,y,str,halign,valign,type,w)

var _x = argument0;
var _y = argument1;
var _str = argument2;
var _halign = argument3;
var _valign = argument4;
var _type = argument5;
var _w = argument6;

var _font = noone;

switch(_type)
{
    case EnText.Text:
        _font = global._def_glb_f_text;
        break;
    case EnText.H1:
        _font = global._def_glb_f_H1;
        break;
    case EnText.H2:
        _font = global._def_glb_f_H2;
        break;
    default:
        exit;      
}

if(font_exists(_font))
{
    draw_set_font(_font);
    draw_set_halign(_halign);
    draw_set_valign(_valign);
    
    if(_w == 0)
    {
        draw_text(_x,_y,string_hash_to_newline(_str));
    }
    else
    {
        var str_h = font_get_size(_font)*1.2;
        draw_text_ext(_x,_y,string_hash_to_newline(_str),str_h,_w);
    }   
}  


