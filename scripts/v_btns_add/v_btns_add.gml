/// @description v_btns_add(x,y,spr, owner, colddown, cb_m_init, cb_m_pressed, cb_m_released, cb_m_lay_on, cb_m_lay_off)

//Проверяем, что существует обработчки кнопок
var inst = global._v_btns_owner;
if(instance_exists(inst))
with(inst)
{
    // Defines a new button and returns it's ID
    var i, arg;
    for(i=0; i<10; i++)
    {
        if(argument_count > i)
            arg[i] = argument[i]
        else arg[i] = -1;       
    }
    
    var tmp_x                   = arg[0];
    var tmp_y                   = arg[1];
    var tmp_spr                 = arg[2];
    var tmp_owner               = arg[3];
    var tmp_colddown            = arg[4];
    var tmp_callback_init       = arg[5];
    var tmp_callback_pressed    = arg[6];
    var tmp_callback_released   = arg[7];
    var tmp_callback_lay_on     = arg[8];
    var tmp_callback_lay_off    = arg[9];
    
    // TODO добавить колбек релиз фрии - т.е событие,
    // которое срабатывает если отпускаем мышь в том месте,
    // где нет наших кнопок. Нужно для того чтобы сбрасывать драг анд дроп событие
    // причем лучше чтобы эта переменная по умолчанию была = -1, т.е пустой ссылке
    // и была отдельная функция установки коллбека - это сразу оптимизация, чтобы 
    // не дергать все коллбеки фрии, а устанавливать такой коллбек только,
    // для активного переноса
    
    for (i = 0; i < _v_btns_count; i += 1)
    {
        if (_v_btns_deleted[i])
        {
            //Button init callback
            if(script_exists(tmp_callback_init))
            {
                if(instance_exists(tmp_owner))
                with(tmp_owner)
                {
                    script_execute(tmp_callback_init,i);
            }}    
        
            _v_btns_xpos[i] = tmp_x;
            _v_btns_ypos[i] = tmp_y;
            _v_btns_active[i] = 1;
            _v_btns_spr_bg[i] = noone;
            _v_btns_spr[i] = tmp_spr;
            _v_btns_spr_icon[i] = noone;
            _v_btns_spr_icon_x[i] = 0;
            _v_btns_spr_icon_y[i] = 0;
            _v_btns_spr_index[i] = 0;
            _v_btns_spr_w[i] = sprite_get_width(tmp_spr);
            _v_btns_spr_h[i] = sprite_get_height(tmp_spr);
            _v_btns_spr_x_offset[i] = sprite_get_xoffset(tmp_spr);
            _v_btns_spr_y_offset[i] = sprite_get_yoffset(tmp_spr);
            _v_btns_spr_scale[i] = 1;
            _v_btns_spr_sc_add[i] = 0.1;
            _v_btns_spr_sc_speed[i] = 0.3;
            _v_btns_spr_sc[i] = 0;
            _v_btns_callback_mouse_pressed[i]    = tmp_callback_pressed;
            _v_btns_callback_mouse_released[i]   = tmp_callback_released; 
            _v_btns_callback_mouse_lay_on[i]     = tmp_callback_lay_on;
            _v_btns_callback_mouse_lay_off[i]    = tmp_callback_lay_off;
            _v_btns_callback_mouse_release_free[i]    = -1;
            _v_btns_mouse_lay_on_flag[i] = false;
            _v_btns_owner[i] = tmp_owner;
            _v_btns_colddown_time_init[i] = tmp_colddown;
            _v_btns_colddown_time[i] = 0;
            _v_btns_colddown[i] = 0;
            _v_btns_cant_draw[i] = 0;
            //////////////////////////////
            // draw text on v_btns
            //////////////////////////////
            _v_btns_text[i] = "";   /// localization key
            _v_btns_noKey[i] = 0;   /// not a key
            _v_btns_text_angle[i] = 0;
            _v_btns_text_scale[i] = 1;
            _v_btns_text_col[i] = c_white;
            _v_btns_text_x[i] = 0;
            _v_btns_text_y[i] = 0;
            _v_btns_text_font_lay_off[i] = noone;
            _v_btns_text_font_lay_on[i] = noone;
            _v_btns_text_halign[i] = fa_center;
            //////////////////////////////
            //////////////////////////////
            // 2click on v_btns
            //////////////////////////////
            _v_btns_2click_start[i] = 0;
            _v_btns_2click_release[i] = 0;
            _v_btns_2click_time[i] = 0;
            _v_btns_2click_cb[i] = -1;
            
            _v_btns_spr_freeze[i] = -1;
            _v_btns_callback_draw[i] = -1;
            
            _v_btns_sound[i] = noone;
            _v_btns_sound_in_array[i] = 0;
            
            _v_btns_analytics_id[i] = "";
            
            _v_btns_deleted[i] = false;
    
            return i;
        }
    }
    
    //Button init callback
    if(script_exists(tmp_callback_init))
    {
        if(instance_exists(tmp_owner))
        with(tmp_owner)
        {
            script_execute(tmp_callback_init,i);
    }}
    
    _v_btns_xpos[_v_btns_count] = tmp_x;
    _v_btns_ypos[_v_btns_count] = tmp_y;
    _v_btns_active[_v_btns_count] = 1;
    _v_btns_spr_bg[_v_btns_count] = noone;
    _v_btns_spr[_v_btns_count] = tmp_spr;
    _v_btns_spr_icon[_v_btns_count] = noone;
    _v_btns_spr_icon_x[_v_btns_count] = 0;
    _v_btns_spr_icon_y[_v_btns_count] = 0;
    _v_btns_spr_index[_v_btns_count] = 0;
    _v_btns_spr_w[_v_btns_count] = sprite_get_width(tmp_spr);
    _v_btns_spr_h[_v_btns_count] = sprite_get_height(tmp_spr);
    _v_btns_spr_x_offset[_v_btns_count] = sprite_get_xoffset(tmp_spr);
    _v_btns_spr_y_offset[_v_btns_count] = sprite_get_yoffset(tmp_spr);
    _v_btns_spr_scale[_v_btns_count] = 1;
    _v_btns_spr_sc_add[_v_btns_count] = 0.1;
    _v_btns_spr_sc_speed[_v_btns_count] = 0.3;
    _v_btns_spr_sc[_v_btns_count] = 0;
    _v_btns_callback_mouse_pressed[_v_btns_count]    = tmp_callback_pressed;
    _v_btns_callback_mouse_released[_v_btns_count]   = tmp_callback_released; 
    _v_btns_callback_mouse_lay_on[_v_btns_count]     = tmp_callback_lay_on;
    _v_btns_callback_mouse_lay_off[_v_btns_count]    = tmp_callback_lay_off;
    _v_btns_callback_mouse_release_free[_v_btns_count]    = -1;
    _v_btns_mouse_lay_on_flag[_v_btns_count] = false;
    _v_btns_owner[_v_btns_count] = tmp_owner;
    _v_btns_colddown_time_init[_v_btns_count] = tmp_colddown;
    _v_btns_colddown_time[_v_btns_count] = 0;
    _v_btns_colddown[_v_btns_count] = 0;
    _v_btns_cant_draw[_v_btns_count] = 0;
    //////////////////////////////
    // draw text on v_btns
    //////////////////////////////
    _v_btns_text[_v_btns_count] = "";   /// localization key
    _v_btns_noKey[_v_btns_count] = 0;
    _v_btns_text_angle[_v_btns_count] = 0;
    _v_btns_text_scale[_v_btns_count] = 1;
    _v_btns_text_col[_v_btns_count] = c_white;
    _v_btns_text_x[_v_btns_count] = 0;
    _v_btns_text_y[_v_btns_count] = 0;
    _v_btns_text_font_lay_off[_v_btns_count] = noone;
    _v_btns_text_font_lay_on[_v_btns_count] = noone;
    _v_btns_text_halign[_v_btns_count] = fa_center;
    //////////////////////////////
    //////////////////////////////
    // 2click on v_btns
    //////////////////////////////
    _v_btns_2click_start[_v_btns_count] = 0;
    _v_btns_2click_release[_v_btns_count] = 0;
    _v_btns_2click_time[_v_btns_count] = 0;
    _v_btns_2click_cb[_v_btns_count] = -1;
    
    _v_btns_spr_freeze[_v_btns_count] = -1;
    _v_btns_callback_draw[_v_btns_count] = -1;
    
    _v_btns_sound[_v_btns_count] = noone;
    _v_btns_sound_in_array[_v_btns_count] = 1;
    
    _v_btns_analytics_id[_v_btns_count] = "";
    
    _v_btns_deleted[_v_btns_count] = false;
    
    _v_btns_count += 1;
    //fed("_v_btns_count = " + string(_v_btns_count));
    return _v_btns_count - 1;
}
else
{
    return -1;
}

