/// @description scr_a_check_colour(inst, colour)

var _inst = argument0;
var _colour = argument1;

if(!instance_exists(_inst))exit;

var res = false;

switch(_inst.a_type)
{
    case EnCellTypes.VORTEX:
        _inst.a_p_vortex_colour = _colour;
        res = false;
    break;
    case EnCellTypes.MULTI:
        res = false;
    break;
    default:
        res = _inst.a_colour != _colour;
}

return res;


