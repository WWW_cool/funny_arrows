/// @description scr_a_lock(dir)

var _dir = argument0;

//fed("lock dir = " + s(_dir));

for(var i = 0; i < 4; i += 1)
{
    var next_cell = collision_circle(x + 1.1*sprite_get_width(sprite_index)*a_dx[i],
    y + 1.1*sprite_get_width(sprite_index)*a_dy[i],
    10,obj_a_parent,false,true);
    if(instance_exists(next_cell))
    {
        if(_dir)
        {
            next_cell.a_lock = 1;
        }
        else
        {
            next_cell.a_lock = 0;
        }
    }
}






