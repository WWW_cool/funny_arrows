/// @description scr_db_update_amount

var tmp_user_lvl = global.user_lvl + 1;
tmp_user_lvl = clamp(tmp_user_lvl,0,array_length_1d(global.user_lvl_amounts) - 1);
global.user_lvl_damount = global.user_lvl_amounts[tmp_user_lvl] - global.user_xp;

while(global.user_lvl_damount <= 0)
{
    if(tmp_user_lvl < array_length_1d(global.user_lvl_amounts) - 1)
    {
        tmp_user_lvl += 1;
        global.user_lvl_damount = global.user_lvl_amounts[tmp_user_lvl] - global.user_xp;
    }
    else
    {
        global.user_lvl_damount = 0;
        break;
    }
}

//fed("user_lvl_damount = " + s(global.user_lvl_damount));

scr_db_set_damount(global.user_lvl_damount);
