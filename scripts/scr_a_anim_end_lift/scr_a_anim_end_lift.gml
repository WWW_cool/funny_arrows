/// @description scr_a_anim_end_lift

var _a_colour   = a_colour;
var _a_dir      = a_dir;
var _a_angle    = a_angle;
var _a_type     = a_type;

var _a_p_shield         = a_p_shield;
var _a_p_turn_count     = a_p_turn_count;
var _a_p_double_colour  = a_p_double_colour;
var _a_p_mover_inst     = a_p_mover_inst;

a_colour  = a_next_colour;
a_dir     = a_next_dir;
a_angle   = a_next_angle;
a_type    = a_next_type;

a_p_shield         = a_next_p_shield;
a_p_turn_count     = a_next_p_turn_count;
a_p_double_colour  = a_next_p_double_colour;
a_p_mover_inst     = a_next_p_mover_inst;

a_next_colour  = _a_colour;
a_next_dir     = _a_dir;
a_next_angle   = _a_angle;
a_next_type    = _a_type;

a_next_p_shield        = _a_p_shield;
a_next_p_turn_count    = _a_p_turn_count;
a_next_p_double_colour = _a_p_double_colour;
a_next_p_mover_inst    = _a_p_mover_inst;
