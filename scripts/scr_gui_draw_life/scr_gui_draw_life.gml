/// @description scr_gui_draw_life(x,y,res[])

var _x = argument0;
var _y = argument1;
var _res = argument2;

if(instance_exists(global.inst_glb))
{
    var _glob = global.inst_glb;
    draw_set_colour(c_white);
    if(_glob.live_time)
    {
        v_draw_progBar(spr_GUI_hpBar,(_glob.live_time/_glob.live_time_need),_x + 10,_y - sprite_get_height(spr_GUI_hpBar)/2,
                                        2,_glob.live_time_need - _glob.live_time);
    }
    else
    {
        v_draw_progBar(spr_GUI_hpBar,(_glob.live_time/_glob.live_time_need),_x + 10,_y - sprite_get_height(spr_GUI_hpBar)/2,false);
    }
    
    v_anim(_res,EnAnim.Heart,3);
    draw_sprite_ext(spr_GUI_icon_heart,0,_x,_y,
    _res[EnAnimParam.Val],_res[EnAnimParam.Val],0,c_white,1);
    
    v_draw_text(_x - 1,_y,
                string(_glob.live_count),fa_center,fa_middle,EnText.H2); 
}
 




