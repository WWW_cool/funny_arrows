/// @description scr_boss_play(btn_id)

var btn_id = argument0;

if(scr_boss_get_amount() == 1)
{
    v_btns_set_text(btn_id,0,0,
        "Случайный!",1,c_black,0,f_H2,f_H2,1);
    v_btns_set_cb(btn_id,"press",scr_play_random_lvl);
    v_btns_set_param(btn_id,"analytick_id","startRandom");
}
else
{
    v_btns_set_text(btn_id,0,0,
        "Играть!",1,c_black,0,f_H2,f_H2,1);
    v_btns_set_cb(btn_id,"press",scr_win_next);
    v_btns_set_param(btn_id,"analytick_id","startNew");
}
