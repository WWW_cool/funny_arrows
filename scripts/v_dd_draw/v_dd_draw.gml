/// @description v_dd_draw()
var drag_ind = 0;
var drags = 0;

if(_dd_count > 0)
{
    for(var i = 0; i < _dd_count; i += 1)
    {
        if (!_dd_deleted[i])
        {
            if(_dd_active[i])
            {
                //v_btns_draw_btn(_dd_v_btn[i]);
                //fed("dd index = " + s(i) + " state: " + _dd_state[i]);
                switch(_dd_state[i])
                {
                    case "empty":// v_btn draw
                        break;
                    case "drag":                    
                        drags[drag_ind] = i;
                        drag_ind +=1;
                    break;
                    case "full":
                        if(sprite_exists(_dd_spr_mid[i]))
                        {
                            draw_sprite(_dd_spr_mid[i],0,
                            _dd_xpos[i],
                            _dd_ypos[i]);
                        }
                        if(sprite_exists(_dd_spr_out[i]))
                        {                        
                             draw_sprite(_dd_spr_out[i],0,
                            _dd_xpos[i] + _dd_x_shift[i],
                            _dd_ypos[i] + _dd_y_shift[i]);
                        }
                        if(script_exists(_dd_cb_draw[i]))
                        {
                            script_execute(_dd_cb_draw[i],i);
                        }
                    break;
                       
                }
            }
        }
    }
    for(var i = 0; i < drag_ind; i += 1)
    {
        if(sprite_exists(_dd_spr_out[drags[i]]))
        {
            //if(_dd_type[i] == "source")
            {
                draw_sprite(_dd_spr_out[drags[i]],0,
                _dd_xpos[drags[i]] + _dd_x_shift[drags[i]],
                _dd_ypos[drags[i]] + _dd_y_shift[drags[i]]);
            }
            if(point_distance(_dd_mouse_x_init[drags[i]],_dd_mouse_y_init[drags[i]],mouse_x,mouse_y) < 20)
            {
                draw_sprite(_dd_spr_out[drags[i]],0,
                _dd_xpos[drags[i]] + _dd_x_shift[drags[i]],
                _dd_ypos[drags[i]] + _dd_y_shift[drags[i]]);
            }
            else
            {
                draw_sprite(_dd_spr_out[drags[i]],0,mouse_x,mouse_y);
            }
        }
    }
}

