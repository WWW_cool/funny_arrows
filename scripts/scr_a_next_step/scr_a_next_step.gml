/// @description scr_a_next_step

if(!instance_exists(global.a_study_inst))
{
    a_move_tiles_count[a_move_index] = a_move_tile_count;
    m_move_movers_count[a_move_index] = m_move_mover_count;
    
    a_move_index++;
}
var need_lift_btn = 0;

for(var i = 0; i < instance_number(obj_a_parent); i++)
{
    var _inst = instance_find(obj_a_parent,i);
    if(instance_exists(_inst))
    {
        if(_inst.a_state != EnCellStates.DESTROY)
        {
            _inst.a_anim_state = EnCellAnimTypes.COMMON;
            // готовим состояние объектов для сохранения
            switch(_inst.a_type)
            {
                case EnCellTypes.TIME:
                    if(_inst.a_p_turn_count > 0)_inst.a_p_turn_count -= 1;
                break;
                case EnCellTypes.DOUBLE:
                    var _a_colour   = _inst.a_colour;
                    _inst.a_colour        = _inst.a_p_double_colour;
                    _inst.a_p_double_colour  = _a_colour;
                break;
            }
    
            if(scr_a_lift_check(_inst) >= 0)
            {
                with(_inst)
                {
                    scr_a_anim_end_lift();
                }
            }

            // сохраняем
            scr_a_move_next_tile(_inst);
            
            with(_inst)
            {
                if(a_type == EnCellTypes.LOCK)
                {
                    scr_a_lock(1);
                }
                if(a_next)
                {
                    need_lift_btn = 1;
                }
            }
                
            // готовим состояние объектов для анимации
            if(scr_a_lift_check(_inst) >= 0)
            {
                with(_inst)
                {
                    scr_a_anim_end_lift();
                }
            }
            else
            {
                with(_inst)
                {
                    scr_a_anim_end();
                }
            }
            // анимации
            if(_inst.a_state == EnCellStates.DESTROY)continue;
            
            switch(_inst.a_type)
            {
                case EnCellTypes.DOUBLE:
                    _inst.a_anim_state      = EnCellAnimTypes.COMMON;
                    _inst.a_state           = EnCellStates.ANIM;
                break;
            }
            
            scr_a_lift(_inst);
            
            
            if(instance_exists(_inst.a_p_mover_inst))
            {
                _inst.a_p_moving = true;
            }
        }
        else
        {
            scr_a_move_next_tile(_inst);
        }
    }
}

for(var i = 0; i < instance_number(obj_a_mover); i++)
{
    var _inst = instance_find(obj_a_mover,i);
    if(instance_exists(_inst))
    {
        _inst.m_anim_state = EnMoverStates.MOVE;
        // готовим состояние объектов для сохранения
        
        
        // сохраняем
        scr_a_move_next_mover(_inst);
        
        // готовим состояние объектов для анимации
    }
}

global.a_hint_reverse_need = need_lift_btn;




