/// @description _GLOBAL_ENUM

enum EnPoint{X = 0,Y,LENGTH};
enum EnText{Text = 0,H1,H2,LENGTH};
enum EnTiled{Hor = 0,Vert,ALL,LENGTH};
enum EnAnim{Heart = 0,Boss_bg,Selector,LENGTH};
enum EnAnimParam{Val = 0,initVal,Point,Time,LENGTH};
enum EnMsgTypes{Share = 0,Lives,FStep,Moveback,BTime,BDMG,BLVL,Price,BossPrice,Advert,Demo,Study,LENGTH};
enum EnBossType{Monster_1 = 0,Monster_2,Monster_3,Monster_4,Monster_5,Monster_6,LENGTH};
enum EnEventType{Price = 0,Advert,Repost,LENGTH};

enum EnPriceType{Life = 0,FirstTurn,MoveBack,Life_clock,LENGTH};
enum EnWaiter{SatrtBTN,LENGTH};

enum EnRequestData{SCRIPT,ARG_COUNT,ARG_1,ARG_2,ARG_3,ARG_4,LENGTH};

enum EnVkShop{ALL,  LIFE_S, LIFE_M, LIFE_L, 
                    BACK_S, BACK_M, BACK_L, 
                    FSTEP_S, FSTEP_M, FSTEP_L, LENGTH};
