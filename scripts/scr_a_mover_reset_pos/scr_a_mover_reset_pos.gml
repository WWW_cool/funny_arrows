/// @description scr_a_mover_reset_pos(inst)

var _inst = argument0;

if(instance_exists(_inst))
{
    if(instance_exists(_inst.m_inst))
    {
        if(point_distance(_inst.x,_inst.y,_inst.m_inst.x,_inst.m_inst.y) > 10)
        {
            _inst.m_pos = 1;
            _inst.m_move_back = 1;
        }
        else
        {
            _inst.m_pos = 0;
            _inst.m_move_back = 0;
        }
    }
}
