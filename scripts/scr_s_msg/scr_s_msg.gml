/// @description scr_s_msg(type)

var _inst = scr_s_share();
var _text = "";
var _icon = spr_GUI_icon_happy;
var _header = "";
var _type = argument0;

if(instance_exists(_inst))
{
    switch(_type)
    {
        case EnMsgTypes.Lives: // lives
            _icon = spr_GUI_icon_fail;
            _header = "Приехали!";
            _text = "Не хватает жизней!";
        break;
        case EnMsgTypes.Moveback: // moveback
            _icon = spr_GUI_icon_quest;
            _header = "Приехали!";
            _text = "Возвращения хода закончились!";
        break;
        case EnMsgTypes.FStep: // first step
            _icon = spr_GUI_icon_quest;
            _header = "Приехали!";
            _text = "Не хватает подсказок!";
        break;
        case EnMsgTypes.BTime: // boss time
        break;
        case EnMsgTypes.BDMG: // boss dmg
        break;
        case EnMsgTypes.BLVL: // boss lvl
        break;
        case EnMsgTypes.Price: // price
            _icon = spr_GUI_icon_happy;
            _header = "Успех!";
            _text = "Ваш подарок!";
        break;
        case EnMsgTypes.BossPrice: // price
            _icon = spr_GUI_icon_happy;
            _header = "Успех!";
            _text = "Ваша награда!##";
        break;
        case EnMsgTypes.Advert: // Advert
            _icon = spr_GUI_icon_quest;
            _header = "Удача!";
            _text = "Получить подарок!## Смотреть рекламу?";
        break;
        case EnMsgTypes.Demo: // Demo
            _icon = spr_GUI_icon_happy;
            _header = "Спасибо за игру!";
            _text = "Вы прошли демо уровни! Подписывайтесь на группу, чтобы узнать когда выйдет полная версия игры!";
        break;
        case EnMsgTypes.Study: // Study
            _icon = spr_GUI_icon_quest;
            _header = "Вопрос";
            _text = "Появились новые типы плиток, хотите пройти обучение?";
        break;
        default:
            _type = EnMsgTypes.Share;
    }
}

_inst.OPT_type = _type;
_inst.OPT_header_icon = _icon;
_inst.OPT_header_text = _header;
_inst.OPT_text = _text;

return _inst;


