 ///scr_a_draw_anim_lift
var res = 0;

switch(anim_l_state)
{
    case 0:
        anim_l_alpha = v_increment(anim_res,anim_l_alpha,
        0,anim_l_alpha_speed,anim_l_alpha_speed/5);
        
        if(!amin_reverse_dir)
        {
            scr_a_draw_cell_ext(1,a_next_type,x + anim_l_dx,y + anim_l_dy,anim_scale,anim_angle,1);
            scr_a_draw_cell_ext(0,a_type,x,y,anim_scale,anim_angle,anim_l_alpha);
        }
        else
        {
            scr_a_draw_cell_ext(0,a_type,x + anim_l_dx,y + anim_l_dy,anim_scale,anim_angle,1);
            scr_a_draw_cell_ext(1,a_next_type,x,y,anim_scale,anim_angle,anim_l_alpha);
        }
        
        if(anim_res[0])
        {
            anim_l_state = 1;
        }
    break;
    case 1:
        anim_l_alpha = v_increment(anim_res,anim_l_alpha,
        1,anim_l_alpha_speed,anim_l_alpha_speed/5);
        anim_l_dx = v_increment(anim_res,anim_l_dx,
        0,1,1/5);
        anim_l_dy = anim_l_dx;
        
        if(!amin_reverse_dir)
        {
            scr_a_draw_cell_ext(0,a_type,x + (anim_l_dx_init - anim_l_dx),
            y + (anim_l_dy_init - anim_l_dy),anim_scale,anim_angle,anim_l_alpha);
            scr_a_draw_cell_ext(1,a_next_type,x + anim_l_dx,y + anim_l_dy,anim_scale,anim_angle,1);
        }
        else
        {
            scr_a_draw_cell_ext(1,a_next_type,x + (anim_l_dx_init - anim_l_dx),
            y + (anim_l_dy_init - anim_l_dy),anim_scale,anim_angle,anim_l_alpha);
            scr_a_draw_cell_ext(0,a_type,x + anim_l_dx,y + anim_l_dy,anim_scale,anim_angle,1);
        }

        res = scr_a_anim_reset(anim_res);
    break;
}

return res;

