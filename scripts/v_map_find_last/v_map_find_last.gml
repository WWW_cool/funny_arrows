/// @description v_map_find_last(map)
var map = argument[0];

/**
        This function returns the last key stored in the v_map. 
        This is not the last key in the order that you have added them! 
        v_maps are not stored in a linear form, for that use ds_lists, 
        so all this does is find the last key as stored by the computer. 
        This can be useful if your have to iterate through the v_map 
        looking for something, but should be avoided if possible as it can be slow. 
**/

if(is_array(map))
{
    //Проходим по массиву начиная с конца
    for(var i=array_height_2d(map)-1; i>=0; i--)
    {
        //Как только встречаем первый непустой ключ, возвращаем его
        if(map[i,0] != -1) return map[i,1];
    }  
}

return undefined;

