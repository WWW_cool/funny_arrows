/// @description scr_T_unhide()
var tmp_angle = 0,tmp_x,tmp_y;

for(var i = 0; i < 4; i += 1)
{
    tmp_angle = 90*i;
    tmp_x = x + lengthdir_x(t_sys_range,tmp_angle);
    tmp_y = y + lengthdir_y(t_sys_range,tmp_angle);
    
    var tmp_tile = collision_point(tmp_x,tmp_y,obj_tile_parent,false,false);
    if(instance_exists(tmp_tile))
    {
        if(tmp_tile.t_state == EnTileStates.HIDED)
        {
            tmp_tile.t_unhide_anim = 1; 
        }
    }
}

