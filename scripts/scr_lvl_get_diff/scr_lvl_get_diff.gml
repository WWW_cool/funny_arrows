/// @description scr_lvl_get_diff(lvl_num)

var num = argument0;
var res = 0;

if(num != 0 && num % 10 == 0)
{
    res = 1;
}

return res;

