/// @description scr_s_rate_lay_on

if(argument_count > 0)
{
    var btn_id = argument[0];
    var _ind = 0;
    for(var i = 0; i < array_length_1d(OPT_controls_type); i += 1)
    {
        if(btn_id == OPT_controls_type[i])
        {
            _ind = i;
        }
    }
    for(var i = 0; i < array_length_1d(OPT_controls_type); i += 1)
    {
        if(i <= _ind)
        {
            v_btns_set_param(OPT_controls_type[i],"spr",spr_menu_star_on);
        }
        else
        {
            v_btns_set_param(OPT_controls_type[i],"spr",spr_menu_star_off);
        }
    }
}



