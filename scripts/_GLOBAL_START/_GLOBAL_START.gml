// при старте комнаты
if(!instance_exists(obj_def_global))
{
    global.inst_glb = instance_create(1,1,obj_def_global);
    global.localization_map = 0;

    enum V_DB_REQUEST_Types{NONE = 0,GET,POST,DELETE,PUT,INVOKE,LENGTH};
    
    
    /*global.col_timer = make_colour_rgb(255,238,167);
    global.col_pink = make_colour_rgb(197,0,78);
    global.col_res = make_colour_rgb(43,0,52);
    global.col_res_2 = make_colour_rgb(225,0,78);
    global.col_res_ok = make_colour_rgb(48,225,0);*/
    global.col_main = make_colour_rgb(37,41,76);
    
    global.col_c_blue = make_colour_rgb(49,174,255);
    global.col_c_violet = make_colour_rgb(130,44,232);
    global.col_c_red = make_colour_rgb(255,49,49);
    global.col_c_orange = make_colour_rgb(247,63,47);
    global.col_c_yellow = make_colour_rgb(245,242,59);
    global.col_c_green = make_colour_rgb(46,255,106);
    
    global._tmp_surf = noone;
    global.load_ind  = -1;
    global.new_game  = 0;
    global.inst_back = noone;
    global.lvlObjList = noone;
    global.lvlEdit = noone;
    global.lvlstr = noone;
    global.lvl_select = noone;
    
    global.user_need_reset = 0;
    global.user_open_all = 0;
    global.user_xp = 0;
    global.user_xp_new = 0;
    global.user_lvl = 0;
    //global.user_lvl_amounts = noone; //in global init
    global.user_lvl_damount = 0;
    global.user_lvl_amount = 0;
    global.user_lvl_amount_new = 0;
    global.user_lvl_ok = 0;
    global.user_lvl_xp = 2;
    //new
    global.user_lvl_boss_ok = 0;
    global.user_boss_type = EnBossType.Monster_1;
    
    global.v_db_address = "https://naogames.ru/rest/v2/simple/users/";
    global.v_db_address_post = "https://naogames.ru/rest/v2/simple/users";
    global.v_db_request_id = noone;
    global.v_db_request_type = V_DB_REQUEST_Types.NONE;
    global.v_db_request_state = 0;
    global.v_db_request_map = noone;
    global.v_db_request_p_id = "";
    global.v_db_request_p_type = "";
    global.v_db_request_p_val = "";
    
    global.fnt_spr_hp_xp = noone;
    global.fnt_spr_lvl = noone;
    global.fnt_spr_wave = noone;
    global.fnt_spr_crit = noone;
    global.fnt_spr_money = noone;
    global.v_db_read_first = 1;
    
    global.a_hint_first_turn = 0;
    global.a_hint_first_turn_snd = noone;
    global.a_hint_reverse = 0;
    global.a_hint_reverse_need = 1;
    global.a_hint_mouse_block = 0;
    
    global.a_study_inst = noone;
    var ind = 0;
    global.a_st_unlock_lvl[ind++] = 1; // empty
    global.a_st_unlock_lvl[ind++] = 21; // dir
    global.a_st_unlock_lvl[ind++] = 11; // floor
    global.a_st_unlock_lvl[ind++] = 35; // shield
    global.a_st_unlock_lvl[ind++] = 35; // lift
    global.a_st_unlock_lvl[ind++] = 35; // double
    global.a_st_unlock_lvl[ind++] = 35; // lock
    global.a_st_unlock_lvl[ind++] = 35; // time
    global.a_st_unlock_lvl[ind++] = 35; // vortex
    global.a_st_unlock_lvl[ind++] = 35; // multi
    /*
    
    global.a_st_unlock_lvl[ind++] = 3; // shield
    global.a_st_unlock_lvl[ind++] = 10; // lift
    global.a_st_unlock_lvl[ind++] = 9; // double
    global.a_st_unlock_lvl[ind++] = 14; // lock
    global.a_st_unlock_lvl[ind++] = 11; // time
    global.a_st_unlock_lvl[ind++] = 29; // vortex
    global.a_st_unlock_lvl[ind++] = 14; // multi
    
    */
    global.a_sound_click_off = noone;
    global.a_sound_bg = noone;
    
    for(var i = 0; i < EnColourTypes.LENGTH; i++)
    {
        global.a_cell_color[i] = i;
    }
    global.a_first_load = 0;
}
else
{
    if(instance_number(obj_def_global) == 1)
    {
        global.inst_glb = instance_find(obj_def_global,0);
    }
    else
    {
        error("_GLOBAL_INIT (20)", " count of obj_def_global != 1");
    }
    global.a_first_load = 1;
}

v_dd_global_init();
v_sld_global_init();
v_btns_init();
v_info_init();

if(!global.a_first_load)
{
    if(os_browser == browser_not_a_browser || global.inst_glb.game_user_id == "demo")
    {
        scr_g_load();
        //fed("file load");
        // load when db loaded in async http
        /*if(global.inst_glb.game_lvl_ok == 0)
        {
            scr_win_next();
        }*/
    }
    else
    {
        fed("load from db game_user_id = " + global.inst_glb.game_user_id);
    }
}

if(os_browser == browser_not_a_browser || global.inst_glb.game_user_id == "demo")
{
    scr_boss_update();
}

//instance_create(1,1,obj_vk_api_test);
instance_create(1,1,obj_GUI_main);
//instance_create(1,1,obj_promo);

v_db_read(global.inst_glb.game_user_id);


//audio_stop_all();
//scr_def_play_snd_loop(snd_bg_1);

