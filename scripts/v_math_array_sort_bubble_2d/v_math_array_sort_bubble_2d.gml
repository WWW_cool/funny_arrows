/// @description v_math_array_sort_bubble_2d(array)
//сортировка массива пузырьком
//сортируем первый столбец, второй столбец привязан жестко к значениям первого
var A = argument0;

if(is_array(A))
{
    var len = array_height_2d(A);

    var i,j,tmp0,tmp1;
    var swapped;
    for (i=0; i<len-1; i++) 
    {
        swapped = false;
         
        for(j=0; j<len-i-1; j++) 
        {
             if (A[j,0] > A[j+1,0]) 
             {
                 tmp0 = A[j,0];
                 tmp1 = A[j,1]; 
                 
                 A[@ j,0] = A[j+1,0];
                 A[@ j,1] = A[j+1,1];
                 
                 A[@ j+1,0] = tmp0;
                 A[@ j+1,1] = tmp1;
                 
                 swapped = true;
             }
         }
         
         if(!swapped) break;
     }
}
