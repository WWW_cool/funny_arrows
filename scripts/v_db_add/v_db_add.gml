/// @description v_db_add(id)

var user_id = argument0;
var params = v_db_get_struct();

ds_map_replace(params,"id",user_id);
scr_db_update_amount();
ds_map_replace(params,"xp_damount",s(global.user_lvl_damount));


var params_str = json_encode(params);
v_db_delete_struct(params);
if(v_db_request(global.v_db_address_post,"POST",params_str))
{
    global.v_db_request_type = V_DB_REQUEST_Types.POST;
    return 1;
}
return 0;



