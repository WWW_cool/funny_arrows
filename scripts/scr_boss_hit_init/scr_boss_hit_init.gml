/// @description scr_boss_hit_init()

hit_pos = irandom(3);
hit_number_max = irandom_range(2,8);
hit_number = hit_number_max;
    
for(var i = 0; i < array_length_1d(GUI_controls); i++)
{
    v_btns_set_param(GUI_controls[i],"scale",1);
    if(i == hit_pos)
    {
        v_btns_freeze(GUI_controls[i],2,scr_boss_hit);
        v_btns_set_param(GUI_controls[i],"icon",btn_icon_inv[i]);
    }
    else
    {
        v_btns_freeze(GUI_controls[i],0,scr_boss_hit);
        v_btns_set_param(GUI_controls[i],"icon",btn_icon[i]);
    }
}    
