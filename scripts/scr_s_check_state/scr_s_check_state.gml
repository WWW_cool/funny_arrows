/// @description scr_s_check_state
if(!instance_exists(global.lvlEdit)&& room != rm_lvlCreate)
{ 
    if(!s_lvl_win)
    {
        if(instance_number(obj_a_parent) == 0)
        {
            s_lvl_win = 1;
            if(!instance_exists(global.a_study_inst))
            {
                if(instance_exists(global.inst_glb))
                {
                    if(!global.inst_glb._def_glb_lvl_start_delay)
                    {
                        global.inst_glb.game_win = 1;
                        global.inst_glb.game_points += global.inst_glb.game_current_lvl_points;
                        var _inst = scr_create_header("Победа!");
                        scr_def_play_snd(snd_lvl_victory);
                        v_a_event(room_get_name(room) + " re","gameRetries",s(global.inst_glb.game_current_lvl_try));
                        global.inst_glb.game_current_lvl_try = 0;
                        // new lvl passed event here
                        if(instance_exists(_inst))
                        {
                            _inst.h_win = 1;
                        }
                        scr_gui_life(1);
                    }
                }
            }
            else
            {
                if(!scr_study_level_load_next())
                {
                    scr_create_header("Отлично!");
                    scr_def_play_snd(snd_lvl_victory);
                    var _btn_id = global.a_study_inst.btn_to_game;
                    var _x = v_btns_get_param(_btn_id,"xpos");
                    var _y = v_btns_get_param(_btn_id,"ypos");
                    instance_create(_x,_y,obj_boss_efx_click);
                    _btn_id = global.a_study_inst.btn_back;
                    _x = v_btns_get_param(_btn_id,"xpos");
                    _y = v_btns_get_param(_btn_id,"ypos");
                    instance_create(_x,_y,obj_boss_efx_click);
                    v_a_event(s(global.a_study_inst.st_hint_number) + "_" +
                    s(global.a_study_inst.st_hint_index),
                    "study","passed");
                }
            }
        }
        else
        {
            if(scr_s_check_step() <= 0)
            {
                s_lvl_win = 1;
                if(!instance_exists(global.a_study_inst))
                {
                    if(instance_exists(global.inst_glb))
                    {
                        scr_create_header("Нет ходов");
                        scr_def_play_snd(snd_lvl_lose);
                        global.inst_glb.game_lose = 1;
                        global.inst_glb.game_current_lvl_try += 1;
                    }
                }
                else
                {
                    scr_create_header("Еще раз!");
                    scr_def_play_snd(snd_lvl_lose);
                    scr_study_level_reload();
                    v_a_event(s(global.a_study_inst.st_hint_number) + "_" +
                    s(global.a_study_inst.st_hint_index),
                    "study","fail");
                }
            }
        }
    }
}






