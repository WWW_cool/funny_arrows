/// @description scr_a_get_colour(inst)

var _inst = argument0;

if(!instance_exists(_inst))exit;

var res;

switch(_inst.a_type)
{
    case EnCellTypes.MULTI:
        res = EnColourTypes.EMPTY;
    break;
    case EnCellTypes.VORTEX:
        res = _inst.a_p_vortex_colour;
    break;
    default:
        res = _inst.a_colour;
}

return res;




