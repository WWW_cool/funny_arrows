/// @description scr_a_move_tile_show

var show_map = ds_map_create();

for(var i = 0; i < __view_get( e__VW.HView, 0 ); i += 36)
{
    for(var k = 0; k < __view_get( e__VW.WView, 0 ); k += 36)
    {
        var _inst = collision_point(i,k,obj_a_parent,false,true);
        if(instance_exists(_inst))
        {
            var _str = ds_map_find_value(show_map,_inst.y)
            if(!is_undefined(_str))
            {
                _str += " a_type = " + scr_EnCellTypes_get_name(_inst.a_type);
                ds_map_replace(show_map,_inst.y,_str);
            }
            else
            {
                _str = "y = " + s(_inst.y) + " a_type = " + scr_EnCellTypes_get_name(_inst.a_type);
                ds_map_add(show_map,_inst.y,_str);
            }
        }
    }    
}


fed("---------------------------------------------");

for(var i = 0; i < __view_get( e__VW.HView, 0 ); i += 1)
{
    var _str = ds_map_find_value(show_map,i);
    if(!is_undefined(_str))
    {
        fed(_str);
    }
}
ds_map_destroy(show_map);
