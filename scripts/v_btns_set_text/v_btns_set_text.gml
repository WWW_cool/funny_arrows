/// @description v_btns_set_text(id,delta_x,delta_y,key,scale,col,rot,f_on,f_off,no_key)

//Проверяем, что существует обработчки кнопок
var inst = global._v_btns_owner;
if(instance_exists(inst))
with(inst)
{
    var i = argument0;
    _v_btns_text_x[i]       = argument1;
    _v_btns_text_y[i]       = argument2;
    _v_btns_text[i]         = argument3;   /// localization key
    _v_btns_text_scale[i]   = argument4;
    _v_btns_text_col[i]     = argument5;
    _v_btns_text_angle[i]   = argument6;   
    _v_btns_text_font_lay_on[i] = argument7;
    _v_btns_text_font_lay_off[i]  = argument8;
    _v_btns_noKey[i] = argument9;
}

