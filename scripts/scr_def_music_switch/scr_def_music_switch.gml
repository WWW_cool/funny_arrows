/// @description scr_def_music_switch()

if(argument_count > 0)
{
    var btn_ind = argument[0];
    if(global._def_glb_var_audio_on_bg)
    {
        global._def_glb_var_audio_on_bg = 0;
    }
    else
    {
        global._def_glb_var_audio_on_bg = 1;
    }
    
    if(script_exists(global._def_glb_var_audio_on_cb))
    {
        script_execute(global._def_glb_var_audio_on_cb,1);
    }
    
    var snd_spr = scr_def_get_snd_sprite(global._def_glb_var_audio_on_bg);
    if(sprite_exists(snd_spr))
    {
        v_btns_set_param(btn_ind,"spr",snd_spr);
    }
}
