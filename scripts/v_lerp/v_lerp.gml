/// @description v_lerp(res[],val_from,val_to,v_speed,delta)

var res         = argument0;
var val_from    = argument1;
var val_to      = argument2;
var v_speed     = argument3;
var delta       = argument4;

if(is_array(res))
{
    if(abs(val_from - val_to) > delta)
    {
        res[@ 0] = false;
        return lerp(val_from,val_to,v_speed);
    }
    else
    {
        res[@ 0] = true;
        return val_to;
    }
}
else
{
    if(abs(val_from - val_to) > delta)
    {
        return lerp(val_from,val_to,v_speed);
    }
    else
    {
        return val_to;
    }
}

