/// @description scr_m_move_next_mover

var recreate_list = ds_list_create();

if(instance_exists(global.a_selector))
{
    with(global.a_selector)
    {
        if(a_move_index < 0)exit;
        
        for(var i = 0; i < m_move_mover_count; i++)
        {
            var _inst = m_move_mover_inst[i];
            if(instance_exists(_inst))
            {
                if(m_move_mover_start_move[i] > a_move_index && m_move_mover_start_move[i] != 0)
                {
                    _inst.m_anim_state = EnMoverStates.DESTROY;
                }
                else
                {
                    _inst.x = m_move_mover_xpos[i,a_move_index];
                    _inst.y = m_move_mover_ypos[i,a_move_index];
                    _inst.image_angle = m_move_mover_angle[i,a_move_index];
                    _inst.m_anim_state = m_move_mover_state[i,a_move_index];
                    
                    _inst.m_move_back = m_move_mover_move_back[i,a_move_index];
                    _inst.m_pos = m_move_mover_pos[i,a_move_index];
                }
            }
            else
            {
                ds_list_add(recreate_list,i);
            }
        }
        
        for(var ind = 0; ind < ds_list_size(recreate_list); ind++)
        {
            var i = recreate_list[| ind];

            if(m_move_mover_deleted[i] <= a_move_index)
            {
                continue;
            }
            var _inst = instance_create(m_move_mover_xpos[i,a_move_index],m_move_mover_ypos[i,a_move_index],obj_a_mover);
            //fed("recreate -- " + s(_inst) + " ind = " + s(i));
            //fed("start move = " + s(m_move_mover_start_move[i]));
            //fed("length xpos = " + s(array_length_2d(m_move_mover_xpos,i)));
            
            _inst.m_move_id     = i;
            m_move_mover_inst[i] = _inst;
            
            _inst.m_anim_state  = m_move_mover_state[i,a_move_index];
            _inst.image_angle = m_move_mover_angle[i,a_move_index];
            
            _inst.m_move_back = m_move_mover_move_back[i,a_move_index];
            _inst.m_pos = m_move_mover_pos[i,a_move_index];
            
            m_move_mover_deleted[i] = -1;
        }
        m_move_mover_count = m_move_movers_count[a_move_index];
        //fed(" - afeter back - m_move_mover_count = " + s(m_move_mover_count)); 
        //fed("******************LOOP END******************"); 
    }
    if(global.a_selector.a_move_index == 0)
    {
        for(var i = 0; i < instance_number(obj_a_mover); i++)
        {
            var _inst = instance_find(obj_a_mover,i);
            scr_a_mover_reset_pos(_inst);
        }
    }
}

ds_list_destroy(recreate_list);



