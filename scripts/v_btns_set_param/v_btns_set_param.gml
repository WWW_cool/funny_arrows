/// @description v_btns_set_param(ind,p_name, val)

//Проверяем, что существует обработчки кнопок
var inst = global._v_btns_owner;
if(instance_exists(inst))
with(inst)
{
    var i = argument0;
    var tmp_name = argument1;
    var val = argument2;
    
    if (!_v_btns_deleted[i])
    {
        switch(tmp_name)
        {
            case "cantDraw": _v_btns_cant_draw[i] = val;
            break;
            
            case "active": _v_btns_active[i] = val;
            break;
            
            case "background": _v_btns_spr_bg[i] = val;
            break;
            
            case "spr":
                if(sprite_exists(val))
                {
                    _v_btns_spr[i] = val;
                    _v_btns_spr_w[i] = sprite_get_width(val);
                    _v_btns_spr_h[i] = sprite_get_height(val);
                    _v_btns_spr_x_offset[i] = sprite_get_xoffset(val);
                    _v_btns_spr_y_offset[i] = sprite_get_yoffset(val);
                }
            break;
            
            case "xpos": _v_btns_xpos[i] = val;
            break;
            
            case "ypos": _v_btns_ypos[i] = val;
            break;
            
            case "freeze": _v_btns_spr_freeze[i] = val;
            break;
            
            case "coldown": _v_btns_colddown_time_init[i] = val;
            break;
            case "t_color": _v_btns_text_col[i] = val;
            break;
            case "t_text": _v_btns_text[i] = val;
            break;
            case "icon": 
                if(sprite_exists(val))
                {
                    _v_btns_spr_icon[i] = val;
                }
            break;
            case "scale":  
            _v_btns_spr_scale[i] = val;
            _v_btns_spr_w[i] = sprite_get_width(_v_btns_spr[i])*_v_btns_spr_scale[i];
            _v_btns_spr_h[i] = sprite_get_height(_v_btns_spr[i])*_v_btns_spr_scale[i];
            _v_btns_spr_x_offset[i] = sprite_get_xoffset(_v_btns_spr[i])*_v_btns_spr_scale[i];
            _v_btns_spr_y_offset[i] = sprite_get_yoffset(_v_btns_spr[i])*_v_btns_spr_scale[i];
            break;
            case "scalability":  
            _v_btns_spr_sc_add[i] = val;
            break;
            case "text_halign":  
            _v_btns_text_halign[i] = val;
            break;
            case "icon_x":  
            _v_btns_spr_icon_x[i] = val;
            break;
            case "icon_y":  
            _v_btns_spr_icon_y[i] = val;
            break;
            case "audio_in_array":  
            _v_btns_sound_in_array[i] = val;
            break;
            case "audio_sound":  
            _v_btns_sound[i] = val;
            break;
            case "analytick_id":  
            _v_btns_analytics_id[i] = val;
            break;
        }
    }
}


