/// @description scr_a_check_mover_inst
var _angle = image_angle;
var x1 = x - 32*dcos(_angle) - 30*dsin(_angle);
var y1 = y - 30*dcos(_angle) + 32*dsin(_angle);
var x2 = x + 104*dcos(_angle) + 30*dsin(_angle);
var y2 = y + 30*dcos(_angle) - 104*dsin(_angle);

var inst = collision_rectangle(x1,y1,x2,y2,obj_a_parent,false,true);
if(instance_exists(inst))
{
    m_inst = inst;
    inst.a_p_is_move = true;
    scr_a_mover_reset_pos(id);
    inst.a_p_mover_inst = id;
    inst.a_next_p_mover_inst = id;
}



