/// @description scr_vk_shop(type)

var type = argument0;
res = noone;

switch(type)
{
    case EnVkShop.LIFE_S:
        res = scr_vk_cb_life_s;
    break;
    case EnVkShop.LIFE_M:
        res = scr_vk_cb_life_m;
    break;
    case EnVkShop.LIFE_L:
        res = scr_vk_cb_life_l;
    break;
    case EnVkShop.BACK_S:
        res = scr_vk_cb_back_s;
    break;
    case EnVkShop.BACK_M:
        res = scr_vk_cb_back_m;
    break;
    case EnVkShop.BACK_L:
        res = scr_vk_cb_back_l;
    break;
    case EnVkShop.FSTEP_S:
        res = scr_vk_cb_fstep_s;
    break;
    case EnVkShop.FSTEP_M:
        res = scr_vk_cb_fstep_m;
    break;
    case EnVkShop.FSTEP_L:
        res = scr_vk_cb_fstep_l;
    break;
    default:
        res = scr_vk_shop_all;
}

return res;








