/// @description scr_def_view_check
var res = false;
if(global._def_glb_var_g_resolution_w != browser_width || 
global._def_glb_var_g_resolution_h != browser_height)
{
    global._def_glb_var_g_resolution_w = browser_width;
    global._def_glb_var_g_resolution_h = browser_height;
    res = true;
}
return res;
