/// @description scr_a_get_dir(inst,from)

var tmp_inst = argument0;
var from = argument1;
var res = -1;

if(instance_exists(tmp_inst))
{
    if(from)
    {
        switch(tmp_inst.a_dir)
        {
            case EnDirTypes.SIDE:
                res = tmp_inst.a_angle + 180/90;
            break;
            case EnDirTypes.CCW:
                res = tmp_inst.a_angle + 90/90;
            break;
            case EnDirTypes.CW:
                res = tmp_inst.a_angle + 270/90;
            break;
            case EnDirTypes.NONE:
                return -2;
            break;
            
        }
    }
    else
    {
        if(tmp_inst.a_dir == EnDirTypes.NONE)
        {
            return -2;
        }
        else
        {
            res = tmp_inst.a_angle;
        }
    }
}


return res%4;
