/// @description v_info_delete(ID);
/// @param ID
//Deactivated info panel to be overwritten by new info panel
var inst = global._v_info_owner;
if(instance_exists(inst))
with(inst)
{
    if(argument0 >=0)
    {
        _v_info_deleted[argument0] = true;
    }
}
return noone;


