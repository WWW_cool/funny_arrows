/// @description v_sld_get_id(v_btn_id)
var tmp_ind = argument0;

if(v_sld_count > 0)
{
    for(var i = 0; i < v_sld_count; i += 1)
    {
        if (!v_sld_deleted[i])
        {
            if(v_sld_v_btn[i] == tmp_ind)
            {
                return i;
            }
        }
    }
}
return -1;
