/// @description scr_def_g_pause_check(type)

var _def_tmp_type = argument0;
var _def_tmp_allow = false;
if(instance_exists(global._def_glb_var_pause_obj))
{
    return _def_tmp_allow; 
}

switch(_def_tmp_type)
{
    case _EnPauseType.SIMPLE:
    if(global._def_glb_p_simple != noone)
    {
        _def_tmp_allow = true;
    }
    break;
    case _EnPauseType.MENU:
    if(global._def_glb_p_menu != noone)
    {
        _def_tmp_allow = true;
    }
    break;
    case _EnPauseType.HINT:
    if(global._def_glb_p_hint != noone)
    {
        _def_tmp_allow = true;
    }
    break;
    case _EnPauseType.LVL_END:
    if(global._def_glb_p_lvl_end != noone)
    {
        _def_tmp_allow = true;
    }
    break;  
    case _EnPauseType.ACHIEV:
    if(global._def_glb_p_achievement != noone)
    {
        _def_tmp_allow = true;
    }
    break;
}

return _def_tmp_allow;


