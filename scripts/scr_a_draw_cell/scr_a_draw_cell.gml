/// @description scr_a_draw_cell

var colour_spr = scr_a_get_col_spr(a_colour);
var dir_spr = scr_a_get_dir_spr(a_dir);

switch(a_type)
{
    case EnCellTypes.NONE:
        scr_a_draw_cell_default(colour_spr,dir_spr);
    break;
    case EnCellTypes.SHIELD:
        scr_a_draw_cell_default(colour_spr,dir_spr);
        if(a_p_shield)
        {
            draw_sprite_ext(spr_cell_shield,a_p_shield - 1,x,y,a_next_scale,a_next_scale,90*a_angle,c_white,1);
        }
    break;
    case EnCellTypes.LIFT:
        scr_a_draw_cell_default(colour_spr,dir_spr);
        draw_sprite_ext(spr_cell_d_bg,0,x,y,a_next_scale,a_next_scale,90*a_angle,c_white,1);
        draw_sprite_ext(spr_cell_reverse,0,x,y,a_next_scale,a_next_scale,90*a_angle,c_white,1);
    break;
    case EnCellTypes.TIME:
        scr_a_draw_cell_default(colour_spr,dir_spr);
        if(a_p_turn_count > 0)
        {
            draw_sprite_ext(spr_cell_d_bg,0,x,y,a_next_scale,a_next_scale,90*a_angle,c_white,1);
            draw_set_colour(scr_a_draw_get_col(a_colour));
            v_draw_text(x - 1,y + 1,s(a_p_turn_count),fa_center,fa_middle,EnText.H2);
        }
    break;
    case EnCellTypes.DOUBLE:
        scr_a_draw_cell_default(colour_spr,dir_spr);
        draw_sprite_ext(spr_cell_d_bg,0,x,y,a_next_scale,a_next_scale,90*a_angle,c_white,1);
        draw_sprite_ext(scr_a_draw_get_double(a_p_double_colour),0,x,y,a_next_scale,a_next_scale,90*a_angle,c_white,1);
    break;
    case EnCellTypes.VORTEX:
    if(a_p_vortex_colour == EnColourTypes.EMPTY)
    {
        draw_sprite_ext(spr_cell_vortex_bg,0,x,y,a_next_scale,a_next_scale,0,c_white,1);
        draw_sprite_ext(spr_cell_vortex_star,0,x,y,a_next_scale,a_next_scale,0,c_white,1);
    }
    else
    {
        draw_sprite_ext(scr_a_get_col_spr(a_p_vortex_colour),0,x,y,a_next_scale,a_next_scale,0,c_white,1);
        draw_sprite_ext(spr_cell_vortex_star,0,x,y,a_next_scale,a_next_scale,0,c_white,1);
    }
    break;
    case EnCellTypes.MULTI:
        draw_sprite_ext(spr_cell_multi,0,x,y,a_next_scale,a_next_scale,0,c_white,1);
    break;
    case EnCellTypes.LOCK:
        scr_a_draw_cell_default(colour_spr,dir_spr);
        draw_sprite_ext(spr_cell_d_bg,0,x,y,a_next_scale,a_next_scale,90*a_angle,c_white,1);
        draw_sprite_ext(spr_cell_lock,0,x,y,a_next_scale,a_next_scale,0,c_white,1);
    break;
}


if(a_next && instance_exists(global.lvlEdit))
{
    var _next_dir = scr_a_get_dir_spr(a_next_dir)
    if(sprite_exists(_next_dir))
    {
        draw_sprite_ext(_next_dir,0,x + a_next_delta_center + 16,y + a_next_delta_center + 16,
                    a_next_delta_scale,a_next_delta_scale,90*a_next_angle,c_black,0.7);
    }
}

if(a_lock && a_state == EnCellStates.IDLE)
{
    anim_lock_alpha = v_lerp(0,anim_lock_alpha,1,1*anim_lock_alpha_speed,anim_lock_alpha_speed/5);
    draw_sprite_ext(spr_cell_d_bg,0,x,y,1,1,0,c_white,anim_lock_alpha);
    draw_sprite_ext(spr_cell_lock,1,x,y,anim_lock_alpha,anim_lock_alpha,0,c_white,1);
    anim_lock_back = 1;
}
else
{
    if(anim_lock_back)
    {
        anim_lock_alpha = v_lerp(anim_res,anim_lock_alpha,0,1*anim_lock_alpha_speed,anim_lock_alpha_speed/5);
        if(anim_res[0])
        {
            anim_lock_back = 0;
        }
        if(a_state == EnCellStates.IDLE)
        {
            draw_sprite_ext(spr_cell_d_bg,0,x,y,1,1,0,c_white,anim_lock_alpha);
            draw_sprite_ext(spr_cell_lock,1,x,y,anim_lock_alpha,anim_lock_alpha,0,c_white,1);
        }
    }
}

if(global.a_hint_first_turn && a_p_first_turn > 0)
{
    anim_f_delay = a_p_first_turn*anim_f_delay_div;
    if(anim_f_delay_time < anim_f_delay)
    {
        anim_f_delay_time += delta_time/1000000;
    }
    else
    {
        if(anim_f_dir)
        {
            anim_f_alpha = v_lerp(anim_res,anim_f_alpha,0,1*anim_f_alpha_speed,anim_f_alpha_speed/5);
            if(anim_res[0])
            {
                anim_f_dir = 0;
            }
        }
        else
        {
            anim_f_alpha = v_lerp(anim_res,anim_f_alpha,1,1.5*anim_f_alpha_speed,anim_f_alpha_speed/5);
            if(anim_res[0])
            {
                anim_f_dir = 1;
            }
        }
        draw_sprite_ext(spr_cell_hint_over,0,x,y,anim_f_scale,anim_f_scale,0,c_white,anim_f_alpha);
        draw_sprite_ext(spr_cell_d_bg,0,x,y,anim_f_scale,anim_f_scale,0,c_white,anim_f_alpha);
        draw_sprite_ext(spr_GUI_icon_eye,0,x,y,anim_f_scale,anim_f_scale,0,c_white,anim_f_alpha);
    }
    if(!audio_is_playing(global.a_hint_first_turn_snd) && a_p_first_turn == 1 && !anim_f_dir)
    {
        global.a_hint_first_turn_snd = scr_def_play_snd(snd_tile_hint_loop);
    }
}
else
{
    anim_f_delay_time = 0;
    anim_f_alpha = 0;
}


