/// @description scr_inst_create(obj)

var inst = instance_create(__view_get( e__VW.WView, 0 )/2, __view_get( e__VW.HView, 0 )/2, argument0);
scr_level_add_obj(inst);
with(inst)
{
    scope_vars[1,1] = other.layer;
    scope_obj_type = argument0;
}

gui_scope_init(inst);




