/// @description scr_level_load_selected(lvl_num)

var tmp_lvl_num = argument0;

scr_level_init();

if(tmp_lvl_num)
{
    scr_level_load_data(tmp_lvl_num);
}
else
{
    scr_level_load_data(gui_get_value(guid,"Level selector",1));
}




