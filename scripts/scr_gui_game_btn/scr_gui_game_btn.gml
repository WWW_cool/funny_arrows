/// @description scr_gui_game_btn

if(argument_count > 0)
{
    var btn_id = argument[0];
    if(ds_exists(a_event_list,ds_type_list))
    {
        ds_list_delete(a_event_list,ds_list_find_index(a_event_list,btn_id));
        v_btns_set_param(btn_id,"active",0);
        switch(btn_id)
        {
            case btn_price:
                global.inst_glb.game_time_price = global.inst_glb.price_time_need;
                if(global.user_open_all)
                {
                    repeat(3)
                    {
                        var _rnd = irandom_range(1,2);
                        scr_gui_price_efx(0,_rnd);
                    }
                }
                else
                {
                    var _count = 0;
                    for(var i = 0; i < 3; i += 1)
                    {
                        var _rnd = irandom_range(1,2);
                        if(_rnd > 0)
                        {
                            _count += _rnd;
                            if(i == 0)_rnd = 1;
                            scr_gui_price_efx(i,_rnd);
                        }
                    }
                    if(!_count)
                    {
                        var _rnd = irandom(2);
                        scr_gui_price_efx(_rnd,2);
                    }
                }
                scr_g_save();
            break;
            case btn_repost:
                scr_s_share();
            break;
            case btn_advert:
                global.inst_glb.game_time_adv = global.inst_glb.price_time_need/2;
                scr_s_msg(EnMsgTypes.Advert);
            break;
        }
    }
}   
