/// @description v_scroll_add(spr,max,size,x,y,cb)

scroll_spr      = argument0;
scroll_max      = argument1;
scroll_separate = argument2;

xscroll         = argument3;
yscroll_init    = argument4;
scroll_cb       = argument5;

for (var i = 0; i < v_scroll_count; i += 1)
{
    if (v_scroll_deleted[i])
    {
        v_scroll_xpos[i] = xscroll;
        v_scroll_ypos[i] = 0;
        v_scroll_ypos_init[i] = yscroll_init;
        v_scroll_ypos_start[i] = 0;
        
        v_scroll_spr[i] = scroll_spr;
        v_scroll_line_max[i] = scroll_max;
        v_scroll_min[i] = 0;
        v_scroll_size[i] = scroll_separate;
        
        v_scroll_callback[i] = scroll_cb;
        
        v_scroll_lay_on[i] = 0;
        v_scroll_pressed[i] = 0;
        v_scroll_up_ind[i] = 0;
        v_scroll_down_ind[i] = 0;
        
        v_scroll_deleted[i] = 0;
        return i;
    }   
}

v_scroll_xpos[v_scroll_count] = xscroll;
v_scroll_ypos[v_scroll_count] = 0;
v_scroll_ypos_init[v_scroll_count] = yscroll_init;
v_scroll_ypos_start[v_scroll_count] = 0;

v_scroll_spr[v_scroll_count] = scroll_spr;
v_scroll_min[v_scroll_count] = 0;
v_scroll_size[v_scroll_count] = scroll_separate;
v_scroll_line_max[v_scroll_count] = scroll_max;

v_scroll_callback[v_scroll_count] = scroll_cb;

v_scroll_lay_on[v_scroll_count] = 0;
v_scroll_pressed[v_scroll_count] = 0;
v_scroll_up_ind[v_scroll_count] = 0;
v_scroll_down_ind[v_scroll_count] = 0;

v_scroll_deleted[v_scroll_count] = 0;

v_scroll_count += 1;
return v_scroll_count - 1;



