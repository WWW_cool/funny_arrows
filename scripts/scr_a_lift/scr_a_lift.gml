/// @description scr_a_lift(inst)

var _inst = argument0;
var _dir = scr_a_lift_check(_inst);

switch(_dir) // 1 - сверху вниз 0 - снизу вверх
{
    case 0:
    case 1:
        _inst.a_anim_state      = EnCellAnimTypes.LIFT;
        _inst.a_state           = EnCellStates.ANIM;
        _inst.amin_reverse_dir  = 0;
    break;
}

