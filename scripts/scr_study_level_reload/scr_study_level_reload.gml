/// @description scr_study_level_reload

if(instance_exists(global.a_study_inst))
{
    with(global.a_study_inst)
    {
        scr_study_deinit();
        if(!scr_study_init_level(st_hint_number,st_hint_index))
        {
            fed("ERROR | fail to RE_load level...");
            scr_study_back_cb();
        }
        st_selector = instance_create(1,1,obj_selector);
        scr_study_init_obj(st_selector);
    }
}




