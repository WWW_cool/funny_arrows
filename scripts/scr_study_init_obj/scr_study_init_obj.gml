/// @description scr_study_init_obj(inst)

var _inst = argument0;

if(instance_exists(_inst))
{
    var _delta = 0;
    if(!global._def_glb_var_default_resolution_pc)
    {
        _delta = -400 + global._def_glb_var_default_resolution_w/2;
    }
    _inst.x +=  st_x_dist*st_level + _delta;// +
                //(view_wview - global._def_glb_var_default_resolution_w)/2;
    if(ds_exists(st_objects,ds_type_list))
    {
        ds_list_add(st_objects,_inst);
    }
}
else
{
    fed("ERROR | fail to init study obj...");
}
