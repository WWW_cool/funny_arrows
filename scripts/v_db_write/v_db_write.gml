/// @description v_db_write(id,p_type,p_val)

var user_id = argument0;
var p_type = argument1;
var p_val = argument2;

global.v_db_request_p_id = user_id;
global.v_db_request_p_type = p_type;
global.v_db_request_p_val = p_val;

var res = 0;
if(global.v_db_request_id == noone)
{
    switch(global.v_db_request_state)
    {
        case 0: // read data
            if(v_db_read(user_id))
            {
                //fed("start put");
                global.v_db_request_state += 1;
                global.v_db_request_type = V_DB_REQUEST_Types.PUT;
            }
        break;
        case 1: // replace and write data
            if(global.v_db_request_id == noone)
            {
                if(ds_exists(global.v_db_request_map,ds_type_map))
                {   
                    //fed("get map");
                    ds_map_replace(global.v_db_request_map,p_type,p_val);
                    var params_str = json_encode(global.v_db_request_map);
                    if(v_db_request(global.v_db_address + s(user_id),"PUT",params_str))
                    {
                        //fed("put");   
                        global.v_db_request_state += 1;
                        global.v_db_request_type = V_DB_REQUEST_Types.PUT;
                    }
                }
            }
        break;
        case 2: // all done
            if(global.v_db_request_id == noone)
            {
                //fed("put ok");
                global.v_db_request_type = V_DB_REQUEST_Types.NONE;
                v_db_read(user_id);
                global.v_db_request_state = 0;
                res = 1;
            }
        break;
        default:
            global.v_db_request_state = 0;
    }
}
else
{
    v_req_create(v_db_write,user_id,p_type,p_val);
}
return res;



