/// @description v_sld_sys_releaseFree


if(argument_count > 0)
{
    var tmp_src_obj = global.v_sld_src_obj;
    if(instance_exists(tmp_src_obj))
    {
        var tmp_src = global.v_sld_src_id;
        if(tmp_src != -1)
        {
            v_btns_set_cb(argument[0],"releaseFree",-1);
            v_sld_cb_release(noone);
        }
    }
}





