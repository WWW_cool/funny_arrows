/// @description scr_db_save_param(num,val)

var tmp_num = argument0;
var tmp_val = argument1;

if(tmp_num < EnDbKeys.LENGTH)
{
    global.inst_glb.glb_db_key_val[tmp_num] = tmp_val;
    global.inst_glb.glb_db_key_update[tmp_num] = 1;
}


