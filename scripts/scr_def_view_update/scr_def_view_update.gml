/// @description scr_def_view_update(w,h)
if(argument_count == 0)
{
    view_enabled = 1;
    __view_set( e__VW.Visible, global._def_glb_var_view_current, 1 );
    __view_set( e__VW.WView, global._def_glb_var_view_current, global._def_glb_var_view_wview );
    __view_set( e__VW.HView, global._def_glb_var_view_current, global._def_glb_var_view_hview );
    __view_set( e__VW.WPort, global._def_glb_var_view_current, global._def_glb_var_view_wport );
    __view_set( e__VW.HPort, global._def_glb_var_view_current, global._def_glb_var_view_hport );
    
    __view_set( e__VW.XView, global._def_glb_var_view_current, global._def_glb_var_view_xview );
    __view_set( e__VW.YView, global._def_glb_var_view_current, global._def_glb_var_view_yview );
    
    surface_resize(application_surface,
    __view_get( e__VW.WPort, global._def_glb_var_view_current ),
    __view_get( e__VW.HPort, global._def_glb_var_view_current ));
    
    window_set_size(global._def_glb_var_view_wport,global._def_glb_var_view_hport);
}
else
{
    if(argument_count > 1)
    {
        var _w = argument[0];
        var _h = argument[1];
        
        global._def_glb_var_view_wview = _w;
        global._def_glb_var_view_hview = _h;
        global._def_glb_var_view_wport = _w;
        global._def_glb_var_view_hport = _h;
        
        global._def_glb_var_view_xview = 0;
        global._def_glb_var_view_yview = 0;
        
        
        view_enabled = 1;
        __view_set( e__VW.Visible, global._def_glb_var_view_current, 1 );
        __view_set( e__VW.WView, global._def_glb_var_view_current, global._def_glb_var_view_wview );
        __view_set( e__VW.HView, global._def_glb_var_view_current, global._def_glb_var_view_hview );
        __view_set( e__VW.WPort, global._def_glb_var_view_current, global._def_glb_var_view_wport );
        __view_set( e__VW.HPort, global._def_glb_var_view_current, global._def_glb_var_view_hport );
        
        __view_set( e__VW.XView, global._def_glb_var_view_current, global._def_glb_var_view_xview );
        __view_set( e__VW.YView, global._def_glb_var_view_current, global._def_glb_var_view_yview );
        
        surface_resize(application_surface,
        __view_get( e__VW.WPort, global._def_glb_var_view_current ),
        __view_get( e__VW.HPort, global._def_glb_var_view_current ));
        
        window_set_size(global._def_glb_var_view_wport,global._def_glb_var_view_hport);
        fed("W = " + s(global._def_glb_var_view_wport) + 
        " H = " + s(global._def_glb_var_view_hport));
    }
}
