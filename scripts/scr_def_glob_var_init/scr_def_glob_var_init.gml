/// @description scr_def_glob_var_init

/// Init all glabal vars for this extension
enum EnMSGType{enINFO = 0,enDEBUG = 0,enWARNING = 1,enERROR = 1,enVIEW = 0,enPAUSE = 0};
////////////////ROOM SETTINGS /////////////////////////??????

//global._def_glb_var_room_width  = 750;
//global._def_glb_var_room_height = 500;

/////////////////////////////////////////////////////////////
////////////////BACKGROUND SETTINGS /////////////////////////

global._def_glb_var_background = noone;

/////////////////////////////////////////////////////////////
////////////////VIEW SETTINGS ///////////////////////////////

global._def_glb_var_default_resolution_pc = 1;

if(!global._def_glb_var_default_resolution_pc)
{
    global._def_glb_var_default_resolution_w = 405;
    global._def_glb_var_default_resolution_h = 720; // defauil res of your graphic
    //JS_vk_mobileOn(1);
}
else
{ 
    global._def_glb_var_default_resolution_w = 800;
    global._def_glb_var_default_resolution_h = 600; // defauil res of your graphic
    JS_vk_mobileOn(0);
    JS_vk_winResize
    (
        global._def_glb_var_default_resolution_w,
        global._def_glb_var_default_resolution_h
    );
}

global._def_glb_var_g_resolution_w = browser_width;
global._def_glb_var_g_resolution_h = browser_height; // in game resolution 
global._def_glb_var_g_resolution_w_prev = 0;
global._def_glb_var_g_resolution_h_prev = 0; // in game resolution 
// FULL SCALE 
global._def_glb_var_view_fullscale = 0;
global._def_GUI_xscale = 1;
global._def_GUI_yscale = 1;
global._def_glb_var_view_current = 0;

global._def_glb_var_view_to_bottom = 0;

/////////////////////////////////////////////////////////////
////////////////GLOBAL SETTINGS /////////////////////////////
global._def_glb_game_name_PC      = "FARR_2_PC_"; 
global._def_glb_game_name_Mobile  = "FARR_2_Mobile_"; 

if(!global._def_glb_var_default_resolution_pc)
{
    global._def_glb_game_name = global._def_glb_game_name_Mobile; 
}
else
{
    global._def_glb_game_name = global._def_glb_game_name_PC;
}


global._def_glb_var_room_start = rm_Start;//rm_Landscape_Start;
global._def_glb_var_room_game  = noone;//;

global._def_glb_var_save_ini   = "";//"sdb"; // ini name for save game data

global._def_glb_var_obj_glb    = obj_def_global; // global object type
global._def_glb_var_obj_gh     = noone;//obj_game_handler; // gamehandler object type
global._def_glb_var_obj_debug  = obj_def_debug; // gamehandler object type

/////////////////////////////////////////////////////////////
////////////////AUDIO SETTINGS //////////////////////////////
global._def_glb_var_audio_on_cb = scr_audio_cb;
global._def_glb_var_audio_on_bg_cb = scr_audio_cb;
global._def_glb_var_audio_on = 1;
global._def_glb_var_audio_on_bg = 1;
global._def_glb_var_audio_gain = 0.5;
global._def_glb_var_audio_back = noone;//snd_def_main_back;
global._def_glb_var_audio_click = snd_click_1;//snd_click;

global._def_glb_var_audio_spr_on = spr_menu_btn_switch_on;//spr_btn_soundOn;
global._def_glb_var_audio_spr_off = spr_menu_btn_switch_off;//spr_btn_soundOff;
/////////////////////////////////////////////////////////////
////////////////SPRITES SETTINGS ////////////////////////////

global._def_glb_var_msg_win     = noone;
global._def_glb_var_msg_lose    = noone;
global._def_glb_var_msg_pause   = noone;

/////////////////////////////////////////////////////////////
////////////////PAUSE VARs //////////////////////////////////
global._def_glb_var_pause = false;
global._def_glb_var_pause_obj    = noone;
global._def_glb_var_pause_sprite = noone;
global._def_glb_p_simple         = obj_def_p_simple;
global._def_glb_p_menu           = obj_def_p_menu;
global._def_glb_p_hint           = noone;
global._def_glb_p_lvl_end        = obj_def_p_lvl_end;
global._def_glb_p_achievement    = noone;

/////////////////////////////////////////////////////////////
////////////////FONTS VARs //////////////////////////////////
global._def_glb_f_text = f_text;
global._def_glb_f_H1 = f_H1;
global._def_glb_f_H2 = f_H2;
/////////////////////////////////////////////////////////////
