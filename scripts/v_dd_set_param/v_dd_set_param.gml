/// @description v_dd_set_param(id,name,val)

var tmp_id      = argument0;
var tmp_name    = argument1;
var tmp_val     = argument2;

if(_dd_count > 0)
if (!_dd_deleted[tmp_id])
{
    switch(tmp_name)
    {
        case "spr_in":
            if(sprite_exists(tmp_val))
            {
                v_btns_set_param(_dd_v_btn[tmp_id],"spr",tmp_val);
            }
        break;
        case "spr_mid":
            if(sprite_exists(tmp_val))
            {
                _dd_spr_mid[tmp_id] = tmp_val;
            }
        break;
        case "btn_active":
                v_btns_set_param(_dd_v_btn[tmp_id],"active",tmp_val);
        break;
        case "shift_x":
                _dd_x_shift[tmp_id]  = tmp_val;
        break;
        case "shift_y":
                _dd_y_shift[tmp_id]  = tmp_val;
        break;
        case "cb_draw":
                 _dd_cb_draw[tmp_id]  = tmp_val;
        break;
        case "active":
                 _dd_active[tmp_id]  = tmp_val;
                 v_btns_set_param(_dd_v_btn[tmp_id],tmp_name,tmp_val);
        break;
        case "xpos":
                 _dd_xpos[tmp_id]  = tmp_val;
                 v_btns_set_param(_dd_v_btn[tmp_id],tmp_name,tmp_val);
        break;
        case "ypos":
                 _dd_ypos[tmp_id]  = tmp_val;
                 v_btns_set_param(_dd_v_btn[tmp_id],tmp_name,tmp_val);
        break;
    }
}


