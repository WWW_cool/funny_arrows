/// @description scr_s_check_alone(inst)

var res = true;
var inst = argument0;

for(var i = 0; i < 4; i += 1)
{
    var next_cell = collision_circle(inst.x + a_side*a_dx[i],
    inst.y + a_side*a_dy[i],10,obj_a_parent,false,true);
    if(instance_exists(next_cell))
    {
        res = false;
        break;
    }
    var next_mover = collision_circle(inst.x + a_side*a_dx[i],
    inst.y + a_side*a_dy[i],10,obj_a_mover,false,true);
    if(instance_exists(next_mover))
    {
        res = false;
        break;
    }
}

return res;
