/// @description v_map_find_value(map, key)
var map = argument[0];
var key = argument[1];

/**
        With this function you can get the value from a specified key. 
        The input values of the function are the (previously created) 
        v_map to use and the key to check for. 

        NOTE: If no such key exists then the function will return <undefined>. 
        You should always check this using the is_undefined() function. 
**/

if(is_string(key))
if(is_array(map))
{
    var length = array_height_2d(map);
    
    for(var i=0; i<length; i++)
    {   
        map_i = map[i,0];
        
        if(map_i == key)
        {
            return map[i,1];   
        }
    }     
}

return undefined;

