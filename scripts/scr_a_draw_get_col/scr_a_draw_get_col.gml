/// @description scr_a_draw_get_col(col_ind)

var _colour = global.a_cell_color[argument0];
var res = c_white;

switch(_colour)
{
    case EnColourTypes.EMPTY: 
        res = c_black;
    break;
    case EnColourTypes.BLUE: 
        res = global.col_c_blue;
    break;
    case EnColourTypes.YELLOW: 
        res = global.col_c_yellow;
    break;
    case EnColourTypes.RED: 
        res = global.col_c_red;
    break;
    case EnColourTypes.GREEN: 
        res = global.col_c_green;
    break;
    case EnColourTypes.VIOLET: 
        res = global.col_c_violet;
    break;
    case EnColourTypes.ORANGE: 
        res = global.col_c_orange;
    break;
}

return res;

