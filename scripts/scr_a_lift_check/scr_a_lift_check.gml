/// @description scr_a_lift_check(inst)


var _inst = argument0;

var _dir = -1;


if(_inst.a_next_type == EnCellTypes.LIFT)
{
    _dir = 0;
}
else
{
    if(_inst.a_type == EnCellTypes.LIFT)
    {
        _dir = 1;
    }
}

return _dir;
