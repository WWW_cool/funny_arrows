/// @description gui_scope_update(dir)
if(argument0)
{
    /*if(instance_exists(global.inst_glb))
    {
        if(lvl_num > 0)
        {
            with(global.inst_glb)
            {
                var tmp_lvl = other.lvl_num - 1;
                game_attempts[tmp_lvl] = gui_get_value(other.guid,"Attempts",game_attempts[tmp_lvl]);
            }
        }
    }    */
    if(instance_exists(scope_inst))
    {
        with(scope_inst)
        {
            for(var i = 0; i < array_height_2d(scope_vars); i += 1)
            {
                if(scope_vars[i,0] != "Layer")
                    scope_vars[i,1] = gui_get_value(other.guid, scope_vars[i,0],scope_vars[i,1]);
            }
        }
    }
}
else
{
    /*if(instance_exists(global.inst_glb))
    {
        if(lvl_num > 0)
        {
            with(global.inst_glb)
            {
                var tmp_lvl = other.lvl_num - 1;
                gui_set_value(other.guid, "Attempts",game_attempts[tmp_lvl]);
            }
        }
    } */
    if(instance_exists(scope_inst))
    {
        with(scope_inst)
        {
            for(var i = 0; i < array_height_2d(scope_vars); i += 1)
            {
                if(scope_vars[i,0] != "GRID" && scope_vars[i,0] != "Layer")
                {
                    gui_set_value(other.guid, scope_vars[i,0],scope_vars[i,1]);
                }
                else
                {
                    if(scope_vars[i,0] == "Layer")
                    {
                        gui_set_value(other.guid,"Object " + scope_vars[i,0],scope_vars[i,1]);
                    }
                }
            }
        }
    }
}
