  ///scr_s_check_step()

var res = scr_s_check_anim();

if(res)return res;

for(var i = 0; i < instance_number(obj_a_parent); i += 1)
{
    var inst = instance_find(obj_a_parent,i);
    if(instance_exists(inst))
    {
        if(scr_s_check_alone(inst))
        {
            return 0;
        }
    }
}

for(var i = 0; i < instance_number(obj_a_parent); i += 1)
{
    var inst = instance_find(obj_a_parent,i);
    if(instance_exists(inst))
    {
        if(inst.a_state != EnCellStates.IDLE)
        {
            res = 1;
            break;
        }
        if(inst.a_type == EnCellTypes.TIME && inst.a_p_turn_count)
        {
            continue;
        }
        if(inst.a_lock)
        {
            continue;
        }
        if(scr_s_check_around(inst))
        {
            res = 1;
            break;
        }
    }
    else
    {
        res = 1;
        break;
    }
}

return res;










