/// @description scr_boss_check_btns

if(instance_exists(global.inst_glb))
{
    if(global.inst_glb.game_boss_index == global.inst_glb.game_boss_max)
    {
        v_btns_set_param(main_arrow_r,"active",0);
    }
    else
    {
        v_btns_set_param(main_arrow_r,"active",1);
    }
    if(global.inst_glb.game_boss_index == 0)
    {
        v_btns_set_param(main_arrow_l,"active",0);
    }
    else
    {
        v_btns_set_param(main_arrow_l,"active",1);
    }
    scr_boss_play(main_play);
}





