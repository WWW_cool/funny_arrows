/// @description v_scroll_reset(id)

var i = argument0;

if(i < v_scroll_count)
{
    if(!v_scroll_deleted[i])
    {
        v_scroll_min[i] = 0;
    }
}
