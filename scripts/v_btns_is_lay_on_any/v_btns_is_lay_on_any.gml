/// @description v_btns_is_lay_on_any()

/**
    Функция проверяет, наведена ли мышка хоть на 1 кнопку 
**/

//Проверяем, что существует обработчки кнопок
var inst = global._v_btns_owner;
if(instance_exists(inst))
with(inst)
{
    if(_v_btns_lay_on_count > 0) return true;
}

return false;
