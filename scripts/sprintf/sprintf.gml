/// @description sprintf(str, arg1,arg2,...)

if(argument_count > 1)
{
    var str = argument[0];
    var agr_ind = 0;
    var str_ind = 0;
    var str_length = 0;
    for(var i = 1; i < argument_count; i += 1)
    {
        str_ind = 0;
        str_length = string_length(str);
        
        while(str_length-- > 0)
        {
            if(string_char_at(str,str_ind) == "&")
            {
                str = string_delete(str,str_ind, 1);
                str = string_insert(s(argument[i]),str,str_ind);
                agr_ind += 1;
                break;
            }
            str_ind += 1;
        }
    }
    return str;
}
