/// @description v_sld_draw

if(v_sld_count > 0)
{
    for(var i = 0; i < v_sld_count; i += 1)
    {
        if (!v_sld_deleted[i])
        {
            v_btns_draw_btn(v_sld_v_btn[i]);
            draw_sprite(v_sld_spr[i],0,
            v_sld_xpos[i] + v_sld_x[i] + v_sld_value[i]*v_sld_width[i],
            v_sld_ypos[i] + v_sld_y[i]);
        }
    }
}


