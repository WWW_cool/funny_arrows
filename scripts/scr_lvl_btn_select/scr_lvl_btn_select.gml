/// @description scr_lvl_btn_select

var res_room_ind = 0;
if(argument_count > 0)
{
    var btn_id = argument[0];
    
    for(var i = 0; i < array_length_1d(OPT_controls_type); i += 1)
    {
        if(OPT_controls_type[i] == btn_id)
        {
            res_room_ind = i + 1 + p_num*16;
        }
    }
}
fed("GO to lvl #" + s(res_room_ind));
//scr_def_rm_goto_room(res_room_ind + rm_Select + 1);
if(os_browser == browser_not_a_browser)
{
    if(instance_exists(glob))
    {
        if(scr_level_get_count() >= res_room_ind)
        {
            glob.game_lvl_to_load = res_room_ind;
            scr_def_rm_goto_room(rm_lvls + 1);
        }
    }
}
else
{
    /**/if(room_exists(rm_alvl_1 + (res_room_ind - 1)))
    {
        scr_def_rm_goto_room(rm_alvl_1 + (res_room_ind - 1));
    }
}

