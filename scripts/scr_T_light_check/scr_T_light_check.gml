/// @description scr_T_light_check(obj)

var tmp_master = argument0;
var tmp_x,tmp_y;
var tmp_range = tmp_master.t_sys_range;
for(var i = 0; i < array_length_1d(tmp_master.t_side_allow); i += 1)
{
    if(tmp_master.t_side_allow[i] != -1)
    {
        var master_angle = (tmp_master.t_side_allow[i] + tmp_master.t_angle)%360;
        tmp_x = tmp_master.x + lengthdir_x(tmp_range,master_angle);
        tmp_y = tmp_master.y + lengthdir_y(tmp_range,master_angle);
        var tmp_light = collision_point(tmp_x,tmp_y,obj_tile_parent,false,false);
        if(instance_exists(tmp_light))
        {
            var tmp_angle = point_direction(tmp_light.x,tmp_light.y,tmp_master.x,tmp_master.y);
                                            
            for(var k = 0; k < 4; k += 1)
            {
                if(tmp_light.t_side_allow[k] != -1)
                {
                    var slave_angle = (tmp_light.t_angle + tmp_light.t_side_allow[k]) % 360;
                    if(slave_angle == tmp_angle)
                    {
                        
                        with(tmp_light)
                        {
                            if(!t_sys_master_check)
                            {
                                t_sys_master_count += 1;
                                if(t_sys_master_count >= t_atr_light_need)
                                {
                                    t_sys_master_check = 1;
                                    scr_T_light_check(tmp_light);
                                }
                            }
                        }
                        break;
                    }
                }
            } 
        }   
    }
}
