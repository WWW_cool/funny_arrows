/// @description scr_glob_points_add()

if(instance_exists(global.inst_glb))
{
    with(global.inst_glb)
    {
        game_current_lvl_points += clamp(game_points_per_tile - game_current_lvl_try,1,game_points_per_tile) div 1;
    }
}


