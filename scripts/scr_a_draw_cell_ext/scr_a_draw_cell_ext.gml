/// @description scr_a_draw_cell_ext(next,type,x,y,scale,angle,alpha)
var _next = argument0;
var _type = argument1;
var _x = argument2;
var _y = argument3;
var _scale = argument4;
var _angle = argument5;
var _alpha = argument6;

if(_next)
{
    var _colour     = a_next_colour;
    var colour_spr  = scr_a_get_col_spr(a_next_colour);
    var dir_spr     = scr_a_get_dir_spr(a_next_dir);
    var _spr_angle  = a_next_angle;
    var _shield     = a_next_p_shield;
    var _turn_count = a_next_p_turn_count;
    var _double_colour = a_next_p_double_colour;
}
else
{
    var _colour     = a_colour;
    var colour_spr  = scr_a_get_col_spr(a_colour);
    var dir_spr     = scr_a_get_dir_spr(a_dir);
    var _spr_angle  = a_angle;
    var _shield     = a_p_shield;
    var _turn_count = a_p_turn_count;
    var _double_colour = a_p_double_colour;
}
switch(_type)
{
    case EnCellTypes.NONE:
        scr_a_draw_cell_default_ext(_x,_y,_scale,_angle,_alpha,colour_spr,dir_spr,_spr_angle);
    break;
    case EnCellTypes.SHIELD:
        scr_a_draw_cell_default_ext(_x,_y,_scale,_angle,_alpha,colour_spr,dir_spr,_spr_angle);
        if(_shield)
        {
            draw_sprite_ext(spr_cell_shield,_shield - 1,_x,_y,_scale,_scale,90*_spr_angle + _angle,c_white,_alpha);
        }
    break;
    case EnCellTypes.LIFT:
        scr_a_draw_cell_default_ext(_x,_y,_scale,_angle,_alpha,colour_spr,dir_spr,_spr_angle);
        draw_sprite_ext(spr_cell_d_bg,0,_x,_y,_scale,_scale,90*_spr_angle + _angle,c_white,_alpha);
        draw_sprite_ext(spr_cell_reverse,0,_x,_y,_scale,_scale,90*_spr_angle + _angle,c_white,_alpha);
    break;
    case EnCellTypes.TIME:
        scr_a_draw_cell_default_ext(_x,_y,_scale,_angle,_alpha,colour_spr,dir_spr,_spr_angle);
        if(_turn_count > 0)
        {
            draw_sprite_ext(spr_cell_d_bg,0,_x,_y,_scale,_scale,90*_spr_angle + _angle,c_white,_alpha);
            draw_set_colour(scr_a_draw_get_col(_colour));
            var _tmp_alpha = draw_get_alpha();
            draw_set_alpha(_alpha);
            v_draw_text(_x - 1,_y + 1,s(_turn_count),fa_center,fa_middle,EnText.H2);
            draw_set_alpha(_tmp_alpha);
        }
    break;
    case EnCellTypes.DOUBLE:
        scr_a_draw_cell_default_ext(_x,_y,_scale,_angle,_alpha,colour_spr,dir_spr,_spr_angle);
        draw_sprite_ext(spr_cell_d_bg,0,_x,_y,_scale,_scale,90*_spr_angle + _angle,c_white,_alpha);
        draw_sprite_ext(scr_a_draw_get_double(_double_colour),0,_x,_y,_scale,_scale,90*_spr_angle + _angle,c_white,_alpha);
    break;
    case EnCellTypes.VORTEX:
        draw_sprite_ext(spr_cell_vortex_bg,0,_x,_y,_scale,_scale,_angle,c_white,_alpha);
        draw_sprite_ext(spr_cell_vortex_star,0,_x,_y,_scale,_scale,_angle,c_white,_alpha);
    break;
    case EnCellTypes.MULTI:
        draw_sprite_ext(spr_cell_multi,0,_x,_y,_scale,_scale,_angle,c_white,_alpha);
    break;
    case EnCellTypes.LOCK:
        scr_a_draw_cell_default_ext(_x,_y,_scale,_angle,_alpha,colour_spr,dir_spr,_spr_angle);
        draw_sprite_ext(spr_cell_d_bg,0,_x,_y,_scale,_scale,90*_spr_angle + _angle,c_white,_alpha);
        draw_sprite_ext(spr_cell_lock,0,_x,_y,_scale,_scale,_angle,c_white,_alpha);
    break;
}


if(a_lock)
{
    anim_lock_alpha = v_lerp(0,anim_lock_alpha,0,2*anim_lock_alpha_speed,anim_lock_alpha_speed/5);
    draw_sprite_ext(spr_cell_d_bg,0,x,y,1,1,0,c_white,anim_lock_alpha);
    draw_sprite_ext(spr_cell_lock,1,x,y,anim_lock_alpha,anim_lock_alpha,0,c_white,1);
}


