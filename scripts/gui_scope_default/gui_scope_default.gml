/// @description gui_scope_default(obj_type)

if(argument_count > 0)
{
    scope_obj_type = argument[0];
    
    var ind = 0;
    scope_vars[ind,0] = "GRID";
    scope_vars[ind++,1] = 36;
    scope_vars[ind,0] = "Layer";
    scope_vars[ind++,1] = 0;
    
    scope_vars[ind,0] = "x";
    scope_vars[ind++,1] = x;
    scope_vars[ind,0] = "y";
    scope_vars[ind++,1] = y;
    
    switch(scope_obj_type)
    {
        case obj_a_parent:
            gui_scope_default_tile(ind);
        break;
        case obj_a_mover:
            gui_scope_default_mover(ind);
        break;
        default:
            show_error("wrong type", false);
    }
    
    scope_drag = 0;
    scope_create = 1;
    
    scope_dif_x = 0;
    scope_dif_y = 0;
}
else
{
    show_error("Init error", true);
}


