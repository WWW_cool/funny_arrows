/// @description gui_create_scope_header(gui)

var editor_name = "Level name - " + lvl_name;

if(global.lvlObjList != noone)
{
    gui_create_string(guid, "text", editor_name, c_green);
    //gui_create_slider(guid, "Layer", layer, 0, 5,c_green);
    gui_create_button(guid, "Start level", scr_T_update,noone, c_green);
    gui_create_menu(guid, "System", false, 14, c_black);
    gui_create_watcher(guid, "FPS", fps, c_yellow);
    gui_create_slider(guid, "GRID", grid, 4, 128,c_yellow);
    gui_create_checkbox(guid, "Show GRID", grid_show, c_yellow);
    gui_create_checkbox(guid, "Show debug", debug_show, c_yellow);
    gui_create_button(guid, "New level", scr_level_new,noone, c_yellow);
    gui_create_button(guid, "Save level", scr_level_save,noone, c_yellow);
    gui_create_button(guid, "Save func level", scr_level_save_func,noone, c_yellow);
    gui_create_button(guid, "Load func level", scr_level_load_func,noone, c_yellow);
    gui_create_button(guid, "Load level", scr_level_load,noone, c_yellow);
    gui_create_button(guid, "Load next", scr_level_load_next,noone, c_yellow);
    gui_create_button(guid, "Replace level", scr_level_replace,noone, c_yellow);
    gui_create_button(guid, "Insert level", scr_level_insert,noone, c_yellow);
    gui_create_button(guid, "Export levels", scr_level_export,noone, c_yellow);
    gui_create_button(guid, "Export func levels", scr_level_export_func,noone, c_yellow);
    
    gui_create_menu(guid, "Object selector", true, 2, c_black);
    gui_create_button(guid, "Create tile",scr_inst_create,obj_a_parent, $00ffff/2);
    gui_create_button(guid, "Create mover",scr_inst_create,obj_a_mover, $00ffff/2);
    //gui_create_menu(guid, "Lvl Settings", false, 1, c_black);
    /*if(instance_exists(global.a_selector))
    {
        
        gui_create_slider(guid, "Attempts", global.a_selector.s_attempts, 1, 25,c_yellow);
    }
    if(instance_exists(global.inst_glb))
    {
        //fed("lvl_num = " + s(lvl_num))
        if(lvl_num > 0)
        {
            var tmp_lvl = lvl_num - 1;
            gui_create_slider(guid, "Attempts", global.inst_glb.game_attempts[tmp_lvl], 1, 25,c_yellow);
        }
    }*/
}
else
{
    gui_create_string(guid, "text", editor_name, c_green);
    gui_create_button(guid, "New level", scr_level_new,noone, c_green);
    gui_create_button(guid, "New func level", scr_level_new_func,noone, c_green);
    gui_create_button(guid, "Load level", scr_level_load,noone, c_green);
    gui_create_button(guid, "Load func level", scr_level_load_func,noone, c_green);
}

