/// @description scr_s_check_anim

var res = 0;

for(var i = 0; i < instance_number(obj_a_mover); i += 1)
{
    var inst = instance_find(obj_a_mover,i);
    if(instance_exists(inst))
    {
        if(inst.m_anim_state != EnMoverStates.IDLE)
        {
            res = 1;
            break;
        }
    }
}

if(res)return res;

for(var i = 0; i < instance_number(obj_a_parent); i += 1)
{
    var inst = instance_find(obj_a_parent,i);
    if(instance_exists(inst))
    {
        if(inst.a_state != EnCellStates.IDLE && inst.a_state != EnCellStates.SELECTED)
        {
            res = 1;
            break;
        }
    }
}

return res;
