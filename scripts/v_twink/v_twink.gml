/// @description v_twink(res[],state[],val,min,max,speed,[type])

var _res = argument0;
var _state = argument1;
var _val = argument2;
var _min = argument3;
var _max = argument4;
var _speed = argument5;

if(is_array(_state) && is_array(_res))
{
    if(argument_count > 6)
    {
        var _type = argument[6]
        if(_type) // v_lerp
        {
            if(_state[0])
            {
                _val = v_lerp(_res,_val,_max,_speed,_speed/10);
                if(_res[0])
                {
                    _state[@ 0] = false;
                }
            }
            else
            {
                _val = v_lerp(_res,_val,_min,_speed,_speed/10);
                if(_res[0])
                {
                    _state[@ 0] = true;
                }
            }
        }
        else // v_inc
        {
            if(_state[0])
            {
                _val = v_increment(_res,_val,_max,_speed,_speed/5);
                if(_res[0])
                {
                    _state[@ 0] = false;
                }
            }
            else
            {
                _val = v_increment(_res,_val,_min,_speed,_speed/5);
                if(_res[0])
                {
                    _state[@ 0] = true;
                }
            }
        }
    }
    else
    {
        if(_state[0])
        {
            _val = v_lerp(_res,_val,_max,_speed,_speed/10);
            if(_res[0])
            {
                _state[@ 0] = false;
            }
        }
        else
        {
            _val = v_lerp(_res,_val,_min,_speed,_speed/10);
            if(_res[0])
            {
                _state[@ 0] = true;
            }
        }
    }
}

return _val;
