/// @description scr_gui_life(val)

if(!instance_exists(global.inst_glb)) exit;

if(argument_count > 0)
{
    var _val = argument[0];
    global.inst_glb.live_count = clamp(_val + global.inst_glb.live_count,0,100);
    var _inst = instance_create(1,1,obj_GUI_life_efx);
    if(_val > 0)
    {
        _inst.OPT_text = "+" + s(_val);
    }
    else
    {
        _inst.OPT_text = s(_val);
    }
    //fed(s(_val) + " life... lifes = " + s(global.inst_glb.live_count));
}
else
{
    if(global.inst_glb.live_count <= 0)
    {
        global.inst_glb.live_count = 0;
        return false;
    }
}
return true;

