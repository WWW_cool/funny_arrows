/// @description scr_def_play_snd_stop(snd)

var tmp_snd = argument0;
if(audio_exists(tmp_snd))
{
    audio_stop_sound(tmp_snd);
}

