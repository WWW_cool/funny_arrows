/// @description v_req_create(scr, arg0...arg3)
var res = 0;
if(argument_count > 0)
{
    res = ds_map_create();
    ds_map_add(res,EnRequestData.SCRIPT, argument[0]);
    ds_map_add(res,EnRequestData.ARG_COUNT, argument_count - 1);
    for(var i = 0; i < argument_count - 1; i++)
    {
        ds_map_add(res,EnRequestData.ARG_1 + i, argument[1 + i]);
    }
    v_req_add(res);
}

return res;
