/// @description scr_create_header(text)

var _text = argument0;

var _inst = instance_create(1,1,obj_GUI_header);
if(instance_exists(_inst))
{
    _inst.h_text = _text;
}

return _inst;


