/// @description scr_play_random_lvl()

if(!instance_exists(global.inst_glb))scr_def_rm_goto_room(rm_Start);

if(!scr_gui_life())
{
    var _inst = scr_s_msg(EnMsgTypes.Lives);
    if(instance_exists(_inst))
    {
        _inst.OPT_msg_type = 0;
    }
    exit;
}
with(global.inst_glb)
{
    var ind = game_boss_index;
    var count = game_boss_lvl_count;
    var rnd_lvl = irandom_range(ind*count,count + ind*count)
    
    game_random = 1;
    //scr_gui_life(-1);
    scr_def_rm_goto_room(rm_lvlCreate + 1 + rnd_lvl);
    fed("rnd_lvl = " + s(rnd_lvl));
}

