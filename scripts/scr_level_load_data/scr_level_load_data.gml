/// @description scr_level_load_data(lvl_num)

lvl_num = argument0;
var tmp_lvl_num = lvl_num;
var tmp_section = string(lvl_num);
var _lvl_name = "";

if(instance_exists(global.lvlEdit))
{
    _lvl_name = global.lvlEdit.use_file_name;
}
if(_lvl_name == "")
{
    if(is_string(global.lvlstr))ini_open_from_string(global.lvlstr);
    else ini_open(global._def_glb_game_name + "Levels");
}
else
{
    ini_open(_lvl_name);
}
/***************************************************
  обязательные объекты в комнате и их свойства
 ***************************************************/
  if(instance_exists(global.inst_glb))
    {
        if(tmp_lvl_num > 0)
        {
            global.inst_glb.game_lvl_to_load = tmp_lvl_num;
            fed(s(tmp_lvl_num));
            tmp_lvl_num -= 1;
            //global.inst_glb.game_attempts[tmp_lvl_num] = ini_read_real(tmp_section,"Attempts",1);
            //fed(s(global.inst_glb.game_attempts[tmp_lvl_num]));
            /*if(instance_exists(global.a_selector))
            {
                fed("attempt after start lvl = " + s(tmp_lvl_num));
                global.a_selector.s_attempts = global.inst_glb.game_attempts[tmp_lvl_num];
            }*/
        } 
    }
/***************************************************
   Созданные объекты и их свойства
 ***************************************************/ 
var i = 0;
lvl_name = tmp_section;

while(ini_key_exists(tmp_section,"type" + string(i)))
{
    scr_level_load_obj(ini_read_string(tmp_section,"type" + string(i),""),tmp_section,i);
    //show_debug_message("loaded object # " + string(i));
    i += 1;
}
ini_close();






