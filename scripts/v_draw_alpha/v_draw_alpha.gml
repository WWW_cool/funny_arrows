/// @description v_draw_alpha(dir)

if(argument_count > 0)
{
    draw_set_alpha(GUI_alpha_tmp);
    if(GUI_alpha < 1)GUI_alpha += GUI_alpha_speed;
}
else
{
    GUI_alpha_tmp = draw_get_alpha();
    draw_set_alpha(GUI_alpha);
}

