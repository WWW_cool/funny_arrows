/// @description v_map_delete(map, key);
/// @param map
/// @param  key
var map = argument[0];
var key = argument[1];

/**
    With this function you can remove any given key 
    (and its corresponding value) from the given, 
    previously created, v_map. 
**/

if(is_array(map))
{
    var length = array_height_2d(map);
    var map_i;
    //Проходим по всем элементам карты и ищем, есть ли такой ключ. 
    //Если есть, удаляем его и value
    for(var i=0; i<length; i++)
    {
        if(map[i,0] == key)
        {
            for(var k = i; k < length - 1; k += 1)
            {
                map[@ k,0] = map[@ k + 1,0];
                map[@ k,1] = map[@ k + 1,1];
            }
            map[@ k,0] = -1;
            map[@ k,1] = -1;
            return true;
        }
    }     
}

return true;

