/// @description v_anim(params[],type,period(sec))
/// @param params[]
/// @param type
/// @param period(sec

var _type = argument1;

switch(_type)
{
    case EnAnim.Heart:
        v_anim_ext(argument0,argument2,p_heart,0.07,0.04,0);
    break;
    case EnAnim.Boss_bg:
    var _speed = 0.003;
        v_anim_ext(argument0,argument2,p_boss_bg,_speed,_speed/5,1);
    break;
    case EnAnim.Selector:
    var _speed = 0.15;
        v_anim_ext(argument0,argument2,p_selector,_speed,_speed/5,0);
    break;
}




