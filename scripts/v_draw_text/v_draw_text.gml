/// @description v_draw_text(x,y,str,halign,valign,type)

var _x = argument0;
var _y = argument1;
var _str = argument2;
var _halign = argument3;
var _valign = argument4;
var _type = argument5;

var _font = noone;

v_draw_text_ext(_x,_y,_str,_halign,_valign,_type,0);
        

