/// @description scr_btn_soundToggle_cb
if(argument_count > 0)
{
    var btn_id = argument[0];
    if(global._def_glb_var_audio_on)
    {
        global._def_glb_var_audio_on = 0;
        scr_btn_soundSetSpr(btn_id);
        audio_stop_all();
    }
    else
    {
        global._def_glb_var_audio_on = 1;
        scr_btn_soundSetSpr(btn_id);
        //TODO play back music
    }
}

