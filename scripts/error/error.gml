/// @description error(info, message) 
/// @param info
/// @param  message
var info    = argument[0];
var message = argument[1];
/**
    desc:   Выводит сообщение об ошибке и закрывает игру.

    args:   
            info:       строка, которая указывает, где произошла
                        ошибка: название ивента, объекта, скрипта..
                    
            message:    строка, текст ошибки
            
    return: void
**/
show_message(info+" ERROR: "+message);


