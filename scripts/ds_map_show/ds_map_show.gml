/// @description ds_map_show(id)

var map = argument0;

var current_key = ds_map_find_first(map);
while (is_string(current_key))
{
   fed(current_key + " " + s(ds_map_find_value(map, current_key)));
   current_key = ds_map_find_next(map, current_key);
}

