/// @description scr_s_price
if(argument_count > 0)
{
    var btn_id = argument[0];
    if(!glob.price_time)
    {
        if(global.user_open_all)
        {
            repeat(3)
            {
                var _rnd = irandom_range(1,3);
                scr_gui_price_efx(0,_rnd);
            }
        }
        else
        {
            for(var i = 0; i < 3; i += 1)
            {
                var _rnd = irandom_range(1,4);
                if(_rnd > 0)
                {
                    if(i == 0)_rnd = 1;
                    scr_gui_price_efx(i,_rnd);
                }
            }
        }
        glob.price_time = glob.price_time_need;
        scr_g_save();
    }
}

