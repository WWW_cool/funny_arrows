/// @description gui_scope_default_tile(ind)

var ind = argument0;

scope_vars[ind,0] = "Type";
scope_vars[ind++,1] = a_type;
scope_vars[ind,0] = "Colour";
scope_vars[ind++,1] = a_colour;
scope_vars[ind,0] = "Dir";
scope_vars[ind++,1] = a_dir;
scope_vars[ind,0] = "Angle";
scope_vars[ind++,1] = a_angle;
scope_vars[ind,0] = "Shield";
scope_vars[ind++,1] = a_p_shield;
scope_vars[ind,0] = "Unlock";
scope_vars[ind++,1] = a_p_turn_count;
scope_vars[ind,0] = "Double";
scope_vars[ind++,1] = a_p_double_colour;
scope_vars[ind,0] = "First";
scope_vars[ind++,1] = a_p_first_turn;

scope_vars[ind,0] = "Child_IS";
scope_vars[ind++,1] = a_next;
scope_vars[ind,0] = "Child_Type";
scope_vars[ind++,1] = a_next_type;
scope_vars[ind,0] = "Child_Colour";
scope_vars[ind++,1] = a_next_colour;
scope_vars[ind,0] = "Child_Dir";
scope_vars[ind++,1] = a_next_dir;
scope_vars[ind,0] = "Child_Angle";
scope_vars[ind++,1] = a_next_angle;
scope_vars[ind,0] = "Child_Shield";
scope_vars[ind++,1] = a_next_p_shield;
scope_vars[ind,0] = "Child_Unlock";
scope_vars[ind++,1] = a_next_p_turn_count;
scope_vars[ind,0] = "Child_Double";
scope_vars[ind++,1] = a_next_p_double_colour;



