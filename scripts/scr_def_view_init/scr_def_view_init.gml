/// @description scr_def_view_init

scr_def_view_vars_init();
//////////////////////////////////////////////////////////
/////////////////// VIEW CONTROL /////////////////////////
//////////////////////////////////////////////////////////
view_enabled = 1;
__view_set( e__VW.Visible, global._def_glb_var_view_current, 1 );
__view_set( e__VW.WView, global._def_glb_var_view_current, global._def_glb_var_view_wview );
__view_set( e__VW.HView, global._def_glb_var_view_current, global._def_glb_var_view_hview );
__view_set( e__VW.WPort, global._def_glb_var_view_current, global._def_glb_var_view_wport );
__view_set( e__VW.HPort, global._def_glb_var_view_current, global._def_glb_var_view_hport );

__view_set( e__VW.XView, global._def_glb_var_view_current, global._def_glb_var_view_xview );
__view_set( e__VW.YView, global._def_glb_var_view_current, global._def_glb_var_view_yview );

surface_resize(application_surface,
__view_get( e__VW.WPort, global._def_glb_var_view_current ),
__view_get( e__VW.HPort, global._def_glb_var_view_current ));

scr_def_background_set(global._def_glb_var_background);

window_set_size(__view_get( e__VW.WPort, global._def_glb_var_view_current ),
__view_get( e__VW.HPort, global._def_glb_var_view_current ));
switch(os_browser)
{
    case browser_safari:
    case browser_safari_mobile:   
        break;
    default:
        window_center();
        break;
}


global._def_GUI_xscale = __view_get( e__VW.WPort, global._def_glb_var_view_current )/__view_get( e__VW.WView, global._def_glb_var_view_current );
global._def_GUI_yscale = __view_get( e__VW.HPort, global._def_glb_var_view_current )/__view_get( e__VW.HView, global._def_glb_var_view_current );
        
/*
if(global._def_glb_var_view_fullscale)
{
    display_set_gui_size(view_wview[global._def_glb_var_view_current],
    view_hview[global._def_glb_var_view_current]);
}
else
{
    {
        //global._def_GUI_xscale = view_wport[global._def_glb_var_view_current]/view_wview[global._def_glb_var_view_current];
        //global._def_GUI_yscale = view_hport[global._def_glb_var_view_current]/view_hview[global._def_glb_var_view_current];
        var _def_GUI_x = abs(view_xview[global._def_glb_var_view_current]) * 
        (view_wport[global._def_glb_var_view_current]/view_wview[global._def_glb_var_view_current]);
        var _def_GUI_y = abs(view_yview[global._def_glb_var_view_current]) * 
        (view_hport[global._def_glb_var_view_current]/view_hview[global._def_glb_var_view_current]);
        
        //display_set_gui_size(750,500);
        display_set_gui_maximise(
        global._def_GUI_xscale,global._def_GUI_yscale,
        _def_GUI_x,_def_GUI_y);
        
        scr_def_show_debug_message("_def_GUI xScale = " + string(global._def_GUI_xscale),"view");
        scr_def_show_debug_message("_def_GUI yScale = " + string(global._def_GUI_yscale),"view");
        scr_def_show_debug_message("_def_GUI x = " + string(_def_GUI_x),"view");
        scr_def_show_debug_message("_def_GUI y = " + string(_def_GUI_y),"view");
    }
}*/
//////////////////////////////////////////////////////////
/////////////////// DEBUG INFO ///////////////////////////
//////////////////////////////////////////////////////////
scr_def_show_debug_message("_def_view INIT (sucsess)","info");
//////////////////////////////////////////////////////////
scr_def_show_debug_message("_def_view_current = " 
+ string(global._def_glb_var_view_current) 
+ " w = " 
+ string(__view_get( e__VW.WView, global._def_glb_var_view_current )) + 
" h = " 
+ string(__view_get( e__VW.HView, global._def_glb_var_view_current )),"view");
//////////////////////////////////////////////////////////
scr_def_show_debug_message("_def_view_port w = " 
+ string(__view_get( e__VW.WPort, global._def_glb_var_view_current )) + 
" h = " 
+ string(__view_get( e__VW.HPort, global._def_glb_var_view_current )),"view");
//////////////////////////////////////////////////////////
scr_def_show_debug_message("_def_GUI_size w = " 
+ string(display_get_gui_width()) + 
" h = " 
+ string(display_get_gui_height()),"view");


scr_def_show_debug_message("_def_view x = " + string(global._def_glb_var_view_xview),"view");
scr_def_show_debug_message("_def_view y = " + string(global._def_glb_var_view_yview),"view");



