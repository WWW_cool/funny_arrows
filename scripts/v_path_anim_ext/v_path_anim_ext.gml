/// @description v_path_anim_ext(res[],val,path,point[],speed,delta,random)

var res         = argument0;
var val         = argument1;
var path        = argument2;
var point       = argument3;
var v_speed     = argument4;
var delta       = argument5;
var _random     = argument6;

var increment   = 128;

if(path_exists(path))
{
    var path_length = path_get_number(path);
    if(point[0] < path_length)
    {
        val = v_increment(res,val,path_get_point_y(path,point[0])/increment,v_speed,delta);
        if(res[0])
        {
            if(!_random)
            {
                point[@ 0] += 1;
            }
            else
            {
                point[@ 0] = irandom(path_get_number(path) - 1);
            }
            res[@ 0] = false;
            return val;
        }
        else
        {
            res[@ 0] = false;
            return val;
        }
    }
    else
    {
        res[@ 0] = true;
        return val;
    }
}
else
{
    res[@ 0] = false;
    return val;
}
