/// @description scr_study_deinit()

if(ds_exists(st_objects,ds_type_list))
{
    for(var i = 0; i < ds_list_size(st_objects); i++)
    {
        var _inst = st_objects[| i];
        if(instance_exists(_inst))
        {
            with(_inst)
            {
                instance_destroy();
            }
        }
    }
}





