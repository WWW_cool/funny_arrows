/// @description gui_scope_init(scope_inst)

scope_inst = argument0;

if(instance_exists(scope_inst))
{
    gui_scope_clear();
    
    switch(scope_inst.scope_obj_type)
    {
        case obj_a_parent:
            gui_scope_tile(scope_inst);
        break;
        case obj_a_mover:
            gui_scope_mover(scope_inst);
        break;
        default:
            show_error("Unknow object type in gui_scope_init line 16", false);
    }
    
    gui_create_button(guid, "Delete", gui_scope_destroy,scope_inst, $d6a12f);
}


