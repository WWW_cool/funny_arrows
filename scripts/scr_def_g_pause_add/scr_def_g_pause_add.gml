/// @description scr_def_g_pause_add(pause,inst)

var _pause = argument0;
var _inst = argument1;

if(instance_exists(_pause) && instance_exists(_inst))
{
    with(_pause)
    {
        var _ind = array_length_1d(_def_p_need_objs);
        _def_p_need_objs[_ind] = _inst;
    }
}   
