/// @description scr_a_check_destroy(inst)

var _inst = argument0;

if(!instance_exists(_inst))exit;

switch(_inst.a_type)
{

    case EnCellTypes.SHIELD:
        if(_inst.a_p_shield)
        {
            _inst.a_p_shield -= 1;
            _inst.a_anim_state      = EnCellAnimTypes.COMMON;
            _inst.a_state           = EnCellStates.IDLE;
            instance_create(_inst.x,_inst.y,obj_efx_shield);
        
            _inst.a_inst_prev = noone;
            _inst.a_inst_next = noone;
            scr_a_reset(_inst);
        }
        else
        {
            _inst.a_state = EnCellStates.DESTROY;
        }
    break;
    default:
        _inst.a_state = EnCellStates.DESTROY;
}





