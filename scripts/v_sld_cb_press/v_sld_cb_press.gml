/// @description v_sld_cb_press

/// system callback

if(argument_count > 0)
{
    var tmp_id = v_sld_get_id(argument[0]);
    if(tmp_id != -1)
    {
        v_sld_state[tmp_id] = "drag";
        global.v_sld_src_obj = id;
        global.v_sld_src_id = tmp_id;
        v_btns_set_cb(argument[0],"releaseFree",v_sld_sys_releaseFree);
    }
}
                
                

