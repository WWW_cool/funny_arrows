/// @description scr_T_light();

/*

Переделал идиологию, теперь распространяем волну от источников и 
рекурсивно проходим по всем веткам, если какой либо светильник 
не может зажечься то дальше не идем, если у него будет больше 
одного источника, то все равно при обсчете других веток его по 
любому достанет и тогда уже расчет пройдет дальше. 

И так до самого конца без лишних возвращений и проверок.


*/
var i,inst, master, master_ind;
master_ind = 0;
for(i = 0; i < instance_number(obj_tile_parent); i += 1)
{
  inst = instance_find(obj_tile_parent,i);
  if(instance_exists(inst))
  {
    if(inst.t_atr_source)
    {
        inst.t_sys_master_check = 1;
        master[master_ind] = inst;
        master_ind += 1;
    }
  }
}

for(i = 0; i < master_ind; i += 1)
{
    //show_debug_message("MASTER ***** " + string(master[i]));
    scr_T_light_check(master[i]);
}
