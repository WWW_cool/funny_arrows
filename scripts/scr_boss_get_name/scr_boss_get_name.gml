/// @description scr_boss_get_name(type)

var _type = argument0;
var res = "";

switch(_type)
{
    case EnBossType.Monster_1:
        res = "Лошарик (1 уровень)";
    break;
    case EnBossType.Monster_2:
        res = "Растяпа (2 уровень)";
    break;
    case EnBossType.Monster_3:
        res = "Злюка (3 уровень)";
    break;
    case EnBossType.Monster_4:
        res = "Доставала (4 уровень)";
    break;
    case EnBossType.Monster_5:
        res = "Это не я (5 уровень)";
    break;
    case EnBossType.Monster_6:
        res = "Дикарь (6 уровень)";
    break;
    default:
}

return res;



