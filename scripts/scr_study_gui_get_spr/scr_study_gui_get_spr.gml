/// @description scr_study_gui_get_spr(index)

var _index = argument0;
var _res = spr_cell_blue;

if(_index < array_length_1d(st_unlock_lvl))
{
    if(instance_exists(global.inst_glb))
    {
        if(global.inst_glb.game_lvl < st_unlock_lvl[_index])
        {
            _res = spr_cell_vortex_bg;
        }
    }
}

return _res;



