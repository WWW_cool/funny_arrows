/// @description scr_a_move_mover_destroy(inst)

var _inst = argument0;
if(instance_exists(global.a_selector))
{
    with(global.a_selector)
    {
        for(var i = 0; i < m_move_mover_count; i++)
        {
            if(m_move_mover_inst[i] == _inst)
            {
                m_move_mover_deleted[i] = a_move_index;
            }
        }
    }
}
