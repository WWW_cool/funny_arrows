/// @description v_math_array_sort_bubble_1d(array)
//сортировка массива пузырьком
var A = argument0;

if(is_array(A))
{
    var len = array_length_1d(A);

    var i,j,b;
    var swapped;
    for (i=0; i<len-1; i++) 
    {
        swapped = false;
         
        for(j=0; j<len-i-1; j++) 
        {
             if (A[j] > A[j+1]) 
             {
                 b = A[j]; 
                 A[@ j] = A[j+1];
                 A[@ j+1] = b;
                 swapped = true;
             }
         }
         
         if(!swapped) break;
     }
}



