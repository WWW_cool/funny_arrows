/// @description scr_study_level_load_next

var _res = false;

if(instance_exists(global.a_study_inst))
{
    with(global.a_study_inst)
    {
        scr_study_deinit();
        st_hint_index += 1;
        _res = scr_study_init_level(st_hint_number,st_hint_index);
        if(_res)
        {
            st_selector = instance_create(1,1,obj_selector);
            scr_study_init_obj(st_selector);
        }
    }
}

return _res;
