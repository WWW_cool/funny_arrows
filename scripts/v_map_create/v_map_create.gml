/// @description v_map_create(size)

/**
    This function is used to create a new, empty, v_map 
    and will return its id which is then used to access 
    the data structure in all other v_map functions. 
**/

/*  ___ВАЖНОЕ ЗАМЕЧАНИЕ ПО МАСИВАМ___
    Массив лучше всего заполнять, начиная с конца - если мы знаем заранее его размер.
    Потому что если сначала написать 
    array[0] = 1;
    А потом
    array[1] = 1;
    это приведет к тому, что при каждом присовении будет перевыделяться память.
    А когда в первую очередь пишешь 
    array[1] = 1;
    А только потом
    array[0] = 1;
    Память выделится сразу 1 раз, а потом будут только присвоения.
    Соответственно для маленьких массивов это неважно, но при больших - будет очень
    тормозить игру при каждом новом присвоении.
    
    ___ЕЩЕ ВАЖНОЕ ЗАМЕЧАНИЕ ПО МАССИВАМ___
    Если массив передается в скрипт - в скрипт передается его копия!
    Чтобы поменялся оригинал, ОБЯЗАТЕЛЬНО нужно вернуть массив из скрипта!!!
    Обязательно при передачи массива в крипт нужно делать примерно следующее:  array = script(array);
    НО!!! Если внутри скрипта использовать аксессор @, значения в массиве можно менять без return array:
    array[@ i] = ...
    
    ___
    Освобождение памяти от массива:  array = 0;
*/
var _v_init_map;
if(argument_count > 0)
{
    _v_init_map = array_create(argument[0]*2);
    for(
    //###############___От этого числа зависит размер всех создаваемых карт
    var i=argument[0]; 
    //###############___ЕСли его поменять, изменится размер создаваемых карт 
    i>=0; i--)
    {
        _v_init_map[i,0] = -1;    _v_init_map[i,1] = -1;
    }
}
else
{
    _v_init_map = array_create(32*2);
    for(
    //###############___От этого числа зависит размер всех создаваемых карт
    var i=31; 
    //###############___ЕСли его поменять, изменится размер создаваемых карт 
    i>=0; i--)
    {
        _v_init_map[i,0] = -1;    _v_init_map[i,1] = -1;
    }
}
//Карта готова к использованию
return _v_init_map;

