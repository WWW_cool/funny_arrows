/// @description scr_def_play_snd_loop(snd)

var tmp_snd = argument0;
if(audio_exists(tmp_snd))
{
    if(global._def_glb_var_audio_on)
    {
        return audio_play_sound(tmp_snd,1,1);
    }
}
return noone;
