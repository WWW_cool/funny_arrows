/// @description v_tbl_set_column(tbl, line, line_max, column, value_1,...)

var tmp_tbl         = argument[0];

if(argument_count > 4)
{
    var tbl_line        = argument[1];
    var tbl_line_max    = argument[2];
    var tbl_column      = argument[3];
    var tmp_value;
    var max_params      = argument_count - 4;
    var i;
    var tmp_line;
    for(i = 0; i < max_params; i += 1)
    {
       /* if(tbl_line + i < tbl_line_max)
        {
            tmp_line = tmp_tbl[tbl_line + i];
            tmp_line[@tbl_column] = argument[4 + i];
            tmp_tbl[@(tbl_line + i)] = tmp_line;
            tmp_line = 0;
        }*/
        if(tbl_line + i < tbl_line_max)
        {
            tmp_tbl[@ tbl_line + i,tbl_column] = argument[4 + i];
        }
    }
}


