/// @description v_draw_tiled(spr,sub,x,y,dir)

var _spr = argument0;
var _sub = argument1;
var _x = argument2;
var _y = argument3;
var _dir = argument4;

var _len = _x;
var _attempts = 25;
var _step = 0;

switch(_dir)
{
    case EnTiled.Hor:
        while(_len < __view_get( e__VW.XView, 0 ) + __view_get( e__VW.WView, 0 ) && _attempts--)
        {
            draw_sprite(_spr,_sub,_x + sprite_get_width(_spr)*_step++,_y);
            _len += sprite_get_width(_spr);
        }
    break;
    case EnTiled.Vert:
    break;
    case EnTiled.ALL:
    break;
}
