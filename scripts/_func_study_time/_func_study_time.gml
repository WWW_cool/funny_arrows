/// @description  _func_study_time(lvl_num)
var _lvl_num = argument0;
var _res = false;
switch(_lvl_num)
{
case 1: // ----------------------------------------------------
    if(global._def_glb_var_default_resolution_pc)
    {
        var tmp_x = 360
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 3
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 432
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 4
            a_next_dir      = 1
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 360
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 4
            a_next_dir      = 2
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 432
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 2
            a_next_dir      = 3
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
    }
    else
    {
        var tmp_x = 360
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 3
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 432
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 4
            a_next_dir      = 1
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 360
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 4
            a_next_dir      = 2
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 432
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 2
            a_next_dir      = 3
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
    }
    _res = true;
break;
case 2: // ----------------------------------------------------
    if(global._def_glb_var_default_resolution_pc)
    {
        var tmp_x = 360
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 432
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 3
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 2
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 1
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 432
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 4
            a_next_dir      = 3
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 360
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 2
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 504
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 1
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 360
        var tmp_y = 216
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 3
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
    }
    else
    {
        var tmp_x = 360
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 3
            a_next_dir      = 0
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 432
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 3
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 2
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 1
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 432
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 4
            a_next_dir      = 3
            a_next_angle    = 0
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 360
        var tmp_y = 360
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 1
            a_next_dir      = 2
            a_next_angle    = 1
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 504
        var tmp_y = 288
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 1
            a_next_angle    = 3
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
        var tmp_x = 360
        var tmp_y = 216
        var inst = instance_create(tmp_x,tmp_y,obj_a_parent);
        with(inst)
        {
            a_type          = 0
            a_colour        = 1
            a_dir           = 0
            a_angle         = 0
            a_p_shield      = 1
            a_p_turn_count  = 1
            a_p_double_colour = 4
            a_p_first_turn    = 0
            a_next          = 0
            a_next_type     = 0
            a_next_colour   = 5
            a_next_dir      = 3
            a_next_angle    = 2
            a_next_p_shield = 1
            a_next_p_turn_count    = 1
            a_next_p_double_colour = 4
        }
        scr_study_init_obj(inst);
            
    }
    _res = true;
break;
}

return _res;


