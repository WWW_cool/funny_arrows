/// @description v_btns_delete(ID);
/// @param ID

//Проверяем, что существует обработчки кнопок
var inst = global._v_btns_owner;
if(instance_exists(inst))
with(inst)
{
    // Deactivates the button to be overwritten by new button
    if(_v_btns_mouse_lay_on_flag[argument0]) 
        _v_btns_lay_on_count--;
    
    _v_btns_deleted[argument0] = true; 
}

return -1;


