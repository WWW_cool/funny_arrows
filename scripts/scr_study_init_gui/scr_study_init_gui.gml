/// @description scr_study_init_gui

if(st_gui_is_init)exit;

var gui_x = st_x_dist;//st_start_x + 

v_choose(point,gui_x + 300,150,gui_x + 125,200);
var start_x = point[EnPoint.X];
var start_y = point[EnPoint.Y];
v_choose(point,100,100,80,100);
var shift_x = point[EnPoint.X];
var shift_y = point[EnPoint.Y];

var _size = array_length_1d(GUI_controls);
var _x = start_x;
var _y = start_y;
var _count = 0;
for(var i = 0; i < 10; i++)
{
    GUI_controls[_size + i] = v_btns_add(_x,_y,scr_study_gui_get_spr(i),id,0,
                            -1,scr_study_tile_cb,-1,-1,-1);
    v_btns_set_cb(GUI_controls[_size + i],"draw",scr_study_draw_cb);
    /// btn efx
    if(i < array_length_1d(st_unlock_lvl))
    {
        if(instance_exists(global.inst_glb))
        {
            if(global.inst_glb.game_lvl == st_unlock_lvl[i])
            {
                instance_create(_x,_y,obj_boss_efx_click);
            }
        }
    }
    _x += shift_x;
    _count++;
    var _div = _count%3;
    if(i != 0 && !_div)
    {
        _x = start_x;
        _y += shift_y;
    }
}

st_gui_is_init = 1;




