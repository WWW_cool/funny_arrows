/// @description v_map_swap(map1,map2)

var tmp_map1 = argument0;
var tmp_map2 = argument1;

var tmp_art = v_map_create();
v_map_copy(tmp_art,tmp_map1);
v_map_copy(tmp_map1,tmp_map2);
v_map_copy(tmp_map2,tmp_art);
tmp_art = v_map_destroy(tmp_art);


