/// @description v_info_set_type(ID, type)
var inst = global._v_info_owner;
if(instance_exists(inst))
with(inst)
{
    _v_info_type[argument0] = argument1;
}
