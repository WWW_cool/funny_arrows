/// @description scr_a_rnd_col_spr
var _list = ds_list_create();
for(var i = 1; i < EnColourTypes.LENGTH; i++)
{
    ds_list_add(_list,i);
}
var ind = 1;
while(ds_list_size(_list))
{
    var _rnd = irandom(ds_list_size(_list) - 1);
    global.a_cell_color[ind++] = _list[| _rnd];
    ds_list_delete(_list,_rnd);
    //fed("col type = " + s(global.a_cell_color[i]));
}

ds_list_destroy(_list);
