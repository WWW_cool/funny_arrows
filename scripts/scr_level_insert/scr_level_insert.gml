/// @description scr_level_insert


var lvl_from = get_integer("Level from #", noone);
var lvl_to = get_integer("Level to #", noone);
var write_count = 0;
var file_str = "";
var file_str_count = 0;

if(lvl_from != noone && lvl_to != noone)
{
    var file = file_text_open_read(global._def_glb_game_name + "Levels");
    if(file)
    {
        fed("read file...");
        file_strings[file_str_count++] = file_text_readln(file);

        while(!file_text_eof(file))
        {
            file_strings[file_str_count++] = file_text_readln(file);
        }
        file_text_close(file);
        for(var i = 0; i < file_str_count; i += 1)
        {
            
            var str = file_strings[i];
            if(string_char_at(str,0) == "[")
            {
                var _lvl_num = scr_string_get_integer(str,"[","]");
                fed("find lvl = " + s(_lvl_num));
                if(_lvl_num >= lvl_to && _lvl_num < lvl_from)
                {
                    file_strings[i] = "[" + s(_lvl_num + 1) + "]";
                    write_count++;
                }
                if(_lvl_num == lvl_from)
                {
                    file_strings[i] = "[" + s(lvl_to) + "]";
                    write_count++;
                }
            }
        }
    }

    if(write_count == (lvl_from - lvl_to) + 1)
    {
        fed("save file...");
        file = file_text_open_write(global._def_glb_game_name + "Levels");
        if(file)
        {
            for(var i = 0; i < file_str_count; i += 1)
            {
                file_text_write_string(file,file_strings[i]);
                file_text_writeln(file);
            }
            file_text_close(file);
            show_message("Insert complite!");
            exit;
        }
    }
    show_message("Insert error! code = " + s(write_count));
}



