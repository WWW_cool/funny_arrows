/// @description gui_scope_tile(inst)

scope_inst = argument0;

/*

"x"
"y"
"Layer"

"Type"
"Colour"
"Dir"
"Angle"
"Shield"
"Unlock"
"Double"
"First"

"Child_IS"

"Child_Type"
"Child_Colour"
"Child_Dir"
"Child_Angle"
"Child_Shield"
"Child_Unlock"
"Child_Double"

*/

gui_create_menu(guid, "Selected object", true, array_height_2d(scope_inst.scope_vars), c_black);
with(scope_inst)
{
    for(var i = 0; i < array_height_2d(scope_vars); i += 1)
    {
        switch(scope_vars[i,0])
        {
            case "x":
                gui_create_menu(other.guid, "Main", true, 2, c_black);
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 0, __view_get( e__VW.WView, 0 ));
            break;
            case "y":
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 0, __view_get( e__VW.HView, 0 ));
            break;
            case "Layer":
                gui_create_watcher(other.guid,"Object " + scope_vars[i,0], scope_vars[i,1], $d6a12f);
            break;
            case "Child_IS":
                gui_create_checkbox(other.guid, "Child_IS", scope_vars[i,1], c_yellow);
            break;
            case "Type":
                gui_create_menu(other.guid, "Options", true, 8, c_black);
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 0, EnCellTypes.LENGTH - 1);
                break;
            case "Child_Type":
                gui_create_menu(other.guid, "Child options", true, 7, c_black);
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 0, EnCellTypes.LENGTH - 1);
            break;
            case "Dir":
            case "Child_Dir":
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 0, 3);
            break;
            case "Colour":
            case "Child_Colour":
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 1, EnColourTypes.LENGTH - 1);
            break;
            case "Angle":
            case "Child_Angle":
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 0, 3);
            break;
            case "Shield":
            case "Child_Shield":
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 0, 3);
            break;
            case "Unlock":
            case "Child_Unlock":
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 1, 5);
            break;
            case "Double":
            case "Child_Double":
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 1, EnColourTypes.LENGTH - 1);
            break;
            case "First":
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 0, 40);
            break;
        }
    }
}


