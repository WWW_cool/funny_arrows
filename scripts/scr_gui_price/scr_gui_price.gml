/// @description scr_gui_price

if(!instance_exists(global.inst_glb)) exit;

for(var i = 0; i < price_count; i += 1)
{
    if(price_item_count[i])
    {
        switch(i)
        {
            case 0: // lives
                global.inst_glb.live_count += price_item_count[i];
                if(global.inst_glb.live_count > global.inst_glb.live_count_max)
                {
                    global.inst_glb.live_count = global.inst_glb.live_count_max;
                }
                break;
            case 1: // fturn
                global.inst_glb.hint_fstep_count += price_item_count[i];
                break;
            case 2: // moveback
                global.inst_glb.hint_moveback_count += price_item_count[i];
                break;
        }
    }
}

instance_destroy();
