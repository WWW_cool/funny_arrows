/// @description v_map_exists(id, key);
/// @param id
/// @param  key
var map = argument[0];
var key = argument[1];
/**
    This function will return true if the specified key 
    exists in the (previously created) v_map, 
    and false if it does not. 
**/

if(is_string(key))
if(is_array(map))
{
    var length = array_height_2d(map);
    var map_i;
    //Проходим по всем элементам карты и ищем, есть ли уже такой ключ. 
    //Если есть, возращаем fail
    for(var i=0; i<length; i++)
    {
        map_i = map[i,0] 
         
        if(map_i == -1) continue;
        else if(map_i == key) return true;
    }
}

return false;



