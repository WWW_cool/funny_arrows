/// @description scr_a_get_col_spr(col_type)

var col_type = global.a_cell_color[argument0];
var res = spr_cell_vortex_bg;

switch(col_type)
{
    case EnColourTypes.EMPTY: 
        res = spr_cell_vortex_bg;
    break;
    case EnColourTypes.BLUE: 
        res = spr_cell_blue;
    break;
    case EnColourTypes.YELLOW: 
        res = spr_cell_yellow;
    break;
    case EnColourTypes.RED: 
        res = spr_cell_red;
    break;
    case EnColourTypes.GREEN: 
        res = spr_cell_green;
    break;
    case EnColourTypes.VIOLET: 
        res = spr_cell_violet;
    break;
    case EnColourTypes.ORANGE: 
        res = spr_cell_orange;
    break;
}

return res;

