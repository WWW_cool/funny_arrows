/// @description scr_vk_init_btn(btn_id, type, text, delta_x, delta_y)

var btn_id = argument0;
var type = argument1;
var text = argument2;
var delta_x = argument3;
var delta_y = argument4;

v_btns_set_text(btn_id, delta_x, delta_y, text, 1, c_black, 0, f_text, f_text, 1);
v_btns_set_param(btn_id, "text_halign", fa_left);
v_btns_set_cb(btn_id, "press", scr_vk_shop(type));
