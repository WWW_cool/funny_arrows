/// @description v_map_show(map)

var map = argument[0];

if(is_array(map))
{
    var length = array_height_2d(map);
    var map_i;
    for(var i=0; i<length; i++)
    {
        map_i = map[i,0];
        if(map_i != -1)
        {
            show_debug_message("| MAP | " + map[i,0] + ": " + string(map[i,1]));
        }
    }
}

