/// @description scr_def_snd_switch(v_btn_id)

if(argument_count > 0)
{
    var btn_ind = argument[0];
    if(global._def_glb_var_audio_on)
    {
        global._def_glb_var_audio_on = 0;
    }
    else
    {
        global._def_glb_var_audio_on = 1;
    }
    
    if(script_exists(global._def_glb_var_audio_on_cb))
    {
        script_execute(global._def_glb_var_audio_on_cb,0);
    }
    
    var snd_spr = scr_def_get_snd_sprite(global._def_glb_var_audio_on);
    if(sprite_exists(snd_spr))
    {
        v_btns_set_param(btn_ind,"spr",snd_spr);
    }
}





