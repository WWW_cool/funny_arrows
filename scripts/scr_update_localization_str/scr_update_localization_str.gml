/// @description scr_update_localization_str(str,leng)
var full_str = argument0;
var current_leng = argument1;
global.localization_map = v_map_create(200);
var resultMap = json_decode(full_str);
var map = ds_map_find_value(resultMap, current_leng);
if(map != undefined)
{
    if(ds_exists(map,ds_type_map))
    {
        var current_key = ds_map_find_first(map);
        
        while (is_string(current_key))
        {
           v_map_add(global.localization_map,current_key,
           ds_map_find_value(map, current_key));
           current_key = ds_map_find_next(map, current_key);
        } 
        ds_map_destroy(map);
        //v_map_show(global.localization_map);
    }
}
ds_map_destroy(resultMap);
