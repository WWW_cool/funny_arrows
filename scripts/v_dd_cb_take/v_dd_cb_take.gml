/// @description v_dd_cb_take

if(argument_count > 0)
{
    var tmp_id = v_dd_get_id(argument[0]);
    global._dd_stop_take = 0;
    if(tmp_id != -1)
    {
        if(_dd_state[tmp_id] == "full")
        {
            if(script_exists(_dd_cb_take[tmp_id]))
            {
                script_execute(_dd_cb_take[tmp_id],tmp_id,argument[0]);
            }
            if(!global._dd_stop_take)
            {
                _dd_state[tmp_id] = "drag";
                v_btns_set_cb(argument[0],"releaseFree",v_dd_sys_releaseFree);
                _dd_mouse_x_delta[tmp_id] = _dd_xpos[tmp_id] - mouse_x;
                _dd_mouse_y_delta[tmp_id] = _dd_ypos[tmp_id] - mouse_y;
                _dd_mouse_x_init[tmp_id] = mouse_x;
                _dd_mouse_y_init[tmp_id] = mouse_y;
                global._dd_src_obj = id;
                global._dd_src_id = tmp_id;
            }
        }
        
    }
}

