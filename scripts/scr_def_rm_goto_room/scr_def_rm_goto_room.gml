/// @description scr_def_rm_goto_room(room)
var _def_rm_dest = argument[0];
/**
  * @brief  Плавный переход между комнатами

Переход в другую комнату осуществляется затемнением экрана.
Каждый шаг рисуем прямоугольник размером с комнату, и плавно
уменьшаем его прозрачность. Когда alpha достигнет 1, меняем 
комнату, а после этого обратно до нуля уменьшаем alpha.

Для удобства создается объект с persistent = true, который выполняет
все, что нужно, а потом самоуничтожается  
    
  * @param  where_going: указатель на комнату, куда хотим попасть
  
  * @retval void
  */

  
if(!room_exists(_def_rm_dest))exit;

//Создаем объект обработки перехода  
var _def_rm_trans_obj = instance_create(0,0,obj_def_rm_transition);

//Передаем в него куда хотим перейти
_def_rm_trans_obj._def_rm_destination = _def_rm_dest;

scr_def_show_debug_message("rm ind = " + string(_def_rm_dest),"debug");


 
