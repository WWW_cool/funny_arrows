/// @description scr_a_move_add(inst)

var _inst = argument0;

if(instance_exists(global.a_selector))
{
    switch(_inst.scope_obj_type)
    {
        case obj_a_parent:
            scr_a_move_add_tile(_inst);
        break;
        case obj_a_mover:
            scr_a_move_add_mover(_inst);
        break;
        default:
            error("scr_a_move_add","unknow obj type");
    }
}


