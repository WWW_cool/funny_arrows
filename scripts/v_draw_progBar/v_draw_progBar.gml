/// @description v_draw_progBar(spr,val,x,y,b_text,text_val)

var spr = argument[0];
var val = argument[1];
var _x = argument[2];
var _y = argument[3];
var b_text = argument[4];
var _text_val = "";

if(argument_count > 5)_text_val = argument[5];

draw_sprite(spr,0,_x,_y);
draw_sprite_part(spr,1,0,0,sprite_get_width(spr)*val,sprite_get_height(spr),_x,_y);

if(b_text)
{
    switch(b_text)
    {
        case 2: // time
            var _time = _text_val % 60;
            if(_time < 10)
            {
                var _text = s((_text_val/60) div 1) + ":0" + s((_text_val % 60) div 1);
            }
            else
            {
                var _text = s((_text_val/60) div 1) + ":" + s((_text_val % 60) div 1);
            }
            v_draw_text(_x + sprite_get_width(spr)/2,_y + sprite_get_height(spr)/2,
            _text,fa_center,fa_middle,EnText.Text);
        break;
        default: // %
            v_draw_text(_x + sprite_get_width(spr)/2,_y + sprite_get_height(spr)/2,
            string(val*100 div 1) + "%",fa_center,fa_middle,EnText.Text);
    }
}




