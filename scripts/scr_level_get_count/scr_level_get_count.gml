/// @description scr_level_get_count

var level_count = 0;
var _lvl_name = "";
if(os_browser == browser_not_a_browser)
{
    if(instance_exists(global.lvlEdit))
    {
        _lvl_name = global.lvlEdit.use_file_name;
    }
    if(_lvl_name == "")
    {
        if(is_string(global.lvlstr))ini_open_from_string(global.lvlstr);
        else ini_open(global._def_glb_game_name + "Levels");
    }
    else
    {
        ini_open(_lvl_name);
    }
    for(var i = 1; i < 255; i += 1)
    {
        if(ini_section_exists(string(i)))
        {
            level_count = i;
        }
        else
        {
            break;
        }
    }
    ini_close();
}
else
{
    if(instance_exists(global.inst_glb))
    {
        level_count = 110;//clamp(global.inst_glb.game_lvl_ok + 1,0,59);
    }
}
if(LVLCREATE)
{
    return level_count;
}
else
{
    return 110;
}


