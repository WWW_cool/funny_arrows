/// @description v_map_get_real(string)

var str = argument0;


if(is_real(real(str)))
{
    return real(str);
}
else
{
    var tmp_str = string_char_at(str,0);
    if(is_real(real(tmp_str)))
    {
        var tmp_length = string_length(str); // длинна до следующей палки
        var tmp_ind = 0;
        while(tmp_length--)
        {
            tmp_ind += 1;
            if(string_char_at(str,1 + tmp_ind) == ".")
            {
                key = string_copy(str,1,tmp_ind);
                fed("FIND ERROR VAL --- "  + key);
                return real(key);
                break;
            }
        }
    }
}

return -1;




