/// @description v_db_request(adr,type,params)

var adr = argument0;
var type = argument1;
var params = argument2;

if(global.v_db_request_type == V_DB_REQUEST_Types.PUT &&
type != "PUT")
{
    return 0;
}

var map = ds_map_create();
var res = 0;
ds_map_add(map, "Content-Type", "application/json");
//ds_map_add(map, "Host", "rest.game.zrabadaber.com");
//fed("request type - " + s(type));
if(global.v_db_request_id == noone)
{
    global.v_db_request_id = http_request(adr,type,map,params);
    res = 1;
}
else
{
    //fed("busy in request");
    v_req_create(v_db_request,adr,type,params);
}

ds_map_destroy(map);

return res;

