/// @description scr_level_load_next

var level_count = scr_level_get_count();

if(global.inst_glb.game_lvl_to_load + 1 < level_count)
{
    gui_scope_clear();
    scr_level_init();
    scr_level_load_data(global.inst_glb.game_lvl_to_load + 1);
}
