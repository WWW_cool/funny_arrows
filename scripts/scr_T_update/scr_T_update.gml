/// @description scr_T_update

for(var i = 0; i < instance_number(obj_tile_parent); i += 1)
{
  var inst = instance_find(obj_tile_parent,i);
  if(instance_exists(inst))
  {
    if(!inst.t_atr_source)
    {
        inst.t_sys_master_check = 0;
        inst.t_sys_master_count = 0;
    }
  }
}

scr_T_light();

var win_res = 1;
for(var i =0; i < instance_number(obj_tile_parent); i += 1)
{
  var inst = instance_find(obj_tile_parent,i);
  if(instance_exists(inst))
  {
    with(inst)
    {
        scr_T_light_update();
        if(!t_sys_lighted)win_res = 0;
    }
  }  
}

if(!instance_exists(global.lvlEdit))
{
    if(win_res && instance_number(obj_tile_parent) > 0)
    {
        for(var i =0; i < instance_number(obj_tile_parent); i += 1)
        {
          var inst = instance_find(obj_tile_parent,i);
          if(instance_exists(inst))
          {
            with(inst)
            {
                t_sys_uncontrol = 1;
            }
          }
        }
        if(instance_exists(global.inst_glb))
        {
          global.inst_glb.win_time = global.inst_glb.win_time_need;
        }
    }
}




