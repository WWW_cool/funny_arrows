/// @description v_map_add(map, key, val);
/// @param map
/// @param  key
/// @param  val
var map = argument[0];
var key = argument[1];
var val = argument[2];

/**
    This function should be used to add sets of key/value pairs 
    into the specified v_map. You can check this function to see 
    if it was successful or not, as it may fail if there already exists 
    the same key in the v_map or you specify a non-existent v_map 
    as the id of the map to add to. Both keys and values can be made up 
    of either integers or strings.
**/

if(is_string(key))
if(is_array(map))
{
    var length = array_height_2d(map);
    var free_place = -1;
    var map_i;
    //Проходим по всем элементам карты и ищем, есть ли уже такой ключ. 
    //Если есть, возращаем fail
    //Одновременно с этим ищем пустое место чтобы в случае чего поместить туда ключ-значение
    //и не искать по второму кругу и не проходить опять весь массив
    for(var i=0; i<length; i++)
    {
        map_i = map[i,0];
        //Если ключ отсутствует
        if(map_i == -1)
        {
            //Помечаем свободное место (если до этого еще не пометили)
            if(free_place == -1) free_place = i;
            //и уходим в следующий шаг цикла
            continue;
        }
        //иначе сверяем ключ
        else if(map_i == key) return false;
    }    
    //Одинакового не нашли, добавляем новые ключ-значение, если free_place > -1
    if(free_place>-1)
    {
         map[@ free_place,0] = key;
         map[@ free_place,1] = val;
         return true;        
    }  
}

return false;





