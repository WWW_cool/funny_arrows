/// @description scr_boss_update

if(instance_exists(global.inst_glb))
{
    with(global.inst_glb)
    {
        if(game_boss_next)exit;
        if(game_random){game_random = 0;exit;}
        
        var _current_index = 0;
        for(var i = 0; i < array_length_1d(game_boss_lock); i++)
        {
            var _lvl_ok = game_lvl_ok - game_boss_lvl_count*i;
            if(_lvl_ok/game_boss_lvl_count >= 0)
            {
                game_boss_lock[i] = 0;
            }
            if(game_lvl_ok >= game_boss_lvl_count*(i + 1))
            {
                _current_index = clamp(i + 1,0,game_boss_max);
            }
        }
        game_boss_index = _current_index;
    }
}

