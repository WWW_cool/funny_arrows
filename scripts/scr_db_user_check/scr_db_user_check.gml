/// @description scr_db_user_check

if(ds_exists(global.v_db_request_map,ds_type_map))
{
    if(instance_exists(global.inst_glb))
    {
        for(var i = 0; i < array_length_1d(global.inst_glb.glb_db_key_update); i += 1)
        {
            if(global.inst_glb.glb_db_key_update[i])
            {
                var tmp_var_str = ds_map_find_value( global.v_db_request_map,
                                                    global.inst_glb.glb_db_keys[i]);
                var tmp_var = real(tmp_var_str);
                //fed("try to update val - " + s(global.inst_glb.glb_db_keys[i]) + " real val -" + s(tmp_var));
                var updated = 0;
                if(is_real(tmp_var))
                {
                    if(tmp_var >= 0)
                    {
                        updated = 1;
                        switch(global.inst_glb.glb_db_keys[i])
                        {
                            case "lvl_ok":
                                if(tmp_var < global.user_lvl_ok)
                                {
                                    ds_map_replace( global.v_db_request_map,
                                                    global.inst_glb.glb_db_keys[i],
                                                    s(global.user_lvl_ok));
                                }
                            break;
                            default:
                                if(tmp_var != global.inst_glb.glb_db_key_val[i])
                                {
                                    ds_map_replace( global.v_db_request_map,
                                                    global.inst_glb.glb_db_keys[i],
                                                    s(global.inst_glb.glb_db_key_val[i]));
                                }
                        }
                    }
                }
                if(!updated)
                {
                    ds_map_replace( global.v_db_request_map,
                                    global.inst_glb.glb_db_keys[i],
                                    s(global.inst_glb.glb_db_key_val[i]));
                }
            }
        }
        if(global.user_need_reset)
        {
            global.user_need_reset = 0;
            ds_map_replace( global.v_db_request_map,
                            "lvl_ok",
                            s(0));
        }
    }
}

