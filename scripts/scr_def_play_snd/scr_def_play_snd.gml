/// @description scr_def_play_snd(snd)

if(argument_count == 1)
{
    var tmp_snd = argument[0];
    if(audio_exists(tmp_snd))
    {
        if(global._def_glb_var_audio_on)
        {
            var _snd = audio_play_sound(tmp_snd,1,0);
            var _gain = audio_sound_get_gain(_snd)*random_range(0.9,1.1);
            audio_sound_gain(_snd,_gain,0);
            return _snd;
        }
    }
}
if(argument_count == 2)
{
    var tmp_snd = argument[0];
    var tmp_type = argument[1];
    var need_play = false;
    if(audio_exists(tmp_snd))
    {
        if(tmp_type)
        {
            if(global._def_glb_var_audio_on)
            {
                need_play = true;
            }
        }
        else
        {
            if(global._def_glb_var_audio_on_bg)
            {
                need_play = true;
            }
        }
        if(need_play)
        {
            var _snd = audio_play_sound(tmp_snd,1,0);
            var _gain = audio_sound_get_gain(_snd)*random_range(0.9,1.1);
            audio_sound_gain(_snd,_gain,0);
            return _snd;
        }
    }
}

return noone;
