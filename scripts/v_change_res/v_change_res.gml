/// @description v_change_res()


if(!global._def_glb_var_default_resolution_pc)
{
    scr_def_view_update(800,600);
    global._def_glb_var_default_resolution_w = 800;
    global._def_glb_var_default_resolution_h = 600;
    global._def_glb_var_default_resolution_pc = 1;
    global._def_glb_game_name      = global._def_glb_game_name_PC;
}
else
{
    scr_def_view_update(405,720);
    global._def_glb_var_default_resolution_w = 405;
    global._def_glb_var_default_resolution_h = 720;
    global._def_glb_var_default_resolution_pc = 0;
    global._def_glb_game_name      = global._def_glb_game_name_Mobile;
}
room_restart();


