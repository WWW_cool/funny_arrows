/// @description scr_a_lock_check

if(a_next_scale == 1 && a_first_idle)
{
    a_first_idle = 0;
    for(var i = 0; i < instance_number(obj_a_parent); i++)
    {
        var _inst = instance_find(obj_a_parent,i);
        if(instance_exists(_inst))
        {
            if(_inst.a_type == EnCellTypes.LOCK)
            {
                var dist = point_distance(x,y,_inst.x,_inst.y);
                //fed("dist = " + s(dist));
                if(dist > 1.5*sprite_get_width(sprite_index) && dist < 2*sprite_get_width(sprite_index))
                {
                    a_lock = 1;
                    break;
                }
            }
        }
    }
}
