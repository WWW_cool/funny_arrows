/// @description scr_a_check_cell(inst)

var new_point = argument0;

var new_x = new_point.x;
var new_y = new_point.y;

var res = true;
var side_res = false;

var line_colour = EnColourTypes.EMPTY;

if(is_array(s_seq_items))
{
    /*if(instance_exists(s_seq_items[0]))
    {
        line_colour = s_seq_items[0].a_colour;
    }*/
    if(array_length_1d(s_seq_items) > 1)
    {
        for(var i = 0; i < s_seq_count; i += 1)
        {
            if(s_seq_items[i] == new_point)
            {
                s_seq_count = i + 1;
                scr_a_create_path();
                res = false;
                break;
            }
        }
    }
    else
    {
        if(new_point == s_seq_items[0])
        {
            res = false;
        }
    }
}

if(s_seq_count > 0 && res)
{
    if(instance_exists(s_seq_items[s_seq_count - 1]))
    {
        line_colour = scr_a_get_colour(s_seq_items[s_seq_count - 1]);
        for(var i = 0; i < 4; i += 1)
        {
            if(point_distance(s_seq_items[s_seq_count - 1].x + a_side*a_dx[i],
            s_seq_items[s_seq_count - 1].y + a_side*a_dy[i],new_x,new_y) < 10)
            {
                if(scr_a_check_colour(new_point,line_colour) 
                && s_seq_items[s_seq_count - 1].a_type != EnCellTypes.MULTI)
                {
                    side_res = false;
                }
                else
                {
                    side_res = false;
                    if(s_seq_items[s_seq_count - 1].a_dir == EnDirTypes.NONE)
                    {
                        var dir_from = scr_a_get_dir(new_point,true);
                        //fed("i = " + s(i) + " dir_from = " + s(dir_from))
                        if(dir_from == (i + 2)%4 or dir_from == -2)
                        {
                            side_res = true;
                        }
                    }
                    else
                    {
                        var dir_from = scr_a_get_dir(new_point,true); 
                        var dir_to = scr_a_get_dir(s_seq_items[s_seq_count - 1],false);
                        if(dir_from == -1 || dir_to == -1)
                        {
                            side_res = false;
                        }
                        else
                        {
                            //fed("i = " + s(i) + " dir_to = " + s(dir_to));
                            if(dir_to == i)
                            {
                                if(dir_from == -2)
                                {
                                    side_res = true;
                                }
                                else
                                {
                                    //fed("dir_to = " + s(dir_to) + " dir_from = " + s(dir_from));
                                    dir_from += 2;
                                    dir_from %= 4;
                                    if(dir_from == dir_to)
                                    {
                                        side_res = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if(!side_res)res = false;
}

//if(res)fed("true");

return res;




