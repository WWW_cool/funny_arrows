/// @description scr_def_background_set(ind)

var _def_tmp_back = argument0;
var _def_tmp_back_ind = 7; // take last index of build in backs;

if(background_exists(_def_tmp_back))
{
    __background_set( e__BG.Index, _def_tmp_back_ind, _def_tmp_back );
    
    __background_set( e__BG.HTiled, _def_tmp_back_ind, 0 );
    __background_set( e__BG.VTiled, _def_tmp_back_ind, 0 );
    
    // Place back on the mid of current view
    
    __background_set( e__BG.X, 0, __view_get( e__VW.XView, global._def_glb_var_view_current ) + 
    __view_get( e__VW.WView, global._def_glb_var_view_current )/2 
    - background_get_width(__background_get( e__BG.Index, _def_tmp_back_ind ))/2 );
    
    __background_set( e__BG.Y, 0, __view_get( e__VW.YView, global._def_glb_var_view_current ) + 
    __view_get( e__VW.HView, global._def_glb_var_view_current )/2 
    - background_get_height(__background_get( e__BG.Index, _def_tmp_back_ind ))/2 );
}


