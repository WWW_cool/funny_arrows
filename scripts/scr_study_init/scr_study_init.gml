/// @description scr_study_init(level,level_prev)

var _level = argument0;
var _level_prev = argument1;

//deinit

switch(_level_prev)
{
    case 1:
        // clear GUI
    break;
    case 2:
        // clear level
        scr_study_deinit();
    break;
}

//init

switch(_level)
{
    case 1:
        // init GUI
        scr_def_g_pause(id,"on");
        scr_def_g_pause_add(global._def_glb_var_pause_obj,global.a_gui_game);
        st_pause_init = 1;
    break;
    case 2:
        // init level
        fed("ini level #" + s(st_hint_number));
        st_hint_index = 1;
        if(st_hint_number >= 0)
        {
            if(!scr_study_init_level(st_hint_number,st_hint_index))
            {
                //no level to load
                fed("ERROR | fail to load level...");
                st_destroy = 1;
            }
            else
            {
                st_selector = instance_create(1,1,obj_selector);
                scr_study_init_obj(st_selector);
            }
        }
    break;
}





