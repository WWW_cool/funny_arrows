/// @description scr_a_draw_next_cell

if(a_next)
{
    var colour_spr = scr_a_get_col_spr(a_next_colour);
    
    switch(a_next_type)
    {
        case EnCellTypes.VORTEX:
            draw_sprite_ext(spr_cell_vortex_bg,0,x + a_next_delta_center - 1,y + a_next_delta_center - 1,
                a_next_scale,a_next_scale,0,c_white,1);
        break;
        case EnCellTypes.MULTI:
            draw_sprite_ext(spr_cell_multi,0,x + a_next_delta_center - 1,y + a_next_delta_center - 1,
                a_next_scale,a_next_scale,0,c_white,1);
        break;
        default:
            if(sprite_exists(colour_spr))
            {
                draw_sprite_ext(colour_spr,0,x + a_next_delta_center - 1,y + a_next_delta_center - 1,
                a_next_scale,a_next_scale,0,c_white,1);
            }
        break;
    }
}
