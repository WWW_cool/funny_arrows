/// @description scr_boss_get_amount([opt]index)
var _res = 1;
if(instance_exists(global.inst_glb))
{
    with(global.inst_glb)
    {
        var _index = game_boss_index;
        if(argument_count > 0)
        {
             _index = argument[0];
        }
        if(game_lvl_ok < game_boss_lvl_count*(1 + _index))
        {
            var _lvl_ok = clamp(game_lvl_ok - game_boss_lvl_count*_index,0,500);
            _res = _lvl_ok/game_boss_lvl_count;
        }
    }
}
return _res;


