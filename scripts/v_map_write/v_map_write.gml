/// @description v_map_write(map)
var map = argument[0];
var str = "&"; // символ начала карты
if(is_array(map))
{
    var length = array_height_2d(map);
    var map_i;
    for(var i=0; i<length; i++)
    {
        map_i = map[i,0];
        if(map_i != -1)
        {
            if(sprite_exists(map[i,1]) && v_map_check_name(map_i) != -1)
            {
                str += "|" + map[i,0] + "/" + sprite_get_name(map[i,1]);
            }
            else
            {
                str += "|" + map[i,0] + "/" + string(map[i,1]);
            }
        }
    }
}
str += "|^";// символ конца карты
return str;




