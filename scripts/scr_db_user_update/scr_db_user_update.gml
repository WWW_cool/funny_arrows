/// @description scr_db_user_update

if(ds_exists(global.v_db_request_map,ds_type_map))
{
    if(instance_exists(global.inst_glb))
    {
        var need_rewrite = 0;
        for(var i = 0; i < array_length_1d(global.inst_glb.glb_db_key_update); i += 1)
        { 
            var tmp_var_str = ds_map_find_value( global.v_db_request_map,
                                                global.inst_glb.glb_db_keys[i]);
            if(tmp_var_str == undefined)
            {
                continue;
            }
            var tmp_var = real(tmp_var_str);
            if(is_real(tmp_var))
            {
                if(tmp_var >= 0)
                {
                    switch(global.inst_glb.glb_db_keys[i])
                    {
                        case "all_ok":
                            if(global.user_open_all != tmp_var && 
                               tmp_var > global.user_open_all)
                            {
                                global.user_open_all = tmp_var;
                            }
                        break;
                        case "live_count":
                            if(global.inst_glb.live_count != tmp_var && 
                               tmp_var > global.inst_glb.live_count)
                            {
                                global.inst_glb.live_count = tmp_var;
                            }
                        break;
                        case "hint_fstep":
                            if(global.inst_glb.hint_fstep_count != tmp_var && 
                               tmp_var > global.inst_glb.hint_fstep_count)
                            {
                                global.inst_glb.hint_fstep_count = tmp_var;
                            }
                        break;
                        case "hint_back":
                            if(global.inst_glb.hint_moveback_count != tmp_var && 
                               tmp_var > global.inst_glb.hint_moveback_count)
                            {
                                global.inst_glb.hint_moveback_count = tmp_var;
                            }
                        break;
                    }
                }
            }

        }
        if(need_rewrite)
        {
            v_db_rewrite(global.inst_glb.game_user_id);
            //fed("need_rewrite");
        }
    }
}





