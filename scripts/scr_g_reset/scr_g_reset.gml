/// @description scr_g_reset

if(os_browser == browser_not_a_browser || game_user_id == "demo")
{
    ini_open("saves");
    if(ini_section_exists("game"))
    {
        ini_section_delete("game");
    }
    ini_close();
}
else
{
    ini_open("saves");
    if(ini_section_exists("game"))
    {
        ini_section_delete("game");
    }
    ini_close();
    //v_db_delete(global.inst_glb.game_user_id);
    scr_db_lvl_reset();
    //fed("delete user from db");
}

if(instance_exists(global.inst_glb))
{
    global.user_xp = 0;
    global.user_lvl_ok = 0;
    global.user_open_all = 0;
    global.user_lvl_damount = 0;
    global.user_lvl_amount = 0;
    
    with(global.inst_glb)
    {
        price_time = 0;
        game_lvl_ok = 0;
        game_boss_index = 0;
        live_count = live_count_max;
        game_points = 0;
        game_current_lvl_try = 0;
        hint_fstep_count = 3;
        hint_moveback_count = 4;
        for(var i = 0; i < array_length_1d(game_boss_lock); i += 1)
        {
            game_boss_lock[i] = 1;
        }
        scr_boss_update();
    }
}
scr_def_rm_goto_room(rm_Start);



