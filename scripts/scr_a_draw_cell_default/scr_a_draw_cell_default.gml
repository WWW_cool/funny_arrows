/// @description scr_a_draw_cell_default(spr_col,spr_dir)

var _col = argument0;
var _dir = argument1;

if(sprite_exists(_col))
{
    draw_sprite_ext(_col,0,x,y,a_next_scale,a_next_scale,0,c_white,1);
}
if(sprite_exists(_dir))
{
    draw_sprite_ext(_dir,0,x,y,a_next_scale,a_next_scale,90*a_angle,c_white,1);
}




