/// @description v_dd_cb_put
if(argument_count > 0)
{
    var tmp_dest = v_dd_get_id(argument[0]);
    var tmp_src_obj = global._dd_src_obj;
    if(instance_exists(tmp_src_obj))
    {
        var tmp_src = global._dd_src_id;//v_dd_find_src();
        if(tmp_dest != -1 && tmp_src != -1 && (tmp_dest != tmp_src || tmp_src_obj != id))
        {
            //var tmp_spr_out = _dd_spr_out[tmp_dest];
            //_dd_spr_out[tmp_dest] = tmp_src_obj._dd_spr_out[tmp_src];
            var old_dest_state = _dd_state[tmp_dest];
            global._dd_stop_put = 0;
            if(script_exists(_dd_cb_put[tmp_dest]))
            {
                script_execute(_dd_cb_put[tmp_dest],tmp_dest,tmp_src_obj,tmp_src,argument[0]);
            }
            
            if(!global._dd_stop_put)
            {
                if(tmp_src_obj._dd_type[tmp_src] == "source")
                {
                    tmp_src_obj._dd_state[tmp_src] = "full";
                }
                else
                {
                    
                    if(old_dest_state == "full")
                    {
                        //tmp_src_obj._dd_spr_out[tmp_src] = tmp_spr_out;
                        tmp_src_obj._dd_state[tmp_src] = "full";
                    }
                    else
                    {
                        tmp_src_obj._dd_state[tmp_src] = "empty";
                        //tmp_src_obj._dd_spr_out[tmp_src] = noone;
                    }  
                }
                _dd_state[tmp_dest] = "full";
            }
            else
            {
                tmp_src_obj._dd_state[tmp_src] = "full";
            }
        }
        else
        {
            if(tmp_src != -1)
            {
                tmp_src_obj._dd_state[tmp_src] = "full";
            }
        }
    }
    global._dd_src_obj = noone;
    global._dd_src_id = 1;
}

