/// @description scr_def_set_analytics(type)

enum AnalyticsType{gSTART = 0,gEND,gRoundEnd,gOpenStory};

var type = argument0;

var GAME_NAME = "CyberSafe";

var Player_id, time, url;
url = url_get_domain();

if(os_browser == browser_not_a_browser)exit;

fed("analytics event type = " + s(type));

switch(type)
{
    case AnalyticsType.gSTART:
        randomize();
        
        ini_open("saves.ini");
        Player_id = ini_read_real("Data", "Player_id", 0);
        if(Player_id == 0)
        {
            Player_id = irandom_range(1,10000000);
            ini_write_real("Data", "Player_id", Player_id);
        }
        ini_close();
        time = current_time/1000;
        analytics_event_ext(GAME_NAME,"URL",url, "Player_id", Player_id, "Game started. load time = ", time);
        break;
    case AnalyticsType.gEND:
        
        
        ini_open("saves.ini");
        Player_id = ini_read_real("Data", "Player_id", 0);
        ini_close();
        
        time = get_timer()/1000000;
        analytics_event_ext(GAME_NAME,"URL",url, "Player_id", Player_id, "Game end. In game time = ", time);
        
        break;
    case AnalyticsType.gRoundEnd:
        
        
        ini_open("saves.ini");
        Player_id = ini_read_real("Data", "Player_id", 0);
        ini_close();
        
        time = get_timer()/1000000;
        analytics_event_ext(GAME_NAME,"URL",url, "Player_id", Player_id, "Round end. In game time = ", time);
        
        break;
    case AnalyticsType.gOpenStory:

        ini_open("saves.ini");
        Player_id = ini_read_real("Data", "Player_id", 0);
        ini_close();
        
        time = get_timer()/1000000;
        analytics_event_ext(GAME_NAME,"URL",url, "Player_id", Player_id, "Story unlock. In game time = ", time);
        
        break;
    default:
        fed("Unknown analytics type");
    break;
}




