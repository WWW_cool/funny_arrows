/// @description v_tbl_show(tbl,line_max, column_max, name)

var i,k;
var tmp_tbl         = argument0;
var line_max        = argument1;
var column_max      = argument2;
var tmp_name        = argument3;

for(i = 0; i < line_max; i += 1)
{
    for(k = 0; k < column_max; k += 1)
    {
        fed(" | " + tmp_name + " | " + string(i) + ":" 
        + string(k) + " " + string(tmp_tbl[i,k]));
    }
}
