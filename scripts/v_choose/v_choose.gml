/// @description v_choose(res[],x_1,y_1,x_2,y_2)

var res = argument0;
var x_1 = argument1;
var y_1 = argument2;
var x_2 = argument3;
var y_2 = argument4;

var condition = global._def_glb_var_default_resolution_pc;

if(condition)
{
    res[@ EnPoint.X] = x_1;
    res[@ EnPoint.Y] = y_1;
}
else
{
    res[@ EnPoint.X] = x_2;
    res[@ EnPoint.Y] = y_2;
}

