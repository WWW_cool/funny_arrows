/// @description v_map_destroy(map)
var _v_init_map = argument[0];

/**
        v_maps take up space in memory, 
        which is allocated to them when they are created. 
        This means that you also need to free this memory 
        when the v_map is not needed to prevent errors, 
        memory leaks and loss of performance when running your game. 
        This function does just that. 
**/


if(is_array(_v_init_map))
{
    _v_init_map = 0;
}

//!!!
//При удалении карты обязательно делать так:  map_name = v_map_destroy(map_name)

return 0;

//TODO:
//Пока ничего лучше не придумал - с помощью @ можно воздействовать внутри скрипта 
//только на элементы массива, удалить массив по ссылке нельзя, или можно, но хз как

