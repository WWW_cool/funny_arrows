/// @description scr_localization_get(key, default)

var key = argument0;
var default_str = argument1;

var localization_str = "";

if(is_array(global.localization_map))
{
    localization_str = v_map_find_value(global.localization_map,key);
    
    if(localization_str != undefined)
    {
        return localization_str;
    }
    else
    {
        return default_str;
    }
}
else
{
    return default_str;
}

