/// @description scr_hint_show_on_path(path_id)

var path_id = argument0;

for(var i = 0; i < path_get_number(path_id); i++)
{
    var _x = path_get_point_x(path_id,i);
    var _y = path_get_point_y(path_id,i);
    var _inst = collision_point(_x,_y,obj_a_parent,false,true);
    if(instance_exists(_inst))
    {
        _inst.depth = -10;
    }
}
