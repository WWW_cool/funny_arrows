/// @description scr_a_anim_reset(res[])

var _anim_res = argument0;
var res = 0;
if(_anim_res[0])
{ 
    res = 1;
    anim_l_state = 0;
    anim_l_alpha = 1;
    anim_l_dx = anim_l_dx_init;
    anim_l_dy = anim_l_dy_init;
}

return res;
