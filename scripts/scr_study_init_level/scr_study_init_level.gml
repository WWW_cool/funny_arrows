/// @description scr_study_init_level(type,index)

var _type = argument0;
var _index = argument1;
var _res = false;

fed("_type = " + s(_type) + " _index = " + s(_index));

if(_type >= 0  && _index > 0)
{
    switch(_type)
    {
        case 0:
            _res = _func_study_simple(_index);
        break;
        case 1:
            _res = _func_study_arrow(_index);
        break;
        case 2:
            _res = _func_study_childs(_index);
        break;
        
        case 3:
            _res = _func_study_shield(_index);
        break;
        
        case 4:
            _res = _func_study_lift(_index);
        break;
        
        case 5:
            _res = _func_study_double(_index);
        break;
        case 6:
            _res = _func_study_lock(_index);
        break;
        case 7:
            _res = _func_study_time(_index);
        break;
        case 8:
            _res = _func_study_vortex(_index);
        break;
        case 9:
            _res = _func_study_multi(_index);
        break;
    }    
}

return _res;    
    


