/// @description scr_level_save(isfunc)

var _isfunc = 0;
if(argument_count > 0)
{
    _isfunc = argument[0];
}

var list = global.lvlObjList;
if(!_isfunc)
{
    ini_open(global._def_glb_game_name + "Levels");
}
else
{
    if(instance_exists(global.lvlEdit))
    {
        ini_open(global.lvlEdit.use_file_name);
    }
    else
    {
        fed("ERROR | lvlEdit not found");
        exit;
    }
}
ini_section_delete(lvl_name);
/***************************************************
  обязательные объекты в комнате и их свойства
 ***************************************************/
 if(instance_exists(global.inst_glb))
    {
        //global.inst_glb.game_lvl_to_load = real(lvl_name);
        var tmp_lvl_num = global.inst_glb.game_lvl_to_load;
        if(tmp_lvl_num > 0)
        {
            lvl_name = s(tmp_lvl_num);
            tmp_lvl_num -= 1;
            fed(s(tmp_lvl_num));
            //ini_write_real(lvl_name,"Attempts",global.inst_glb.game_attempts[tmp_lvl_num]);
            //fed(s(global.inst_glb.game_attempts[tmp_lvl_num]));
        }
    }
/***************************************************
   Созданные объекты и их свойства
 ***************************************************/
if(ds_exists(list,ds_type_list))
{
    for(var i = 0; i < ds_list_size(list); i += 1)
    {
        if(is_real(list[|i]))
        {
            if(instance_exists(list[|i]))
            {
                with(list[|i])
                {
                    ini_write_string(other.lvl_name,"type" + string(i),object_get_name(scope_obj_type));
                    switch(scope_obj_type)
                    {
                        case obj_a_parent:
                            for(var k = 0; k < array_height_2d(scope_vars); k += 1)
                            {
                                switch(scope_vars[k,0])
                                {
                                    case "y":
                                    case "x":
                                    case "Layer":
                                    case "GRID":
                                    case "Type":
                                    case "Colour":
                                    case "Dir":
                                    case "Angle":
                                    case "Shield":
                                    case "Unlock":
                                    case "Double":
                                    case "First":
                                    case "Child_IS":
                                    case "Child_Type":
                                    case "Child_Colour":
                                    case "Child_Dir":
                                    case "Child_Angle":
                                    case "Child_Shield":
                                    case "Child_Unlock":
                                    case "Child_Double":
                                        ini_write_real(other.lvl_name,scope_vars[k,0] + string(i),scope_vars[k,1]);
                                    break;
                                }
                            }
                        break;
                        case obj_a_mover:
                            for(var k = 0; k < array_height_2d(scope_vars); k += 1)
                            {
                                switch(scope_vars[k,0])
                                {
                                    case "y":
                                    case "x":
                                    case "Layer":
                                    case "GRID":
                                    case "Angle":
                                        ini_write_real(other.lvl_name,scope_vars[k,0] + string(i),scope_vars[k,1]);
                                    break;
                                }
                            }
                        break;
                        default:
                            show_error("wrong type", false);
                    }
                }
            }
        }
    }
}
ini_close();
show_debug_message("save level...");




