/// @description v_map_copy(map_dest, map_src)
var map_dest    = argument[0];//Куда
var map_src     = argument[1];//Откуда
/**
    You can use this function to copy the contents of one map 
    into another one that you have previously created using 
    v_map_create(). If the v_map that is being copied 
    to is not empty, then this function will clear it first before copying. 
    The original v_map remains un-changed by this process. 
**/

if(is_array(map_src)&&is_array(map_dest))
{
    var i;
    var length = array_height_2d(map_dest);
    for(i=0; i<length; i++)
    {
        map_dest[@ i,0] = map_src[i,0];
        map_dest[@ i,1] = map_src[i,1];
    }
    return true;
}

return false;



