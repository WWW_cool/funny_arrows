/// @description scr_level_init

var list = global.lvlObjList;

if(ds_exists(list,ds_type_list))
{
    for(var i = 0; i < ds_list_size(list); i += 1)
    {
        if(is_real(list[|i]))
        {
            if(instance_exists(list[|i]))
            {
                with(list[|i])
                {
                    instance_destroy();
                }
            }
        }
    }
    ds_list_clear(list);
}
else
{
    list = ds_list_create();
}

global.lvlObjList = list;
