/// @description scr_a_move_add_mover(inst)

var _inst = argument0;

with(global.a_selector)
{
    
    for(var i = 0; i < m_move_mover_count; i++)
    {
        if(m_move_mover_inst[i] == _inst)exit;
    }
    
    m_move_mover_inst[m_move_mover_count]              = _inst;
    m_move_mover_start_move[m_move_mover_count]        = a_move_index;
    
    m_move_mover_xpos[m_move_mover_count,a_move_index] = _inst.x;
    m_move_mover_ypos[m_move_mover_count,a_move_index] = _inst.y;
    m_move_mover_angle[m_move_mover_count,a_move_index] = _inst.image_angle;
    
    if(_inst.m_anim_state == EnMoverStates.CREATE)
    {
        m_move_mover_state[m_move_mover_count,a_move_index]      = EnMoverStates.IDLE
    }
    else
    {
        m_move_mover_state[m_move_mover_count,a_move_index]      = _inst.m_anim_state;
    }
    m_move_mover_move_back[m_move_mover_count,a_move_index]      = _inst.m_move_back;
    m_move_mover_pos[m_move_mover_count,a_move_index]            = _inst.m_pos;
    
    m_move_mover_deleted[m_move_mover_count] = -1;
    _inst.m_move_id = m_move_mover_count;
    //fed("id = " + s(_inst.id));
    m_move_mover_count += 1;
    //fed("m_move_mover_count = " + s(m_move_mover_count));
}



