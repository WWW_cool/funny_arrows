/// @description cb_gui_reverse

if(global.a_hint_reverse)
{
    global.a_hint_reverse = 0;
    for(var i = 0; i < instance_number(obj_a_parent); i++)
    {
        var _inst = instance_find(obj_a_parent,i);
        if(instance_exists(_inst))
        {
            if(_inst.a_state == EnCellStates.DESTROY)continue;
            
            if(_inst.a_next)
            {
                _inst.a_anim_state = EnCellAnimTypes.LIFT;
                _inst.amin_reverse_dir = 1;
            }
        }
    }
}
else
{
    global.a_hint_reverse = 1;
    for(var i = 0; i < instance_number(obj_a_parent); i++)
    {
        var _inst = instance_find(obj_a_parent,i);
        if(instance_exists(_inst))
        {
            if(_inst.a_state == EnCellStates.DESTROY)continue;
            
            if(_inst.a_next)
            {
                _inst.a_anim_state = EnCellAnimTypes.LIFT;
                _inst.a_state = EnCellStates.ANIM;
                _inst.amin_reverse_dir = 0;
            }
            
        }
    }
}

if(global.a_hint_first_turn)
{
    global.a_hint_first_turn = 0;
}



