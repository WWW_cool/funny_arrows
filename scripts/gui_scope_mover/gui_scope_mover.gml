/// @description gui_scope_mover(inst);
/// @param inst

scope_inst = argument0;

/*
"x"
"y"
"Layer"
*/

gui_create_menu(guid, "Selected object", true, array_height_2d(scope_inst.scope_vars), c_black);
with(scope_inst)
{
    for(var i = 0; i < array_height_2d(scope_vars); i += 1)
    {
        switch(scope_vars[i,0])
        {
            case "x":
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 0, __view_get( e__VW.WView, 0 ));
            break;
            case "y":
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 0, __view_get( e__VW.HView, 0 ));
            break;
            case "Layer":
                gui_create_watcher(other.guid,"Object " + scope_vars[i,0], scope_vars[i,1], $d6a12f);
            break;
            case "Angle":
                gui_create_slider(other.guid, scope_vars[i,0], scope_vars[i,1], 0, 3);
            break;
        }
    }
}





