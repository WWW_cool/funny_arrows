/// @description v_tbl_get(tbl, line, column)

var tmp_tbl     = argument0;
var tbl_line    = argument1;
var tbl_column  = argument2;

return tmp_tbl[tbl_line,tbl_column];


