/// @description v_pack_array(x0,x1,x2,x3 .. x15)
/**
    Запаковывает аргументы в массив. Только для числа аргументов меньше 17
    
    Example:
    array = v_pack_array(5,5,7,8,10);
**/
if(argument_count > 0)
{
    var array;
    
    for(var i=0; i<argument_count; i++)
    {
        array[i] = argument[i];
    }
    
    return array;
}
else return 0;

