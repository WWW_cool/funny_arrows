/// @description scr_a_move_tile_destroy(inst)

var _inst = argument0;
if(instance_exists(global.a_selector))
{
    with(global.a_selector)
    {
        for(var i = 0; i < a_move_tile_count; i++)
        {
            if(a_move_tile_inst[i] == _inst)
            {
                a_move_tile_deleted[i] = a_move_index;
            }
        }
    }
}
