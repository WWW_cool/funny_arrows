/// @description scr_EnCellTypes_get_name(enum)

var _type = argument0;
var _res = "";

switch(_type)
{
    case EnCellTypes.NONE:
        var _res = "NONE";
    break;
    case EnCellTypes.SHIELD:
        var _res = "SHIELD";
    break;
    case EnCellTypes.LIFT:
        var _res = "LIFT";
    break;
    case EnCellTypes.TIME:
        var _res = "TIME";
    break;
    case EnCellTypes.DOUBLE:
        var _res = "DOUBLE";
    break;
    case EnCellTypes.VORTEX:
        var _res = "VORTEX";
    break;
    case EnCellTypes.MULTI:
        var _res = "MULTI";
    break;
    case EnCellTypes.LOCK:
        var _res = "LOCK";
    break;
}

return _res;

