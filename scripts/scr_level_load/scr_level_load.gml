/// @description scr_level_load()
gui_scope_clear();

var level_count = scr_level_get_count();

if(level_count > 0)
{
    if(level_count > 1)
    {
        gui_create_slider(guid, "Level selector", 1, 1, level_count,c_yellow);
        gui_create_button(guid, "Load selected level",
                scr_level_load_selected,0, c_yellow);
    }
    else
    {
        gui_create_button(guid, "Load level " + string(level_count),
                scr_level_load_selected,level_count, c_yellow);
    }
}







