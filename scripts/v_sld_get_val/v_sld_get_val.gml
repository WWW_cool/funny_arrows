/// @description v_sld_get_val(v_sld_id)
var tmp_id = argument0;

var tmp_m_x = mouse_x;
var res = 0;

if(tmp_m_x > v_sld_xpos[tmp_id] + v_sld_x[tmp_id] + v_sld_width[tmp_id])
{
    res = 1;
}
else
{
    if(tmp_m_x < v_sld_xpos[tmp_id] + v_sld_x[tmp_id])
    {
        res = 0;
    }
    else
    {
        tmp_m_x -= v_sld_xpos[tmp_id] + v_sld_x[tmp_id];
        res = tmp_m_x/v_sld_width[tmp_id];
    }
}

return res;



