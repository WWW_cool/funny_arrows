/// @description scr_a_create_path()

if(is_array(s_seq_items))
{
    if(array_length_1d(s_seq_items) > 1)
    {
        for(var i = 0; i < instance_number(obj_a_parent); i += 1)
        {
            var inst = instance_find(obj_a_parent,i);
            if(instance_exists(inst))
            {
                inst.a_state = EnCellStates.IDLE;
                inst.a_inst_prev = noone;
                inst.a_inst_next = noone;
                scr_a_reset(inst);
            }
        }
        s_seq_items[0].a_state = EnCellStates.SELECTED;
        for(var i = 1; i < s_seq_count; i += 1)
        {
            if(instance_exists(s_seq_items[i]))
            {
                if(instance_exists(s_seq_items[i - 1]))
                {
                    s_seq_items[i - 1].a_inst_next = s_seq_items[i];
                    s_seq_items[i].a_inst_prev = s_seq_items[i - 1];
                    
                    s_seq_items[i].a_state = EnCellStates.SELECTED;
                    scr_a_check_colour(s_seq_items[i],scr_a_get_colour(s_seq_items[i - 1]));
                }
            }
        }
    }
}



