/// @description scr_level_export

var level_count = scr_level_get_count();

var lvl_shift = get_integer("Enter level index from", 0);

if(level_count > 0)
{
    if(level_count > 1)
    {
        for(var i = 0; i < level_count + 1; i += 1)
        {
            scr_export_lvl(i,lvl_shift);
        }
    }
    else
    {
        scr_export_lvl(0,lvl_shift);
    }
}



