/// @description v_btns_move(btn_id,delta_x,delta_y)

var btn_id = argument0;
var _dx = argument1;
var _dy = argument2;

var _x = v_btns_get_param(btn_id,"xpos");
var _y = v_btns_get_param(btn_id,"ypos");

v_btns_set_param(btn_id,"xpos",_x + _dx);
v_btns_set_param(btn_id,"ypos",_y + _dy);
