/// @description scr_event_list_add(type)

var _type = argument0;

var _index = ds_list_find_index(a_event_list,a_event_inst[_type]);
if(_index == -1)
{
    ds_list_add(a_event_list,a_event_inst[_type]);
    v_btns_set_param(a_event_inst[_type],"active",1);
    v_btns_set_param(a_event_inst[_type],"ypos",a_event_y_init);
}

