/// @description gui_scope_step
if(instance_exists(global.lvlEdit))
{
    var press_rd = 25;

    switch(scope_obj_type)
    {
        case obj_a_parent:
            gui_scope_step_tile();
        break;
        case obj_a_mover:
            gui_scope_step_mover();
        break;
        default:
            show_error("wrong type", false);
    }
    
    if(mouse_check_button_pressed(mb_left))
    {
        
        if(scope_vars[1,1] == global.lvlEdit.layer)
        {
            if(mouse_x > x - press_rd && mouse_x < x + press_rd
             && mouse_y > y - press_rd && mouse_y < y + press_rd)
             {
                scope_dif_x = x - mouse_x;
                scope_dif_y = y - mouse_y;
                with(global.lvlEdit)
                {
                    gui_scope_init(other.id);
                }
                scope_drag = 1;
             }
         }
         scope_create = 0;
    }
    if(mouse_check_button_released(mb_left))
    {
        scope_drag = 0;
    }

    if(scope_drag || scope_create)
    {
        for(var i = 0; i < array_height_2d(scope_vars); i += 1)
        {
            switch(scope_vars[i,0])
            {
                case "x":
                    scope_vars[i,1] = ((mouse_x + scope_dif_x) div scope_vars[0,1])*scope_vars[0,1];
                break;
                case "y":
                    scope_vars[i,1] = ((mouse_y + scope_dif_y) div scope_vars[0,1])*scope_vars[0,1];
                break;
            }
        }
    }
}




