/// @description scr_boss_get_spr(type)

var _type = argument0;
var res = noone;

switch(_type)
{
    case EnBossType.Monster_1:
        res = spr_monster_1;
    break;
    case EnBossType.Monster_2:
        res = spr_monster_2;
    break;
    case EnBossType.Monster_3:
        res = spr_monster_3;
    break;
    case EnBossType.Monster_4:
        res = spr_monster_4;
    break;
    case EnBossType.Monster_5:
        res = spr_monster_5;
    break;
    case EnBossType.Monster_6:
        res = spr_monster_6;
    break;
    default:
}

return res;
