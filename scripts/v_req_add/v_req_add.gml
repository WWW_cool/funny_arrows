/// @description v_req_add(req)

var req = argument0;
//fed("add request to queue - " + ds_map_write(req));

if(instance_exists(global.inst_glb))
{
    with(global.inst_glb)
    {  
        if(ds_exists(glb_db_req_list, ds_type_list))
        {
            ds_list_add(glb_db_req_list, req);
        }
    }
}


