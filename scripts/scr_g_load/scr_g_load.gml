/// @description scr_g_load()
//fed("load game");
if(instance_exists(global.inst_glb))
{
    with(global.inst_glb)
    {
        if(os_browser == browser_not_a_browser || game_user_id == "demo")
        {
            ini_open("saves");
                game_lvl_ok             = ini_read_real("game","lvl_ok",0);
                hint_fstep_count        = ini_read_real("game","hint_fstep",hint_fstep_count);
                hint_moveback_count     = ini_read_real("game","hint_back",hint_moveback_count);
                live_time               = ini_read_real("game","live_time",0);
                live_count              = ini_read_real("game","live_count",live_count_max);
                var _now_time           = date_get_second_of_year(date_current_datetime());
                var _last_time          = ini_read_real("game","game_time",_now_time);
                price_time              = ini_read_real("game","price_time",0);
                game_points             = ini_read_real("game","game_points",0);
                game_current_lvl_try    = ini_read_real("game","game_lvl_try",0);
                global._def_glb_var_audio_on    = ini_read_real("system","sound",1);
                global._def_glb_var_audio_on_bg = ini_read_real("system","music",1);
            ini_close();
            if(game_lvl_ok >= 0)
            {
                global.user_lvl_ok = game_lvl_ok;
                scr_boss_update();
            }
        }
        else
        {
            /*if(ds_exists(global.v_db_request_map,ds_type_map))
            {
                var lvl_ok = ds_map_find_value(global.v_db_request_map,"lvl_ok");
                if(lvl_ok != undefined)
                {
                    var ok_count = real(lvl_ok);
                    if(is_real(ok_count))
                    {
                        if(ok_count >= 0)
                        {
                            fed("load lvl ok - " + s(ok_count));
                            game_lvl_ok = ok_count;
                            
                        }
                    }
                }
                else
                {
                    ini_open("saves");
                        game_lvl_ok = ini_read_real("game","lvl_ok",0);
                    ini_close();
                }
            }*/
            ini_open("saves");
                game_lvl_ok             = ini_read_real("game","lvl_ok",0);
                hint_fstep_count        = ini_read_real("game","hint_fstep",hint_fstep_count);
                hint_moveback_count     = ini_read_real("game","hint_back",hint_moveback_count);
                live_time               = ini_read_real("game","live_time",0);
                live_count              = ini_read_real("game","live_count",live_count_max);
                var _now_time           = date_get_second_of_year(date_current_datetime());
                var _last_time          = ini_read_real("game","game_time",_now_time);
                price_time              = ini_read_real("game","price_time",0);
                game_points             = ini_read_real("game","game_points",0);
                game_current_lvl_try    = ini_read_real("game","game_lvl_try",0);
                global._def_glb_var_audio_on    = ini_read_real("system","sound",1);
                global._def_glb_var_audio_on_bg = ini_read_real("system","music",1);
            ini_close();
            hint_fstep_count    = scr_db_load_param(global.v_db_request_map,EnDbKeys.HINT_FSTEP,hint_fstep_count);
            hint_moveback_count = scr_db_load_param(global.v_db_request_map,EnDbKeys.HINT_BACK,hint_moveback_count);
            live_count          = scr_db_load_param(global.v_db_request_map,EnDbKeys.LIVE_COUNT,live_count);
            live_time           = scr_db_load_param(global.v_db_request_map,EnDbKeys.LIVE_TIME,live_time);
            game_time           = scr_db_load_param(global.v_db_request_map,EnDbKeys.GAME_TIME,_last_time);
            game_points         = scr_db_load_param(global.v_db_request_map,EnDbKeys.GAME_POINTS,game_points);
            game_current_lvl_try= scr_db_load_param(global.v_db_request_map,EnDbKeys.GAME_LVL_TRY,game_current_lvl_try);
            if(game_time >= _last_time)
            {
                game_lvl_ok     = scr_db_load_param(global.v_db_request_map,EnDbKeys.LVL_OK,game_lvl_ok);
            }
            global.user_lvl_ok = game_lvl_ok;
            scr_boss_update();
        }
        
        if(_now_time > _last_time)
        {
            var _span = _now_time - _last_time;
            if(live_count < live_count_max)
            {
                live_time += _span mod live_time_need;
                live_count += _span div live_time_need;
                live_count = clamp(live_count,0,live_count_max);
            }
            if(price_time > 0)
            {
                price_time = clamp(price_time - _span,0,price_time_need);
            }
        }
        scr_audio_cb(1);
        if(room == rm_Start && game_lvl_ok == 0)
        {
            scr_win_next();
        }
    }
}

