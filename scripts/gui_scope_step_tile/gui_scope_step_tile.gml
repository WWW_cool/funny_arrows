

if(is_array(scope_vars))
{
    for(var i = 0; i < array_height_2d(scope_vars); i += 1)
    {
        switch(scope_vars[i,0])
        {
            case "x":
                x = (scope_vars[i,1] div scope_vars[0,1])*scope_vars[0,1];
                scope_vars[i,1] = x;
            break;
            case "y":
                y = (scope_vars[i,1] div scope_vars[0,1])*scope_vars[0,1];
                scope_vars[i,1] = y;
            break;
            case "Type":
                a_type      = scope_vars[i,1];
            break;
            case "Dir":
                a_dir       = scope_vars[i,1];
            break;
            case "Angle":
                a_angle     = scope_vars[i,1];
            break;
            case "Colour":
                a_colour    = scope_vars[i,1];
            break;
            case "Shield":
                a_p_shield  = scope_vars[i,1];
            break;
            case "Unlock":
                a_p_turn_count = scope_vars[i,1];
            break;
            case "Double":
                a_p_double_colour = scope_vars[i,1];
            break;
            case "First":
                a_p_first_turn = scope_vars[i,1];
            break;
            case "Child_IS":
                a_next      = scope_vars[i,1];
            break;
            case "Child_Type":
                a_next_type = scope_vars[i,1];
            break;
            case "Child_Dir":
                a_next_dir  = scope_vars[i,1];
            break;
            case "Child_Angle":
                a_next_angle  = scope_vars[i,1];
            break;
            case "Child_Colour":
                a_next_colour  = scope_vars[i,1];
            break;
            case "Child_Shield":
                a_next_p_shield = scope_vars[i,1];
            break;
            case "Child_Unlock":
                a_next_p_turn_count = scope_vars[i,1];
            break;
            case "Child_Double":
                a_next_p_double_colour = scope_vars[i,1];
            break;
        }
    }
}

