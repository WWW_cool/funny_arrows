/// @description  gui_get_value(gui_array,name, default);
/// @param gui_array
/// @param name
/// @param  default

var n = ds_list_size(argument0);

var i,item;
for (i = 1; i < n; ++i) {
    item = argument0[| i];
    if item[0] = argument1 return item[2];
}

return argument2;

