/// @description scr_def_show_debug_message(str, level)

var _def_tmp_string = argument0;
var _def_tmp_debug_level = argument1;

switch(_def_tmp_debug_level)
{
    case "info":
        if(EnMSGType.enINFO) // MACRO
        {
            fed(" | INFO | " + _def_tmp_string);
        }
    break;
    case "debug":
        if(EnMSGType.enDEBUG) // MACRO
        {
            fed(" | DEBUG | " + _def_tmp_string);
        }
    break;
    case "warning":
        if(EnMSGType.enWARNING) // MACRO
        {
            fed(" | WARNING | " + _def_tmp_string);
        }
    break;
    case "error":
        if(EnMSGType.enERROR) // MACRO
        {
            fed(" | ERROR | " + _def_tmp_string);
        }
    case "view":
        if(EnMSGType.enVIEW)
        {
            fed(" | VIEW | " + _def_tmp_string);
        }    
    break;
    case "pause":
        if(EnMSGType.enPAUSE)
        {
            fed(" | PAUSE | " + _def_tmp_string);
        }    
    break;
}




