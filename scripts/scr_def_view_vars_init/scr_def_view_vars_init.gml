/// @description scr_def_view_vars_init()

global._def_glb_var_view_xview = 0;
global._def_glb_var_view_yview = 0;
global._def_glb_var_view_scaler = 1;

var _def_tmp_small_res = 0;

if(global._def_glb_var_g_resolution_w < global._def_glb_var_default_resolution_w ||
global._def_glb_var_g_resolution_h < global._def_glb_var_default_resolution_h)
{
    _def_tmp_small_res = 1; 
    scr_def_show_debug_message("small res detected","view");   
}

    if(global._def_glb_var_view_fullscale)
    {
        global._def_glb_var_view_wview = global._def_glb_var_default_resolution_w;
        global._def_glb_var_view_hview = global._def_glb_var_default_resolution_h;
        global._def_glb_var_view_wport = global._def_glb_var_g_resolution_w;
        global._def_glb_var_view_hport = global._def_glb_var_g_resolution_h;
    }
    else
    {
        var _def_tmp_wdiv = global._def_glb_var_default_resolution_w/
        global._def_glb_var_g_resolution_w;
        var _def_tmp_hdiv = global._def_glb_var_default_resolution_h/
        global._def_glb_var_g_resolution_h;
        
        var _def_tmp_div_res = 0;
        if(_def_tmp_wdiv == _def_tmp_hdiv) // 750/500  550/ 400
        {
            _def_tmp_div_res = 2;
        }
        else
        {
            if(_def_tmp_wdiv > _def_tmp_hdiv)
            {
                _def_tmp_div_res = 1;
            }    
        }  
        
        if(_def_tmp_small_res)
        {
            _def_tmp_wdiv = 1/_def_tmp_wdiv;
            _def_tmp_hdiv = 1/_def_tmp_hdiv;
            switch(_def_tmp_div_res)
            {
                case 1:
                    global._def_glb_var_view_wview = global._def_glb_var_default_resolution_w;
                    global._def_glb_var_view_hview = global._def_glb_var_g_resolution_h
                    /_def_tmp_wdiv;
                    global._def_glb_var_view_wport = global._def_glb_var_g_resolution_w;
                    global._def_glb_var_view_hport = global._def_glb_var_g_resolution_h;
                    
                    global._def_glb_var_view_yview = 0.5*
                    (global._def_glb_var_default_resolution_h - global._def_glb_var_view_hview);
                    if(global._def_glb_var_view_to_bottom)
                    {
                        global._def_glb_var_view_yview = 
                        (global._def_glb_var_default_resolution_h - global._def_glb_var_view_hview);
                    }
                break;
                
                case 0:
                    global._def_glb_var_view_wview = global._def_glb_var_g_resolution_w
                    /_def_tmp_hdiv;
                    global._def_glb_var_view_hview = global._def_glb_var_default_resolution_h;
                    
                    global._def_glb_var_view_wport = global._def_glb_var_g_resolution_w;
                    global._def_glb_var_view_hport = global._def_glb_var_g_resolution_h;
                    global._def_glb_var_view_xview = 0.5*
                    (global._def_glb_var_default_resolution_w - global._def_glb_var_view_wview);
                break;
                case 2:
                    global._def_glb_var_view_wview = global._def_glb_var_default_resolution_w;
                    global._def_glb_var_view_hview = global._def_glb_var_default_resolution_h;
                    global._def_glb_var_view_wport = global._def_glb_var_g_resolution_w;
                    global._def_glb_var_view_hport = global._def_glb_var_g_resolution_h;
                    global._def_glb_var_view_scaler = 1/_def_tmp_hdiv;
                break;
            }
        } 
        else
        {
            switch(_def_tmp_div_res)
            {
                case 1:
                    global._def_glb_var_view_wview = global._def_glb_var_default_resolution_w;
                    global._def_glb_var_view_hview = global._def_glb_var_g_resolution_h*_def_tmp_wdiv;
                    
                    global._def_glb_var_view_wport = global._def_glb_var_g_resolution_w;
                    global._def_glb_var_view_hport = global._def_glb_var_g_resolution_h;
                    
                    global._def_glb_var_view_yview = 0.5*
                    (global._def_glb_var_default_resolution_h - global._def_glb_var_view_hview);
                    if(global._def_glb_var_view_to_bottom)
                    {
                        global._def_glb_var_view_yview = 
                        (global._def_glb_var_default_resolution_h - global._def_glb_var_view_hview);
                    }
                break;
                
                case 0:
                    global._def_glb_var_view_wview = global._def_glb_var_g_resolution_w*_def_tmp_hdiv;
                    global._def_glb_var_view_hview = global._def_glb_var_default_resolution_h;
                    
                    global._def_glb_var_view_wport = global._def_glb_var_g_resolution_w;
                    global._def_glb_var_view_hport = global._def_glb_var_g_resolution_h;
                    global._def_glb_var_view_xview = 0.5*
                    (global._def_glb_var_default_resolution_w - global._def_glb_var_view_wview);
                break;
                case 2:
                    global._def_glb_var_view_wview = global._def_glb_var_default_resolution_w;
                    global._def_glb_var_view_hview = global._def_glb_var_default_resolution_h;
                    global._def_glb_var_view_wport = global._def_glb_var_g_resolution_w;
                    global._def_glb_var_view_hport = global._def_glb_var_g_resolution_h;
                    global._def_glb_var_view_scaler = 1/_def_tmp_hdiv;
                break;
            }
        }    
        
        
        scr_def_show_debug_message("_def_view divres = " + string(_def_tmp_div_res),"view");
        scr_def_show_debug_message("_def_view wdiv = " + string(_def_tmp_wdiv),"view");
        scr_def_show_debug_message("_def_view hdiv = " + string(_def_tmp_hdiv),"view");
    }
    
    scr_def_show_debug_message("_def_view x = " + string(global._def_glb_var_view_xview),"view");
    scr_def_show_debug_message("_def_view y = " + string(global._def_glb_var_view_yview),"view");
    
    scr_def_show_debug_message("_def_view wres = " + string(global._def_glb_var_view_wview),"view");
    scr_def_show_debug_message("_def_view hres = " + string(global._def_glb_var_view_hview),"view");
    
    scr_def_show_debug_message("_def_view wport = " + string(global._def_glb_var_view_wport),"view");
    scr_def_show_debug_message("_def_view hport = " + string(global._def_glb_var_view_hport),"view");
    
    ////////////////WINDOW SETTINGS /////////////////////////////
    global._def_glb_var_view_wwin = global._def_glb_var_view_wport;
    global._def_glb_var_view_hwin = global._def_glb_var_view_hport;
    /////////////////////////////////////////////////////////////

