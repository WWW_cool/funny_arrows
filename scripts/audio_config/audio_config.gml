//Audio Lists
/*
volume[0,0] = snd_def_main_back;
volume[1,0] = snd_def_menu_click;
volume[2,0] = snd_HERO_at_bomb;
volume[3,0] = snd_HERO_at_bomb_child;
volume[4,0] = snd_HERO_at_dagger;
volume[5,0] = snd_HERO_at_rain;
volume[6,0] = snd_HERO_at_simple;
volume[7,0] = snd_INV_place;
volume[8,0] = snd_INV_take;
volume[9,0] = snd_item_fall;
volume[10,0] = snd_LVL_UP;


for(var i = 10; i < size_volume; i += 1)
{
    volume[i,0] = snd_LVL_UP;
}

draw_set_font(fnt_Hint_text);
*/
for(var i = 0; i < size_volume; i += 1)
{
    var snd = volume[i,0];
    //Audio Labels
    label[i] = sound_get_name(snd);
    if(i == 0)
    {
        max_lenght = string_width(string_hash_to_newline(label[i]));
    }
    var tmp_len = string_width(string_hash_to_newline(label[i]));
    if(tmp_len > max_lenght)
    {
        max_lenght = tmp_len;
    }
}



