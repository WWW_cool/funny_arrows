/// @description gui_scope_get_type(inst)

var scope_inst = argument0;


for(var i = 0; i < instance_number(obj_a_parent); i++)
{
    var inst = instance_find(obj_a_parent,i);
    if(inst == scope_inst)
    {
        return obj_a_parent;
    }
}

for(var i = 0; i < instance_number(obj_a_mover); i++)
{
    var inst = instance_find(obj_a_mover,i);
    if(inst == scope_inst)
    {
        return obj_a_mover;
    }
}
