/// @description scr_a_move_next_mover(inst)

var _inst = argument0;
if(instance_exists(global.a_study_inst))exit;

if(instance_exists(global.a_selector))
{
    with(global.a_selector)
    {
        m_move_mover_xpos[_inst.m_move_id,a_move_index] = _inst.x;
        m_move_mover_ypos[_inst.m_move_id,a_move_index] = _inst.y;
        m_move_mover_angle[_inst.m_move_id,a_move_index] = _inst.image_angle;
        m_move_mover_state[_inst.m_move_id,a_move_index] = _inst.m_anim_state;
        m_move_mover_move_back[_inst.m_move_id,a_move_index] = _inst.m_move_back;
        m_move_mover_pos[_inst.m_move_id,a_move_index]       = _inst.m_pos;
    }
}

