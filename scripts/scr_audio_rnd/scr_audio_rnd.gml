/// @description scr_audio_rnd(snd_1,snd_2)

if(argument_count > 1)
{
    var snd = choose(argument[0],argument[1]);
    scr_def_play_snd(snd);
}
