/// @description scr_a_draw_get_double(duble_ind)

var _duble = global.a_cell_color[argument0];
var res = spr_cell_d_bg;

switch(_duble)
{
    case EnColourTypes.EMPTY: 
        res = spr_cell_d_bg;
    break;
    case EnColourTypes.BLUE: 
        res = spr_cell_d_blue;
    break;
    case EnColourTypes.YELLOW: 
        res = spr_cell_d_yellow;
    break;
    case EnColourTypes.RED: 
        res = spr_cell_d_red;
    break;
    case EnColourTypes.GREEN: 
        res = spr_cell_d_green;
    break;
    case EnColourTypes.VIOLET: 
        res = spr_cell_d_violet;
    break;
    case EnColourTypes.ORANGE: 
        res = spr_cell_d_orange;
    break;
}

return res;

