/// @description v_map_size(map)
var map = argument[0];

/**
        With this function you can find how many key/values pairs 
        the (previously created) v_map contains. 
**/

if(is_array(map))
{
    var length = array_height_2d(map);
    var key_cnt = 0;
    var i;
    for(i=0; i<length; i++)
    {
        if(map[i,0] != -1) key_cnt++;
    }
    return key_cnt;
}

return -1;


