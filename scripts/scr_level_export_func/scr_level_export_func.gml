/// @description scr_level_export_func()

var str = "";
var _func_name = string_delete(use_file_name,string_length(use_file_name)-4,5);

fed("_func_name -- " + s(_func_name));

str += "/// _func_study_" + _func_name + @"(lvl_num)
var _lvl_num = argument0;
var _res = false;
switch(_lvl_num)
{
";

var level_count = scr_level_get_count();
for(var k = 1; k < level_count + 1; k++)
{
    str += "case " + s(k) + ": // ----------------------------------------------------"
    str += @"
    if(global._def_glb_var_default_resolution_pc)
    {"
        ini_open(use_file_name);
        var i = 0;
        while(ini_key_exists(s(k),"type" + string(i)))
        {
            str += scr_export_inst(ini_read_string(s(k),"type" + string(i),""),k,i);
            str += @"
        scr_study_init_obj(inst);
            "
            i += 1;
        }
        ini_close();
    str += @"
    }
    else
    {"
        ini_open(use_file_name);
        i = 0;
        while(ini_key_exists(s(k),"type" + string(i)))
        {
            str += scr_export_inst(ini_read_string(s(k),"type" + string(i),""),k,i);
            str += @"
        scr_study_init_obj(inst);
            "
            i += 1;
        }
        ini_close();
    
    str += @"
    }
    _res = true;
break;
"
}

str += @"}

return _res;

";
var file_name = s("_func_study_",_func_name,".gml");

if !file_exists(file_name)
{
   var file = file_text_open_write(file_name); 
   file_text_close(file);
}
if file_exists(file_name)
{
    var file = file_text_open_write(file_name);
    file_text_write_string(file,str);
    file_text_writeln(file);
    file_text_close(file);
}
