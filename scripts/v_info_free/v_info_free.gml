/// @description v_info_free
var inst = global._v_info_owner;
if(instance_exists(inst))
with(inst)
{
    _v_info_xpos = 0;
    _v_info_ypos = 0;
    _v_info_owner = 0;
    _v_info_callback = 0;
    _v_info_timeStart = 0;
    _v_info_alpha = 0;
    _v_info_type = 0;
            
    _v_info_deleted = 0;
}

