/// @description scr_string_get_integer(str, char_from, char_to)

var _str = argument0;
var _char_from = argument1;
var _char_to = argument2;

var _res = noone;
var _str_len = string_length(_str);
var _str_integer = "";
var _start_index = noone;
var _end_index = noone;

for(var i = 0; i < _str_len; i++)
{
    if(string_char_at(_str,i) == _char_from)
    {
        _start_index = i + 1;
    }
    if(string_char_at(_str,i) == _char_to)
    {
        _end_index = i;
    }
}

if(_start_index != noone && _end_index != noone)
{
    _str_integer = string_copy(_str,_start_index,_end_index - _start_index);
    if(is_real(real(_str_integer)))
    {
        _res = real(_str_integer);
    }
}
return _res;
