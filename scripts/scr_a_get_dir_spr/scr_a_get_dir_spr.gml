/// @description scr_a_get_dir_spr(dir_type)

var dir_type = argument0;
var res = noone;

switch(dir_type)
{
    case EnDirTypes.NONE: 
        res = noone;
    break;
    case EnDirTypes.SIDE: 
        res = spr_arrow_side;
    break;
    case EnDirTypes.CCW: 
        res = spr_arrow_ccw;
    break;
    case EnDirTypes.CW: 
        res = spr_arrow_cw;
    break;
}

return res;






