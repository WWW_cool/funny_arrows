/// @description v_scroll_step_on(id, max_now, dir)
var i = argument0;
var v_scroll_max_count = argument1;
var v_scroll_dir  = argument2;

if(i < v_scroll_count)
{
    if(!v_scroll_deleted[i])
    {
        var v_scroll_y_offset = (((v_scroll_line_max[i] - 1)*v_scroll_size[i])/(v_scroll_max_count - v_scroll_line_max[i])) div 1;
        v_scroll_ypos[i] = v_scroll_ypos_init[i] + v_scroll_y_offset*v_scroll_min[i];
        if(v_scroll_dir == "down")
        {
            v_scroll_min[i] += 1;
            v_scroll_ypos_start[i] += v_scroll_y_offset;
            if(v_scroll_min[i] > (v_scroll_max_count - v_scroll_line_max[i]))
            {
                v_scroll_min[i] = v_scroll_max_count - v_scroll_line_max[i];
            }
            if(script_exists(v_scroll_callback[i]))
            {
                script_execute(v_scroll_callback[i],v_scroll_min[i]);
            }
        }
        if(v_scroll_dir == "up")
        {
            v_scroll_min[i] -= 1;
            v_scroll_ypos_start[i] -= v_scroll_y_offset;
            if(v_scroll_min[i] < 0)
            {
                v_scroll_min[i] = 0;
            }
            if(script_exists(v_scroll_callback[i]))
            {
                script_execute(v_scroll_callback[i],v_scroll_min[i]);
            }
        }
    }
}


    
    
