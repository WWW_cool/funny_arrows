/// @description v_sld_set_param(id,name,val)

var tmp_id      = argument0;
var tmp_name    = argument1;
var tmp_val     = argument2;

if(v_sld_count > 0)
if (!v_sld_deleted[tmp_id])
{
    switch(tmp_name)
    {
        case "xpos":
                 v_sld_xpos[tmp_id]  = tmp_val;
                 v_btns_set_param(v_sld_v_btn[tmp_id],tmp_name,tmp_val);
        break;
        case "ypos":
                 v_sld_ypos[tmp_id]  = tmp_val;
                 v_btns_set_param(v_sld_v_btn[tmp_id],tmp_name,tmp_val);
        break;
        case "val":
                 v_sld_value[tmp_id]  = tmp_val;
        break;
    }
}






