/// @description scr_db_load_param(map,num,default)

var tmp_map = argument0;
var tmp_num = argument1;
var tmp_val = argument2;

if(tmp_num < EnDbKeys.LENGTH)
{
    if(ds_exists(tmp_map,ds_type_map))
    {
        var val = ds_map_find_value(tmp_map,global.inst_glb.glb_db_keys[tmp_num]);
        if(val != undefined)
        {
            var ok_real = real(val);
            if(is_real(ok_real))
            {
                if(ok_real >= 0)
                {
                    return ok_real;
                }
            }
        }
        return tmp_val;
    }
}
