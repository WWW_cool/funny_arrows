/// @description scr_def_g_pause(owner,cmd,type,params...)

var _def_tmp_result     = false;

if(argument_count > 0)
{
    var _def_tmp_owner = argument[0];
    fed(s("pause id = ", _def_tmp_owner));
    switch(argument_count)
    {
        case 1:
            var _def_tmp_cmd = argument[0];
            if(_def_tmp_cmd == "off")
            {
                global._def_glb_var_pause = false;
                if(instance_exists(global._def_glb_var_pause_obj))
                {
                    with(global._def_glb_var_pause_obj)
                    {
                        instance_destroy();
                    }
                }
                if(sprite_exists(global._def_glb_var_pause_sprite))
                {
                    sprite_delete(global._def_glb_var_pause_sprite);
                }
                instance_activate_all();
                _def_tmp_result = true;
                return _def_tmp_result;
            }
        break;
        case 2:
            var _def_tmp_cmd = argument[1];
            switch(_def_tmp_cmd)
            {
                case "on":
                    if(!scr_def_g_pause_check(_EnPauseType.SIMPLE))
                    {
                        return _def_tmp_result;
                    }
                    scr_def_g_pause("off");
                    if(sprite_exists(global._def_glb_var_pause_sprite))
                    {
                        sprite_delete(global._def_glb_var_pause_sprite);
                    }
                    global._def_glb_var_pause = true;
                    global._def_glb_var_pause_obj = 
                    instance_create(0,0,global._def_glb_p_simple);
                    if(instance_exists(global._def_glb_var_pause_obj))
                    {
                        _def_tmp_result = true; 
                    }
                    else
                    {
                        _def_tmp_result = false;
                    }
                break;
                case "off":
                    global._def_glb_var_pause = false;
                    if(instance_exists(global._def_glb_var_pause_obj))
                    {
                        with(global._def_glb_var_pause_obj)
                        {
                            instance_destroy();
                        }
                    }
                    if(sprite_exists(global._def_glb_var_pause_sprite))
                    {
                        sprite_delete(global._def_glb_var_pause_sprite);
                    }
                    instance_activate_all();
                    _def_tmp_result = true;
                break;
            }
        break;
        case 3:
            var _def_tmp_cmd = argument[1];
            if(_def_tmp_cmd == "on")
            {
                
                var _def_tmp_type = argument[2];
                if(!scr_def_g_pause_check(_def_tmp_type))
                {
                    return _def_tmp_result;
                }
                scr_def_g_pause("off");
                if(sprite_exists(global._def_glb_var_pause_sprite))
                {
                    sprite_delete(global._def_glb_var_pause_sprite);
                }
                switch(_def_tmp_type)
                {
                    case _EnPauseType.SIMPLE:
                        global._def_glb_var_pause = true;
                        global._def_glb_var_pause_obj = 
                        instance_create(0,0,global._def_glb_p_simple);
                        if(instance_exists(global._def_glb_var_pause_obj))
                        {
                            _def_tmp_result = true; 
                        }
                        else
                        {
                            _def_tmp_result = false;
                        }
                    break;
                    case _EnPauseType.MENU:
                        global._def_glb_var_pause = true;
                        global._def_glb_var_pause_obj = 
                        instance_create(0,0,obj_def_p_menu);
                        if(instance_exists(global._def_glb_var_pause_obj))
                        {
                            _def_tmp_result = true; 
                        }
                        else
                        {
                            _def_tmp_result = false;
                        }
                    break;
                    default:
                        _def_tmp_result = false;
                    break;
                }
            }
            else
            {
                return _def_tmp_result;
            }
        break;
        case 4:
            var _def_tmp_cmd  = argument[1];
            var _def_tmp_type = argument[2];
            var _def_tmp_param1 = argument[3];
             if(_def_tmp_cmd == "on" && _def_tmp_type == _EnPauseType.LVL_END)
             {
                if(!scr_def_g_pause_check(_def_tmp_type))
                {
                    return _def_tmp_result;
                }
                scr_def_g_pause("off");
                if(sprite_exists(global._def_glb_var_pause_sprite))
                {
                    sprite_delete(global._def_glb_var_pause_sprite);
                }
                global._def_glb_var_pause = true;
                global._def_glb_var_pause_obj = 
                instance_create(0,0,global._def_glb_p_lvl_end);
                if(instance_exists(global._def_glb_var_pause_obj))
                {
                    global._def_glb_var_pause_obj._def_p_msg_type = _def_tmp_param1;
                    _def_tmp_result = true; 
                }
                else
                {
                    _def_tmp_result = false;
                }
             }
        break;
        default:
            scr_def_show_debug_message("wrong argument number","error");
        break;
    }
}
else
{
    global._def_glb_var_pause = false;
    if(instance_exists(global._def_glb_var_pause_obj))
    {
        with(global._def_glb_var_pause_obj)
        {
            instance_destroy();
        }
    }
    if(sprite_exists(global._def_glb_var_pause_sprite))
    {
        sprite_delete(global._def_glb_var_pause_sprite);
    }
    instance_activate_all();
    _def_tmp_result = true;
    return _def_tmp_result;
}
scr_def_show_debug_message("pause state after = " + string(global._def_glb_var_pause),"pause");
if(_def_tmp_result == true)
{
    if(instance_exists(global._def_glb_var_pause_obj))
    {
        global._def_glb_var_pause_obj._def_p_owner = _def_tmp_owner;
    }
}

return _def_tmp_result;


