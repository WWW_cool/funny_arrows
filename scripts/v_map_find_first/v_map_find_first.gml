/// @description v_map_find_first(map)
var map = argument[0];

/**
        This function returns the first key stored in the v_map. 
        This is not the first key in the order you added them! 
        v_maps are not stored in a linear form, for that use ds_lists, 
        so all this does is find the first key as stored by the computer. 
        This can be useful if your have to iterate through 
        the v_map looking for something, but should be avoided if possible 
        as it can be slow. 
**/

if(is_array(map))
if(map[0,0] != -1) return map[0,0];
else return undefined;


