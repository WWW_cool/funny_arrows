/// @description v_map_replace(map, key, val);
/// @param map
/// @param  key
/// @param  val
var map = argument[0];
var key = argument[1];
var val = argument[2];

/**
    With this function you can change the value for the given key 
    within the (previously created) v_map. Please note that if the key 
    to be replaced does not exists, it is created 
    and the given value assigned to it. 
**/

if(is_string(key))
if(is_array(map))
{
    var length = array_height_2d(map);
    var free_place = -1;
    var map_i;
    //Проходим по всем элементам карты и ищем, есть ли уже такой ключ. 
    //Если есть, заменяем его
    //Одновременно с этим ищем пустое место чтобы в случае чего поместить туда ключ-значение
    //и не искать по второму кругу и не проходить опять весь массив
    for(var i=0; i<length; i++)
    {
        map_i = map[i,0];
        //Если ключ отсутствует
        if(map_i == -1)
        {
            //Помечаем свободное место (если до этого еще не пометили)
            if(free_place == -1) free_place = i;
            //и уходим в следующий шаг цикла
            continue;
        }
        //иначе сверяем ключ, если совпадают - заменяем value
        else if(map_i == key)
                {
                    map[@ i,1] = val;
                    return true;
                }
    }    
    //Одинакового не нашли, добавляем новые ключ-значение, если free_place > -1
    if(free_place>-1)
    {
         map[@ free_place,0] = key;
         map[@ free_place,1] = val;
         return true;        
    }  
}

return false;

