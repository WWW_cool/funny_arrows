/// @description v_scroll_draw(id,max_now)
var i = argument0;
var v_scroll_max_count = argument1;
if(i < v_scroll_count)
{
    if(!v_scroll_deleted[i])
    {
        var v_scroll_y_offset = (((v_scroll_line_max[i] - 1)*v_scroll_size[i])/(v_scroll_max_count - v_scroll_line_max[i])) div 1;  
        if(v_scroll_lay_on[i] || v_scroll_pressed[i])
        {
            colored2 = c_gray;
        }
        else
        {
            colored2 = c_white;
        }
        
        if(v_scroll_pressed[i])
        {
            var tmp_y = mouse_y;
            if(tmp_y > v_scroll_ypos_init[i] + v_scroll_y_offset*(v_scroll_max_count - v_scroll_line_max[i]))
            {
                tmp_y = v_scroll_ypos_init[i] + v_scroll_y_offset*(v_scroll_max_count - v_scroll_line_max[i]);
            }
            if(tmp_y < v_scroll_ypos_init[i])
            {
                tmp_y = v_scroll_ypos_init[i];
            }
            draw_sprite_ext(v_scroll_spr[i],0,v_scroll_xpos[i],tmp_y,1,1,0,colored2,1);
        }
        else
        {
            draw_sprite_ext(v_scroll_spr[i],0,v_scroll_xpos[i],v_scroll_ypos[i],1,1,0,colored2,1);
        } 
    }
}

 



