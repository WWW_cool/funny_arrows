/// @description v_array_get_index(array,val)

var _array = argument0;
var _val = argument1;
var _res = -1;
if(is_array(_array))
{
    for(var i = 0; i < array_length_1d(_array); i++)
    {
        if(_array[i] == _val)
        {
            _res = i;
            break;
        }
    }
}

return _res;

