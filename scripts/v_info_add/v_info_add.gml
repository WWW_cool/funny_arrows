/// @description v_info_add(x,y,owner, info_draw_callback, background) 
/// @param x
/// @param y
/// @param owner
/// @param  info_draw_callback
/// @param  background

//Defines a new info panel and returns it's ID
var inst = global._v_info_owner;
if(instance_exists(inst))
{
    with(inst)
    {
        var i, arg;
        for(i=0; i<5; i++)
        {
            if(argument_count > i)
                arg[i] = argument[i]
            else arg[i] = -1;       
        }
        
        var tmp_x                   = arg[0];
        var tmp_y                   = arg[1];
        var tmp_owner               = arg[2];
        var tmp_info_draw_callback  = arg[3];
        var tmp_bg                  = arg[4];
        //msg(s(_v_info_count))
        for (i = 0; i < _v_info_count; i++)
        {
            if (_v_info_deleted[i])
            {
                _v_info_xpos[i]          = tmp_x;
                _v_info_ypos[i]          = tmp_y;
                _v_info_owner[i]         = tmp_owner;
                _v_info_callback[i]      = tmp_info_draw_callback;
                _v_info_timeStart[i]     = room_speed/3;
                _v_info_alpha[i]         = 0;
                _v_info_type[i]          = -1;
                _v_info_bg[i]            = tmp_bg;
                _v_info_deleted[i] = false;
                //msg("add")
                return i;
            }
        }
        
        _v_info_xpos[_v_info_count]          = tmp_x;
        _v_info_ypos[_v_info_count]          = tmp_y;
        _v_info_owner[_v_info_count]         = tmp_owner;
        _v_info_callback[_v_info_count]      = tmp_info_draw_callback;
        _v_info_timeStart[_v_info_count]     = room_speed/3;//tmp_timeStart;
        _v_info_alpha[_v_info_count]         = 0;
        _v_info_type[_v_info_count]          = -1;
        _v_info_bg[_v_info_count]            = tmp_bg;        
        _v_info_deleted[_v_info_count] = false;
        
        _v_info_count++;
        //msg("new")
        return _v_info_count - 1;
    }
}
else
{
    return -1;
}


