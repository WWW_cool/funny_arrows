/// @description v_map_read(string)

var map = v_map_create();
var str = argument0;
if(string_length(str) < 3)
{
    v_map_destroy(map);
    return -1;
}
if(is_array(map))
{
    var str2 = "";
    var key = "";
    var val = 0;
    var map_ind = 0;
    var str_ind = 0;
    while(string_length(str) > 0)
    {
        if(string_char_at(str,str_ind) == "&")
        {
            str = string_delete(str,str_ind + 1,1);
            continue;
        }
        if(string_char_at(str,str_ind) == "^")
        {
            return map;
        }
        if(string_char_at(str,str_ind) == "|")
        {
            var tmp_length = 30; // длинна до следующей палки
            var tmp_ind = 0;
            while(tmp_length--)
            {
                tmp_ind += 1;
                if(string_char_at(str,str_ind + tmp_ind) == "/")
                {
                    key = string_copy(str,str_ind + 2,tmp_ind - 2);
                    str = string_delete(str,str_ind + 1,tmp_ind - 1);
                    break;
                }
            }
            if(tmp_length <= 0)
            {
                str = string_delete(str,str_ind + 1,1);
            }
            continue;
        }
        if(string_char_at(str,str_ind) == "/")
        {
            var tmp_length = 30; // длинна до следующей палки
            var tmp_ind = 0;
            while(tmp_length--)
            {
                tmp_ind += 1;
                if(string_char_at(str,str_ind + tmp_ind) == "|")
                {
                    val = string_copy(str,str_ind + 2,tmp_ind - 2);
                    str = string_delete(str,str_ind + 1,tmp_ind - 1);
                    
                    var tmp_val = v_map_get_real(val);
                    
                    if(tmp_val != -1
                    &&(
                    string_length(val) == string_length(s(tmp_val))
                    || tmp_val > 0
                    ))
                    {
                        v_map_add(map,key,real(val));
                    }
                    else
                    {
                        var tmp_ind = asset_get_index(val);
                        if(tmp_ind != -1)
                        {
                            val = tmp_ind;
                        }
                        v_map_add(map,key,val);
                    }
                    break;
                }
            }
            continue;
        }
        str_ind += 1;
    }
}

