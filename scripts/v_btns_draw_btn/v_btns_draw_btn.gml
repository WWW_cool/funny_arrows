/// @description v_btns_draw_btn(id)
var i = argument0;
var inst = global._v_btns_owner;
if(instance_exists(inst))
with(inst)
{
 if (!_v_btns_deleted[i] && _v_btns_active[i])
 {             
   
   var tmp_alpha = draw_get_alpha();
   if(sprite_exists(_v_btns_spr[i]))
   {
    
    var _scale = _v_btns_spr_scale[i] - _v_btns_spr_sc[i];
    
    if(_v_btns_spr_freeze[i] == -1)
    {
        draw_sprite_ext(    _v_btns_spr[i],_v_btns_spr_index[i],
                        _v_btns_xpos[i],_v_btns_ypos[i],
                        _scale,
                        _scale,
                        0,c_white,tmp_alpha);
    } 
    else
    {
        _v_btns_spr_index[i] = 0;
        draw_sprite_ext(    _v_btns_spr[i],_v_btns_spr_freeze[i],
                        _v_btns_xpos[i],_v_btns_ypos[i],
                        _v_btns_spr_scale[i],_v_btns_spr_scale[i],
                        0,c_white,tmp_alpha);
    }
    if(sprite_exists(_v_btns_spr_icon[i]))
    {
        draw_sprite_ext(_v_btns_spr_icon[i],_v_btns_spr_index[i],
                        _v_btns_xpos[i] + _v_btns_spr_icon_x[i]*_scale,
                        _v_btns_ypos[i] + _v_btns_spr_icon_y[i]*_scale,
                        _scale,_scale,
                        0,c_white,tmp_alpha);
    } 
    if(_v_btns_callback_draw[i] != -1)
    {
        if(script_exists(_v_btns_callback_draw[i]))
        {
            if(instance_exists(_v_btns_owner[i]))
                {
                    with(_v_btns_owner[i])
                    {
                        script_execute(other._v_btns_callback_draw[i],i);
            }}
        }
    }             
    if(_v_btns_text[i] != "")
    {
        draw_set_valign(fa_middle);
        draw_set_halign(_v_btns_text_halign[i]);
        var str = scr_localization_get(_v_btns_text[i],"No text");
        
        if(_v_btns_noKey[i])str = _v_btns_text[i];
        
        if(_v_btns_spr_index[i])
        {
             draw_set_colour(global.col_main);
             if(font_exists(_v_btns_text_font_lay_on[i]))
             {
                draw_set_font(_v_btns_text_font_lay_on[i]);
             }
             draw_text_transformed(_v_btns_xpos[i] + _v_btns_text_x[i]*_scale,
             _v_btns_ypos[i] + _v_btns_text_y[i]*_scale,string_hash_to_newline(str),
             _v_btns_text_scale[i] - _v_btns_spr_sc[i],
             _v_btns_text_scale[i] - _v_btns_spr_sc[i],
             _v_btns_text_angle[i]);
        }
        else
        {
             draw_set_colour(_v_btns_text_col[i]);
             if(font_exists(_v_btns_text_font_lay_off[i]))
             {
                draw_set_font(_v_btns_text_font_lay_off[i]);
             }
             draw_text_transformed(_v_btns_xpos[i] + _v_btns_text_x[i]*_scale,
             _v_btns_ypos[i] + _v_btns_text_y[i]*_scale,string_hash_to_newline(str),
             _v_btns_text_scale[i] - _v_btns_spr_sc[i],
             _v_btns_text_scale[i] - _v_btns_spr_sc[i],
             _v_btns_text_angle[i]);
        }
    }
   }               
   if(_v_btns_colddown[i])
   {
       _v_btns_colddown_time[i] -= 1;
       if(_v_btns_colddown_time[i] <= 0)
           _v_btns_colddown[i] = 0;
        
       // вверх    
       var tmp_height = (_v_btns_colddown_time_init[i] - _v_btns_colddown_time[i])
       *(_v_btns_spr_h[i]/_v_btns_colddown_time_init[i]); 
       
       var tmp_top = tmp_height; 
       tmp_height = _v_btns_spr_h[i] - tmp_height;
       
     /*  // вниз 
       var tmp_height = (_v_btns_colddown_time_init[i] - _v_btns_colddown_time[i])
       *(_v_btns_spr_h[i]/_v_btns_colddown_time_init[i]); 
       
       var tmp_top = _v_btns_spr_h[i] - tmp_height; 
      */ /*
       draw_sprite_part(   _v_btns_spr[i],_v_btns_spr_index[i] + 1,
                           0,tmp_top,
                           _v_btns_spr_w[i],
                           tmp_height,
                           _v_btns_xpos[i] - _v_btns_spr_x_offset[i],
                           _v_btns_ypos[i] - _v_btns_spr_y_offset[i]
                       );
       */
       var tmp_alpha = draw_get_alpha();
       draw_set_alpha(0.7);
       draw_set_blend_mode(bm_normal);
       var tmp_x = _v_btns_xpos[i] - _v_btns_spr_x_offset[i] + _v_btns_spr_w[i]; 
       var tmp_y = _v_btns_ypos[i] - _v_btns_spr_y_offset[i] + _v_btns_spr_h[i];
       draw_rectangle(tmp_x,tmp_y,tmp_x - _v_btns_spr_w[i],tmp_y -  tmp_height,0);
       draw_set_alpha(tmp_alpha);
   }
 }                               
}

