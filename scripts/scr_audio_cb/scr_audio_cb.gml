/// @description scr_audio_cb(type)

var type = argument0;


if(global._def_glb_var_audio_on || global._def_glb_var_audio_on_bg)
{
    audio_set_master_gain(0,global._def_glb_var_audio_gain);
}
else
{
    audio_set_master_gain(0,0);
}

if(type == 1)
{
    if(!global._def_glb_var_audio_on_bg)
    {
        /*if(audio_exists(global.a_sound_bg))
        {
            if(audio_is_playing(global.a_sound_bg))
            {
                //audio_sound_gain(global.a_sound_bg,0,1000);
                audio_stop_sound(global.a_sound_bg);
            }
        }*/
        audio_stop_all();
        //audio_stop_sound(global.a_sound_bg);
    }
    else
    {
        if(os_browser == browser_not_a_browser)
        {
            if(audio_exists(global.a_sound_bg))
            {
                if(!audio_is_playing(global.a_sound_bg))
                {
                    scr_audio_start_bg();
                }
            }
            else
            {
                scr_audio_start_bg();
            }
        }
    }
}






