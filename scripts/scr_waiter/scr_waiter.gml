/// @description scr_waiter(type,callback)

var type = argument0;
var callback = argument1;

if(instance_number(obj_waiter) > 0)exit;

var inst = instance_create(1,1,obj_waiter);
if(instance_exists(inst))
{
    inst.wait_type = type;
    inst.wait_cb = callback;
}



