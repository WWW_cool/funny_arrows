/// @description scr_def_rm_goto_next()

if(room_exists(room + 1))
{
    scr_def_rm_goto_room(room + 1);
}
else 
{
   if(room_exists(global._def_glb_var_room_start))
   {
       scr_def_rm_goto_room(global._def_glb_var_room_start);
   }
   else
   {
       scr_def_show_debug_message("room NOT found!","error");
   }
}
