/// @description scr_win_next

if(!instance_exists(global.inst_glb))scr_def_rm_goto_room(rm_Start);

if(room != rm_Start)
{
    with(global.inst_glb)
    {
        if(game_random)
        {
            scr_def_rm_goto_room(rm_Start);
            exit;
        }
        if(!game_boss_next)
        {
            game_lvl_ok += 1;
            
            if(scr_boss_get_amount() == 1)
            {
                //game_boss_next = 1;
                game_lvl_ok -= 1;
                scr_g_save();
                scr_def_rm_goto_room(rm_Start);
                exit;
            }
            game_lvl_ok -= 1;
        }
    }
}

if(!scr_gui_life())
{
    var _inst = scr_s_msg(EnMsgTypes.Lives);
    if(instance_exists(_inst))
    {
        _inst.OPT_msg_type = 0;
    }
    exit;
}

//scr_def_rm_goto_next();
/*if(os_browser == browser_not_a_browser)
{
    if(room == rm_boss)
    {
        scr_gui_life(-1);
        scr_def_rm_goto_room(rm_load);
        exit;
    }
    if(room == rm_Start)
    {
        fed("lvl loaded = " + s(global.inst_glb.game_lvl_ok));
        global.inst_glb.game_lvl_to_load = global.inst_glb.game_lvl_ok;
    }
    if(scr_level_get_count() >= global.inst_glb.game_lvl_to_load + 1)
    {
        //if(global.inst_glb.game_lvl_to_load + 1 >= array_length_1d(global.inst_glb.game_lvl_need))
        //{
        //    global.inst_glb.game_lvl_to_load = 0;
        //    scr_def_rm_goto_room(rm_Start);
        //    exit;
        //}
           
        //if(global.user_lvl < global.inst_glb.game_lvl_need[global.inst_glb.game_lvl_to_load + 1])
        //{
        //    instance_create(1,1,obj_GUI_menu_shop);
            //global.inst_glb.game_lvl_to_load = 0;
            //scr_def_rm_goto_room(rm_Start);
        //    exit;
        //}
        global.inst_glb.game_lvl_to_load += 1;
        //if((global.inst_glb.game_lvl_to_load % 6) == 0)
        //{
        //    scr_def_rm_goto_room(rm_boss);
        //}
        //else
        //{
            scr_gui_life(-1);
            scr_def_rm_goto_room(rm_load);
        //}
    }
    else
    {
        global.inst_glb.game_lvl_to_load = 0;
        scr_def_rm_goto_room(rm_Start);
        fed("ERROR | lvl MAX count = " + s(scr_level_get_count()));
        //instance_create(1,1,obj_GUI_menu_shop);
    }
}
else
{*/
if(LVLCREATE)
{
    if(room == rm_Start)
    {
        global.inst_glb.game_lvl_to_load = global.inst_glb.game_lvl_ok;
    }
    if(scr_level_get_count() >= global.inst_glb.game_lvl_to_load + 1)
    {
        global.inst_glb.game_lvl_to_load += 1;
        scr_def_rm_goto_room(rm_load);
    }
    else
    {
        scr_def_rm_goto_room(rm_Start);
    }
}
else
{
    global.inst_glb.game_hint_fstep = 0;
    if(room == rm_Start)
    {
        var _lvl_num = 0;
        //fed("lvl loaded = " + s(global.inst_glb.game_lvl_ok));
        _lvl_num = global.inst_glb.game_lvl_ok;
        
        if(_lvl_num < scr_level_get_count())
        {
            scr_def_rm_goto_room(rm_lvlCreate + 1 + _lvl_num);
        }
        else
        {
            scr_s_msg(EnMsgTypes.Demo);
        }
        global.inst_glb.game_time_price = global.inst_glb.price_time_need;
    }
    else
    {
        if(room_exists(room + 1))
        {
            /*if(global.inst_glb.game_lvl_ok + 1 >= array_length_1d(global.inst_glb.game_lvl_need))
            {
                scr_def_rm_goto_room(rm_Start);
                exit;
            }
            if(!global.user_open_all)
            {  
                if(global.user_lvl < global.inst_glb.game_lvl_need[global.inst_glb.game_lvl_ok + 1])
                {
                    instance_create(1,1,obj_GUI_menu_shop);
                    exit;
                }
            }*/
            
            if(room + 1 - rm_lvlCreate > scr_level_get_count())
            {
                scr_def_rm_goto_room(rm_Start);
            }
            else
            {
                scr_def_rm_goto_room(room + 1);
            }
        }
        else
        {
            scr_def_rm_goto_room(rm_Start);
        }
    }
}
//}

