/// @description scr_a_draw_cell_default_ext(x,y,scale,angle,alpha,spr_col,spr_dir,spr_angle)

var _x = argument0;
var _y = argument1;
var _scale = argument2;
var _angle = argument3;
var _alpha = argument4;
var _col = argument5;
var _dir = argument6;
var _spr_angle = argument7;

if(sprite_exists(_col))
{
    draw_sprite_ext(_col,0,_x,_y,_scale,_scale,_angle,c_white,_alpha);
}
if(sprite_exists(_dir))
{
    draw_sprite_ext(_dir,0,_x,_y,_scale,_scale,90*_spr_angle + _angle,c_white,_alpha);
}
