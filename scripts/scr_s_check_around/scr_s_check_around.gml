/// @description scr_s_check_around(inst)

var res = false;
var inst = argument0;

for(var i = 0; i < 4; i += 1)
{
    var next_cell = collision_circle(inst.x + a_side*a_dx[i],
    inst.y + a_side*a_dy[i],10,obj_a_parent,false,true);
    if(instance_exists(next_cell))
    {
        if(next_cell.a_lock)
        {
            continue;
        }
        if(next_cell.a_type == EnCellTypes.TIME && next_cell.a_p_turn_count)
        {
            continue;
        }
        if(inst.a_colour == next_cell.a_colour
            || inst.a_type == EnCellTypes.MULTI 
            || next_cell.a_type == EnCellTypes.MULTI
            || next_cell.a_type == EnCellTypes.VORTEX)
        {
            var dir_to = scr_a_get_dir(inst,true); 
            var dir_from = scr_a_get_dir(next_cell,false);
            if(inst.a_dir == EnDirTypes.NONE 
                || inst.a_type == EnCellTypes.MULTI)
            {
                if(next_cell.a_type == EnCellTypes.MULTI
                    || next_cell.a_type == EnCellTypes.VORTEX)
                {
                    res = true;
                    break;
                }
                
                if(dir_from == (i + 2)%4 or dir_from == -2)
                {
                    res = true;
                    break;
                }
            }
            else
            {
                if(dir_to == i)
                {
                    if(next_cell.a_type == EnCellTypes.MULTI
                    || next_cell.a_type == EnCellTypes.VORTEX)
                    {
                        res = true;
                        break;
                    }
                    if(dir_from == -2)
                    {
                        res = true;
                        break;
                    }
                    else
                    {
                        dir_from += 2;
                        dir_from %= 4;
                        if(dir_from == dir_to)
                        {
                            res = true;
                            break;
                        }
                    }
                }
            }
        }
    }
}

return res;

