/// @description scr_export_lvl(lvl_num,name_shift)

var tmp_section = argument0;
var name_shift = argument1;
var str = "";
str += scr_export_start_str();

str += "_GLOBAL_INIT();"
//global.a_selector.s_attempts = " + s(ini_read_real(s(tmp_section),"Attempts",1)) + ";";
//global.inst_glb.game_attempts[" + s(tmp_section) + "] = " + s(ini_read_real(s(tmp_section),"Attempts",1));
str += @"
if(global._def_glb_var_default_resolution_pc)
{"
    ini_open(global._def_glb_game_name_PC + "Levels");
    var i = 0;
    while(ini_key_exists(s(tmp_section),"type" + string(i)))
    {
        str += scr_export_inst(ini_read_string(s(tmp_section),"type" + string(i),""),tmp_section,i);
        i += 1;
    }
    ini_close();
str += @"
}
else
{"
    ini_open(global._def_glb_game_name_Mobile + "Levels");
    i = 0;
    while(ini_key_exists(s(tmp_section),"type" + string(i)))
    {
        str += scr_export_inst(ini_read_string(s(tmp_section),"type" + string(i),""),tmp_section,i);
        i += 1;
    }
    ini_close();

str += @"
}
"

str += scr_export_end_str();
var file_name = s("rm_lvl_",tmp_section + name_shift,".room.gmx");

if !file_exists(file_name)
{
   var file = file_text_open_write(file_name); 
   file_text_close(file);
}
if file_exists(file_name)
{
    var file = file_text_open_append(file_name);
    file_text_write_string(file,str);
    file_text_writeln(file);
    file_text_close(file);
}









