/// @description v_sld_add(x,y,owner,spr_back,spr_sld,offset,cb)

var tmp_x = argument[0];
var tmp_y = argument[1];

var tmp_owner       = argument[2]; 
var tmp_spr_back    = argument[3]; // my for v_btn
var tmp_spr_sld     = argument[4]; // when drag

var tmp_offset  = argument[5]; 
var tmp_cb      = argument[6]; 
for (var i = 0; i < v_sld_count; i += 1)
{
    if (v_sld_deleted[i])
    {
        v_sld_xpos[i]     = tmp_x;
        v_sld_ypos[i]     = tmp_y;
        v_sld_owner[i]    = tmp_owner;
        v_sld_spr[i]      = tmp_spr_sld;
        v_sld_value[i]    = 0;
        v_sld_y[i]        = sprite_get_height(tmp_spr_back)/2;
        v_sld_x[i]        = sprite_get_width(tmp_spr_back)*tmp_offset;
        v_sld_width[i]    = sprite_get_width(tmp_spr_back)*(1 - tmp_offset*2);
        v_sld_v_btn[i]      = v_btns_add(tmp_x,tmp_y,tmp_spr_back,tmp_owner,0,
                                        NO_SCRIPT,
                                        v_sld_cb_press,
                                        v_sld_cb_release, 
                                        NO_SCRIPT,
                                        NO_SCRIPT
                                        );
        v_btns_set_param(v_sld_v_btn[i],"cantDraw",1);
        v_sld_offset[i]   = tmp_offset;
        v_sld_callback[i] = tmp_cb;
        v_sld_state[i]    = "idle";
        v_sld_deleted[i]  = 0;
        return i;
    }
}
    v_sld_xpos[v_sld_count]     = tmp_x;
    v_sld_ypos[v_sld_count]     = tmp_y;
    v_sld_owner[v_sld_count]    = tmp_owner;
    v_sld_spr[v_sld_count]      = tmp_spr_sld;
    v_sld_value[v_sld_count]    = 0;
    v_sld_y[v_sld_count]        = sprite_get_height(tmp_spr_back)/2;
    v_sld_x[v_sld_count]        = sprite_get_width(tmp_spr_back)*tmp_offset;
    v_sld_width[v_sld_count]    = sprite_get_width(tmp_spr_back)*(1 - tmp_offset*2);
    v_sld_v_btn[v_sld_count]      = v_btns_add(tmp_x,tmp_y,tmp_spr_back,tmp_owner,0,
                                    NO_SCRIPT,
                                    v_sld_cb_press,
                                    v_sld_cb_release, 
                                    NO_SCRIPT,
                                    NO_SCRIPT
                                    );
    v_btns_set_param(v_sld_v_btn[v_sld_count],"cantDraw",1);
    v_sld_offset[v_sld_count]   = tmp_offset;
    v_sld_callback[v_sld_count] = tmp_cb;
    v_sld_state[v_sld_count]    = "idle";
    v_sld_deleted[v_sld_count]  = 0;
    
    v_sld_count += 1;
    return v_sld_count - 1;

