/// @description v_req_resend

if(instance_exists(global.inst_glb))
{
    with(global.inst_glb)
    {  
        if(ds_exists(glb_db_req_list, ds_type_list))
        {
            var size = ds_list_size(glb_db_req_list);
            if(size > 0)
            {
                var index = size - 1;
                var req = glb_db_req_list[| index];
                ds_list_delete(glb_db_req_list, index);
                //fed("take request from queue - " + ds_map_write(req));
                if(ds_exists(req, ds_type_map))
                {
                    var scr = ds_map_find_value(req, EnRequestData.SCRIPT);
                    if(script_exists(scr))
                    {
                        var arg_count = ds_map_find_value(req, EnRequestData.ARG_COUNT);
                        var args = array_create(arg_count);
                        
                        for(var i = 0; i < arg_count; i++)
                        {
                            args[@ i] = ds_map_find_value(req, EnRequestData.ARG_1 + i);
                        }
                        switch(arg_count)
                        {
                            case 1:
                                script_execute(scr, args[0]);
                                break;
                            case 2:
                                script_execute(scr, args[0], args[1]);
                                break;
                            case 3:
                                script_execute(scr, args[0], args[1], args[2]);
                                break;
                            case 4:
                                script_execute(scr, args[0], args[1], args[2], args[3]);
                                break;
                            default:
                                script_execute(scr);
                        }
                    }
                    ds_map_destroy(req);
                }
            }
        }
    }
}




