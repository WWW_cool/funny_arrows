/// @description s(agrs...)

var str = "";

if(argument_count > 0)
{
    for(var i = 0; i < argument_count; i += 1)
    {
        if(is_real(argument[i]))
        {
            str += string(argument[i]);
        }
        else
        {
            str += argument[i];
        }
    }
}

return str; 
