/// @description v_scroll_step(id,max_now)

var i = argument0;
var v_scroll_max_count = argument1;
if(i < v_scroll_count)
{
    if(!v_scroll_deleted[i])
    {
        var v_scroll_y_offset = (((v_scroll_line_max[i] - 1)*v_scroll_size[i])/(v_scroll_max_count - v_scroll_line_max[i])) div 1;  
        v_scroll_ypos[i] = v_scroll_ypos_init[i] + v_scroll_y_offset*v_scroll_min[i];

        v_scroll_lay_on[i] = point_in_rectangle(mouse_x,mouse_y,
        v_scroll_xpos[i] - sprite_get_xoffset(v_scroll_spr[i]),
        v_scroll_ypos[i] - sprite_get_yoffset(v_scroll_spr[i]),
        v_scroll_xpos[i] - sprite_get_xoffset(v_scroll_spr[i]) + sprite_get_width(v_scroll_spr[i]),
        v_scroll_ypos[i] - sprite_get_yoffset(v_scroll_spr[i]) + sprite_get_height(v_scroll_spr[i]));
         
        if(v_scroll_lay_on[i])
        { 
            if(mouse_check_button_pressed(mb_left))
            {
            
                v_scroll_pressed[i] = 1;
                v_scroll_ypos_start[i] = mouse_y;
            }
        }
        
        if(v_scroll_pressed[i])
        {
            if(mouse_check_button_released(mb_left))
            {
               v_scroll_pressed[i] = 0;
            }
            
            if(mouse_y - v_scroll_ypos_start[i] > v_scroll_y_offset)
            {
                v_scroll_min[i] += 1;
                v_scroll_ypos_start[i] += v_scroll_y_offset;
                if(v_scroll_min[i] > (v_scroll_max_count - v_scroll_line_max[i]))
                {
                    v_scroll_min[i] = v_scroll_max_count - v_scroll_line_max[i];
                }
                if(script_exists(v_scroll_callback[i]))
                {
                    script_execute(v_scroll_callback[i],v_scroll_min[i]);
                }
            }
            if(mouse_y - v_scroll_ypos_start[i] < -v_scroll_y_offset)
            {
                v_scroll_min[i] -= 1;
                v_scroll_ypos_start[i] -= v_scroll_y_offset;
                if(v_scroll_min[i] < 0)
                {
                    v_scroll_min[i] = 0;
                }
                if(script_exists(v_scroll_callback[i]))
                {
                    script_execute(v_scroll_callback[i],v_scroll_min[i]);
                }
            }
        }
        
        return v_scroll_min[i];
    }
}
return 0;

