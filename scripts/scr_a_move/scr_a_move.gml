/// @description scr_a_move


switch(m_anim_state)
{
    case EnMoverStates.CREATE:
        if(m_anim_start_delay > 0)
        {
            m_anim_start_delay -= delta_time/1000000;
        }
        else
        {
            m_anim_alpha = v_increment(m_anim_res,m_anim_alpha,1,0.1,0.2);
            if(m_anim_res[0])
            {
                m_anim_state = EnMoverStates.IDLE;
            }
        }
    break;
    case EnMoverStates.IDLE:
        m_anim_alpha = v_increment(m_anim_res,m_anim_alpha,1,0.2,0.2);
    break;
    case EnMoverStates.MOVE:
        if(m_move_back)
        {
            if(instance_exists(m_inst))
            {
                m_pos -= m_move_speed;
                if(m_pos <= 0)
                {
                    m_pos = 0;
                    m_anim_state = EnMoverStates.IDLE;
                    m_inst.a_p_moving = false;
                    m_move_back = 0;
                    m_move_speed = m_move_speed_init;
                }
                m_inst.x = path_get_x(m_path,m_pos);
                m_inst.y = path_get_y(m_path,m_pos);
            }
        }
        else
        {
            if(instance_exists(m_inst))
            {
                m_pos += m_move_speed;
                if(m_pos >= 1)
                {
                    m_pos = 1;
                    m_anim_state = EnMoverStates.IDLE;
                    m_inst.a_p_moving = false;
                    m_move_back = 1;
                    m_move_speed = m_move_speed_init;
                }
                m_inst.x = path_get_x(m_path,m_pos);
                m_inst.y = path_get_y(m_path,m_pos);
            }
        }
        m_move_speed *= 1 + m_move_boost;
    break;
    case EnMoverStates.DESTROY:
        m_anim_alpha = v_increment(m_anim_res,m_anim_alpha,0,0.1,0.2);
        if(m_anim_res[0])
        {
            scr_a_move_mover_destroy(id);
            instance_destroy();
        }
    break;
}




