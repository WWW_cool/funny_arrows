/// @description v_tbl_set_line(tbl, line, column,column_max, value_1,...)

if(argument_count > 4)
{
    var tmp_tbl         = argument[0];
    var tbl_line        = argument[1];
    var tbl_column      = argument[2];
    var tbl_column_max  = argument[3];
    var tmp_value;
    var max_params      = argument_count - 4;
    var i;
    //show_debug_message("param_count = " + string(max_params))
    //var tmp_line = tmp_tbl[tbl_line];
    
    for(i = 0; i < max_params; i += 1)
    {
        if(tbl_column + i < tbl_column_max)
        {
            //tmp_line[(tbl_column + i)] = argument[4 + i];
            tmp_tbl[@ tbl_line,tbl_column + i] = argument[4 + i];
        }
    }
    
    //tmp_tbl[@ tbl_line] = tmp_line;
}


