/// @description scr_def_g_lvl_end(type)

var _def_result = argument0;

if(instance_exists(global._def_glb_var_obj_gh) 
&& instance_exists(global._def_glb_var_obj_glb))
{
    var _def_tmp_glb = global._def_glb_var_obj_glb;
    var _def_tmp_gh = global._def_glb_var_obj_gh;
    
    _def_tmp_gh._def_gh_g_end = 1;
    
    if(_def_result) // if win lvl
    {
        /*_def_tmp_glb._def_glb_show_msg_win = 1;
        _def_tmp_glb._def_glb_star_rating_new   = _def_tmp_gh._def_gh_g_lvl_rating;
        _def_tmp_glb._def_glb_g_time_mnt        = _def_tmp_gh._def_gh_g_time_mnt;
        _def_tmp_glb._def_glb_g_time_sec        = _def_tmp_gh._def_gh_g_time_sec;
        _def_tmp_glb._def_glb_g_time_mSec       = _def_tmp_gh._def_gh_g_time_mSec;
        _def_tmp_glb._def_glb_current_lvl       = _def_tmp_gh._def_gh_current_lvl - 1;*/
    }
    else 
    {
        _def_tmp_glb._def_glb_show_msg_lose = 1;
    } 
    scr_def_show_debug_message("in lvl end = " + string(_def_result),"debug");
    with(_def_tmp_glb)
    {
        event_user(0);
    }
}
