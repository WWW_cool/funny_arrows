/// @description scr_export_inst(type,section,num)

var obj_type    = argument0;
var tmp_section = argument1;
var obj_num     = argument2;
var str = "";


switch(obj_type)
{
    case "obj_a_parent":
        
        str += @"
        var tmp_x = " + s(ini_read_real(s(tmp_section),"x"+string(obj_num),0));
        str += @"
        var tmp_y = " + s(ini_read_real(s(tmp_section),"y"+string(obj_num),0));
        str += @"
        var inst = instance_create(tmp_x,tmp_y," +s(obj_type) +");";
        str += @"
        with(inst)
        {
            a_type          = " + s(ini_read_real(s(tmp_section),"Type"+string(obj_num),0));
            str += @"
            a_colour        = " + s(ini_read_real(s(tmp_section),"Colour"+string(obj_num),1));
            str += @"
            a_dir           = " + s(ini_read_real(s(tmp_section),"Dir"+string(obj_num),0));
            str += @"
            a_angle         = " + s(ini_read_real(s(tmp_section),"Angle"+string(obj_num),0));
            str += @"
            a_p_shield      = " + s(ini_read_real(s(tmp_section),"Shield"+string(obj_num),0));
            str += @"
            a_p_turn_count  = " + s(ini_read_real(s(tmp_section),"Unlock"+string(obj_num),0));
            str += @"
            a_p_double_colour = " + s(ini_read_real(s(tmp_section),"Double"+string(obj_num),0));
            str += @"
            a_p_first_turn    = " + s(ini_read_real(s(tmp_section),"First"+string(obj_num),0));
            str += @"
            a_next          = " + s(ini_read_real(s(tmp_section),"Child_IS"+string(obj_num),0));
            str += @"
            a_next_type     = " + s(ini_read_real(s(tmp_section),"Child_Type"+string(obj_num),0));
            str += @"
            a_next_colour   = " + s(ini_read_real(s(tmp_section),"Child_Colour"+string(obj_num),0));
            str += @"
            a_next_dir      = " + s(ini_read_real(s(tmp_section),"Child_Dir"+string(obj_num),0));
            str += @"
            a_next_angle    = " + s(ini_read_real(s(tmp_section),"Child_Angle"+string(obj_num),0));
            str += @"
            a_next_p_shield = " + s(ini_read_real(s(tmp_section),"Child_Shield"+string(obj_num),0));
            str += @"
            a_next_p_turn_count    = " + s(ini_read_real(s(tmp_section),"Child_Unlock"+string(obj_num),0));
            str += @"
            a_next_p_double_colour = " + s(ini_read_real(s(tmp_section),"Child_Double"+string(obj_num),0));
            str += @"
        }";
    break;
    case "obj_a_mover":
        
        str += @"
        var tmp_x = " + s(ini_read_real(s(tmp_section),"x"+string(obj_num),0));
        str += @"
        var tmp_y = " + s(ini_read_real(s(tmp_section),"y"+string(obj_num),0));
        str += @"
        var inst = instance_create(tmp_x,tmp_y," +s(obj_type) +");";
        str += @"
        with(inst)
        {
            image_angle          = 90*" + s(ini_read_real(s(tmp_section),"Angle"+string(obj_num),0));
            str += @"
        }";
    break;  
}


return str;



