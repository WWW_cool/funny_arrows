/// @description scr_a_move_back_tiles

var recreate_list = ds_list_create();
if(instance_exists(global.a_selector))
{
    with(global.a_selector)
    {
        if(a_move_index > 0)a_move_index--;
        else exit;
        
        for(var i = 0; i < a_move_tile_count; i++)
        {
            var _inst = a_move_tile_inst[i];
            if(instance_exists(_inst))
            {
                if(a_move_tile_start_move[i] > a_move_index && a_move_tile_start_move[i] != 0)
                {
                    _inst.a_state = EnCellStates.DESTROY;
                    _inst.a_back_die = 1;
                }
                else
                {
                    _inst.x = a_move_tile_xpos[i,a_move_index];
                    _inst.y = a_move_tile_ypos[i,a_move_index];
                    
                    _inst.a_state = a_move_tile_state[i,a_move_index];
                    
                    _inst.a_colour      = a_move_tile_colour[i,a_move_index];
                    _inst.a_dir         = a_move_tile_dir[i,a_move_index];
                    _inst.a_angle       = a_move_tile_angle[i,a_move_index];
                    _inst.a_type        = a_move_tile_type[i,a_move_index];
                    _inst.a_first       = a_move_tile_first[i,a_move_index];
                    _inst.a_p_shield    = a_move_tile_p_shield[i,a_move_index]; 
                    _inst.a_p_turn_count    = a_move_tile_p_turn_count[i,a_move_index];
                    _inst.a_p_double_colour = a_move_tile_p_double_colour[i,a_move_index]; 
                    _inst.a_p_mover_inst    = a_move_tile_p_mover_inst[i,a_move_index];
                    _inst.a_p_first_turn    = a_move_tile_p_first_turn[i,a_move_index];
                    
                    _inst.a_next_colour     = a_move_tile_next_colour[i,a_move_index];
                    _inst.a_next_dir        = a_move_tile_next_dir[i,a_move_index];
                    _inst.a_next_angle      = a_move_tile_next_angle[i,a_move_index];
                    _inst.a_next_type       = a_move_tile_next_type[i,a_move_index];
                    
                    _inst.a_next_p_shield   = a_move_tile_next_p_shield[i,a_move_index];
                    _inst.a_next_p_turn_count    = a_move_tile_next_p_turn_count[i,a_move_index];
                    _inst.a_next_p_double_colour = a_move_tile_next_p_double_colour[i,a_move_index];
                    _inst.a_next_p_mover_inst    = a_move_tile_next_p_mover_inst[i,a_move_index];
                }
            }
            else
            {
                ds_list_add(recreate_list,i);
            }
        }
        
        for(var ind = 0; ind < ds_list_size(recreate_list); ind++)
        {
            var i = recreate_list[| ind];

            if(a_move_tile_deleted[i] <= a_move_index)
            {
                continue;
            }
            var _inst = instance_create(a_move_tile_xpos[i,a_move_index],a_move_tile_ypos[i,a_move_index],obj_a_parent);
            //fed("recreate -- " + s(_inst) + " ind = " + s(i));
            //fed("start move = " + s(a_move_tile_start_move[i]));
            //fed("length xpos = " + s(array_length_2d(a_move_tile_xpos,i)));
            
            _inst.a_move_id     = i;
            a_move_tile_inst[i] = _inst;
            
            _inst.a_state       = a_move_tile_state[i,a_move_index];
            _inst.a_next        = a_move_tile_next[i];
            
            _inst.a_colour      = a_move_tile_colour[i,a_move_index];
            _inst.a_dir         = a_move_tile_dir[i,a_move_index];
            _inst.a_angle       = a_move_tile_angle[i,a_move_index];
            _inst.a_type        = a_move_tile_type[i,a_move_index];
            _inst.a_first       = a_move_tile_first[i,a_move_index];
            _inst.a_p_shield    = a_move_tile_p_shield[i,a_move_index]; 
            _inst.a_p_turn_count    = a_move_tile_p_turn_count[i,a_move_index];
            _inst.a_p_double_colour = a_move_tile_p_double_colour[i,a_move_index]; 
            _inst.a_p_mover_inst    = a_move_tile_p_mover_inst[i,a_move_index];
            _inst.a_p_first_turn    = a_move_tile_p_first_turn[i,a_move_index];
            
            _inst.a_next_colour     = a_move_tile_next_colour[i,a_move_index];
            _inst.a_next_dir        = a_move_tile_next_dir[i,a_move_index];
            _inst.a_next_angle      = a_move_tile_next_angle[i,a_move_index];
            _inst.a_next_type       = a_move_tile_next_type[i,a_move_index];
            
            _inst.a_next_p_shield   = a_move_tile_next_p_shield[i,a_move_index];
            _inst.a_next_p_turn_count    = a_move_tile_next_p_turn_count[i,a_move_index];
            _inst.a_next_p_double_colour = a_move_tile_next_p_double_colour[i,a_move_index];
            _inst.a_next_p_mover_inst    = a_move_tile_next_p_mover_inst[i,a_move_index];
            
            if(_inst.a_p_mover_inst != 0)
            {
                var _mover = collision_point(_inst.x,_inst.y,obj_a_mover,false,true);
                if(instance_exists(_mover))
                {
                    _mover.m_inst = _inst;
                    _inst.a_p_mover_inst = _mover;
                    _inst.a_next_p_mover_inst = _mover;
                }
            }
            scr_glob_points_return();
            
            a_move_tile_deleted[i] = -1;
        }
        a_move_tile_count = a_move_tiles_count[a_move_index];
        //fed(" - afeter back - a_move_tile_count = " + s(a_move_tile_count)); 
        //fed("******************LOOP END******************");
        scr_check_reverse_need();
    }
}

ds_list_destroy(recreate_list);



