/// @description v_map_check_name(key)

var name = argument0;
var res = -1;

switch(name)
{
    case "ART_spr_lvl_0":
    case "ART_spr_lvl_1":
    case "ART_spr_lvl_2":
    case "ART_spr_lvl_3":
    case "ART_spr_lvl_4":
    case "ART_spr_lvl_5":
    case "ART_spr_lvl_6":
    case "ART_sprite":
    res = 1;
    break;
}
return res;
