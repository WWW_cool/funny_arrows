/// @description v_map_find_next(map, key)
var map = argument[0];
var key = argument[1];

/**
        This function returns the next key stored in the v_map after 
        the one specified in the function. This can be useful if your have 
        to iterate through the v_map looking for something, but should be 
        avoided if possible as it can be slow. If no such key exists then 
        the function will return <undefined>. You should always 
        check this using the is_undefined() function. 
**/

if(is_array(map))
{
    var length = array_height_2d(map);

    for(var i=0; i<length; i++)
    {
        var map_i = map[i,0];
        
        if(map_i == key)
        {
            var next_ind = i+1;
            if(next_ind < length)
            {
                if( map[next_ind,0] != -1) return map[next_ind,0];
                else return undefined;
            }
            else return undefined;
        }
    }
    
    return undefined
}

return undefined


