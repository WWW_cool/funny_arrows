/// @description scr_a_draw_anim_common
var res = 1;

switch(a_type)
{
    case EnCellTypes.NONE:
    
    break;
    case EnCellTypes.SHIELD:
        scr_a_draw_next_cell();
        scr_a_draw_cell();
        var mult = 2;
        anim_l_alpha = v_lerp(anim_res,anim_l_alpha,
        0,mult*anim_l_alpha_speed,mult*anim_l_alpha_speed/5);
        
        /*draw_sprite_ext(spr_cell_shield,a_p_shield,x,y,
        anim_scale + mult*(1 - anim_l_alpha),anim_scale + mult*(1 - anim_l_alpha),
        90*a_angle + anim_angle,c_white,anim_l_alpha);*/

        res = scr_a_anim_reset(anim_res);
    break;
    case EnCellTypes.LIFT:
        res = scr_a_draw_anim_lift();
    break;
    case EnCellTypes.TIME:
    
    break;
    case EnCellTypes.DOUBLE:
        anim_l_alpha = v_increment(anim_res,anim_l_alpha,
        0,anim_l_alpha_speed,anim_l_alpha_speed/5);
        
        scr_a_draw_next_cell();
        
        var colour_spr  = scr_a_get_col_spr(a_colour);
        var dir_spr     = scr_a_get_dir_spr(a_dir);
        
        scr_a_draw_cell_default_ext(x,y,anim_scale,anim_angle,anim_l_alpha,colour_spr,dir_spr,a_angle);
        draw_sprite_ext(spr_cell_d_bg,0,x,y,anim_scale,anim_scale,90*a_angle + anim_angle,c_white,anim_l_alpha);
        draw_sprite_ext(scr_a_draw_get_double(a_p_double_colour),0,x,y,anim_scale,anim_scale,
        90*a_angle + anim_angle,c_white,anim_l_alpha);
        
        colour_spr  = scr_a_get_col_spr(a_p_double_colour);
        
        scr_a_draw_cell_default_ext(x,y,anim_scale,anim_angle,1 - anim_l_alpha,colour_spr,dir_spr,a_angle);
        draw_sprite_ext(spr_cell_d_bg,0,x,y,anim_scale,anim_scale,90*a_angle + anim_angle,c_white,1 - anim_l_alpha);
        draw_sprite_ext(scr_a_draw_get_double(a_colour),0,x,y,anim_scale,anim_scale,
        90*a_angle + anim_angle,c_white,1 - anim_l_alpha);

        res = scr_a_anim_reset(anim_res);
    break;
}

return res;



