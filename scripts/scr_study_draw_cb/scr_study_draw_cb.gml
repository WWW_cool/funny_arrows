/// @description scr_study_draw_cb

if(argument_count > 0)
{
    var _btn_id     = argument[0];
    var _btn_index  = v_array_get_index(GUI_controls,_btn_id);    
    var _x          = other._v_btns_xpos[_btn_id];
    var _y          = other._v_btns_ypos[_btn_id];
    var _scale      = other._v_btns_spr_scale[_btn_id] - other._v_btns_spr_sc[_btn_id];
    var _spr        = other._v_btns_spr[_btn_id];
    
    if(_spr == spr_cell_vortex_bg)
    {
        draw_sprite_ext(spr_cell_lock,0,_x,_y,_scale,_scale,0,c_white,1);
    }
    else
    {
        if(_btn_index != -1)
        {
            switch(_btn_index)
            {
                case 2:
                break;
                case 3:
                    draw_sprite_ext(spr_arrow_side,0,_x,_y,_scale,_scale,0,c_white,1);
                break;
                case 4:
                    draw_sprite_ext(spr_cell_yellow,0,_x - 5,_y - 5,_scale,_scale,0,c_white,1);
                break;
                case 5:
                    draw_sprite_ext(spr_cell_shield,0,_x,_y,_scale,_scale,0,c_white,1);
                break;
                case 6:
                    draw_sprite_ext(spr_cell_d_bg,0,_x,_y,_scale,_scale,0,c_white,1);
                    draw_sprite_ext(spr_cell_reverse,0,_x,_y,_scale,_scale,0,c_white,1);
                break;
                case 7:
                    draw_sprite_ext(spr_cell_d_bg,0,_x,_y,_scale,_scale,0,c_white,1);
                    draw_sprite_ext(spr_cell_d_yellow,0,_x,_y,_scale,_scale,0,c_white,1);
                break;
                case 8:
                    draw_sprite_ext(spr_cell_d_bg,0,_x,_y,_scale,_scale,0,c_white,1);
                    draw_sprite_ext(spr_cell_lock,0,_x,_y,_scale,_scale,0,c_white,1);
                break;
                case 9:
                    draw_sprite_ext(spr_cell_d_bg,0,_x,_y,_scale,_scale,0,c_white,1);
                    draw_set_colour(scr_a_draw_get_col(EnColourTypes.BLUE));
                    v_draw_text(_x,_y,s(1),fa_center,fa_middle,EnText.H2);
                break;
                case 10:
                    v_btns_set_param(_btn_id,"spr",spr_cell_vortex);
                break;
                case 11:
                    v_btns_set_param(_btn_id,"spr",spr_cell_multi);
                break;
            }
        }
    }
}




