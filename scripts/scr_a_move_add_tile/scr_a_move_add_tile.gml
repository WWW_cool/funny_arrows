/// @description scr_a_move_add_tile(inst)

var _inst = argument0;
/// - obj - turns
/// - stat type

with(global.a_selector)
{
    
    for(var i = 0; i < a_move_tile_count; i++)
    {
        if(a_move_tile_inst[i] == _inst)exit;
    }
    
    a_move_tile_inst[a_move_tile_count]              = _inst;
    a_move_tile_start_move[a_move_tile_count]        = a_move_index;
    
    a_move_tile_xpos[a_move_tile_count,a_move_index] = _inst.x;
    a_move_tile_ypos[a_move_tile_count,a_move_index] = _inst.y;
    
    if(_inst.a_state == EnCellStates.CREATE)
    {
        a_move_tile_state[a_move_tile_count,a_move_index]      = EnCellStates.IDLE
    }
    else
    {
        a_move_tile_state[a_move_tile_count,a_move_index]      = _inst.a_state;
    }
    a_move_tile_next[a_move_tile_count]                        = _inst.a_next;
    
    a_move_tile_colour[a_move_tile_count,a_move_index]         = _inst.a_colour;
    a_move_tile_dir[a_move_tile_count,a_move_index]            = _inst.a_dir;
    a_move_tile_angle[a_move_tile_count,a_move_index]          = _inst.a_angle;
    a_move_tile_type[a_move_tile_count,a_move_index]           = _inst.a_type;
    a_move_tile_first[a_move_tile_count,a_move_index]          = _inst.a_first;
    a_move_tile_p_shield[a_move_tile_count,a_move_index]       = _inst.a_p_shield; 
    a_move_tile_p_turn_count[a_move_tile_count,a_move_index]   = _inst.a_p_turn_count;
    a_move_tile_p_double_colour[a_move_tile_count,a_move_index]= _inst.a_p_double_colour; 
    a_move_tile_p_mover_inst[a_move_tile_count,a_move_index]   = _inst.a_p_mover_inst;
    a_move_tile_p_first_turn[a_move_tile_count,a_move_index]   = _inst.a_p_first_turn;
    
    a_move_tile_next_colour[a_move_tile_count,a_move_index]    = _inst.a_next_colour;
    a_move_tile_next_dir[a_move_tile_count,a_move_index]       = _inst.a_next_dir;
    a_move_tile_next_angle[a_move_tile_count,a_move_index]     = _inst.a_next_angle;
    a_move_tile_next_type[a_move_tile_count,a_move_index]      = _inst.a_next_type;
    
    a_move_tile_next_p_shield[a_move_tile_count,a_move_index]  = _inst.a_next_p_shield;
    a_move_tile_next_p_turn_count[a_move_tile_count,a_move_index]      = _inst.a_next_p_turn_count;
    a_move_tile_next_p_double_colour[a_move_tile_count,a_move_index]   = _inst.a_next_p_double_colour;
    a_move_tile_next_p_mover_inst[a_move_tile_count,a_move_index]      = _inst.a_next_p_mover_inst;
    
    
    a_move_tile_deleted[a_move_tile_count] = -1;
    _inst.a_move_id = a_move_tile_count;
    //fed("id = " + s(_inst.id));
    a_move_tile_count += 1;
    //fed("a_move_tile_count = " + s(a_move_tile_count));
}




