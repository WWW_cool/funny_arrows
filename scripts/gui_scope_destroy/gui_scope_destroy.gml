/// @description gui_scope_destroy(inst)
var inst = argument0;

scr_level_delete_obj(inst);

if(instance_exists(inst))
{
    with(inst)
    {
        instance_destroy();
    }
}


