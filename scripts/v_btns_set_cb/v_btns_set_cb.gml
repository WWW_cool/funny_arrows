/// @description v_btns_set_cb(id,type,cb_ref)
var inst = global._v_btns_owner;
if(instance_exists(inst))
with(inst)
{
    var tmp_id      = argument0;
    var tmp_type    = argument1;
    var tmp_cb      = argument2;
    if(tmp_id < _v_btns_count)
    {
        switch(tmp_type)
        {
            case "press":
                _v_btns_callback_mouse_pressed[tmp_id] = tmp_cb;
                break;
            case "releaseFree":
                _v_btns_callback_mouse_release_free[tmp_id] = tmp_cb;
                break;
            case "layOn":
                _v_btns_callback_mouse_lay_on[tmp_id] = tmp_cb;
                break;
            case "layOff":
                _v_btns_callback_mouse_lay_off[tmp_id] = tmp_cb;
                break;
            case "2click":
                _v_btns_2click_cb[tmp_id] = tmp_cb;
                break;
            case "draw":
                _v_btns_callback_draw[tmp_id] = tmp_cb;
                break;
        }
    }
}

