package ${YYAndroidPackageName};

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import android.util.Log;

import ${YYAndroidPackageName}.R;
import ${YYAndroidPackageName}.RunnerActivity;
import com.yoyogames.runner.RunnerJNILib;

public class GMSGoogleAds implements RewardedVideoAdListener {
    private static final String AD_UNIT_ID = "ca-app-pub-3940256099942544/5224354917";
    private static final String APP_ID = "ca-app-pub-3940256099942544~3347511713";
	private InterstitialAd interstitialAd;
    private RewardedVideoAd mRewardedVideoAd;

	private AdListener adlistener = new AdListener(){
		@Override
		 public void onAdLoaded() {
			Log.i("yoyo","onAdLoaded called");
		 }

		@Override
		public void onAdFailedToLoad(int errorCode) {
			Log.i("yoyo","onAdFailedToLoad called");
		}
		@Override
		public void onAdClosed()
		{
			Log.i("yoyo","onAdClosed called");
		}

	};
	
    public double Init(double arg0) {
        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(RunnerActivity.CurrentActivity, APP_ID);
		GoogleMobileAds_LoadInterstitial();
		return 0;
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        Log.d("TAG", "onRewardedVideoAdLoaded");
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Log.d("TAG", "onRewardedVideoAdOpened");
    }

    @Override
    public void onRewardedVideoStarted() {
        Log.d("TAG", "onRewardedVideoStarted");

    }

    @Override
    public void onRewardedVideoAdClosed() {
        Log.d("TAG", "onRewardedVideoAdClosed");

    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        Log.d("TAG", "onRewarded");
        if (!mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.loadAd(AD_UNIT_ID, new AdRequest.Builder().build());
        }
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Log.d("TAG", "onRewardedVideoAdLeftApplication");

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        Log.d("TAG", "onRewardedVideoAdFailedToLoad");

    }
	
	public double testInterstitialAd(double arg0)
	{
		GoogleMobileAds_ShowInterstitial();
		return 0;
	}
	public double RewardedVideoAd(double arg0)
	{
		if (mRewardedVideoAd.isLoaded()) {
			mRewardedVideoAd.show();
		}else {
			Log.d("TAG", "The mRewardedVideoAd wasn't loaded yet.");
		}
		return 0;
	}
	
	public double AddTwoNumbers(double arg0, double arg1)
	{
		double value = arg0 + arg1;
		Log.i("yoyo", arg0 + "+" + arg1 + " = " + value);
		
		return value;
	}
	public void GoogleMobileAds_ShowInterstitial()
	{
		if(interstitialAd!=null)
		{
			RunnerActivity.ViewHandler.post( new Runnable() {
    		public void run() 
    		{
				Log.i("yoyo","showinterstitial called");
				if (interstitialAd.isLoaded()) 
				{
					RunnerActivity.CurrentActivity.runOnUiThread(new Runnable() {
						public void run() {
							interstitialAd.show();
						}
					});
				} 
				else
				{
					Log.i("yoyo", "Interstitial ad was not ready to be shown.");
				}
    		}});
		}
    	
	}
	
	private void initInterstitial()
	{
	
		interstitialAd = new InterstitialAd(RunnerActivity.CurrentActivity);
		interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
			
		interstitialAd.setAdListener(adlistener);
	}
	
	public void GoogleMobileAds_LoadInterstitial()
	{
		RunnerActivity.ViewHandler.post( new Runnable() {
    	public void run() 
    	{
			if(interstitialAd==null)
				initInterstitial();
				
			interstitialAd.loadAd(new AdRequest.Builder().build());
		}});
	}
}














