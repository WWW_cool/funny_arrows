{
    "id": "31ffa742-44f3-469c-8fff-57a3235a2831",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "GoogleAds",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "GMSGoogleAds",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 8,
    "date": "2019-45-19 10:04:07",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "3d34413c-3c1a-4410-a49a-c8cdf4144e1f",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 9223372036854775807,
            "filename": "GoogleAds.ext",
            "final": "",
            "functions": [
                {
                    "id": "353fff14-3e41-4ea7-8e6d-fd9247225d12",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "Init",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "GMSGoogleAds_Init",
                    "returnType": 2
                },
                {
                    "id": "e98ba93d-89ba-43f5-adfd-f340f963ce49",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "testInterstitialAd",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "GMSGoogleAds_testInterstitialAd",
                    "returnType": 2
                },
                {
                    "id": "dc5ad6e0-fe82-4c87-928c-f18ef0a102e0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "AddTwoNumbers",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "GoogleAds_AddTwoNumbers",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 4,
            "order": [
                
            ],
            "origname": "extensions\\GoogleAds.ext",
            "uncompress": false
        }
    ],
    "gradleinject": "compile 'com.google.android.gms:play-services-ads:11.8.0'",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "1.0.0"
}