var vk_params;
var vk_status_update = 0;
var vk_mobile_on = 0;
var vk_user_id;
var vk_app_id = 5900777;//6156102;
var vk_ad_ready = 0;
var vk_adman = 0;

function vk_loader() 
{
	if(!vk_mobile_on)
	{
		vk_params = window
			.location
			.search
			.replace('?','')
			.split('&')
			.reduce(
				function(p,e){
					var a = e.split('=');
					p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
					//console.log(a[0] + ' ' + a[1]);
					return p;
				},
				{}
			);
	}
}

function vk_get_param(p_name)
{
	return vk_params[p_name];
}

function vk_get_status()
{
	return vk_status_update;
}

function vk_reset_status()
{
	vk_status_update = 0;
}

function vk_order(item_type,item_name) 
{
	
	var params = {
	  type: item_type,
	  item: item_name
	};
	
	vk_status_update = 0;
	
	VK.callMethod('showOrderBox', params);
	
	var callbacksResults = document.getElementById('callbacks');

	VK.addCallback('onOrderSuccess', function(order_id) {
		vk_status_update = 1;
	callbacksResults.innerHTML += '<br />onOrderSuccess '+order_id;
	});
	VK.addCallback('onOrderFail', function() {
	callbacksResults.innerHTML += '<br />onOrderFail';
	});
	VK.addCallback('onOrderCancel', function() {
	callbacksResults.innerHTML += '<br />onOrderCancel';
	});
}

function vk_invite() {
	VK.callMethod("showInviteBox");
}

function vk_install() {
	if(!vk_mobile_on)
	{
		if(vk_params["user_id"] != 0)
		{
			VK.callMethod("showSettingsBox", 256);
		}
	}
}

function vk_settings() {
	if(!vk_mobile_on)
	{
		if(vk_params["user_id"] != 0)
		{
			var par = 8192 + 2 + 1;// wall + install + freinds + уведомления
			if((vk_params["api_settings"] & par) != par)
			{
				VK.callMethod("showSettingsBox", par + 256);
			}
			else
			{
				VK.api("stats.trackVisitor");
			}
		}
		else
		{
			var par = 4096; // messages
			if((vk_params["api_settings"] & par) != par)
			{
				VK.callMethod("showGroupSettingsBox", 4096);
			}
		}
	}
	else
	{
		VK.callMethod("showSettingsBox", {"settings":8192 + 256 + 2 + 1});
	}
}

function vk_wall_post(str) {
	if(!vk_mobile_on)
	{
		var params = {
		  message: decodeURI(str),
		  attachments: decodeURI("https://vk.com/app5900777")
		};
		VK.api("wall.post", params);
	}
	else{
		var params = {
		  message: str,
		  target: "wall"
		};
		VK.callMethod("showShareBox", params);
	}
}

function vk_winResize(_w,_h) {
	if(!vk_mobile_on)
	{
		if(_w > 0 && _h > 0)
		{
			VK.callMethod("resizeWindow", _w, _h);
		}
	}
}

function vk_mobileOn(param) 
{
	vk_mobile_on = param;
}

function vk_showLeaderBox() 
{
	if(vk_mobile_on)
	{
		//VK.callMethod("showLeaderboardBox",);
	}
}

function vk_AdInit(_user_id)
{
	vk_user_id = _user_id;
	vk_ad_ready = 0;
	admanInit({
	  user_id: vk_user_id,
	  app_id: vk_app_id,
	  type: 'rewarded',
	  //params: {preview: 1} //for tests
	}, onAdsReady, onNoAds);
}

function vk_AdCheck()
{
	return vk_ad_ready;
}

function vk_AdStart()
{
	if(vk_adman != 0)
	{
		vk_adman.start('preroll');
		vk_ad_ready = 0;
		vk_adman = 0;
	}
}

function onAdsReady(adman) {
  adman.onStarted(function () { admanStat(vk_app_id, vk_user_id); });
  adman.onCompleted(function(){ console.log("Adman: Completed"); });          
  adman.onSkipped(function() { console.log("Adman: Skipped"); });          
  adman.onClicked(function() { console.log("Adman: Clicked"); });
  vk_adman = adman;
  vk_ad_ready = 1;
};

function onNoAds() 
{ 
	vk_ad_ready = 2;
	vk_adman = 0;
};

    
    

