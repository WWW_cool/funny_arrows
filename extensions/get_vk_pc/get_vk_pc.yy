{
    "id": "b6446bf6-325c-4f07-b736-6d54373c023e",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "get_vk_pc",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 32,
    "date": "2019-45-19 10:04:06",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "6fd0e1cd-b0e3-4fd6-bda0-74bf5aaa0794",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 542113896,
            "filename": "vk_loader_pc.js",
            "final": "",
            "functions": [
                {
                    "id": "ff71290c-cffe-41c7-9bc0-4bbc2834644d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_loader",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_loader",
                    "returnType": 2
                },
                {
                    "id": "58a80713-13ea-4a58-b623-2c6694f48d1a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_get_param",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_get_param",
                    "returnType": 1
                },
                {
                    "id": "c1185974-db7e-4f6f-81d6-228693b7090a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_order",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_order",
                    "returnType": 2
                },
                {
                    "id": "353fd745-847c-48b7-b120-72861e4e4fe0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_get_status",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_get_status",
                    "returnType": 2
                },
                {
                    "id": "e665cd3a-2a00-4e6a-9122-055ac5963029",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_reset_status",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_reset_status",
                    "returnType": 2
                },
                {
                    "id": "20dee645-d5ce-4e4a-ac71-3f6e89ef8682",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_invite",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_invite",
                    "returnType": 2
                },
                {
                    "id": "d482110f-26f6-4dc5-99ce-ec23b45d8d2c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_install",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_install",
                    "returnType": 2
                },
                {
                    "id": "96c2e903-dcd5-4e5a-b323-585c86ad1c42",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_showLeaderBox",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_showLeaderBox",
                    "returnType": 2
                },
                {
                    "id": "7e841121-d45b-44aa-9a47-9f07b10ddbe2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_wall_post",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_wall_post",
                    "returnType": 2
                },
                {
                    "id": "8ab6a1a3-4027-493f-8ec9-1217ec05b7f7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_settings",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_settings",
                    "returnType": 2
                },
                {
                    "id": "57549794-4253-426f-8aad-b35e4856c301",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_winResize",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_winResize",
                    "returnType": 2
                },
                {
                    "id": "1c360896-56d5-4267-ac88-3aa7adc154e7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_mobileOn",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_mobileOn",
                    "returnType": 2
                },
                {
                    "id": "6030cdf2-fc57-4ff1-b21f-21cec7a1b1b9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_AdInit",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_AdInit",
                    "returnType": 1
                },
                {
                    "id": "9da5b6f6-92bf-4837-b64f-551108cae5d9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_AdCheck",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_AdCheck",
                    "returnType": 2
                },
                {
                    "id": "34913e89-24a4-4e2b-828a-eb45e9470d30",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_AdStart",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_AdStart",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 5,
            "order": [
                
            ],
            "origname": "extensions\\vk_loader.js",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "1.0.0"
}