{
    "id": "84c3218d-2600-49b9-9770-09eb47ec3b64",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "FMS_Loading_SCreen",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 32,
    "date": "2019-45-19 10:04:05",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "46ecccdb-5423-4cc5-8713-12d33ebe93a4",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 550605038,
            "filename": "FMS_loading.js",
            "final": "",
            "functions": [
                {
                    "id": "8ed1b691-985f-4525-910a-ce1fbba6d011",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 6,
                    "args": [
                        2,
                        2,
                        2,
                        2,
                        2,
                        2
                    ],
                    "externalName": "FMS_custom_loading",
                    "help": "FMS_custom_loading(graphics,width,height,total,current,image)",
                    "hidden": false,
                    "kind": 11,
                    "name": "FMS_custom_loading",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 5,
            "order": [
                
            ],
            "origname": "extensions\\FMS_loading.js",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "1.0.0"
}