{
    "id": "f484a1d3-488b-4c82-b1f2-b61d9f1cd9c5",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "GoogleAnalytics",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 32,
    "date": "2019-45-19 10:04:07",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "81949c79-6758-490b-8ea3-967e8289777a",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 9223372036854775807,
            "filename": "GoogleAnalytics.js",
            "final": "",
            "functions": [
                {
                    "id": "db588fde-44a5-4e7c-8a88-d9350408af89",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "gmsGaEvent",
                    "help": "JS_gmsGaEvent(tag,name,action)",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_gmsGaEvent",
                    "returnType": 2
                },
                {
                    "id": "a9c920e6-7796-482e-b3e5-da2f9f8a853e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "gmsGaTime",
                    "help": "JS_gmsGaTime(timeCategory,timeAction, time)",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_gmsGaTime",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 5,
            "order": [
                
            ],
            "origname": "extensions\\GoogleAnalytics.js",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "1.0.0"
}