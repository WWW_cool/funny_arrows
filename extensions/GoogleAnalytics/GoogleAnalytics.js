function gmsGaEvent(eventTag,eventCategory,eventAction)
{
	ga('send', 'event', eventTag, eventAction, eventCategory);
	return 0;
}

function gmsGaTime(timeCategory,timeAction, time)
{
	ga('send', 'timing', timeCategory, timeAction, time);
	return 0;
}

