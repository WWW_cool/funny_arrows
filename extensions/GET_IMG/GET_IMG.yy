{
    "id": "2dcaee6b-2704-4820-994b-773b16bb8393",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "GET_IMG",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 32,
    "date": "2019-45-19 10:04:06",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "8751423f-50d7-41f3-aad6-1dedbc838e95",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 9223372036854775807,
            "filename": "getIMG.js",
            "final": "",
            "functions": [
                {
                    "id": "e5a43896-cc39-4fe2-ba33-f144c3bd46ad",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "getIMG",
                    "help": "JS_getIMG(img_ind)",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_getIMG",
                    "returnType": 1
                },
                {
                    "id": "3fb2a57b-0c75-49b8-a82e-8a249713e3fc",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "getTxt",
                    "help": "getTxt(path)",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_getTxt",
                    "returnType": 1
                },
                {
                    "id": "08dae8c1-ce7c-436c-aa55-5ac83241e73f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "readReady",
                    "help": "JS_readReady",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_readReady",
                    "returnType": 2
                },
                {
                    "id": "24a088a6-30b3-4465-9103-f7cb4290a85b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "readTxt",
                    "help": "JS_readTxt",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_readTxt",
                    "returnType": 1
                }
            ],
            "init": "",
            "kind": 5,
            "order": [
                
            ],
            "origname": "extensions\\getIMG.js",
            "uncompress": false
        },
        {
            "id": "a78c9c6f-c7cf-4baa-b508-ac113baac9a7",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 9223372036854775807,
            "filename": "getOrient.js",
            "final": "",
            "functions": [
                {
                    "id": "875b46c3-3f4a-4e9f-89bb-dbd9c35586db",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "getOrient",
                    "help": "JS_getOrient",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_getOrient",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 5,
            "order": [
                
            ],
            "origname": "extensions\\getOrient.js",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "1.0.0"
}