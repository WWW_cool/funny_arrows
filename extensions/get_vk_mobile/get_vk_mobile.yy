{
    "id": "74d3675d-e2b6-4e83-99d8-9d0841a4b301",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "get_vk_mobile",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 0,
    "date": "2019-45-19 10:04:08",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "aff4502d-2510-4117-b815-793afcf9a186",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 542113896,
            "filename": "vk_loader_mobile.js",
            "final": "",
            "functions": [
                {
                    "id": "3a26166e-66f1-4caf-9724-0ececc4a724b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_loader",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_loader",
                    "returnType": 2
                },
                {
                    "id": "544d5d00-1e19-45be-bc51-34bd3be4e66d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_get_param",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_get_param",
                    "returnType": 1
                },
                {
                    "id": "0cc04757-5428-496d-ba8a-ee88b97818ee",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_get_status",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_get_status",
                    "returnType": 2
                },
                {
                    "id": "77779a7f-13bd-4e96-b60b-db4698340765",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_reset_status",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_reset_status",
                    "returnType": 2
                },
                {
                    "id": "7233ce01-e032-4ff8-aab0-2e23569cc6d7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_order",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_order",
                    "returnType": 2
                },
                {
                    "id": "e7780031-1107-4cc1-a8c9-430e61b76bb2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_invite",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_invite",
                    "returnType": 2
                },
                {
                    "id": "d994a5e8-b840-450d-9fe1-daddecdaa3eb",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_install",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_install",
                    "returnType": 2
                },
                {
                    "id": "4c9d9fad-a9ec-4602-978f-787fad830326",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_settings",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_settings",
                    "returnType": 2
                },
                {
                    "id": "8998ee88-4f4d-4c40-8eac-6289bf73dac8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "vk_wall_post",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "JS_vk_wall_post",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 5,
            "order": [
                
            ],
            "origname": "extensions\\vk_loader.js",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "1.0.0"
}