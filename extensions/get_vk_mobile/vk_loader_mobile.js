var vk_params;
var vk_status_update;
var vk_user_id;
var vk_app_id = 5900777;
var vk_ad_ready = 0;
var vk_adman;

function vk_loader() 
{
	vk_params = window
		.location
		.search
		.replace('?','')
		.split('&')
		.reduce(
			function(p,e){
				var a = e.split('=');
				p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
				console.log(a[0] + ' ' + a[1]);
				return p;
			},
			{}
		);
	console.log("test loader");
}

function vk_get_param(p_name)
{
	return vk_params[p_name];
}

function vk_get_status()
{
	return vk_status_update;
}

function vk_reset_status()
{
	vk_status_update = 0;
}

function vk_order(item_type,item_name) 
{
	VK.callMethod("showOrderBox", {"type":item_type,"item":item_name});
}

function vk_invite() 
{
	VK.callMethod("showInviteBox");
}

function vk_install() 
{
	if(vk_params["user_id"] != 0)
	{
		VK.callMethod("showSettingsBox", 256);
	}
}

function vk_settings() 
{
	if(vk_params["user_id"] != 0)
	{
		var par = 8192 + 2 + 1;// wall + install + freinds + уведомления
		if((vk_params["api_settings"] & par) != par)
		{
			VK.callMethod("showSettingsBox", {"settings": par + 256});
		}
	}
}

function vk_wall_post(str) 
{
	var params = 
	{
	  message: decodeURI(str),
	  target: "wall"
	};
	VK.callMethod("showShareBox", params);
}

    
    

