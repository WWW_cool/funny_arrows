{
    "id": "898f44bc-a934-4ba0-8312-0788e2d1e63c",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "f_H2",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "CTC Splash Rounded",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6368b45d-299d-44e3-9d09-204fefb13cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 283,
                "y": 108
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "aa6591a1-6600-479e-b664-d2330e7d255b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 289,
                "y": 108
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d1cc370d-e1ca-45a5-85c6-f794a83fbc0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 10,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 330,
                "y": 108
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "8417e4d0-45c4-469a-b12e-ab0a3ba491a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 240,
                "y": 30
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "40f5d3c6-3332-42cf-b2fd-30913d3569ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": 4,
                "shift": 20,
                "w": 12,
                "x": 194,
                "y": 58
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0c429961-e76f-457d-8712-4b17f9713215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 420,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "00cd6572-6c09-429f-a890-2d8ce459d75a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 401,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "5901cf53-aa6d-425a-8eea-9e2e06b7f119",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 10,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 339,
                "y": 108
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d8713c14-51bb-4018-b67f-ebac5f7710da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 78,
                "y": 108
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5a478162-a371-4c5f-8a1f-cd386483c198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 67,
                "y": 108
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "bc3762e1-1095-494b-a1d4-51075832b9aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 272,
                "y": 108
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2205bc7c-c91f-4868-9db6-d01fe4997aa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 480,
                "y": 85
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9ee77a12-17c5-4684-8942-abb1e7afeb48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 265,
                "y": 108
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8dedefdb-0016-4ff6-95b1-8507fa247f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 248,
                "y": 108
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5f07d4c5-ac2a-40cd-96e4-5c2b9a1943c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 295,
                "y": 108
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1b40de11-fc35-43dd-b563-0380f4662bff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 123,
                "y": 108
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b5d6b6ab-3989-4664-9591-6360390e424a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 459,
                "y": 30
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "37aae800-ace8-4734-bdb5-94dd4b2ec5c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 220,
                "y": 108
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "0d48829f-5839-4ca3-a78a-061bee00f1b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 365,
                "y": 58
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "326e78cb-1c24-4d5a-b259-9beced63c69b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 62,
                "y": 85
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a811a132-3fad-4a8e-9cec-1f171e5df31e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 58
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1ee1caa1-6208-49c9-afc5-33a6e056ab22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 248,
                "y": 85
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "bb9450fe-ddd5-42ab-9f7c-f8675d4cf2a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 85
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "88d0f8b4-21ef-42bb-bfcd-f799cbb310a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 85
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b42cb295-4526-430b-bb6d-88400a030f95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 335,
                "y": 58
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f948ba56-46ef-444c-9083-aada04befaa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 455,
                "y": 58
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f1e967a5-bf70-407b-8cab-69a6e71c0b9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 313,
                "y": 108
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "defc5557-35d7-40a2-8e91-478fe666ff84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 258,
                "y": 108
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "330e54d9-1bf2-485c-914f-610b3043c9dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 198,
                "y": 108
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "899b688a-b031-4723-b767-8b2b07d1a61d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 163,
                "y": 108
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d4e63dfc-6ca8-48c7-862d-76d4838184ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 209,
                "y": 108
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "63f85789-3937-4346-bf7e-34981757842d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 54,
                "y": 108
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3e171441-1032-4314-b7d8-35883810b8e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "046b73bd-4ac1-4dec-a443-19a2ee865fbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 96,
                "y": 30
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1ebe054d-5093-43db-9049-b4bc1d654389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 82,
                "y": 58
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ecbcc980-7171-4253-a57c-89a9dbc30f41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 342,
                "y": 30
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "33752506-9b10-4f4a-94ec-3b6cba70b75a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 325,
                "y": 30
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4676134b-b0b1-4e99-bb78-3044001e4250",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 395,
                "y": 58
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "57fceb70-51bf-4d8d-ab7a-558cd876ac89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 332,
                "y": 85
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "ab6cad97-642f-4ec1-9931-005919d0afdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 62,
                "y": 30
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bb62eb95-cbd0-46c2-9344-5db0c8310061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 58
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "70e63e06-8e0f-4e25-b4fe-aa4e9db42651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 307,
                "y": 108
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0b8cf84d-034d-4ff2-a30c-dca2fce5dfe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 230,
                "y": 108
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2fe427ec-09d9-4c33-a829-6430434a5233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 425,
                "y": 58
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "103cde73-653d-4537-a381-3dbc475caa2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 318,
                "y": 85
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "cc0f26b6-c30e-422a-9a6b-29c618ce5191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 132,
                "y": 30
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "156696b2-cb8f-4950-b4bc-b2f2ae5d3330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 58
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "80325cf2-0b7b-4919-91b4-a1a843d5c81e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 150,
                "y": 30
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "29850c38-6e54-401b-a5f6-589535ea76c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 92,
                "y": 85
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "39251e0b-f049-43b7-a489-8282b2ebb5d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 363,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ce7d4e92-3d58-4200-99ce-02407aa8a9af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 77,
                "y": 85
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "322f558c-90fb-41d2-8ee2-a51119dba5a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 470,
                "y": 58
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3b307240-1cd7-48ce-a4db-b31499f69625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 257,
                "y": 30
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f1b21d60-17ea-47f4-ba3a-0400610036c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 58
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "77f676f5-329c-4182-b4b4-a38a95f6d4a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 114,
                "y": 30
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "143fb81c-e48d-4f25-b63c-f64bef37f29b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a2a4b495-7571-40c4-83f9-fef7b9a5d433",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 308,
                "y": 30
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b944407d-431c-4dcd-94f1-6031f9c667f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 274,
                "y": 30
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "160112bf-5a20-42ae-9130-39d95311f1ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "73b9821d-c8e9-4570-8b7c-c8dfcfe336d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 113,
                "y": 108
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1549240f-b20a-486f-9085-1558ca488d6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 133,
                "y": 108
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "037c60aa-2c32-48c3-aac2-9f2867624b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 143,
                "y": 108
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4a5031f1-32a0-4c4e-b407-fab8beca7d04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 153,
                "y": 108
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7a85aeef-a6f5-4218-ae11-f302122e535c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 80,
                "y": 30
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "721de8c5-dda3-4e58-9b90-97a2e736ba19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 427,
                "y": 30
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "669755d3-ed74-40a6-a42b-7a5b6231f90d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 346,
                "y": 85
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "18ddffd3-85ee-44ec-9437-29ecbfe1b6ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 275,
                "y": 58
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "bf709db7-ee23-41e5-9a1a-84bd8a566ac8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 234,
                "y": 85
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "89c71455-c311-4d47-85b5-8d07b3c5346a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 290,
                "y": 58
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "689477ad-fecf-401a-a966-6d9d10cb4241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 220,
                "y": 85
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e1b5bafa-3754-4c79-af40-85e7f14c17db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 89,
                "y": 108
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a299660b-54c8-49e6-9dc7-351b18814d06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 489,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "65fc0e44-6d47-4a84-b0ad-e157e98edfe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 150,
                "y": 85
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7406a3bc-4217-4bcf-81ae-f26f20e68386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 319,
                "y": 108
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "badea196-c9c3-4113-875f-6d27a1fd536d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 240,
                "y": 108
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "0e1602f8-2780-4eec-b58f-ba2678a8d9a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 108
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "224edd04-f6be-4d64-876e-c6fb06b5c72a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 301,
                "y": 108
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "480620bd-960c-457b-bb6b-ef9fe70f8734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ab8fc3c5-26bf-494d-8925-86a40ee0776e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 206,
                "y": 85
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b10d9c12-b884-49a5-aee0-c0ef6464f1ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 440,
                "y": 58
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "98ae8f4b-89c5-45e7-b34a-d7ea4a69ec36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 30
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d44927d5-eeda-43b7-b0f6-90df7fd88181",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 47,
                "y": 30
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "47c7d89e-9629-4d7e-8cac-d7a8cf19c347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 176,
                "y": 108
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c769549a-8e0e-4807-a0e7-23b4bff133d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 108
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0130087d-d84c-4578-997d-4f8e40a67786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 101,
                "y": 108
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b410c195-1b89-4baa-87fa-7857c00b1e9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 122,
                "y": 85
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "237a0652-e717-4910-afbb-7a8a22b6b9b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 17,
                "y": 85
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f59d3c4d-9828-4fc2-b13e-c08084a21476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 343,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "43e70896-7f80-4b16-8d9e-f1616c942cbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 374,
                "y": 85
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a29e6b28-1f60-4beb-b782-2cfa4d35d4f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 17,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "213ec027-b646-44d2-a39e-00efc4514fdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 415,
                "y": 85
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a3b9b676-e7fe-49f6-aa98-c696cde613b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 221,
                "y": 58
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9c25ef8a-0dff-453e-945e-901ba003dfa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 325,
                "y": 108
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "91091643-3f73-4a54-b6bb-dbd6d42ca965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 247,
                "y": 58
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8dd0e53a-f7bf-48e3-aff6-437e5226e272",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 234,
                "y": 58
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "0da84ffd-8280-4759-9fe6-1b78ea71dc14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 208,
                "y": 58
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "741609f5-01ee-4e49-8232-79a8ee3706b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 168,
                "y": 30
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "f9249e8a-2120-4c47-b7c7-abebfb752512",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 192,
                "y": 85
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "31249033-1004-473b-9e68-0927e95d016a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 66,
                "y": 58
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "3c66fb20-37ed-4fb2-a8a1-124350c5b9b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 454,
                "y": 85
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "32952c04-38f3-490e-b0d7-5986b45dd7d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "65b25381-a157-470f-bf61-a024643418e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 320,
                "y": 58
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "711493a6-fa9c-44ce-98d4-93359fc4316d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "f96374b0-0b93-4785-9920-0429bff5b52f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 47,
                "y": 85
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "26800bb4-09ca-477c-8c0b-1489769970ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 443,
                "y": 30
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "d49cc005-60dd-4885-83f5-2cf74fa369cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 146,
                "y": 58
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "59a27a1a-904b-46f4-8448-30b541dcf787",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 485,
                "y": 58
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "f898497b-9c98-4200-8ba0-a570668a1655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 376,
                "y": 30
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "593c7746-b96a-47fc-8c23-068e692ade85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 21,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 204,
                "y": 30
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "a26f79ca-a53b-4b99-98b4-c832564a41ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 58
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "bd8cefb6-523d-4423-a2c1-ce0bd6175931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 21,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 222,
                "y": 30
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "9ea844b5-a49c-410f-aae7-4d6522e5b8de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 58
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "5bfe0f7f-8e2d-4571-ada0-ac59c1253fd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 107,
                "y": 85
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "af37e518-6832-4015-b4ea-5ae05e1407ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 410,
                "y": 30
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "cc2c3cf7-87ca-41f2-9fc3-84b1becf4cb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 291,
                "y": 30
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "8964a9f7-e778-43ce-8304-740e024ccc16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 475,
                "y": 30
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "4739a6fa-7000-41d7-aae4-b76b58554ec1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 21,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "2ea1dcdc-83d2-4311-bdaf-345396570663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 393,
                "y": 30
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "bfaa98a9-1c51-4584-9621-1c7772efc7dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 25,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "72defa4e-7053-4858-b8fb-1ec34020d4ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 260,
                "y": 58
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "5dd8f9e3-0387-4ea4-82a0-d0eafe988ce9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 21,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "765f1500-2940-4006-b0a8-5e2c71ff35b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 25,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "ec564ab1-78c1-4160-a779-fde3c058466b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 382,
                "y": 2
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "69674cea-724d-4084-9ba1-2fe49e331dae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 21,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "9ec7ae67-1c05-4dde-9862-54bd63c1c64c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 136,
                "y": 85
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "89651d35-bfde-4005-b179-2dd7e230f069",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 162,
                "y": 58
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "68c46ebd-ca74-498c-8f33-288f8c76ad69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 21,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "4db363ce-2caf-4407-b39d-59c3817053ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 305,
                "y": 58
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "be2da8d3-efb0-4b22-8f27-af8d2c2e49f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 262,
                "y": 85
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "a71ae1e1-5bb9-42f3-a3a5-a54a8c2bfde6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 410,
                "y": 58
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "011c6c79-efd7-4b01-be5f-165d471cd3e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 28,
                "y": 108
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "6225b8ca-d04a-48ac-8689-209c82553bb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 187,
                "y": 108
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "0d87fd32-6555-4408-bc5f-68a142ad299d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "23638dd4-7b8d-4742-8ad6-c5243987d0d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 178,
                "y": 85
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "06356f12-983c-4ca7-86b8-f5b9feaa975a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 323,
                "y": 2
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "d0d672c1-13a1-4376-adbf-e822f628bf27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 428,
                "y": 85
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "bbbbf0f0-8bf5-4121-98da-164796e7466c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 304,
                "y": 85
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "460466f8-90bd-4454-8a2a-056f6b5d5538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 164,
                "y": 85
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "de14a653-ccab-4e35-8ef8-2878771c36a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 467,
                "y": 85
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "ac903882-6ff6-4d16-a3be-9bb7f196e324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 178,
                "y": 58
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "a4bd5fa7-936d-4d58-bf33-c562c630bdac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 491,
                "y": 30
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "c4f0dc3f-6fb6-45f2-9cf5-04db8ac21358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 360,
                "y": 85
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "1c6758c7-1949-47b1-ab40-ff4aacd38447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 350,
                "y": 58
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "424d8bf9-5195-4165-8273-65542d93d206",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 290,
                "y": 85
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "70fb81d4-47e5-488f-bbf0-370f130aeb1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "a28a3eac-4bc6-4ac4-a600-fdd41dc22f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 276,
                "y": 85
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "132eb411-3bec-4eb5-99f3-c468c3c158bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 380,
                "y": 58
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "09196459-d37d-4166-bbc0-844bda9e1165",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 474,
                "y": 2
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "b9db4932-47d3-4e56-b680-6213cb098d28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "e85151c9-b191-4c62-bbdc-cf08cd59db8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 388,
                "y": 85
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "8c37f272-4511-4628-91ba-65c0109bbdf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 458,
                "y": 2
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "1f43362d-7b56-41c0-8f49-a2657b169918",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 41,
                "y": 108
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "913356de-0aa8-4077-9b73-12e12dfccdae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 21,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 261,
                "y": 2
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "f5967bc5-e047-42de-8fb0-60d5e850cb33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 25,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "6a93ef5c-52ba-46ba-8a26-139e9fff9744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 359,
                "y": 30
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "848ec4f4-af3c-475a-a99e-c9b94afe9b31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 21,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 186,
                "y": 30
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "50be6628-c508-45b7-a344-185208372aa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 441,
                "y": 85
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "f6355d37-2a3d-4159-9779-b22bd18d901c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 493,
                "y": 85
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "a9d5f900-8a80-4a6b-b254-2051e1497d96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 21,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 303,
                "y": 2
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "4a6a6e2f-bb20-4d2c-b90c-28e9b3fa65a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 402,
                "y": 85
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1040,
            "y": 1103
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}