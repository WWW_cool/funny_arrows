{
    "id": "bf5fb568-33da-483d-9246-eb8063862f8c",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "f_gGUI",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "816b46ae-9bfe-4e12-8583-b901ae73a57c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 188,
                "y": 72
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "debe7d6c-74dd-4573-bc06-70c01b8e8cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 208,
                "y": 72
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3edce829-31ff-40e1-9d76-7ef419ce80f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 58,
                "y": 72
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "8e94e236-c4ef-4fc6-8832-ad11b63f5eef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 72
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d5a7af29-ce86-481d-bdd3-0950cdaa97c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 122,
                "y": 58
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7cf6df4a-f68f-4e2d-b2a2-e9ddd3bb559d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 12,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "29a9f9ff-9a92-46e1-84c1-5ddcb18e81e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 68,
                "y": 16
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8d8c0c28-5739-4209-b4b1-0638da745521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 7,
                "y": 86
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5c33fc7e-6ab6-4e4a-8857-7242127a4f31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 70,
                "y": 72
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d3f1b41c-3f3b-40b0-8f77-5111cfb09aab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 46,
                "y": 72
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e133c124-38d1-4c8e-bdff-747672dbec0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 52,
                "y": 72
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f8f5170a-8990-4b1a-a446-424e94cd4fe4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 242,
                "y": 58
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9bcb3ea9-2ebb-41b9-ae04-49347355e6e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 218,
                "y": 72
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d5598315-4812-457b-b62a-a6e655dd5298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 76,
                "y": 72
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0aab7f29-784e-4292-ad44-187dbae29bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 11,
                "y": 86
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "cd8ee30f-c9e0-4eab-aa57-9da9068e5b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 64,
                "y": 72
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "67b34bfe-9f30-4bd0-9bf2-b78b7bae5a3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 210,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b8eb9c2b-0872-4661-a38b-a7ac95e03422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 40,
                "y": 72
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "588d8fee-5254-4c82-95ea-83e465916059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 58
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a689e0aa-799f-4202-8a6e-8d63cfa50d56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 58
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6bc54a76-28c8-4dae-9972-6bb830716561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 146,
                "y": 44
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7bd91d11-aeca-4fd8-93f4-ff86b4af3a5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 58
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0a245ebb-3d64-4a49-aaf3-e7abb0073ea5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 58
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0e99c968-2330-4dfb-8fa9-37185e197dd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 58
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f9c89b21-5526-452e-b259-247ee5a729df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 58
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9b2b62f3-2bcc-46cd-a9ee-f10a5077f764",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 58
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b7723561-35cc-4a75-b8fd-4d19ee15b387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 15,
                "y": 86
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "abd271f8-f28b-4c7d-9c05-1a6436d7e1e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8ecdfb29-316a-4b59-a2ee-90753ee9143a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 58
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d96019fb-82e7-4391-8aa3-35feb829920f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 58
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6cf0784f-1e24-40a5-b4e1-a590b1021391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7c66be28-794a-46d5-9ac2-ff009c86f197",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 242,
                "y": 44
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c06e2c56-ea86-4f95-a303-7595dc07d60e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 12,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "855ba04f-be1a-4312-ae73-c3837093cb4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 12,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "735feae0-1d49-46cd-b5d7-c03d04d9999a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 124,
                "y": 30
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "76985f5b-c39b-4245-9564-5bdab122770e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 32,
                "y": 30
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9b448cbc-5d0f-431f-96b8-3cf273d0de6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 30
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c9d33b95-dfda-4993-be9e-fc9f6720460f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 115,
                "y": 30
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6002d930-a8b0-4d17-ba4a-402275b21879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 142,
                "y": 30
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7756ce19-96a0-4b19-8328-749a2072f62a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 30
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1398baf6-f476-4ef2-bbb0-6f4ae3b54acd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a19fe2a6-5402-4839-9219-14941d2ab8f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 35,
                "y": 86
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f4042535-2f00-4b04-a996-daffcbeb0fc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 26,
                "y": 72
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "503a7d3d-8c70-4f12-a9df-7a9fa3ec0604",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 238,
                "y": 16
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "cf403d1e-1388-40a1-9e1f-df2a47be1fbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 226,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "663e421b-9e85-4bff-8db6-531946276cb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "38373873-2bb9-47a3-909f-3748d064935d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 158,
                "y": 16
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f5c9ae29-c299-4348-9a33-737d972252a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "58e8ebac-d683-4761-a254-e96581332116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 133,
                "y": 30
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "85b8bb66-65da-47e3-8691-ca935713faa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b01550a2-2983-4f08-90d9-6ba6336c4929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 98,
                "y": 16
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e88d7330-d116-40f0-9b2e-99ecaf5cad9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 169,
                "y": 30
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a29132e2-3847-4862-a8ad-42e1a201416a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 160,
                "y": 30
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "500f0a95-b80f-421b-b59b-83960ce1e43d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 108,
                "y": 16
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "743a295a-e5de-4fa4-aabd-0e1bde595a36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 118,
                "y": 16
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "39834b53-014e-4a34-b30f-bcdf28c258ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 12,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "27a5be01-ad8f-4ce1-8ee0-a2251a0c59e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 208,
                "y": 16
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c4f28c05-9b9a-4e47-a15f-fcdedc101301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 168,
                "y": 16
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "20f2f5ca-6327-417e-8995-c447d5a992cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 97,
                "y": 30
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7e6daf86-a1f1-4101-a837-d81ae3d4a7c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 248,
                "y": 72
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "810b8b5d-ebb8-4b97-acda-372c9fb87d23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 160,
                "y": 72
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "030e2fa4-0b13-47d2-bb4d-7211bcd96c3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 243,
                "y": 72
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "27186df0-3076-4721-95ee-133471e81ea3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 33,
                "y": 72
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "874fe8f2-8b0c-4574-967b-690f1ee1f00f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 198,
                "y": 16
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1d6878c7-8349-46ba-91a1-18a3423a37c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 238,
                "y": 72
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6f64dbea-ca54-4963-843b-1164ca04a8a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 186,
                "y": 44
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4eca4643-6e6e-4157-aa70-b3a8bffc88ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 178,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e3ca2ff4-f634-4411-b8b3-9636e926062a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 170,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "cf06f7f4-d647-49cc-84f1-693ea07fecd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 162,
                "y": 44
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1a07dbb1-937a-4346-9bf2-9596c5e6b83b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 154,
                "y": 44
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d3643cc2-f9be-4034-a99c-3d5bc76f6457",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 148,
                "y": 72
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "adcafac7-54a7-4db8-a25b-c0ac3ba37ddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 58
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b6b337e8-1095-40ca-be2f-83e0570432f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 58
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c5d38dec-a0a6-4c19-85cd-f1d9fa15626c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 19,
                "y": 86
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "157fd210-83b3-4107-9a6a-45d1994f12d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 12,
                "offset": -1,
                "shift": 2,
                "w": 3,
                "x": 228,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "699c59b6-9a57-4442-b7be-02ff93de31bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 210,
                "y": 58
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "81df8434-722b-4b67-8ab8-7fb00dabc70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 23,
                "y": 86
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ede9ec99-af8f-4e5d-be39-7b8c97becf79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "95dbea3b-6320-47c6-98ca-c2ac71eed5f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 186,
                "y": 58
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "97d4ab47-74ce-4451-857c-c8e659e0d9ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 170,
                "y": 58
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3d545cba-b563-4f34-9627-c703c92756a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 154,
                "y": 58
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5bd7228a-4169-40e3-af0a-b852d796ab1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 130,
                "y": 58
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b250265b-a8b5-4b3f-a402-3b4a80b199b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 172,
                "y": 72
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d904772b-c095-43b4-8be7-6a8ce63478ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 178,
                "y": 58
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c8bfe8eb-41d4-4bbc-b035-725f78576a10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 223,
                "y": 72
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "827ac204-9df3-4f2a-a61f-f2821c0ab06f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 162,
                "y": 58
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1761c52f-c48b-429c-b38e-036b58630bd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 194,
                "y": 58
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "bd697524-252b-4b14-a6ae-ba93a1c5ff9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 138,
                "y": 16
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "08147087-9610-4379-af7e-93262f513cf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 218,
                "y": 58
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f81ee00c-6d83-415a-b62c-f7b6236bb288",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 202,
                "y": 44
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "dcbf08a9-859c-49fb-912b-bacee1b27ce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 122,
                "y": 44
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "eb90e392-b2eb-402b-b76f-d55d4f65fc54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 82,
                "y": 72
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "95db2c0e-07d8-4669-a52c-e4d073e62bd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 39,
                "y": 86
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e4a877ec-ebd0-4157-bf3c-ac77b32ffd29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 100,
                "y": 72
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4b59c07f-ce88-401d-965c-ed60fe8a371a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 44
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "6f5c905b-dbc8-4002-93b4-adecb698bc17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 0,
                "x": 45,
                "y": 86
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "2784e9f5-2a6e-46ae-803c-2c95e442d395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 27,
                "y": 86
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "4694ad49-ee78-46db-ba4e-d7246ae9694f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 194,
                "y": 30
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "6417c576-e99c-479a-8005-99749e039ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 202,
                "y": 30
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "9fdec36a-b955-42d7-9c4c-c89c87a24961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 210,
                "y": 30
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "8a7ba0c0-0bc6-4fbb-bdc4-0b16d2bc61d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 12,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 148,
                "y": 16
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "3cbb6854-9360-4e55-b35e-f0f11a62c063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 42,
                "y": 86
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "a5244e72-e945-40b3-bd92-a720126438c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 226,
                "y": 30
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "07df36b1-3c53-49b6-8cf9-d33c34d96388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 112,
                "y": 72
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "15b1c309-43af-4d07-8231-e77c544687b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "109bb5ed-5a96-4385-8072-ea7453faa2f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 118,
                "y": 72
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "0f3615a7-6d1a-449b-87d8-9db40afcb6e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 242,
                "y": 30
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "8b619e07-3431-4b27-988c-c8fc2aac5e4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "c2910a4a-bfa7-4998-b8d1-56ab216e3d71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 130,
                "y": 72
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "4b4c2253-82c6-4b58-97be-0f27f07200be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "25f1d8ae-044a-4201-bddc-35ccfb3de457",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 12,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 128,
                "y": 16
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "2c820c39-aa22-484d-8b0d-d34150a1b8a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 166,
                "y": 72
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "2bb98d99-f334-4759-b0a7-ffb979a7a09e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 44
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "21af0261-73bd-4586-973c-e244f739f384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 136,
                "y": 72
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "737e2ac8-b571-4523-8724-a041e48470ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 142,
                "y": 72
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "c33884b5-e5f4-4fbf-b00a-132fed476d51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 213,
                "y": 72
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "18dee38b-3d9f-4870-8336-b4ea5286ddeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 44
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "c0df6f0b-7b3f-4fc2-b2a7-c3399f632c79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 44
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "61384465-d222-4557-a127-c9426c49625e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 31,
                "y": 86
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "ae969271-7e11-48fb-b4be-d3191838ea2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 203,
                "y": 72
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "3e77eb02-6723-42c6-80e6-59b0c8cf2988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 198,
                "y": 72
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "4a0039e8-fb5a-4fe1-a3be-473a07588afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 154,
                "y": 72
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "41d08556-3818-4586-a346-12ff54c997c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 44
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "d3916445-60f4-40e6-a9ef-6c30d0067de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "16d787cd-c50c-431a-9e12-b5e3109e78d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "b5eafa6a-035f-432c-94d4-1642f803058b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "141fc4b3-0da9-46e6-bbb7-756686fe40a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 98,
                "y": 44
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "ab182580-de54-4c8c-b56a-3639fd6c3a31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 12,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "ae5ab016-3cfe-469b-940d-c57a16061a99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 12,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "dcb38861-d8ba-431e-b501-204b9ad04b09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 12,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "5b8c5e77-1452-4377-9ad6-9c1b841d1290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 12,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "3e45bc0d-2b38-41d8-8b18-f69063a4c285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 12,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "75b8aa2b-0950-44e8-a0cf-54ee2a779a7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 12,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "b2e55757-bab6-463e-a0bd-b1ba5338b1b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 12,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "06af2ee7-65eb-4930-81db-e8ce979cdc4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 42,
                "y": 30
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "867c19f1-577e-4d93-83c8-10433611f420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 88,
                "y": 30
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "3b548c4e-5b21-4c4a-b887-2a256e65b545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 106,
                "y": 30
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "cb00bb91-4275-4320-9da9-dbe9e2bf43d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 61,
                "y": 30
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "adfe3af5-30cc-4128-83c7-b824b370564e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 151,
                "y": 30
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "a9490d86-e038-406c-b293-e16ec9f8934b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 193,
                "y": 72
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "f87b5fb7-916b-4306-8bbb-c978f6a696ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 233,
                "y": 72
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "671931ea-3341-46a1-b16d-2bf7daac3c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 88,
                "y": 72
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "6971277b-ae85-4e5f-915a-8d0e37d7c61c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 124,
                "y": 72
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "a332c857-9959-404d-9a2b-3983dd6c1829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 12,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "fec96668-61cc-46cb-98bb-fcd64e980fc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 218,
                "y": 16
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "9390bafc-7f7c-42b4-960a-127970bbca05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 16
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "b4d030a4-1615-4121-8c01-6630210e7bc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 16
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "83ac9b6b-dd13-4f61-98b2-5938a65f0bfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 16
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "469b8430-f7bd-468f-bfac-80a14e55e052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 16
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "29462825-68d4-4a5c-ac68-64a39f754940",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 16
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "5e8ecb5d-7ddb-46fb-974c-d7ac5e0a176e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 44
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "06f3c732-04f9-46bc-b15b-716b6939db61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 16
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "8d97a96b-325a-4b56-af02-ba5156ec2dc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 78,
                "y": 16
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "6c5e2148-547b-4afe-8fd2-a13a920118e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 88,
                "y": 16
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "7303323b-722d-4761-96c0-40e9cd8e3e5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 228,
                "y": 16
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "49cd3a66-cb67-49ac-85a8-b35191f3c985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 178,
                "y": 16
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "a0926ef0-1146-4886-9883-c1226beae043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 188,
                "y": 16
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "01b7da69-9d92-4d6f-986c-ca267c8c9c16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 79,
                "y": 30
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "13a61e1a-19ac-4c14-9e82-1882c27c6ded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 70,
                "y": 30
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "e3a7d3d9-052f-4ee5-b101-38107ee30689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 44
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "f65cbf01-f190-40e7-a81e-0504ddd271c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 44
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "a569ff5d-4ed9-4e61-853e-690b0cf0bb0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 44
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "d796be6e-5b77-4605-9487-e34e67eece4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 178,
                "y": 30
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "c2f9a502-a42a-4f1a-970b-2d10baa05e15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 234,
                "y": 30
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "7ccdaa45-7237-430a-a7ad-cf986c5b8914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 58
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "a4a41a35-02e6-4799-ae43-84006883e07b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 12,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "b623e5fb-fc47-4840-b81f-e61db98dc6a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 44
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "40e75060-7eba-4249-9948-67eb39d8b42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 218,
                "y": 30
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "49350e19-cfcc-433b-87a9-f54bd1e45f2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 44
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "a7ea8afe-1fd6-4e79-a734-6f4169cfa62d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 44
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "9277b008-302b-4f28-9178-9455499c999e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 44
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "dbbb4b38-a5aa-4577-a215-ba0db37f893d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 178,
                "y": 72
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "65fadd61-64e8-4a1c-8982-e8eb22642ea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 183,
                "y": 72
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "52e6b55b-2859-4caa-af61-968efba24324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 106,
                "y": 72
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "d1957759-3a22-4f6f-a50f-ab6ecab152e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 94,
                "y": 72
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "eb3f007f-fdf1-434e-8d69-f922b019a40f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 72
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "f35e5932-b64f-4c07-b56d-2d52de926fe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 202,
                "y": 58
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "2d10d195-c2cb-4c30-bada-b3b3cc106da2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 226,
                "y": 58
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "fd00310a-dd50-46ac-9795-cc9a8bc7221b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 146,
                "y": 58
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "c32aa6a8-8d9d-4eca-992d-fe51801cf86f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 194,
                "y": 44
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "188f41a2-ed11-4231-aa0e-45bc996c5f57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 138,
                "y": 44
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "e78d1d91-5725-496d-8c7f-2cd9a098dc8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 218,
                "y": 44
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "1a60f360-b7af-490c-bae3-a9af27014fbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 234,
                "y": 44
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "50f27257-f003-4f15-bccf-54255c522a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 52,
                "y": 30
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "04714c75-9689-4851-af8b-2dac09a6a093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 58
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "d313acff-952b-433a-ba06-2d126ad07ee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 58
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "bcbebf5d-de7b-4d15-8089-569ed8795bad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "dc35ab2a-9e61-4f1f-a784-2fa4aadeb0d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 138,
                "y": 58
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "a0b7995e-615d-4737-9737-b4b0636edbf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 234,
                "y": 58
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "ec4d3bda-38f7-4a66-a76e-0794eca4ab6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 186,
                "y": 30
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "654a179f-3c49-4a78-bcd6-cdeaa22779ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 130,
                "y": 44
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "992172a0-ec07-4b25-8a2d-88482df06b3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "25a7669a-2011-4578-837c-6489e34f9321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "c4b072da-a6ec-47dc-9535-7e1865f0ace7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "1ee088b4-4e3e-4440-929b-71408e587493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "93f9f54f-edbb-4d9f-9c7e-03304ce9ad53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "5069d116-60d1-4a00-b717-2cdf29c46d90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "c59ce1a7-1514-4d8f-a8ac-7830e50d2d01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "18705d7e-7f0a-4a23-9e83-93a3225a4bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "7c688b3d-1802-4913-a667-9e7dbdf35634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "22822402-3dfb-41cf-8127-cd0ecfc56f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "9079e789-4c23-4dd7-9eae-1cfe11c5af7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "d9a191ae-9968-4b4c-bde9-68a2f92365f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "7a99a469-56ba-4208-9647-fd0dabbc3860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "80b6a150-b966-47eb-8352-f6f22ee89d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "72bc7412-05ff-4d96-af46-772e43dab2d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "ea1f8234-e515-4232-b16f-f20d571b921f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}