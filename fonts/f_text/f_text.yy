{
    "id": "e081bd54-f567-470a-8215-e5e8d8fc4e51",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "f_text",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "CTC Splash Rounded",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4531c968-82e9-4856-ba6b-29b5cda9c014",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 163,
                "y": 119
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8f1c602c-b0a6-4f62-bad6-d363c4d15bf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 203,
                "y": 119
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ed8d4ce1-daa5-4411-ab58-035222bcdc3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 8,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 212,
                "y": 119
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d982c0fa-3eca-4131-b7a9-7d275d2e9442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 240,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "cfbc2738-6e2e-4484-9664-0719da3c6b06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "727a439c-ff17-43fc-8684-1e4d3e399bd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 121,
                "y": 22
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3c819853-1d06-4951-a94e-f9d4ee5873f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 160,
                "y": 22
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e8f90b4e-380b-4ecc-b99c-7065542ab68a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 8,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 218,
                "y": 119
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "61eb387a-f355-4188-a730-e9c71c0676b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 64,
                "y": 119
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "bbf9960a-75ab-4f5f-819b-4faccff6e3d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 72,
                "y": 119
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "822b3887-1b2d-49e0-9739-a311a58aeebc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 147,
                "y": 119
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8c1c30be-e8e8-474b-be25-86417f91b1b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 72,
                "y": 99
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9ca8a0f1-fe26-402b-ad71-c42b19755be4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 173,
                "y": 119
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9ee9de94-5b19-423a-aa22-762542e058b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 156,
                "y": 119
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0718d515-dc94-4e7e-998a-22f0a485ff6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 183,
                "y": 119
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ad68c23f-991a-4edf-8cc3-19d286962e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 119
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "edd3d949-c887-4f56-9ba4-b920fe4f8acf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 131,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c0fc039a-5aff-4b85-8434-f2ef368e60be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 80,
                "y": 119
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "28c6a584-051d-4de3-9393-bee51a7c20ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 219,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8fa72c5f-260f-4e04-b4b1-4e2b14b1e844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 156,
                "y": 82
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f5876282-0547-4b07-8562-b1858e9a9468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5f4ed28d-e716-4cdf-aacd-e64bbda343cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 102,
                "y": 99
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "72acc076-6a1b-48f4-9a9f-8034994725c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 112,
                "y": 82
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e0b025b8-7880-44b1-8c46-8beb95be614c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 82
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cdd48c6c-5c9d-4a9f-a7ed-7654cc86a2fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 82
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "21ed0a3e-8bc2-404e-a831-1bb343a55cc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 76,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a8bd970e-988d-45be-ad94-2bc79dfd4152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 178,
                "y": 119
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b3ad8c1f-01f8-4339-8989-ada10b474616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 168,
                "y": 119
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5fb8df1a-4a5b-4ec9-aca4-293a35c3f88b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 104,
                "y": 119
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8b255bff-8847-408a-be4c-5f6b8e45da41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 54,
                "y": 119
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5af69e25-9ae3-4956-b867-d077ec4d1616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 88,
                "y": 119
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "025a3ba3-67e7-43a4-a43f-86be12b69886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 142,
                "y": 99
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f699e0ac-c877-4a23-8a8a-adebed53a3f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "51df622d-aa3f-4193-91b2-727543400afb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 56,
                "y": 22
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d48cfa9e-0a1b-459a-93bb-09ca4e25c6b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 153,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "95491d92-515a-4713-b4dc-ce875f1b0837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 204,
                "y": 42
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "408c747f-c4d3-4034-9486-3309532c9430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 168,
                "y": 42
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "889ce63a-b6aa-4214-9400-838ac5035607",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 82
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "47e0e916-51bb-4f3b-a536-bdb45dff204e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 182,
                "y": 99
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e4e64677-2efc-4fe1-8158-4d447f5eb359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 228,
                "y": 42
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ee30102e-fcb4-4b29-85f7-6f848a80edaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 120,
                "y": 42
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "73c977b7-c4bd-4471-afc0-979c5baf3b2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 193,
                "y": 119
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "68d05705-8a54-427a-891f-fa1c4944ef25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 134,
                "y": 119
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c7db34de-0ee2-497d-bcc5-7f4c2219a1ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 208,
                "y": 62
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e3734491-0ec3-4771-b417-5d6cb0759016",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 192,
                "y": 99
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "635c0ad4-e1fa-436f-83c1-b0164e9d65fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 108,
                "y": 22
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3b8cf7c7-455d-475c-9424-3c97503f6c90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 132,
                "y": 42
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "05f50038-c47b-4692-b0ba-7a64f3ad7e89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 95,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c14356ba-5da6-4271-b1a4-e3fd69de193a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 98,
                "y": 62
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6e2a1038-7c6d-4f36-ae56-a7f5746d26d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "79165150-fb25-4dcd-86e0-033bf2bec650",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 87,
                "y": 62
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d432d66c-36bb-4b84-8781-9da72787d618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 82
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "baaa9e69-a9f6-4d55-8752-3d54a1273924",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "48230405-c8a9-49ca-840a-e493972a936b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 197,
                "y": 62
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f07f7246-a815-4034-a973-1bd7297d3803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 134,
                "y": 22
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "68d4730f-05ac-4e5d-a98c-30dfae856619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9506aa07-3c71-45e0-9ddc-ec3fd7fca516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 84,
                "y": 42
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "141d2ec4-787e-4398-b148-05dba3e9b756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 180,
                "y": 42
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "dce159f7-203c-42df-b9e5-da4b3de1cf3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ad8ade3d-7927-4127-86b0-e79e9570452e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 120,
                "y": 119
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4068c083-1a6d-448e-ab89-d7f708a4f521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 242,
                "y": 99
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c7b38559-3bb0-40e1-89bd-73c7ef970c7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2d624b68-8977-443c-b1ef-2a40e6fffe10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 127,
                "y": 119
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8b35ef1a-3a9a-4db8-b962-51b4e1abc3ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 62
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ba785865-5b96-491c-bfdc-276baa3d8ece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "18f3c370-ae84-4263-9224-082a1b694ba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 209,
                "y": 82
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5256c0c9-2ea0-4b82-9815-518373ec59aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 120,
                "y": 62
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7b0887a6-b995-4ab9-b18d-f73cbbb181df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 112,
                "y": 99
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c9bb94c4-ef93-49b5-ab3a-467e81b922ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 82
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "897a187c-818c-49cc-8951-596a5381864d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 82,
                "y": 99
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "879075e3-1462-4c7c-8d04-df170cf31c27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 18,
                "y": 119
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b8fab9f8-1288-448a-9e93-f62740b82f90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 230,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "80c940f9-b8b7-40ac-be8c-4ac51444fdc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 232,
                "y": 99
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f27180a0-d70f-4f88-8a6f-1c2043e67603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 188,
                "y": 119
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3439cc53-ece0-4ead-818d-8666c2def889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 141,
                "y": 119
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "193fd469-e7d2-4d1a-a56d-fd43912225ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 222,
                "y": 99
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d4996e2e-affe-4b38-95d2-c279ed7aa115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 198,
                "y": 119
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c3ab8c64-7f04-4904-8d9c-11824c827893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3a3801de-4229-497b-8091-af6c2ded2238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 92,
                "y": 99
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "bd692736-a9d4-4538-9bc5-a685485c467c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 82
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b054bd36-cb85-4854-bc29-dc84c49a9457",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ae95d817-3326-413f-a02c-229e4eb28e06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 241,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0b5689c6-e69d-4b2c-b73d-3b0ff1683659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 96,
                "y": 119
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8d4667bf-226d-4af8-8194-b648323c5952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 36,
                "y": 119
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e62ea69b-54e1-4e21-b2b5-cf47705be006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 27,
                "y": 119
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3a1e11b5-2084-4050-bb92-fd3caaa48618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 99
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "70ce88f5-42a7-4d06-8c4b-903a101a5558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 82
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "afe79d2d-4fd6-48d8-a812-d6e853fd554d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0e345c04-c308-4139-8ae3-16ebb4748fc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 99
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b4c21393-f06c-4023-8ec5-2de4c702c8c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 197,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f7c5bf55-3a1c-4e43-aa71-9244b1355c4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 45,
                "y": 119
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "64a4f66b-41b5-458e-8c02-0b6c5f1a1048",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 46,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "65cab5b2-2bfe-48aa-a68d-1123aa2d283c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 2,
                "shift": 4,
                "w": 2,
                "x": 208,
                "y": 119
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a1155932-fa9f-4987-bf0b-f83ea70e4788",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 14,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "fa7b4df0-f870-4830-abb4-a30d96c79970",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 56,
                "y": 62
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "77945cae-5344-45a5-883b-eb8d5bd87af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 17,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 66,
                "y": 62
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "15111ae6-0000-41b8-8c59-82c7cd5c55eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 82,
                "y": 22
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "60697893-0bf9-4341-8f8d-975ac45b7417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 134,
                "y": 82
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "e7e6ab6c-422c-4073-8fcd-b073fe4c1018",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 62
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "08c5c988-498b-482a-8c94-d03d2b678978",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 42,
                "y": 99
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "56983930-f3c2-4f55-aba2-d7dc8fbd426d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "3c664242-1ab8-4c16-8220-e4739d298a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 175,
                "y": 62
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "0c059d3d-f46a-4fea-ae0d-cf1058d3c891",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 15,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "1b19f250-7f2e-432b-9818-a0b5ef480c26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 145,
                "y": 82
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "8c9b8290-f838-4916-8e70-2d1ba4be1895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 144,
                "y": 42
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "4574eef6-5437-49b6-8e54-db188c1f22af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 108,
                "y": 42
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "096c57ab-b856-416a-b7c5-3e29e3e29dbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 167,
                "y": 82
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "c5d8515d-aae8-4790-97df-67a92de21a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 156,
                "y": 42
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "5c0d9e53-5e8d-49e4-9584-e2cba76c6e64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 173,
                "y": 22
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "7dfe1efc-5508-4948-b082-bb0d06078aeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 192,
                "y": 42
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "0ba92b02-ea46-4478-a1cb-7a0fa9ec4594",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 69,
                "y": 22
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "c67bde77-a500-48d2-8db3-623d923a6060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 48,
                "y": 42
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "eefc3b6e-9e52-4082-87a0-86904c5a6519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 90,
                "y": 82
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "a3357092-dec5-47a8-b7f9-a66ef8b8f368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 216,
                "y": 42
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "32dc7764-0f56-4dd5-9204-86d6035ba5ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 36,
                "y": 42
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "ebebd2aa-f0fb-4541-9da5-b0da26182608",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 82
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "ce53238a-6c28-4691-8634-cc88e2c83b80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "1d60b085-5ee4-4c0e-87a8-cf60c67f3748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 96,
                "y": 42
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "ceb3d59a-0504-4d7b-8462-cd9875060bef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "9936d876-a9b7-4889-8bc0-af5b16d35ed1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 109,
                "y": 62
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "63ef09cb-fae8-44d7-be3b-9f10646afd99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 15,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "602a7942-71ad-468c-b021-59e8b6a447c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 18,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "3873440e-3fdd-4127-84eb-aebf45f35c29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "42a3552f-b2e4-45e5-b72f-8d86d733e13b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 15,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 22
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "fca674ab-1a85-4947-b4e1-47ebcb231f03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 142,
                "y": 62
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "4febf21c-4192-41d7-bb19-aa5f66ad7d47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 72,
                "y": 42
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "5ecaf546-f254-41a4-b0f8-8283306fd9ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 15,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "49079ad6-9ac4-4247-b262-bea5b736191d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 164,
                "y": 62
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "0a223446-5321-4435-bdb9-e96dfda46b97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 199,
                "y": 82
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "9ba9acfd-87cf-49e8-b0fe-c3fa7a5a1cb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 178,
                "y": 82
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "38d27c38-3653-4bf6-ad8a-123dce2894a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 99
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "859455cd-d8b8-4c76-8853-61181762fc6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 112,
                "y": 119
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "0711105b-40a4-441f-b84f-7d48ee0fc560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 44,
                "y": 22
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "265b122a-6de4-47be-b20c-67422d5dd11c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 239,
                "y": 82
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "95466bf6-15f0-4924-8ad2-14b4f7bb10db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 30,
                "y": 22
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "da2be6a7-cab0-4f0c-88df-1d2259808516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 189,
                "y": 82
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "6437bbf6-4707-4615-b5e4-3c466848222c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 99
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "9d49772b-6f7e-4812-91c6-b2c6662c2787",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 162,
                "y": 99
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "25409084-cfe3-415e-981b-e36bcf6dbafd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 172,
                "y": 99
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "b46aacf2-9726-4d75-9cd2-ae39aa40533e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 82
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "d7bc21c9-d141-4b49-b555-11d6b541b3e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 241,
                "y": 62
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "dfa6fa6e-8a49-41ae-ba67-a14c4873cc34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 202,
                "y": 99
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "0a58d5c0-062a-47f5-b2b3-6cd5553c4162",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "3a5156d3-2a9f-4b5d-b3ce-36d8c664ef5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 219,
                "y": 82
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "295afc94-167e-420b-bc92-49e6dcc70bd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 208,
                "y": 22
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "faa9ea1f-7490-4522-8551-45232483c928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 99
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "2bd5cba5-488d-45ac-86dd-246eb60e9af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 229,
                "y": 82
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "9b6bd5e5-d89e-4f6e-8661-933468ae1258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 219,
                "y": 22
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "bb8ea159-f337-4fa5-bbee-6ad1ed1ffc7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "f046a2c6-14d3-481e-acb3-0730667ee684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 99
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "31a00555-fed0-4e9e-aefc-f56279b4af75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 186,
                "y": 22
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "690f1471-150b-42a6-9620-5a72dd2278d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 132,
                "y": 99
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "e29474cd-6977-457e-82e5-5fc3b67844f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 15,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "7d438dcf-ea82-4ce6-be3a-2d5ee855ca4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "a4621833-6eba-4c88-8a22-265a4739deb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 60,
                "y": 42
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "a83d829f-b385-4793-a006-1cfb98f89467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 147,
                "y": 22
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "4a74bf4f-0f5f-4f54-9513-4fc99c64cd76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 152,
                "y": 99
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "1ec630be-ef58-4576-82a4-8725a9345931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 212,
                "y": 99
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "22831123-8918-41a0-ab77-cc0dd1f02ce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 15,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "3e8870d1-e31c-4f85-b91f-f09b21b1c36b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 122,
                "y": 99
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1040,
            "y": 1103
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}