{
    "id": "83d76ddc-d3f9-4a33-a0c8-c52a8890fde8",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "f_H1",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "CTC Splash Rounded",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "12b23af2-417e-4e17-9c1c-d6437ba032c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 50,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 240,
                "y": 414
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "01272797-de18-479c-b480-e9fd825e4abf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 42,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 290,
                "y": 414
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "eb396860-3711-4e06-9410-626bc7d4a144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 309,
                "y": 414
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ac604adc-d3b7-491b-94cf-a5e3366fc42b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 42,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 2,
                "y": 164
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "eac685cf-7768-46ec-8882-a29baee211b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 47,
                "offset": 8,
                "shift": 40,
                "w": 24,
                "x": 372,
                "y": 211
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0afad60c-d7c0-41bc-b02a-687da9918dd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 42,
                "offset": 2,
                "shift": 36,
                "w": 33,
                "x": 288,
                "y": 56
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "57c710da-7a82-4967-8dda-4e0a5c11b865",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 43,
                "offset": 4,
                "shift": 40,
                "w": 33,
                "x": 218,
                "y": 56
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3885dc11-64b4-4280-a8ab-756cc20c6a65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 331,
                "y": 414
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e911340e-41e1-4eaa-bba0-630798841e1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 50,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 365,
                "y": 360
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "94208811-2840-44d0-9cc9-e7ac802d6368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 50,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 384,
                "y": 360
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "88b61d37-510b-4e22-a15c-76ae2adf3ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 26,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 197,
                "y": 414
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "67fd9d4b-dd76-4d79-8a27-9c5a67e2bee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 41,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 341,
                "y": 360
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "16e824e3-8d50-4a55-b9fd-22e4a91cad3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 49,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 218,
                "y": 414
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ba83d87e-d517-4f86-822e-819418ad5e8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 180,
                "y": 414
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1f6a4b15-762c-4eb6-9f7c-5bea0fd6d4fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 42,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 260,
                "y": 414
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "50449b57-0baf-4213-94f2-4fb00a9a4eef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 52,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 425,
                "y": 360
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6edb2171-6bd4-4fa0-872c-3560d122d7da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 43,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 198,
                "y": 211
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f12d0e52-5057-444c-be43-62403c39d22b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 15,
                "x": 149,
                "y": 414
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "036bcdfe-19a6-47fc-9592-79eac5a0b2e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 42,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 2,
                "y": 315
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4e4fabe2-b762-4e8c-bf11-f0387bc87b24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 43,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 270,
                "y": 264
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7ab2fceb-84c5-4141-8991-007b25df0e3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 42,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 279,
                "y": 164
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "995c14bb-1432-4b3d-856c-23b3272c6ed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 42,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 56,
                "y": 315
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "2eabe468-ec8a-4e77-9a4e-210ba1616d11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 43,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 426,
                "y": 211
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "128cde29-dd3a-4d59-9363-c261ef8a47b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 43,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 162,
                "y": 264
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9f01f1b1-ce54-48d8-914a-6ffbb24d1bd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 43,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 398,
                "y": 211
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ffdfe927-78f3-4d07-bf70-6a5df5ff8667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 43,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 82,
                "y": 211
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "94b202bd-bbbc-4b29-bb38-a6ae01e3f48b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 42,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 270,
                "y": 414
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "3abeb22d-9f71-42be-8fa9-9d166e8cea4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 48,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 229,
                "y": 414
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c1eb9d23-286b-44af-b4b7-3d0a008d0d79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 40,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 73,
                "y": 414
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c2740049-e3a3-40a4-8e21-60aab785151c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 36,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 482,
                "y": 360
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0f8f4d3c-f860-4958-998a-aa54e85387a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 40,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 53,
                "y": 414
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "62aaef78-6486-418b-9586-ffa60514a823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 42,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 150,
                "y": 360
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "62a578ad-8e6b-424c-ab31-5a63fa27b5b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 48,
                "offset": 2,
                "shift": 36,
                "w": 33,
                "x": 471,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "109956d2-23f9-4e6e-8d4b-f65c1b3b6778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 42,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 92,
                "y": 110
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "59898623-0fa4-4cd3-9bed-0c1f7625c9e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 42,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 285,
                "y": 211
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2c875b90-75c2-41da-a9a1-8d28256207ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 43,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 65,
                "y": 164
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1ccb72da-564c-45f2-8197-82b2a783f71c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 42,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 96,
                "y": 164
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d321a23c-aa30-4fb5-aab2-ba3f3b895e33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 42,
                "offset": 3,
                "shift": 28,
                "w": 25,
                "x": 459,
                "y": 264
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3ca7201a-61d4-4ef9-8858-a39a480d3cde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 42,
                "offset": 3,
                "shift": 26,
                "w": 24,
                "x": 293,
                "y": 315
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a8010823-73e2-40a7-a3e7-d991a2f3ad7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 43,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 311,
                "y": 110
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3f2e7aa9-20ae-434c-a301-48df29e23b80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 42,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 339,
                "y": 164
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e7b1f014-d3e0-4559-ad3e-2ea5a2eb4ba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 42,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 250,
                "y": 414
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "67152099-e710-47b6-85e3-e1753fb78bf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 42,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 131,
                "y": 414
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "82cd5646-d337-4daa-893d-c5561b2318d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 42,
                "offset": 2,
                "shift": 28,
                "w": 26,
                "x": 2,
                "y": 264
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e7b4e7d3-3825-4f16-8779-2cc29256daa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 42,
                "offset": 2,
                "shift": 25,
                "w": 23,
                "x": 2,
                "y": 360
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4e299dcb-48ce-4c51-a8c3-2f66b016cbed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 42,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 59,
                "y": 110
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fcd0138d-e4fe-4ac9-aaff-150c48e8a79a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 42,
                "offset": 3,
                "shift": 32,
                "w": 28,
                "x": 369,
                "y": 164
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "29b2de31-536a-4ca3-ac5b-328ca3d195ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 43,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 358,
                "y": 56
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "da2f806a-bd4c-4214-a792-28929f8a1f13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 42,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 432,
                "y": 264
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "16b2925d-8e57-404c-b36f-04b9dde398ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 43,
                "offset": 2,
                "shift": 36,
                "w": 33,
                "x": 253,
                "y": 56
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "094bab0e-3022-4c4c-a85d-6e59c5ef4ac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 42,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 482,
                "y": 211
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "87fc1bb9-9b1d-42eb-806e-99780d2b4adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 43,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 189,
                "y": 264
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "60b180c9-38d0-4c29-bdf6-819adeffe921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 42,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 2,
                "y": 211
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7fca9938-b9f3-4113-ac59-1a3295ea0f75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 42,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 227,
                "y": 211
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "bf43dd61-5034-45b5-8d7e-b46644b400c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 42,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 125,
                "y": 110
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7c85b75f-1f12-4fd6-bbb0-007cbf77aeea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 42,
                "offset": 1,
                "shift": 43,
                "w": 43,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1a023795-6267-41f5-848c-b03c5bd66366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 42,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 468,
                "y": 110
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d9023a96-c683-4358-985c-05f5ec233238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 42,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 404,
                "y": 110
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6387194b-4921-422d-94be-b27b4c56623f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 42,
                "offset": 2,
                "shift": 29,
                "w": 27,
                "x": 343,
                "y": 211
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f3ebba5b-4d43-4c2d-a2df-092603f92299",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 52,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 2,
                "y": 414
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "406ea5f3-e176-4b4a-9c11-05946f8905c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 52,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 443,
                "y": 360
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5f309f52-ccf4-429a-a22f-e030c8264b11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 52,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 19,
                "y": 414
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ff975642-710d-4236-9dd1-4a0f571fac41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 49,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 414
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "62670958-c6d4-489f-9826-9a6f5c92d981",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 47,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 375,
                "y": 110
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d3b2c1b6-15ac-4aa0-83e9-0d1f1ca5ec08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 45,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 220,
                "y": 164
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "91c16e2d-28b3-4fcf-b717-693f99eb2161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 42,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 446,
                "y": 315
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e8e365b8-ed52-4d2b-8115-800916ce5680",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 42,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 29,
                "y": 315
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "78702d86-1fe3-4b65-972b-230705c504d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 42,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 77,
                "y": 360
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1f1e32bb-9bf0-4b99-ade3-5957ef94fe6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 42,
                "offset": 1,
                "shift": 29,
                "w": 26,
                "x": 30,
                "y": 264
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4b0fcc88-34be-4054-841a-dc1841a17087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 42,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 405,
                "y": 264
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5f68d3cd-6369-4cfc-831e-60130fc6ce6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 403,
                "y": 360
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "dfc147c7-564c-4fc2-9c6c-ca3fbe87914b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 52,
                "offset": 0,
                "shift": 24,
                "w": 26,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "76fb6948-0c99-41ca-b3e9-572e62b614d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 42,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 371,
                "y": 315
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "559e5d36-ae64-472f-8cd6-93e28289216b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 42,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 280,
                "y": 414
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7532828a-559b-4bcf-bf02-a8cbee209b20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 51,
                "offset": -2,
                "shift": 11,
                "w": 12,
                "x": 166,
                "y": 414
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "639c99af-a970-4c8c-acd0-7646231e2eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 42,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 174,
                "y": 360
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "41764b6a-3d16-4a8e-8959-7981723b0684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 42,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 300,
                "y": 414
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c04cdb00-3e52-4c17-96ee-ee2fb271e28a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 42,
                "offset": 2,
                "shift": 42,
                "w": 38,
                "x": 392,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "4ebdc338-9f11-4c6f-bb66-8fc5131b3d1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 42,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 396,
                "y": 315
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "4759c7e1-390a-4a07-89bb-07c425255ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 43,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 169,
                "y": 211
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c2b499f7-e41e-496a-a074-3d542a9c7afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 52,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 426,
                "y": 56
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "bf70c4e6-9adb-4a00-9819-57088d1422ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 52,
                "offset": 1,
                "shift": 29,
                "w": 26,
                "x": 482,
                "y": 56
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "969e1d31-57d0-4fb7-a248-e0acfbcf4e42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 112,
                "y": 414
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "91e282fe-58a9-4a80-bfc6-616d3eba88f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 43,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 318,
                "y": 360
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "86b9af11-57e3-42cd-a3bf-f06318b2702a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 43,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 461,
                "y": 360
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2e5ac526-83e6-458f-bd41-3adf12b6c83f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 43,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 137,
                "y": 315
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b5bf3b16-7fb1-4698-941d-c72cc382613e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 43,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 216,
                "y": 264
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "96f323b9-ce97-43bc-901b-00e850505a00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 43,
                "offset": 1,
                "shift": 36,
                "w": 36,
                "x": 36,
                "y": 56
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ff8b2889-6e6f-4f66-9109-7ea3e8e49437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 43,
                "offset": 1,
                "shift": 24,
                "w": 24,
                "x": 163,
                "y": 315
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b87f8cb0-90c7-4f30-a4fb-1c82dd5a6ee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 52,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 284,
                "y": 110
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "fae524b6-4c0d-47f9-a2df-13eabec58767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 42,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 270,
                "y": 360
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a62a1090-8e4a-4222-b73e-2ca58d5ad278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 51,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 57,
                "y": 211
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9967f89c-8c7e-4bde-af2c-59473b3b17c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 51,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 324,
                "y": 414
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "04dfe1ca-9b48-4139-a118-2bd01323c453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 51,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 32,
                "y": 211
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "58dd4477-9c08-4604-8a34-e69e6b4fbf4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 49,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 114,
                "y": 264
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "55b0a8ee-2f0e-49ab-8f2c-704ef418769c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 49,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 138,
                "y": 264
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "8bffb911-cf7e-4ed2-b15b-b9d7a543832d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 42,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 158,
                "y": 110
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "e96fc01b-dd11-4268-bc62-c6c8241ee68f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 42,
                "offset": 2,
                "shift": 27,
                "w": 25,
                "x": 351,
                "y": 264
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "7a05a1a6-eb94-4406-8545-c499c2e34a7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 42,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 256,
                "y": 211
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "3623b3a8-693a-4e99-888c-11f629ecf45e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 42,
                "offset": 3,
                "shift": 24,
                "w": 22,
                "x": 222,
                "y": 360
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "a6f57e2c-28e0-494a-9ffd-b7e382ec26f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 50,
                "offset": 0,
                "shift": 36,
                "w": 36,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "d52cead1-1519-4de4-8a61-79d038efb6fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 42,
                "offset": 3,
                "shift": 28,
                "w": 25,
                "x": 297,
                "y": 264
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "6b02a0e0-78de-4af7-92cb-ac6c7f6596db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 42,
                "offset": 0,
                "shift": 42,
                "w": 42,
                "x": 308,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "52d2e5f3-85b8-4cf5-824c-01e2df4264ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 43,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 243,
                "y": 264
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "03ddaf3e-f36e-4e9d-bea9-b9ff6f057cf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 42,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 189,
                "y": 164
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "79ffd103-802c-4029-80eb-d88fe25f11e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 42,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 127,
                "y": 164
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "5615183f-3985-48bc-a44c-45e049ee1008",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 42,
                "offset": 2,
                "shift": 28,
                "w": 26,
                "x": 86,
                "y": 264
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "2da87364-d3d7-4fc3-8dc7-0015df52cecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 43,
                "offset": 0,
                "shift": 32,
                "w": 30,
                "x": 343,
                "y": 110
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "617715de-db15-47fa-a585-ffb4eca416a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 42,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 191,
                "y": 110
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "b8eeaee3-a6a6-405b-af1e-d34cf439ea9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 42,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 459,
                "y": 164
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "2037deac-2b34-4738-aaa0-08516c2cb474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 43,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 392,
                "y": 56
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "bd8710ee-14b6-48c0-9373-81b709fdadc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 42,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 399,
                "y": 164
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "857eace2-740e-4fde-862e-1c50a66dcd3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 42,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 83,
                "y": 315
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "e191b9eb-9c79-4f78-8de2-117999ab39de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 43,
                "offset": 2,
                "shift": 31,
                "w": 29,
                "x": 34,
                "y": 164
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "a982311a-6669-4732-9aea-c1b81f2d94c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 42,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 429,
                "y": 164
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "2dbd4338-593f-4a83-b04e-7fbfa62c1ae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 42,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 314,
                "y": 211
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "45b8d98a-ac2b-4f5d-b8e0-15d90f949f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 43,
                "offset": 1,
                "shift": 39,
                "w": 38,
                "x": 352,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "a5ab0dce-7d3f-4e78-b41c-9dfcc0e56564",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 42,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 436,
                "y": 110
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "b56beec6-5125-4434-ac04-411b155b41c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 49,
                "offset": 2,
                "shift": 34,
                "w": 32,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "505823fe-cf28-4038-8845-e0d1a0ec2229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 42,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 454,
                "y": 211
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "560fad17-f749-4f2c-a384-2ec02b7b41d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 42,
                "offset": 2,
                "shift": 49,
                "w": 45,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "0abab26c-21cf-4668-a7a4-4f36d15bc62e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 49,
                "offset": 2,
                "shift": 51,
                "w": 49,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "bf769abc-a5f9-48d5-b5f4-f8f91d316df2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 42,
                "offset": 0,
                "shift": 34,
                "w": 33,
                "x": 323,
                "y": 56
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "36bd0d45-a931-4847-9f81-76ef9d6401c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 42,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 182,
                "y": 56
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "0ea481b6-95a9-4139-b661-acf657ce7a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 42,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 378,
                "y": 264
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "da09f1c7-d86b-4c08-bd9f-4e3b75e86781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 43,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 249,
                "y": 164
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "976b89bb-1adc-4ac5-acbf-5975254b8de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 43,
                "offset": 2,
                "shift": 48,
                "w": 45,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "10a46aaf-a476-482d-a160-f0f27b2533a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 42,
                "offset": 1,
                "shift": 29,
                "w": 25,
                "x": 324,
                "y": 264
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "f2d8bc25-80c7-449b-9e91-a640a401bfe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 42,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 471,
                "y": 315
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "8e9d43ca-5539-448d-adcd-0fb320d84574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 43,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 140,
                "y": 211
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "3cb915f6-6bd2-4bde-ae06-c5c302cd1af4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 42,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 27,
                "y": 360
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "7f05e6da-6e74-4067-88a8-96807b4f3959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 93,
                "y": 414
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "89cd804e-3d61-4a30-a4d0-2c35103130fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 49,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 74,
                "y": 56
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "3bd47bda-661b-47a7-b2ff-6bb977cf58ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 42,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 110,
                "y": 315
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "7cae0cd4-110a-4f76-9bb9-b68c044b00e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 42,
                "offset": 0,
                "shift": 34,
                "w": 35,
                "x": 145,
                "y": 56
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "e56210d0-97fe-4f56-aa7d-bc762913f817",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 42,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 421,
                "y": 315
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "f455246d-8e0d-48c7-b814-8f063a69eae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 42,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 319,
                "y": 315
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "0e5fcb07-cb59-4591-b5f5-5e408db4cc2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 42,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 345,
                "y": 315
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "b694a98d-d92a-4b33-85f2-86cce09461ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 42,
                "offset": 2,
                "shift": 23,
                "w": 22,
                "x": 294,
                "y": 360
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "3f14ee0e-55f0-469e-ba87-31df3c269853",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 42,
                "offset": 0,
                "shift": 28,
                "w": 26,
                "x": 58,
                "y": 264
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "07e7b410-ff3e-4440-8c37-e9ae6cf7287b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 42,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 309,
                "y": 164
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "6559191c-ab1a-40a6-ba4f-6fc59da9a73a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 42,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 241,
                "y": 315
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "bb2b276d-ff75-4cd4-8fc6-3e414e962926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 43,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 111,
                "y": 211
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "4eccf803-fde5-48d2-a3b1-cf3eb4993c23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 42,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 267,
                "y": 315
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "3cba4ad6-6a0e-4a26-a177-4306135eaee6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 52,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 454,
                "y": 56
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "2e4c32d6-8fc9-47df-8e3a-1867c7240761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 42,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 52,
                "y": 360
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "d933b2cd-340d-44ca-b4cc-4d21ada9ceb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 42,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 215,
                "y": 315
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "29f3c464-a598-4a3f-a160-871704a28d8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 52,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 257,
                "y": 110
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "5ab941c3-cb13-4385-adc0-47bf6779be62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 52,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 272,
                "y": 2
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "eea742ad-31dd-433e-b4ec-52b30d9ed931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 43,
                "offset": 2,
                "shift": 26,
                "w": 24,
                "x": 189,
                "y": 315
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "85fc88da-65b1-4eff-bc1d-905feb37cc57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 50,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 30,
                "y": 110
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "1778147e-9ee7-476b-8787-5d2d00057b82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 42,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 102,
                "y": 360
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "836e0bab-9194-4c18-bf4a-f365046c63a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 42,
                "offset": 2,
                "shift": 40,
                "w": 36,
                "x": 107,
                "y": 56
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "8ba678f0-870a-4437-b7fe-2aa02ccc9df0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 50,
                "offset": 2,
                "shift": 42,
                "w": 40,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "01490cfa-6bec-4c0b-99c2-b5d7293eb3f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 42,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 158,
                "y": 164
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "92375418-5798-4dbf-9dea-4f411184b927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 42,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 224,
                "y": 110
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "4356309d-0507-42e9-97c0-f99432d3b72c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 42,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 246,
                "y": 360
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "661dcf45-04ef-47f7-bfc1-f19f454f1efc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 42,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 198,
                "y": 360
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "731938aa-742b-4f07-ac56-d698286d7680",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 43,
                "offset": 2,
                "shift": 39,
                "w": 37,
                "x": 432,
                "y": 2
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "38d04676-e104-464b-a399-a67ad1efca3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 42,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 126,
                "y": 360
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1040,
            "y": 1103
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 36,
    "styleName": "",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}